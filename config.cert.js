﻿requirejs.config({
    config: {
        config: {
            "fcs-api": {
                "notificationType": "websocket",
                "callAuditTimer": "30000",
                "codecsToRemove": ["103", "104", "105", "106", "107"],
                "protocol": "https",
                "restUrl": "lab.af.centurylink.com",
                "restPort": "443",
                "clientControlled": "true",
                "services":["CallControl", "custom"],
                "websocketProtocol": "wss",
                "websocketPort": "8590",
                "websocketIP": "lab.af.centurylink.com"
            },
            "templatesPath": "templates",
            "themepath": "theme",
            "languagesPath": "lang",
            "supportedLanguages": {
                "en": "en",
                "en-us": "en",
                "en-ca": "en",
                "en-gb": "en",
                "fr": "fr",
                "fr-fr": "fr",
                "fr-ca": "fr",
                "tr": "tr",
                "tr-tr": "tr"
            },
            "defaultLanguage": "en",
            "imrn": {
                "realm": ["ottawa", "istanbul", "usa"]
            },
            "noImage": "images/no_image_.jpg",
            "macOsMediaPluginLink": "plugin/GCFWEnabler.pkg",
            "windowsMediaPluginLink": "plugin/GCFWEnabler.exe",
            "pluginLogLevel": 2,
            "pluginMode": "auto",
            "iceserver": "stun:137.107.64.210:3478",
            "ice": "STUN stun:137.107.64.210:3478",
            "webrtcdtls": false,
            "CallGrabber@genband.com": "*25",
            "VoiceMail@genband.com": "voicemail",
            "helpLink": "help.pdf",
            "displayMode": "newwindow",
            "windowsPluginLink": "/fcs-plugin/gcfw.exe",
            "macOsPluginLink": "/fcs-plugin/gcfw.pkg",
            "companyName": "Century Link Web",
            "ARCUS": {
                "proxyForURLPatterns": ["/rest/version/[0-9]+","/rest/version/latest"],
                "AFProxyPrependURL": "/WEBRTC/RequestServletRWS/restful/",
                "AFAdditionalURLDetails": "/spidr2.0",
                "AFProxyHost": "https://lab.af.centurylink.com/",
                "UseCtlIdLogin": "true",
                "CtlIDLoginUrl": "https://login-cert.centurylink.com/login-cert/Intake.aspx?f=CenturyLinkIDAuth&si=cvoipwebclient&ReturnUrl=",
                "CtlIDFacadeUrl": "https://lab.af.centurylink.com/WEBRTC/RequestServletRWS/restful/PreRegister/WebRtcClient/CtlIDFacade?CtlFacadeUrl=http://lttncomns57.lab.af.qwest.net:8080/CTLIDFacade/WebRtc/ctlid&r=",
                "services": {
                    "Activity":     "https://lab.af.centurylink.com/WEBRTC/AFMetaDataManagerRWS/restful/",
                    "Memory":     "https://lab.af.centurylink.com/WEBRTC/AFMetaDataManagerRWS/restful/",
                    "Message":      "https://lab.af.centurylink.com/WEBRTC/AFMetaDataManagerRWS/restful/",
                    "Telephony":    "https://lab.af.centurylink.com/WEBRTC/AFTelephonyRWS/restful/",
                    "Contact":     "https://lab.af.centurylink.com/WEBRTC/AFContactsRWS/restful/",
                    "Media": "https://lab.af.centurylink.com/WEBRTC/MediaManagerRWS/restful/",
                    "IMChat": "https://lab.af.centurylink.com/WEBRTC/RequestServletRWS/restful/",
                    "PreRegistration": "https://lab.af.centurylink.com/WEBRTC/RequestServletRWS/restful/PreRegister/WEB/register",
                    "Settings": "https://lab.af.centurylink.com/WEBRTC/AFIMSUserWS/",
                    "Settings911": "https://lab.af.centurylink.com/VoIP_OMWF_WebServices/com/qwest/voip/subscriberinformation/UpdateSubscriberInfo.jws?WSDL",
                    "Settings911AddressChange": "https://lab.af.centurylink.com/AFServiceOrderProxyWS/AFServiceOrderProxyService?WSDL"
                },
                CallLogs: {
                    "maxLogs": 2000
                }
            },
            "afClient": {
                "callMethod": "restful",
                "rootToken": "WEBRTC",
                "afPreRegisterToken": "WEBRTC/RequestServletRWS/restful/PreRegister/WEB/register?sessionID={sessionId}&sessionAuthKey={sessionAuthKey}&dc={dataCenter}&r={random}"
            }
        }
    }
});
﻿requirejs.config({
    config: {
        config: {
            "fcs-api": {
                "notificationType": "websocket",
                "callAuditTimer": "30000",
                "codecsToRemove": ["103", "104", "105", "106", "107"],
                "protocol": "https",
                "restUrl": "intg.iaf.centurylink.com",
                "restPort": "443",
                "clientControlled": "true",
                "services":["CallControl", "custom"],
                "websocketProtocol": "ws",
                "websocketPort": "8581",
                "websocketIP": "137.107.64.100"
            },
            "templatesPath": "templates",
            "themepath": "theme",
            "languagesPath": "lang",
            "supportedLanguages": {
                "en": "en",
                "en-us": "en",
                "en-ca": "en",
                "en-gb": "en",
                "fr": "fr",
                "fr-fr": "fr",
                "fr-ca": "fr",
                "tr": "tr",
                "tr-tr": "tr"
            },
            "defaultLanguage": "en",
            "imrn": {
                "realm": ["ottawa", "istanbul", "usa"]
            },
            "noImage": "images/no_image_.jpg",
            "macOsMediaPluginLink": "plugin/GCFWEnabler.pkg",
            "windowsMediaPluginLink": "plugin/GCFWEnabler.exe",
            "pluginLogLevel": 2,
            "pluginMode": "auto",
            "iceserver" : [
                {"url":"stun:137.107.64.100:3478"}
            ],
            "webrtcdtls": false,
            "CallGrabber@genband.com": "*25",
            "VoiceMail@genband.com": "voicemail",
            "helpLink": "help.pdf",
            "displayMode": "newwindow",
            "windowsPluginLink": "/fcs-plugin/gcfw.exe",
            "macOsPluginLink": "/fcs-plugin/gcfw.pkg",
            "companyName": "Century Link Web",
            "ARCUS": {
                "proxyForURLPatterns": ["/rest/version/[0-9]+","/rest/version/latest"],
                "AFProxyPrependURL": "/WEBRTC/RequestServletRWS/restful/",
                "AFAdditionalURLDetails": "/spidr1.1",
                "AFProxyHost": "https://intg.iaf.centurylink.com/",
                "UseCtlIdLogin": "false",
                "CtlIDLoginUrl": "https://setup-test1.centurylink.com/quickconnectitv1/Intake.aspx?f=CenturyLinkIDAuth&si=cvoipwebclient&ReturnUrl=",
                "CtlIDFacadeUrl": "https://intg.iaf.centurylink.com/WEBRTC/RequestServletRWS/restful/PreRegister/WebRtcClient/CtlIDFacade?CtlFacadeUrl=http://lttncomns60.lab.af.qwest.net:8080/CTLIDFacade/WebRtc/ctlid&r=",
                "services": {
                    "Activity":     "https://intg.iaf.centurylink.com/WEBRTC/AFMetaDataManagerRWS/restful/",
                    "Memory":     "https://intg.iaf.centurylink.com/WEBRTC/AFMetaDataManagerRWS/restful/",
                    "Message":      "https://intg.iaf.centurylink.com/WEBRTC/AFMetaDataManagerRWS/restful/",
                    "Telephony":    "https://intg.iaf.centurylink.com/WEBRTC/AFTelephonyRWS/restful/",
                    "Contact":     "https://intg.iaf.centurylink.com/WEBRTC/AFContactsRWS/restful/",
                    "Media": "https://intg.iaf.centurylink.com/WEBRTC/MediaManagerRWS/restful/",
                    "IMChat": "https://intg.iaf.centurylink.com/WEBRTC/RequestServletRWS/restful/",
                    "PreRegistration": "https://intg.iaf.centurylink.com/WEBRTC/RequestServletRWS/restful/PreRegister/WEB/register",
                    "Settings": "https://intg.iaf.centurylink.com/WEBRTC/AFIMSUserWS/",
                    "Settings911": "https://intg.iaf.centurylink.com/VoIP_OMWF_WebServices/com/qwest/voip/subscriberinformation/UpdateSubscriberInfo.jws?WSDL",
                    "Settings911AddressChange": "https://intg.iaf.centurylink.com/AFServiceOrderProxyWS/AFServiceOrderProxyService?WSDL"
                },
                CallLogs: {
                    "maxLogs": 2000
                }
            }
        }
    }
});


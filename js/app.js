define('app', ['logger', 'config', 'loginController', 'enums'], function(logger, config, loginController) {
    var app = function() {
        var _this = this;

        return {
            init: function() {
                logger.log('app.init');

                this.getBrowserInfo();

                //init webrtc settings
                fcs.setup(config.fcsApiSettings());

                // login
                loginController.autologin();
            },

            getBrowserInfo: function() {
                //detection browser
                var userAgent = navigator.userAgent.toLowerCase();

                $.browser = {
                    version: _this.getBrowserVersion,
                    safari: !/chrome/.test(userAgent) && /safari/.test(userAgent),
                    opera: /opera/.test(userAgent),
                    msie: /msie/.test(userAgent) && !/opera/.test(userAgent),
                    mozilla: /mozilla/.test(userAgent) && !/(compatible|webkit)/.test(userAgent),
                    chrome: /chrome/.test(userAgent)
                };
            },

            getBrowserVersion: function(browser) {
                var re, rv = -1, userAgent = navigator.userAgent.toLowerCase();
                if (browser === 'msie') {
                    re = new RegExp("msie ([0-9]{1,}[\\.0-9]{0,})");
                    if (re.exec(userAgent) !== null) {
                        rv = parseFloat(RegExp.$1);
                    }
                } else if (browser === 'chrome') {
                    rv = parseInt(navigator.appVersion.match(/Chrome\/(\d+)\./)[1], 10);
                }
                else if (browser === 'mozilla') {
                    rv = parseInt(userAgent.substring(userAgent.indexOf("firefox") + 8), 10);
                }
                else if (browser === 'safari') {
                    rv = parseFloat(userAgent.substring(userAgent.indexOf("version") + 8, userAgent.indexOf("safari") - 2), 10);
                }
                return rv;
            },

            initGlobalErrorHandler: function() {
                $(document).ajaxError(function(event, jqxhr, settings) {

                    //exception = exception || "Server error";
                    //exception += " - " + window.$wrtc.enums.NetworkErrors[jqxhr.status];

                    var title = "Server error - " + window.$wrtc.enums.NetworkErrors[jqxhr.status];

                    $.gritter.add({
                        title: title,
                        text: settings.type + ' ' + settings.url,
                        class_name: 'gritter-error gritter-center'
                    });
                });
            }
        };
    };
    return app();
});

require(['app', 'ace', 'utils', 'enums', 'gritter'], function(app) {
    document.addEventListener('deviceready', onDeviceReady, false);

    function onDeviceReady() {
        require(['app', 'ace', 'utils', 'enums', 'gritter'], function(app) {
            app.init();
        });
    }
});

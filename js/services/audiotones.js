define('audiotones', [], function () {
    var _this, msgInTone,chatInTone,chatOutTone, ringInTone, ringOutTone, busyTone, msgFailTone;

    function getToneFromString(toneType) {
        switch (toneType) {
            case _this.MSG_IN:
                return msgInTone;
            case _this.CHAT_IN:
                return chatInTone;
            case _this.CHAT_OUT:
                return chatOutTone;
            case _this.RING_IN:
                return ringInTone;
            case _this.RING_OUT:
                return ringOutTone;
            case _this.BUSY:
                return busyTone;
            case _this.MSG_FAIL:
                return msgFailTone;
            default:
                return null;
        }
    }

    var audiotones = function () {
        return {
            init: function () {
                _this = this;

                _this.audioContext = window.AudioContext || window.webkitAudioContext ||
                    window.mozAudioContext || window.oAudioContext || window.msAudioContext;

                if (_this.audioContext) {
                    _this.audioContextClass = new _this.audioContext();
                }

                _this.minPlayingTime = 200;

                var audioTonesDiv = $('#audio_tones');
                msgInTone = $("<audio id='msg_in_audio'><source src='tones/Electric Chime.mp3' type='audio/mp3'></audio>")[0];
                chatInTone = $("<audio id='chat_in_audio'><source src='tones/chatin.mp3' type='audio/mp3'></audio>")[0];
                chatOutTone = $("<audio id='chat_out_audio'><source src='tones/Spin Jump.mp3' type='audio/mp3'></audio>")[0];
                ringInTone = $("<audio id='ring_in_audio' loop='loop'><source src='tones/ringin.mp3' type='audio/mp3'><source src='tones/ringin.ogg' type='audio/ogg'></audio>")[0];
                ringOutTone = $("<audio id='ring_out_audio' loop='loop'><source src='tones/ringout.mp3' type='audio/mp3'><source src='tones/ringout.ogg' type='audio/ogg'></audio>")[0];
                busyTone = $("<audio id='busy_audio' loop='loop'><source src='tones/busy.mp3' type='audio/mp3'><source src='tones/busy.ogg' type='audio/ogg'></audio>")[0];
                msgFailTone = $("<audio id='msg_fail_audio'><source src='tones/MessageFailed.wav' type='audio/mp3'></audio>")[0];


                audioTonesDiv.append(msgInTone);
                audioTonesDiv.append(ringInTone);
                audioTonesDiv.append(ringOutTone);
                audioTonesDiv.append(busyTone);
            },

            MSG_IN: "msgin",
            CHAT_IN: "chatin",
            CHAT_OUT: "chatout",
            RING_IN: "ringin",
            RING_OUT: "ringout",
            BUSY: "busy",
            MSG_FAIL: "msgfail",

            play: function (toneType) {
                var tone = getToneFromString(toneType);
                if (tone) {
                    tone.play();
                }
            },

            stop: function (toneType) {
                var tone = getToneFromString(toneType);
                if (tone) {
                    tone.pause();
                    tone.currentTime = 0;
                }
            },

            /**
             * generate DTMF with the help of HTML5 web audio api
             * @param freq1
             * @param freq2
             */
            dialTonePlay: function(freq1, freq2) {
                if (_this.audioContextClass && freq1 && freq2) {
                    var gainNode1, gainNode2;
                    // first frequency generator
                    _this.oscillator1 = _this.audioContextClass.createOscillator();
                    _this.oscillator1.type = 0; // "sine"
                    _this.oscillator1.frequency.value = freq1;
                    if (_this.audioContextClass.createGain) {
                        gainNode1  = _this.audioContextClass.createGain();
                    } else if (_this.audioContextClass.createGainNode) {
                        gainNode1 = _this.audioContextClass.createGainNode();
                    }

                    if (gainNode1) {
                        _this.oscillator1.connect(gainNode1, 0, 0);
                        gainNode1.connect(_this.audioContextClass.destination);
                        gainNode1.gain.value = 0.1;
                        gainNode1.gain.defaultValue = 0.1;
                    }

                    if (_this.oscillator1.start) {
                        _this.oscillator1.start(0);
                    } else if (_this.oscillator1.noteOn) {
                        _this.oscillator1.noteOn(0);
                    }
                    // second frequency generator
                    _this.oscillator2 = _this.audioContextClass.createOscillator();
                    _this.oscillator2.type = 0; // "sine"
                    _this.oscillator2.frequency.value = freq2;
                    if (_this.audioContextClass.createGain) {
                        gainNode2  = _this.audioContextClass.createGain();
                    } else if (_this.audioContextClass.createGainNode) {
                        gainNode2 = _this.audioContextClass.createGainNode();
                    }

                    if (gainNode2) {
                        _this.oscillator2.connect(gainNode2, 0, 0);
                        gainNode2.connect(_this.audioContextClass.destination);
                        gainNode2.gain.value = 0.1;
                        gainNode2.gain.defaultValue = 0.1;
                    }

                    if (_this.oscillator2.start) {
                        _this.oscillator2.start(0);
                    } else if (_this.oscillator2.noteOn) {
                        _this.oscillator2.noteOn(0);
                    }

                    var osc1 = _this.oscillator1, osc2 = _this.oscillator2;

                    var dialToneStop = function() {

                        return function() {
                            if (osc1 && osc1.stop && osc1.disconnect) {
                                osc1.stop();
                                osc1.disconnect();
                            }
                            if (osc2 && osc2.stop && osc2.disconnect) {
                                osc2.stop();
                                osc2.disconnect();
                            }
                        };
                    };

                    setTimeout(dialToneStop(), _this.minPlayingTime);
                }
            }
        };
    };

    var audioTonesInstance = new audiotones();
    audioTonesInstance.init();
    return audioTonesInstance;
});
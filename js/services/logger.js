define('logger', function () {
    var logger = function () {
        var _this;
        return {
            init: function() {
                _this = this;
                return _this;
            },
            log: function (message) {
                if (window && window.console) window.console.log(message);
            }
        };
    };
    return logger().init();
});

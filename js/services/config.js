define('config', ['logger', 'module'], function (logger, module) {
    var config = function () {
        var _this;
        return {
            init: function() {
                _this = this;
                this.config = {};
                this.config = module.config();
                return this;
            },

            /**
             * Gets afClient settings from config
             */
            afClientSettings: function () {
                if(_this.config && _this.config.afClient){
                    return _this.config.afClient;
                }
                return {};
            },

            /**
             * Gets fcsApiSettings settings from config
             */
            arcusSettings: function() {
                return _this.config ? _this.config.ARCUS : {};
            },
            /**
             * Gets fcsApiSettings settings from config
             */
            fcsApiSettings: function() {
                return _this.config ? _this.config['fcs-api'] : {};
            },

            /**
             * Gets config value by key
             */
            getValue: function (key, defaultValue) {
                return _this.config ? _this.config[key] : defaultValue; // (_config[parameter] === undefined) ? defaultValue : _config[parameter];
            }
        };
    };
    return config().init();
});
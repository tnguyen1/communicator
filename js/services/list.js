define('list', [], function() {
    var list = function() {
        var _this;

        return {
            init: function() {
                _this = this;
                _this.setupOverrides();
                return _this;
            },
            contains: function(a, i, m) {
                return $(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
            },
            setupOverrides: function() {
                $.expr[':'].contains = function(a, i, m) {
                    return _this.contains(a, i, m);
                };
                $.expr[':'].Contains = function(a, i, m) {
                    return _this.contains(a, i, m);
                };
            },
            filterList: function(options) {
                // do filtering
                if (options.val === '') {
                    options.$list.find('.list-item.filterable').show();
                } else {
                    options.$list.find('.list-item.filterable:not(:contains(' + options.val + '))').hide();
                    options.$list.find('.list-item.filterable:contains(' + options.val + ')').show();
                }

                // set list count if necessary
                if (options.$listCount.length) {
                    var visible = options.$list.find('.list-item.filterable:visible').length;
                    var total = options.$list.find('.list-item.filterable').length;
                    if (options.onlyVisible) {
                        options.$listCount.html(visible);
                    } else {
                        options.$listCount.html(visible == total ? total : visible + '/' + total);
                    }
                }
            }
        };
    };
    return list().init();
});
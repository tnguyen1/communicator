define('router', ['logger', 'chatController', 'messageController', 'activityController', 'memoryController', 'settingsController', 'callLogsController', 'eventEmitter'],
    function (logger, chatController, messageController, activityController, memoryController, settingsController, callLogsController, EventBus) {

        var router = function () {
            var _this,
                pages = {
                    CALLLOGS : 'calllogs',
                    CHAT: "chat",
                    ACTIVITIES: "activities",
                    MESSAGES: "messages",
                    MEMORIES: "memories",
                    SETTINGS: "settings"
                },
                rawRoutes = [
                    {
                        uri: '#/calllogs/',
                        controller: callLogsController,
                        routeName: pages.CALLLOGS
                    },
                    {
                        uri: '#/chat/',
                        controller: chatController,
                        routeName: pages.CHAT
                    },
                    {
                        uri: '#/activities/',
                        controller: activityController,
                        routeName: pages.ACTIVITIES
                    },
                    {
                        uri: "#/messages/",
                        controller: messageController,
                        routeName: pages.MESSAGES
                    },
                    {
                        uri: "#/memories/",
                        controller: memoryController,
                        routeName: pages.MEMORIES
                    },
                    {
                        uri: "#/settings/",
                        controller: settingsController,
                        routeName: pages.SETTINGS
                    },
                    // default route is Messages
                    {
                        uri: ".*",
                        controller: messageController,
                        routeName: pages.MESSAGES,
                        default: true
                    }
                ];

            return {

                init: function () {
                    _this = this;
                    _this.setupHandlers();
                    return _this;
                },

                pages: pages,

                routes: $.map(rawRoutes, function (rawRoute) {
                    var regexpRoutePart = '[a-zA-Z0-9\\.:\\-_]+',
                        optionalRoutePart = '(' + regexpRoutePart + ')?\/?',
                        re = "^" + rawRoute.uri
                            .replace(/:optional/g, optionalRoutePart)
                            .replace(/([\.\/])/, "\\$1");

                    return {
                        matcher: new RegExp(re, "gi"),
                        controller: rawRoute.controller,
                        routeName: rawRoute.routeName,
                        default: rawRoute.default
                    };
                }),

                setupHandlers: function () {
                    var triggerResize = function() {
                        EventBus.trigger(EventBus.Event.VIEW_RESIZE, _this.route);
                    };

                    window.onresize = triggerResize;

                    window.onhashchange = function () {
                        _this.navigate();
                        ga('send', 'pageview', location.hash);
                    };
                },

                navigate: function (initial, hash) {

                    hash = hash || window.location.hash;

                    _this.route = _this.resolveRoute(hash);

                    if (!_this.route.controller) {
                        return;
                    }

                    //if (route.controller instanceof Redirect) {
                    //    window.location.hash = route.controller.getHash(route);
                    //    return this;
                    //}

                    //route = $.extend(route, {hash: hash, from: this.currentPage, to: route.controller});

                    if (window.localStorage.getItem('RTCServiceEnabled') === 'true') {
                        EventBus.trigger(EventBus.Event.ROUTER_CHANGE, _this.route);

                        _this.route.controller.buildMainView({
                            success: function (view) {
                                EventBus.trigger(EventBus.Event.ROUTER_AFTER_CHANGE, { routeName: _this.route.routeName, view: view, initial: initial, routeInfo: _this.route, routing: _this });
                            },
                            routing: _this
                        });
                    }
                },

                /**
                 * Navigate to other page
                 * @param {string} page - The page name navigate to
                 * @param {object} [data] - Params to pass to navigated page, object with key/values pairs
                 */
                navigateTo: function (page, data) {

                    var routes,
                        paramStr = "";

                    if(data) {
                        paramStr = _this.generateUrlHashParameters(data);
                    }

                    routes = $.grep(rawRoutes, function (route) {
                        return route.routeName === page;
                    });

                    if (routes.length > 0) {

                        var hashToNavigate = routes[0].uri + paramStr;

                        if (window.location.hash === hashToNavigate) {
                            _this.navigate();
                        }
                        else {
                            window.location.hash = hashToNavigate;
                        }
                    }
                    else {
                        //route not found so redirecting to default page
                        if (window.location.hash === "") {
                            _this.navigate();
                        }
                        else {
                            window.location.hash = "";
                        }
                    }
                },

                resolveRoute: function (hash) {

                    function notEmpty(value) {
                        return !(value === undefined || value === null);
                    }

                    var controller = null,
                        params = null,
                        filteredParams = null,
                        namedParams = _this.getUrlHashParameters(hash),
                        i,
                        routesLength = _this.routes.length,
                        route;

                    for (i = 0; i < routesLength; i += 1) {
                        route = _this.routes[i];
                        route.matcher.lastIndex = 0;
                        params = route.matcher.exec(hash);
                        filteredParams = params && params.filter(notEmpty);
                        if (filteredParams) {  // have at least one match
                            controller = route.controller;
                            break;
                        }
                    }

                    return {controller: controller, routeName: route.routeName, params: filteredParams, namedParams: namedParams, default : route.default};
                },

                getUrlHashParameters: function (hash) {

                    // Hash begins with the #page, then ?, then name=value parameters separated by &
                    // e.g. #contact_details?contact_id=1&show_all=true

                    // Slice off the page and ?, then split into key/value strings
                    var pairStrings = hash.slice(hash.indexOf('?') + 1).split('&'),
                        pairs = {},
                        i = 0,
                        pairString, pair = {}, equalSignValidator;

                    for (; i < pairStrings.length; i++) {
                        pairString = pairStrings[ i ];
                        /*jslint regexp: false*/
                        equalSignValidator = pairString.replace(/[^=]/g, "").length;
                        if (equalSignValidator > 1) {
                            pair[0] = pairString.slice(0, pairString.indexOf("="));
                            pair[1] = pairString.slice(pairString.indexOf("=") + 1);
                        } else {
                            pair = pairString.split('='); // Split into name/value array
                        }
                        pairs[ pair[ 0 ] ] = pair[ 1 ];
                    }

                    return pairs;
                },

                generateUrlHashParameters: function(params) {

                    var paramsStr = "?";

                    for(var param in params) {
                        paramsStr += [param, '=', params[param], '&'].join();
                    }

                    //removes last character & or ? if no params was passed
                    paramsStr = paramsStr.slice(0,-1);

                    return paramsStr;
                }
            };
        };
        return router().init();
    });




















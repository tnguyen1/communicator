define('dates', [], function () {
    var dates = function () {
        var _this;

        // SAMPLE USAGE FOR A FORM
        // --------------------------------------------------------
        // Pre Render:
        // If the type is already a date:
        // $('#date').val(dates.toDateString({{date}}));
        // $('#time').val(dates.toTimeString({{date}}));
        // If the types are strings use the dates.convertDateStringToLocal() method to wrap {{date}} above
        //
        // On Posting:
        // var date = $('#date', $form).datepicker("getDate");
        // var time = $('#time', $form).val();
        // var dateUtc = dates.combineJQueryDatePickerAndTimeString(date, time);
        // Note: to parse dates in c# i had to replace 'UTC' with 'GMT' before posting the date (as a string)

        return {
            init: function () {
                _this = this;
                return _this;
            },

            convertDateStringToLocal: function (dateString) {
                return _this.ConvertDateToLocal(new Date(dateString));
            },

            convertDateToLocal: function (date) {
                var since1970 = date.getTime(); // ms since 1970
                var offset = new Date().getTimezoneOffset() * 60 * 1000; // ms of offset to be local time
                date.setTime(since1970 - offset); // sets ms since 1970
                return date;
            },

            combineJQueryDatePickerAndTimeString: function (jDate, time) {
                if (_this.isEmpty(jDate)) { return ''; }
                else {
                    var date = new Date(jDate);
                    return new Date(((date.getMonth() + 1).toString() + '/' + date.getDate().toString() + '/' + date.getFullYear().toString() + ' ' + time).trim());
                }
            },

            toDateString: function (date) {
                var dd = date.getDate();
                var mm = date.getMonth() + 1; //January is 0!
                var yyyy = date.getFullYear();
                if (dd < 10) dd = '0' + dd;
                if (mm < 10) mm = '0' + mm;
                return mm + '/' + dd + '/' + yyyy;
            },

            toTimeString: function (date) {
                var hh = date.getHours();
                var m = date.getMinutes();
                var s = date.getSeconds();

                var h = (hh >= 12 ? hh - 12 : hh);
                if (h === 0) h = 12;

                m = m < 10 ? '0' + m : m;
                s = s < 10 ? '0' + s : s;
                return h + ':' + m + ':' + s + (hh >= 12 ? ' PM' : ' AM');
            },

            toDateTimeString: function (date) {
                return _this.toDateString(date) + ' ' + _this.toTimeString(date);
            },

            toDifferenceString: function (start, finish) {
                var span = (finish - start);
                var d = Math.floor(span / (60 * 60 * 1000 * 24) * 1);
                var h = Math.floor((span % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
                var m = Math.floor(((span % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
                var s = Math.floor((((span % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);

                if (isNaN(d) || isNaN(h) || isNaN(m) || isNaN(s)) return '';
                else return d + ' d, ' + h + ' h, ' + m + ' m, ' + s + ' s';
            },

            currentDateString: function () {
                return _this.toDateString(new Date());
            },

            currentTimeString: function () {
                return _this.toTimeString(new Date());
            },

            currentDateTimeString: function () {
                return _this.toDateTimeString(new Date());
            },

            isEmtpy: function (value) {
                return value === undefined || value === null || value === '';
            }
        };
    };
    return dates().init();
});
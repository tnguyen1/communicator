define('template', ['logger', 'handlebars'], function (logger) {
    var template = function () {
        var _this;

        return {
            init: function () {
                _this = this;
                _this.setupHandlers();
                return _this;
            },
            setupHandlers: function () {
                var Handlebars = window.Handlebars;

                Handlebars.registerHelper('eq', function (a, b, opts) {
                    if (a == b) // Or === depending on your needs
                        return opts.fn(this);
                    else
                        return opts.inverse(this);
                });
                Handlebars.registerHelper('toLowerCase', function (options) {
                    return options.fn(this).toLowerCase();
                });
                Handlebars.registerHelper('toPhoneNoFormat', function (options) {
                    var phoneNumber = options.fn(this);
                    if(phoneNumber.length === 10){
                        phoneNumber = phoneNumber.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
                    }
                    return phoneNumber;
                });
                Handlebars.registerHelper('include', function (options) {
                    // http://stackoverflow.com/questions/9411538/handlebars-is-it-possible-to-access-parent-context-in-a-partial
                    var context = {},
                        mergeContext = function (obj) {
                            for (var k in obj) context[k] = obj[k];
                        };
                    mergeContext(this);
                    mergeContext(options.hash);
                    return options.fn(context);
                });

                Handlebars.getTemplate = function (name) {
                    //logger.log('Handlebars.getTemplate - ' + name);
                    if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
                        $.ajax({
                            url: 'templates/' + name + '.html',
                            headers: { "Accept": "text/html" },
                            dataType: 'html',
                            success: function (response) {
                                if (Handlebars.templates === undefined) {
                                    Handlebars.templates = {};
                                }
                                //Handlebars.templates[name] = Handlebars.compile(jqXHR.responseText);
                                Handlebars.templates[name] = Handlebars.compile(response);
                            },
                            error: function () {
                                logger.log('Handlebars.getTemplate - ' + name + ' failed');
                            },
                            async: false
                        });
                    }
                    return Handlebars.templates[name];
                };
                Handlebars.getPartial = function (name) {
                    logger.log('Handlebars.getPartial - ' + name);
                };
            },
            registerPartial: function (name) {
                var Handlebars = window.Handlebars;
                $.ajax({
                    url: 'templates/' + name + '.html',
                    headers: { "Accept": "text/html" },
                    datatype: 'text/javascript',
                    success: function (response) {
                        Handlebars.registerPartial(name, response);
                    },
                    async: false
                });
            },
            buildTemplate: function (name, data) {
                var Handlebars = window.Handlebars;
                var _template = Handlebars.getTemplate(name);
                return _template(data);
            },
            renderTemplate: function (name, data, elementId) {
                var _template = this.buildTemplate(name, data);
                $('#' + elementId).html(_template);
            },
            appendTemplate: function (name, data, elementId) {
                var _template = this.buildTemplate(name, data);
                $('#' + elementId).append(_template);
            },
            prependTemplate: function (name, data, elementId) {
                var _template = this.buildTemplate(name, data);
                $('#' + elementId).prepend(_template);
            }
        };
    };
    return template().init();
});
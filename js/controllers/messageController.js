define('messageController',
            ['Arcus', 'diagnosticWorkMemoryController', 'logger', 'template', 'messageManager', 'contactManager', 'groupManager', 'eventEmitter', 'videojs', 'bootbox', 'audiotones', 'validations',/* hidden use => */ 'aceEdit', 'enums', 'blockUI'],
    function(arcus, diagnosticWorkMemoryController, logger, template, messageManager, contactManager, groupManager, eventEmitter, videojs, bootbox, audiotones, validations) {
    var messageController = function() {
        var _this;
        return {

            title: "Messages",

            init: function() {
                _this = this;
                _this.STATE = 'INIT';
                _this.scrolls = {};
                _this.inbox = [];
                _this.sent = [];
                _this.trash = [];
                _this.draft = [];
                _this.$messageLayout = $();
                _this.$messageDetailContainer = $();
                _this.$messageEditContainer = $();
                _this.artifactTypes = [];
                _this.sortBy = '-createDate';
                _this.groupState = false;
                template.registerPartial('message/message-list-item');
                _this.minLoadingTime = 600; // used for "perceived working" - avoids choppy screen flashes
                _this.fadeInStartingOpacity = 0.35;
                _this.messagesOverlayCounter = 0;
                /**
                 * represent the current state of messages layout
                 */
                _this.state = {
                    activeLayouts : 0,
                    renderLayout : function () {
                        _this.state.activeLayouts++;
                    },
                    destroyLayout : function () {
                        _this.state.activeLayouts--;
                    },
                    invalidateLayouts : function () {
                        _this.state.activeLayouts = 0;
                    }
                };

                _this.progressBarTemplate = '<div class="progress progress-striped active">' +
                    '<div class="progress-bar progress-bar-success" style="width: 0%"></div>' +
                '</div>';
                return _this;
            },

            initScroll: function(scrollName, scrollableElement, scrollViewport) {
                _this.scrolls[scrollName] = {
                    scrollName: scrollName,
                    $scrollableElement: $(scrollableElement),
                    $scrollViewport: scrollViewport ? $(scrollViewport) : $(scrollableElement).closest('.scroll-viewport'),
                    resize: function() {
                        var elem = _this.scrolls[scrollName]
                            , $this = elem.$scrollViewport
                            , offsetTop = $this.offset().top
                            , pageHeight = window.$wrtc.getPageDimensions().height
                            , resultHeight = pageHeight - offsetTop - 15
                            ;
                        $this.height(resultHeight);
                        return resultHeight;
                    }
                };
            },

            resizeScroll: function(scrollName, _padding) {
                if (scrollName && _this.scrolls[scrollName]) {
                    _this.scrolls[scrollName].resize(undefined, _padding);
                } else {
                    for (var key in _this.scrolls) {
                        if (_this.scrolls.hasOwnProperty(key)) {
                            _this.scrolls[key].resize();
                        }
                    }
                }
            },

            scrollTo: function(scrollName, keyCode) {
                if (scrollName && _this.scrolls[scrollName]) {
                    var positionToScroll = 0
                        , delta = _this.scrolls[scrollName].$scrollViewport.outerHeight()
                        , curPosition = _this.scrolls[scrollName].$scrollViewport.get(0).scrollTop
                        , max_positionToScroll = _this.scrolls[scrollName].$scrollViewport.get(0).scrollHeight
                        , lockScroll = false;
                    switch(keyCode) {
                        case 36: // home
                            positionToScroll = 0;
                            break;
                        case 33: // page up
                            positionToScroll = curPosition - delta;
                            break;
                        case 35: // end
                            positionToScroll = max_positionToScroll;
                            break;
                        case 34: // page down
                            positionToScroll = curPosition + delta;
                            break;
                        default:
                            lockScroll = true;
                    }

                    if (positionToScroll < 0) {
                        positionToScroll = 0;
                    }
                    if (positionToScroll > max_positionToScroll) {
                        positionToScroll = max_positionToScroll;
                    }
                    if (!lockScroll) {
                        var scroll =_this.scrolls[scrollName].$scrollViewport.get(0);
                        scroll.scrollTop = positionToScroll;
                    }
                }
            },

            buildMainView: function(options) {
                _this.done = false;

                // var counter=setInterval(timer, 1000);
                var left = _this.minLoadingTime,
                    invokeTimeout = 10,
                    checkTohideLoading = function() {
                        left = left - invokeTimeout;
                        if (left <= 0 && _this.globalSpinner && _this.done) {
                            clearInterval(counter);
                            _this.hideGlobalLoading();
                            return;
                        }

                        //Do code for showing the number of seconds here
                    },
                    counter = setInterval(checkTohideLoading, invokeTimeout);

                _this.$messageLayout = $(template.buildTemplate('message/message-layout', {}));
                if (options.success) options.success(_this.$messageLayout);
                if (_this.state.activeLayouts <= 0) {
                    diagnosticWorkMemoryController.checkStatus();
                    _this.state.renderLayout();
                }
                _this.globalSpinner = true;
                _this.showGlobalLoading(true);

                contactManager.load()
                    .always(function() {
                        messageManager.load()
                            .done(function() {
                                _this.renderMessageLayout(options);
                            })
                            .fail(function() {
                                var $container = $('#message-list');
                                var errorHTML = '<div class="bigger-110"><div class="alert alert-danger">Error loading messages!</div></div>';
                                $container.html(errorHTML);
                                _this.hideGlobalLoading();
                            });
                    });
            },

            renderMessageLayout: function(options) {
                // TODO replace with routing
                _this.messageLocation = window.$wrtc.enums.messageLocation.inbox;
                _this.artifactTypes = [
                    messageManager.ArtifactTypeEnum.SMS,
                    messageManager.ArtifactTypeEnum.MMS,
                    messageManager.ArtifactTypeEnum.VoiceMail,
                    messageManager.ArtifactTypeEnum.EMail,
                    messageManager.ArtifactTypeEnum.Draft
                ];

                _this.prepareData();

                options.renderData = {
                    inbox: _this.inbox,
                    sent: _this.sent,
                    trash: _this.trash,
                    draft: _this.draft,
                    messageLocation: _this.messageLocation
                };

                _this.renderMessageList(options);
                _this.setupHandlers(_this.$messageLayout);
            },

            renderMessageList: function(options) {

                var $messageList = _this.$messageLayout.find('.message-list'),
                    totalHTML = '',
                    dataArray = [];

                switch (options.renderData.messageLocation) {
                    case window.$wrtc.enums.messageLocation.inbox:
                        dataArray = options.renderData.inbox;
                        break;
                    case window.$wrtc.enums.messageLocation.sent:
                        dataArray = options.renderData.sent;
                        break;
                    case window.$wrtc.enums.messageLocation.trash:
                        dataArray = options.renderData.trash;
                        break;
                    case window.$wrtc.enums.messageLocation.draft:
                        dataArray = options.renderData.draft;
                        break;
                }

                $.each(dataArray, function(ind, curItem) {
                    var HTML = '',
                        _curItem = $.extend({}, curItem),
                        childrenHTML = '';
                    if (_curItem.type === 'Conversation') {
                        _curItem.messageLocation = _this.messageLocation;
                        _curItem.text = _curItem.messages[0].text;
                        _curItem.sentBy = _curItem.messages[0].sentBy;
                        _curItem.sentTo = _curItem.messages[0].sentTo;
                        HTML = template.buildTemplate('message/message-list-item', _curItem);
                        $.each(_curItem.messages, function(ind, _curMessage) {
                            _curMessage.type = 'Message';
                            childrenHTML += template.buildTemplate('message/message-list-item', _curMessage);
                        });
                        HTML = HTML.replace("<!--text-->", childrenHTML);
                    } else {
                        _curItem.type = 'Message';
                        _curItem.messageLocation = _this.messageLocation;
                        HTML = template.buildTemplate('message/message-list-item', _curItem);
                    }
                    totalHTML = totalHTML + HTML;
                });

                $messageList.html(totalHTML);

                _this.initScroll('message-list', $('.message-list-scroll'));
                _this.resizeScroll('message-list');

                _this.updateCounters({
                    inbox: {
                        total: messageManager.countMessages(options.renderData.inbox),
                        unread: messageManager.countUnreadMessages(options.renderData.inbox)
                    },
                    sent: {
                        total: messageManager.countMessages(options.renderData.sent),
                        unread: ''
                    },
                    trash: {
                        total: messageManager.countMessages(options.renderData.trash),
                        unread: messageManager.countUnreadMessages(options.renderData.trash)
                    },
                    draft: {
                        total: messageManager.countMessages(options.renderData.draft),
                        unread: messageManager.countUnreadMessages(options.renderData.draft)
                    }
                });

                _this.hideLoadingSpinner(_this.$messageLayout);
                _this.done = true;
                eventEmitter.trigger('LOADED', _this);
                _this.STATE = 'READY';
            },

            updateCounters: function(options) {
                var $inboxCounter = $('#unread-inbox-count'),
                    $sentCounter = $('#unread-sent-count'),
                    $trashCounter = $('#unread-trash-count'),
                    $draftCounter = $('#unread-draft-count'),
                    $filterCounter = $('#message-list-count');

                if (options.inbox) {
                    $inboxCounter.html(options.inbox.unread);
                }
                if (options.sent) {
                    $sentCounter.html(options.sent.unread);
                }
                if (options.trash) {
                    $trashCounter.html(options.trash.unread);
                }
                if (options.draft) {
                    $draftCounter.html(options.draft.unread);
                }

                switch (_this.messageLocation) {
                    case window.$wrtc.enums.messageLocation.inbox:
                        $filterCounter.html(options.inbox.total);
                        break;
                    case window.$wrtc.enums.messageLocation.sent:
                        $filterCounter.html(options.sent.total);
                        break;
                    case window.$wrtc.enums.messageLocation.trash:
                        $filterCounter.html(options.trash.total);
                        break;
                    case window.$wrtc.enums.messageLocation.draft:
                        $filterCounter.html(options.draft.total);
                        break;
                }
            },

            setupMessageDetail: function(options) {
                var id = options.id,
                    foundMessage = options.foundMessage || messageManager.getById(id),
                    messageHTML = template.buildTemplate('message/message-detail', foundMessage),
                    scrollMarginTop = 30;

                _this.$messageDetailContainer = _this.$messageLayout.find('.message-detail-container');
                var dataId = _this.$messageDetailContainer.find('[data-id]');
                if (dataId.length) {
                    dataId = dataId.attr('data-id');
                }
                if (dataId && dataId == foundMessage.id) {
                    _this.transitionDetailIn(_this.$messageDetailContainer);
                    return;
                }

                _this.$messageDetailContainer.html(messageHTML);
                _this.transitionDetailIn(_this.$messageDetailContainer);

                if (foundMessage.artifactType == messageManager.ArtifactTypeEnum.VoiceMail) {

                    var player = _this.$messageDetailContainer.find('#voicemail-player').get(0);
                    videojs(player, {}, function() {
                        var currentPlayer = this;
                        currentPlayer.on("ended", function(){
                            currentPlayer.currentTime(0);
                            currentPlayer.pause();
                        });
                    });
                    scrollMarginTop = 110;

                }

                //back to message list
                _this.$messageDetailContainer.off('click', '[data-action]').on('click', '[data-action]', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    var actionAttr = $(this).attr('data-action'), confirmText;
                    actionAttr = actionAttr.substring(actionAttr.indexOf(':') + 1);

                    switch (actionAttr) {
                        case 'reply':
                            options.message = foundMessage;
                            options.action = actionAttr;
                            _this.setupMessageEditing(options);
                            break;
                        case 'forward':
                            options.message = foundMessage;
                            options.action = actionAttr;
                            _this.setupMessageEditing(options);
                            break;
                        case 'delete':
                            if (_this.messageLocation !== window.$wrtc.enums.messageLocation.trash) {
                                confirmText = "Are you sure you want to delete this message ?";
                            } else {
                                confirmText = "Are you sure you want to permanently delete this message ?";
                            }
                            bootbox.confirm(confirmText, function(result) {
                                if (result) {
                                    _this.showLoadingSpinner(_this.$messageLayout).done( function() {
                                        var onSuccessDelete = function() {
                                            _this.prepareData();
                                            _this.reloadList();
                                            _this.hideSecondaryLayouts();
                                        };
                                        var onFailureDelete = function() {
                                            if (_this.messageLocation !== window.$wrtc.enums.messageLocation.trash) {
                                                foundMessage.isMarkForDelete = false; // restore message state
                                            }
                                            $.gritter.add({
                                                text: 'Error delete message:: ' + foundMessage.text,
                                                class_name: 'gritter-error gritter-center'
                                            });
                                        };
                                        // hard or soft delete => define Arcus
                                        return messageManager.deleteMessageRequest(foundMessage, onSuccessDelete, onFailureDelete);
                                    });
                                }
                            });
                            break;
                        case 'back':
                            var $player = _this.$messageDetailContainer.find('#voicemail-player')
                                , player;
                            if ($player.length) {
                                player = $player.get(0).player;
                            }
                            if (player && player.pause) {
                                player.pause();
                            }
                            _this.hideDetailLayout();
                            break;
                    }
                });

                _this.initScroll('message-detail', $('.message-detail-scroll'));
                _this.resizeScroll('message-detail', {
                    top: scrollMarginTop
                });
            },

            setupMessageEditing: function(options) {
                var foundMessage = $.extend({}, options.message),
                    messageHTML,messageHTMLError,  subjectPrefix, headerText = 'New Message',
                    view = {}, validator = null;
                _this.$messageEditContainer = _this.$messageLayout.find('.message-edit-container');

                switch (options.action) {
                    case 'reply':
                        subjectPrefix = "Re:";
                        headerText = 'Reply to message';
                        foundMessage.text = '';
                        break;
                    case 'forward':
                        subjectPrefix = "Fwd:";
                        headerText = 'Forward message';
                        break;
                }

                messageHTMLError = template.buildTemplate('message/message-edit-error');
                foundMessage.subject = $.isNotNull(foundMessage.subject) ? foundMessage.subject : "";
                if ((options.action === 'reply' || options.action === 'forward') && foundMessage.subject.indexOf(subjectPrefix) === -1) {
                    foundMessage.subject = [subjectPrefix, foundMessage.subject].join(' ');
                }
                messageHTML = template.buildTemplate('message/message-edit', foundMessage);
                _this.$messageEditContainer.html(messageHTML);

                _this.transitionDetailIn(_this.$messageEditContainer).done(function() {

                    view.$methodsInput = _this.$messageEditContainer.find('#recipients');
                    view.$header = _this.$messageEditContainer.find('.message-detail-action-header');
                    view.$recipients = _this.$messageEditContainer.find('.js-view-recipients');
                    view.$recipients.display = function(visible) {
                        if (visible) {
                            view.$recipients.removeClass('hide');
                        } else {
                            view.$recipients.addClass('hide');
                        }
                    };
                    view.$buttonSend = _this.$messageEditContainer.find('[data-action="action:send"]');
                    view.$buttonSend.display = function(visible) {
                        if (visible) {
                            view.$buttonSend.removeClass('hide');
                        } else {
                            view.$buttonSend.addClass('hide');
                        }
                    };
                    view.$buttonSave = _this.$messageEditContainer.find('[data-action="action:save"]');
                    view.$buttonSave.display = function(visible) {
                        if (visible) {
                            view.$buttonSave.removeClass('hide');
                        } else {
                            view.$buttonSave.addClass('hide');
                        }
                    };
                    view.$artifactTypeSelect = _this.$messageEditContainer.find("select[name='artifactType']");
                    view.$subject = _this.$messageEditContainer.find(".js-view-subject");
                    view.$subject.display = function(visible) {
                        if (visible) {
                            view.$subject.removeClass('hide');
                        } else {
                            view.$subject.addClass('hide');
                        }
                    };
                    view.$attachments = _this.$messageEditContainer.find(".js-view-attachments");
                    view.$attachments.display = function(visible) {
                        if (visible) {
                            view.$attachments.removeClass('hide');
                        } else {
                            view.$attachments.addClass('hide');
                        }
                    };
                    view.$voiceemailurl = _this.$messageEditContainer.find(".js-view-URLVoiceMail");
                    if (foundMessage.artifactType != "VoiceMail") {
                        view.$voiceemailurl.remove();
                    }
                    // temp hide empty block
                    // else {
                    // }
                    view.displayByArtifactType = function(_artifactType) {
                        switch (_artifactType) {
                            case messageManager.ArtifactTypeEnum.SMS:
                                view.$subject.display();
                                view.$attachments.display();
                                view.$buttonSend.display(true);
                                break;
                            case messageManager.ArtifactTypeEnum.EMail:
                                view.$subject.display(true);
                                view.$attachments.display(true);
                                view.$buttonSend.display(true);
                                break;
                        }
                    };

                    var rawMethods = contactManager.getAutocompleteContactsMethods({
                        groupManager: groupManager
                    });

                    var tagsinputData = view.$methodsInput.webrtc_tagsinput({
                        tagClass: function () {
                            return 'label label-info attendees-label';
                        },
                        itemValue: 'id',
                        itemText: 'fullInfo',
                        typeahead: {
                            source: rawMethods
                        },
                        onItemAdded: function() {
                            if(validator) {
                                validator.element('input[name="bootstrapRecipients"]');
                            }
                        },
                        onItemRemoved: function() {
                            if(validator) {
                                validator.element('input[name="bootstrapRecipients"]');
                            }
                        }
                    });

                    var checkArtifactType;
                    if (options.message.draftType) {
                        checkArtifactType = options.message.draftType;
                    } else if (options.message.artifactType !== messageManager.ArtifactTypeEnum.VoiceMail) {
                        checkArtifactType = options.message.artifactType;
                    } else {
                        checkArtifactType = messageManager.ArtifactTypeEnum.SMS;
                    }

                    switch (options.action) {
                        case 'reply':
                            if (_this.messageLocation === window.$wrtc.enums.messageLocation.sent) {
                                $.each(foundMessage.sentTo, function(ind, _curSentTo) {
                                    view.$methodsInput.webrtc_tagsinput('add', contactManager.wrapAutocompleteContactMethod(_curSentTo.contact, _curSentTo));
                                });
                            } else {
                                if (messageManager.isSentByMyself(foundMessage)) {
                                    $.each(foundMessage.sentTo, function(ind, _curSentTo) {
                                        view.$methodsInput.webrtc_tagsinput('add', contactManager.wrapAutocompleteContactMethod(_curSentTo.contact, _curSentTo));
                                    });
                                } else {
                                    view.$methodsInput.webrtc_tagsinput('add', contactManager.wrapAutocompleteContactMethod(foundMessage.sentBy.contact, foundMessage.sentBy));
                                }
                            }
                            break;
                        case 'writeTo':
                            $.each(options.autocompleteMethods, function(ind, _curMethod) {
                                view.$methodsInput.webrtc_tagsinput('add', _curMethod);
                            });
                            break;
                        case 'edit':
                            $.each(foundMessage.sentTo, function(ind, _curSentTo) {
                                view.$methodsInput.webrtc_tagsinput('add', contactManager.wrapAutocompleteContactMethod(_curSentTo.contact, _curSentTo));
                            });
                            break;
                    }

                    view.$header.text(headerText);
                    view.$artifactTypeSelect.val(checkArtifactType);
                    view.displayByArtifactType(checkArtifactType);

                    var sendMessage = function() {
                        if (arcus.diagnosticStatus.SMSMessageService === false&&checkArtifactType=='SMS') {
                            _this.$messageEditContainer.html(messageHTMLError);
                        }
                        else if (arcus.diagnosticStatus.EMailService === false&&checkArtifactType=='EMail') {
                            _this.$messageEditContainer.html(messageHTMLError);
                        }
                        else{
                        _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                            var onMessageSent = function() {
                                _this.prepareData();

                                _this.renderMessageList({
                                    renderData: {
                                        inbox: _this.inbox,
                                        sent: _this.sent,
                                        trash: _this.trash,
                                        draft: _this.draft,
                                        messageLocation: _this.messageLocation
                                    }
                                });

                                _this.hideSecondaryLayouts();
                                // self off subscribe
                                eventEmitter.off(messageManager.events.RECORD_ADDED_LOCALLY, onMessageSent);

                                audiotones.play(audiotones.CHAT_OUT);

                            };

                            eventEmitter.off(messageManager.events.RECORD_ADDED_LOCALLY, onMessageSent);
                            eventEmitter.on(messageManager.events.RECORD_ADDED_LOCALLY, onMessageSent);

                            $.waitFor(messageManager.sendMessageRequest(_this.serializeMessage()))
                                .done(function(){
                                    switch(checkArtifactType){
                                        case "SMS":
                                            ga('send', 'event', 'SMS', 'send');
                                            break;
                                        case "EMail":
                                            ga('send', 'event', 'Email', 'send');
                                            break;
                                    }
                                })
                                .fail(function(){
                                    audiotones.play(audiotones.MSG_FAIL);
                                    switch(checkArtifactType){
                                        case "SMS":
                                            ga('send', 'event', 'SMS', 'error', "failed to send");
                                            break;
                                        case "EMail":
                                            ga('send', 'event', 'Email', 'error', "failed to send");
                                            break;
                                    }
                            });
                        });
                    }
                    };

                    var messageForm = $('#id-message-form');

                    if (tagsinputData && tagsinputData.length) {
                        var $bootstrapInput = tagsinputData[0].$input;
                        $bootstrapInput.attr('name', 'bootstrapRecipients');
                        $bootstrapInput.on('paste', function (event) {
                            // hack to do native handler
                            // then resize the field
                            var clipboardData = event.originalEvent.clipboardData || window.clipboardData
                                , data = clipboardData.getData('Text')
                                , pasteLength = Math.max(data.length, $bootstrapInput.val().length);
                            $bootstrapInput.get(0).style.setProperty('width', (pasteLength < 3 ? 3 : pasteLength) + 'em', 'important');
                        });

                        $.validator.addMethod('tags', function() {
                            var tagsPresent = messageForm.find('.bootstrap-tagsinput .label').length > 0;
                            var textLength = messageForm.find('.bootstrap-tagsinput input').val();
                            return tagsPresent || textLength.length > 0;
                        });

                        $.validator.addMethod('phoneNumber', function(value) {
                            return $.trim(value) === '' || validations.isValidPhone(value);
                        }, validations.invalidPhoneNumberMessage());

                        $.validator.addMethod('recipientsEmailsValid', function() {
                            return _this.validateTags(tagsinputData[0], validator, 'email');
                        }, 'Some of the recipients has invalid email. Email address must be in the valid format localpart@domain (e.g. johndoe@centurylink.net)');

                        $.validator.addMethod('recipientsPhonesValid', function() {
                            return _this.validateTags(tagsinputData[0], validator, 'sms');
                        }, 'Some of the recipients has invalid phone. Phone number must be in the valid format (XXX) XXX-XXXX or XXXXXXXXXX or XXX-XXX-XXXX');

                        validator = messageForm.validate({
                            ignore: "",
                            errorClass: 'help-block',
                            errorElement: 'div',
                            highlight: function(e) {
                                $(e).closest('.form-group').addClass('has-error');
                            },
                            unhighlight: function(e) {
                                $(e).closest('.form-group').removeClass('has-error');
                            },
                            errorPlacement: function(error, element) {
                                error.insertAfter(element.closest('.validation-placement'));
                            },
                            submitHandler: function() {
                                this.form();
                                if (this.valid()) {
                                    sendMessage();
                                }
                            },
                            rules: {
                                bootstrapRecipients: { tags: true },
                                messageText: { required: true }
                            },
                            messages: {
                                bootstrapRecipients: { tags: 'At least 1 recipient is required' },
                                messageText: { required: 'Message text is required' }
                            }
                        });

                        var addEmailValidationRule = function() {
                            messageForm.find('.bootstrap-tagsinput input').rules('add', { email: true, recipientsEmailsValid: true });
                        };

                        var removeEmailValidationRule = function() {
                            messageForm.find('.bootstrap-tagsinput input').rules('remove', 'email');
                            messageForm.find('.bootstrap-tagsinput input').rules('remove', 'recipientsEmailsValid');
                        };

                        var addPhoneValidationRule = function() {
                            messageForm.find('.bootstrap-tagsinput input').rules('add', { phoneNumber: true, recipientsPhonesValid: true });
                        };

                        var removePhoneValidationRule = function() {
                            messageForm.find('.bootstrap-tagsinput input').rules('remove', 'phoneNumber');
                            messageForm.find('.bootstrap-tagsinput input').rules('remove', 'recipientsPhonesValid');
                        };

                        if (checkArtifactType === messageManager.ArtifactTypeEnum.EMail) {
                            addEmailValidationRule();
                        }

                        if (checkArtifactType === messageManager.ArtifactTypeEnum.SMS) {
                            addPhoneValidationRule();
                        }

                        messageForm.find('[name=artifactType]').off('change').on('change', function(){
                            var messageType = $(this).find('option:selected').val();
                            if(messageType === messageManager.ArtifactTypeEnum.EMail) {
                                removePhoneValidationRule();
                                addEmailValidationRule();
                            } else if(messageType === messageManager.ArtifactTypeEnum.SMS){
                                removeEmailValidationRule();
                                addPhoneValidationRule();
                            } else {
                                removeEmailValidationRule();
                                removePhoneValidationRule();
                            }

                            validator.element('input[name="bootstrapRecipients"]');

                            view.displayByArtifactType(messageType);
                        });

                        validator.element('input[name="bootstrapRecipients"]');
                    }
                });

                _this.$messageEditContainer.off('click', '[data-action]').on('click', '[data-action]', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    var $target = $(event.currentTarget),
                        actionAttr = $target.attr('data-action');
                    actionAttr = actionAttr.substring(actionAttr.indexOf(':') + 1);
                    switch (actionAttr) {
                        case 'cancel':
                            _this.transitionDetailOut(_this.$messageEditContainer);
                            break;
                        case 'send':
                            $('#id-message-form').submit();
                            break;
                        case 'save':
                            _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                                var serializedMessageData = _this.serializeMessage()
                                    , onSuccesMessageSaved = function() {

                                        ga('send', 'event', 'Draft', 'saved');
                                        _this.prepareData();

                                        _this.renderMessageList({
                                            renderData: {
                                                inbox: _this.inbox,
                                                sent: _this.sent,
                                                trash: _this.trash,
                                                draft: _this.draft,
                                                messageLocation: _this.messageLocation
                                            }
                                        });

                                        _this.hideSecondaryLayouts();
                                    }
                                    , onFailureMessageSaved = function() {
                                        ga('send', 'event', 'Draft', 'error', 'failed to save');
                                        _this.hideLoadingSpinner(_this.blocked$Element);
                                        $.gritter.add({
                                            text: 'failure to save message draft!',
                                            class_name: 'gritter-error gritter-center'
                                        });
                                    };

                                $.waitFor(messageManager.saveMessageRequest(serializedMessageData, onSuccesMessageSaved, onFailureMessageSaved));
                            });
                            break;
                    }
                });

                _this.initScroll('message-edit', $('.message-edit-scroll'));
                _this.resizeScroll('message-edit', {
                    top: 25
                });
                
                $('.bootstrap-tagsinput input').focus();
            },

            showLoadingSpinner: function($element) {
                var deferred = $.Deferred();

                $.blockUI.defaults.css.cursor = 'default';

                $element.block({
                    baseZ: 16,
                    css: {
                        border: 'none',
                        backgroundColor: 'transparent',
                        cursor: 'auto'
                    },
                    overlayCSS: {
                        backgroundColor: 'white',
                        opacity: 0.7,
                        cursor: 'auto'
                    },
                    onBlock: function() {
                        // when element is blocked
                        _this.STATE = 'BUSY';
                        _this.blocked$Element = $element;
                        eventEmitter.off(messageManager.events.ERROR).on(messageManager.events.ERROR, function(event, data) {
                            _this.hideLoadingSpinner($element);
                            if (data) {
                                $.gritter.add({
                                    title: data.statusCode,
                                    text: data.text,
                                    class_name: 'gritter-error gritter-center'
                                });
                            }
                        });
                        deferred.resolve();
                    },
                    message: '<i class="icon-spin icon-spinner icon-4x green"></i>'
                });

                return deferred;
            },

            hideLoadingSpinner: function($element) {
                if ($element && $element.length) {
                    $element.unblock();
                } else if (_this.blocked$Element && _this.blocked$Element.length) {
                    _this.blocked$Element.unblock();
                    _this.blocked$Element = $();
                }
                _this.STATE = 'READY';
            },

            showGlobalLoading: function(showSpinner) {
                var spinner = $('#main-container .widget-box-overlay .icon-spinner');

                if (showSpinner) {
                    spinner.show();
                } else {
                    spinner.hide();
                }

                $('#main-container .widget-box-overlay').show();
            },

            hideGlobalLoading: function() {
                $('#main-container .widget-box-overlay').hide();
            },

            showLocalOverlay: function(done) {
                _this.messagesOverlayCounter++;
                var secondaryOverlay = $('#main-container .widget-box-overlay-secondary');
                secondaryOverlay.show();
                secondaryOverlay.on('click', function(e){
                    e.preventDefault();
                    secondaryOverlay.off('click');
                    if (done) {
                        done();
                    }
                });
            },

            hideLocalOverlay: function() {
                if (_this.messagesOverlayCounter > 0) {
                    if (_this.messagesOverlayCounter === 1) {
                        $('#main-container .widget-box-overlay-secondary').hide();
                    }
                    _this.messagesOverlayCounter--;
                }
            },

            transitionDetailIn: function(_$selector) {
                var $deferred = $.Deferred();

                _this.showLocalOverlay(function(){
                    _this.transitionDetailOut(_$selector);
                });
                if (_this.state.activeLayouts < 2) {
                    _$selector.removeClass('off-tr');
                    _$selector.addClass('on-tr');

                    _$selector.addClass('on-scr');
                    _$selector.removeClass('off-scr');
                    $deferred.resolve();
                    _this.state.renderLayout();
                } else {
                    _this.showLoadingSpinner(_this.$messageLayout);
                    _$selector.removeClass('on-tr');
                    _$selector.addClass('off-tr');

                    _$selector.css('opacity', _this.fadeInStartingOpacity);

                    setTimeout(function() {

                        _$selector.animate({
                            opacity: 1
                        });

                        _$selector.addClass('on-scr');
                        _$selector.removeClass('off-scr');

                        _this.hideLoadingSpinner(_this.$messageLayout);

                        $deferred.resolve();
                        _this.state.renderLayout();

                    }, _this.minLoadingTime);
                }

                return $deferred;
            },

            transitionDetailOut: function(_$selector) {
                var $deferred = $.Deferred();

                if (_this.state.activeLayouts < 3) {
                    _$selector.removeClass('off-tr');
                    _$selector.addClass('on-tr');

                    _$selector.addClass('off-scr');
                    _$selector.removeClass('on-scr');
                    $deferred.resolve();
                    _this.state.destroyLayout();
                } else {
                    _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                        _$selector.removeClass('on-tr');
                        _$selector.addClass('off-tr');

                        setTimeout(function() {

                            _$selector.removeClass('on-scr');
                            _$selector.addClass('off-scr');

                            _this.hideLoadingSpinner(_this.$messageLayout);

                            $deferred.resolve();
                            _this.state.destroyLayout();

                        }, _this.minLoadingTime);
                    });
                }

                _this.hideLocalOverlay();
                return $deferred;
            },

            hideSecondaryLayouts: function() {
                if (_this.$messageEditContainer.hasClass('on-scr')) {
                    _this.transitionDetailOut(_this.$messageEditContainer).done(function() {
                        _this.hideDetailLayout();
                    });
                } else {
                    _this.hideDetailLayout();
                }
            },

            hideDetailLayout: function() {
                if (_this.$messageDetailContainer.hasClass('on-scr')) {
                    _this.transitionDetailOut(_this.$messageDetailContainer);
                }
            },

            serializeMessage: function() {
                var $form = $('#id-message-form'),
                    data = $form.serializeObject(),
                    textRecipients = $form.find('.bootstrap-tagsinput input').val(),
                    recipients = textRecipients.split(',');

                data.text = $form.find('#text').val();

                data.id = $form.attr('data-id');

                data.artifactTypeSnapshot = $form.attr('data-artifactType');

                data.recipients = $form.find('#recipients').webrtc_tagsinput('items');

                $.each(recipients, function(ind, recipient) {
                    var curRecipient = recipient.trim();
                    if (curRecipient !== "") {
                        data.recipients.push( contactManager.wrapAutocompleteContactMethod( undefined, {
                                id: -1,
                                name: "",
                                number: curRecipient
                            }));
                    }
                });

                return data;
            },

            resizeSubscriber: function(event, data) {
                if (data.routeName && data.routeName === 'messages') {
                    _this.resizeScroll('message-list');
                }
            },

            routerSubscriber: function(event, data) {
                if (data.routeName && data.routeName !== data.routing.pages.MESSAGES) {
                    eventEmitter.off(eventEmitter.Event.ROUTER_AFTER_CHANGE, _this.routerSubscriber);
                    _this.state.invalidateLayouts();
                }
            },

            scrollListByKeyCode: function(keyCode) {
                if (_this.state.activeLayouts && _this.state.activeLayouts < 2) {
                    _this.scrollTo('message-list', keyCode);
                }
            },

            setupHandlers: function($messagesView) {
                eventEmitter.off(eventEmitter.Event.VIEW_RESIZE, _this.resizeSubscriber);
                eventEmitter.on(eventEmitter.Event.VIEW_RESIZE, _this.resizeSubscriber);

                eventEmitter.off(eventEmitter.Event.ROUTER_AFTER_CHANGE, _this.routerSubscriber);
                eventEmitter.on(eventEmitter.Event.ROUTER_AFTER_CHANGE, _this.routerSubscriber);

                var $dropdownFilterSort = $messagesView.find('.dropdown-filter');
                $dropdownFilterSort.$parentFilter = $dropdownFilterSort.find('[data-filter="filter:ALL"]');
                $dropdownFilterSort.$parentFilter.toggle = function(toggle) {
                    $dropdownFilterSort.$parentFilter.get(0).checked = toggle ? toggle : false;
                };
                $dropdownFilterSort.$childrenFilter = $dropdownFilterSort.find('[data-filter]:not([data-filter="filter:ALL"])');
                $dropdownFilterSort.$childrenFilter.toggle = function(toggle) {
                    $.each($dropdownFilterSort.$childrenFilter, function(ind, item) {
                        item.checked = toggle ? toggle : false;
                    });
                };
                $dropdownFilterSort.$childrenFilter.count = function() {
                    var countTrue = 0,
                        countFalse = 0,
                        isMarkedFalse = false,
                        isMarkedTrue = false;
                    $dropdownFilterSort.$childrenFilter.each(function() {
                        if (this.checked) {
                            isMarkedTrue = true;
                            countTrue++;
                        } else {
                            isMarkedFalse = true;
                            countFalse++;
                        }
                    });

                    return {
                        countTrue: countTrue,
                        isMarkedTrue: isMarkedTrue,
                        countFalse: countFalse,
                        isMarkedFalse: isMarkedFalse
                    };
                };
                $dropdownFilterSort.$childrenFilter.on('click', function() {
                    var result = $dropdownFilterSort.$childrenFilter.count();
                    // if at least one is not checked
                    if (result.isMarkedFalse) {
                        $dropdownFilterSort.$parentFilter.toggle();
                    } else if (result.isMarkedTrue && !result.isMarkedFalse) {
                        // if all contact methods are checked
                        $dropdownFilterSort.$parentFilter.toggle(true);
                    }
                });
                $dropdownFilterSort.$allFilter = $dropdownFilterSort.find('[data-filter]');
                $dropdownFilterSort.$allFilter.on('click', function(event) {
                    event.stopPropagation(); // do not close the dropdown list
                    // {{ invalidate
                    $selectFilter.$parentSelect.toggle();
                    $actionToolbar.display();
                    $search.val('');
                    // }} invalidate
                    $search.val('');

                    var self = this,
                        $target = $(event.target),
                        filterAttr = $target.attr('data-filter');
                    if (filterAttr && filterAttr.indexOf('filter') > -1) {

                        _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                            filterAttr = filterAttr.substring(filterAttr.indexOf(':') + 1);
                            if (filterAttr === 'ALL') {
                                $dropdownFilterSort.$childrenFilter.toggle(self.checked);
                            }

                            $dropdownFilterSort.getFilterState();

                            _this.prepareData();

                            _this.renderMessageList({
                                renderData: {
                                    inbox: _this.inbox,
                                    sent: _this.sent,
                                    trash: _this.trash,
                                    draft: _this.draft,
                                    messageLocation: _this.messageLocation
                                }
                            });
                        });
                    }
                });
                $dropdownFilterSort.getFilterState = function() {
                    var $filterALL = $dropdownFilterSort.find('[data-filter="filter:ALL"]'),
                        $filterSMS = $dropdownFilterSort.find('[data-filter="filter:SMS"]'),
                        $filterMMS = $dropdownFilterSort.find('[data-filter="filter:MMS"]'),
                        $filterVoiceMail = $dropdownFilterSort.find('[data-filter="filter:VoiceMail"]'),
                        $filterEMail = $dropdownFilterSort.find('[data-filter="filter:EMail"]');
                    _this.artifactTypes = [];

                    if ($filterALL.get(0).checked) {
                        _this.artifactTypes.push(
                            messageManager.ArtifactTypeEnum.SMS,
                            messageManager.ArtifactTypeEnum.MMS,
                            messageManager.ArtifactTypeEnum.VoiceMail,
                            messageManager.ArtifactTypeEnum.EMail,
                            messageManager.ArtifactTypeEnum.Draft
                        );
                    } else {
                        if ($filterSMS.get(0).checked) {
                            _this.artifactTypes.push(messageManager.ArtifactTypeEnum.SMS);
                        }

                        if ($filterMMS.get(0).checked) {
                            _this.artifactTypes.push(messageManager.ArtifactTypeEnum.MMS);
                        }

                        if ($filterVoiceMail.get(0).checked) {
                            _this.artifactTypes.push(messageManager.ArtifactTypeEnum.VoiceMail);
                        }

                        if ($filterEMail.get(0).checked) {
                            _this.artifactTypes.push(messageManager.ArtifactTypeEnum.EMail);
                        }

//                        if ($filterDraft.length && $filterDraft.get(0).checked) {
//                            _this.artifactTypes.push(messageManager.ArtifactTypeEnum.Draft);
//                        }
                        _this.artifactTypes.push(messageManager.ArtifactTypeEnum.Draft);
                    }
                };
                $dropdownFilterSort.$group = $dropdownFilterSort.find('[data-group="group"]');
                $dropdownFilterSort.$group.toggle = function(toggle) {
                    $dropdownFilterSort.$group.get(0).checked = toggle ? toggle : false;
                };
                $dropdownFilterSort.$group.on('click', function(event) {
                    event.stopPropagation(); // do not close the dropdown list
                    // {{ invalidate
                    $selectFilter.$parentSelect.toggle();
                    $actionToolbar.display();
                    $search.val('');
                    // }} invalidate
                    var self = this;
                    _this.groupState = self.checked;

                    _this.showLoadingSpinner(_this.$messageLayout).done(function() {

                        _this.prepareData(_this.messageLocation);

                        _this.renderMessageList({
                            renderData: {
                                inbox: _this.inbox,
                                sent: _this.sent,
                                trash: _this.trash,
                                draft: _this.draft,
                                messageLocation: _this.messageLocation
                            }
                        });
                    });
                });
                $dropdownFilterSort.$allSort = $dropdownFilterSort.find('[data-sort]');
                $dropdownFilterSort.$allSort.on('click', function(event) {
                    event.stopPropagation(); // do not close the dropdown list
                    // {{ invalidate
                    $selectFilter.$parentSelect.toggle();
                    $actionToolbar.display();
                    $search.val('');
                    // }} invalidate
                    var $this = $(this),
                        attrDataSort = $this.attr('data-sort');

                    $.each($dropdownFilterSort.$allSort, function(ind, item) {
                        var $item = $(item),
                            $icon = $item.find('i');
                        if ($item.attr('data-sort') === attrDataSort) {
                            $icon.removeClass('invisible');
                            $icon.addClass('green');
                        } else {
                            $icon.addClass('invisible');
                            $icon.removeClass('green');
                        }
                    });

                    _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                        _this.sortBy = attrDataSort.substring(attrDataSort.indexOf(':') + 1);

                        _this.prepareData(_this.messageLocation);

                        _this.renderMessageList({
                            renderData: {
                                inbox: _this.inbox,
                                sent: _this.sent,
                                trash: _this.trash,
                                draft: _this.draft,
                                messageLocation: _this.messageLocation
                            }
                        });
                    });
                });

                // by defaul all filters are checked
                $dropdownFilterSort.$parentFilter.toggle(true);
                $dropdownFilterSort.$childrenFilter.toggle(true);
                $dropdownFilterSort.$group.toggle(_this.groupState);

                var $messageList = $messagesView.find('.message-list');
                $messageList.toggleItemCheckbox = function(item, toggle) {
                    var $item = $(item),
                        $messageItem = $item.closest('.message-item');
                    if (toggle) {
                        $messageItem.addClass('selected');
                    } else {
                        $messageItem.removeClass('selected');
                    }
                    $item.get(0).checked = toggle ? toggle : false;
                };
                $messageList.toggleItemsCheckboxes = function(items, toggle) {
                    $.each(items, function(ind, item) {
                        $messageList.toggleItemCheckbox(item, toggle);
                    });
                };
                $messageList.getAllCheckboxes = function(checked, searchVal, $findIn) {
                    var filterVisible = '',
                        $rawItems = $(),
                        $filteredCheckboxes = [],
                        filterChecked = '',
                        addItem;

                    if (searchVal && searchVal !== '') {
                        filterVisible = ':not(.li-filtered)';
                    }
                    if (checked && checked !== '') {
                        filterChecked = ':checked';
                    }

                    $rawItems = $messageList.findButNotInside('.message-item' + filterVisible, $findIn);

                    addItem = function(currentItem) {
                        var found = currentItem.find('.js-message-item-checkbox' + filterChecked);
                        if (found.length) {
                            $filteredCheckboxes.push(found.get(0));
                        }
                    };

                    $.each($rawItems, function(ind, _curItem) {
                        var $item = $(_curItem),
                            $subitems;

                        if ($item.hasClass('js-message-wrapper')) {
                            $subitems = $item.find('.message-item' + filterVisible);
                            $.each($subitems, function(ind, _curSubItem) {
                                addItem($(_curSubItem));
                            });
                        } else {
                            addItem($item);
                        }

                    });
                    return $filteredCheckboxes;
                };
                $messageList.findButNotInside = function(selector, $findIn) {
                    var origElement = $findIn ? $findIn : $(this);
                    return origElement.find(selector).filter(function() {
                        var nearestMatch = $(this).parent().closest(selector);
                        return nearestMatch.length === 0 || origElement.find(nearestMatch).length === 0;
                    });
                };
                $messageList.getAllItems = function(checked, searchVal) {
                    var filterVisible = '',
                        $rawItems,
                        $filteredItems = [],
                        filterChecked = '',
                        addItem;

                    if (searchVal && searchVal !== '') {
                        filterVisible = ':not(.li-filtered)';
                    }
                    if (checked && checked !== '') {
                        filterChecked = ':checked';
                    }

                    addItem = function(currentItem) {
                        if ($(currentItem).find('.js-message-item-checkbox' + filterChecked).length) {
                            $filteredItems.push(currentItem);
                        }
                    };

                    $rawItems = $messageList.findButNotInside('.message-item' + filterVisible);
                    $.each($rawItems, function(ind, _curItem) {
                        var $item = $(_curItem),
                            $subitems;
                        if ($item.hasClass('js-message-wrapper')) {
                            $subitems = $item.find('.message-item' + filterVisible);
                            $.each($subitems, function(ind, _curSubItem) {
                                addItem(_curSubItem);
                            });
                        } else {
                            addItem(_curItem);
                        }
                    });
                    return $filteredItems;
                };
                $messageList.getWrapperCheckboxes = function(checked, searchVal) {
                    var filterVisible = '',
                        $rawItems = $(),
                        $filteredCheckboxes = [],
                        filterChecked = '';
                    if (searchVal && searchVal !== '') {
                        filterVisible = ':not(.li-filtered)';
                    }
                    if (checked && checked !== '') {
                        filterChecked = ':checked';
                    }
                    $rawItems = $messageList.findButNotInside('.message-item' + filterVisible);
                    $.each($rawItems, function(ind, _curItem) {
                        var $item = $(_curItem),
                            $wrapperCheckbox = $item.find('.js-message-wrapper-checkbox' + filterChecked);
                        if ($wrapperCheckbox.length) {
                            $filteredCheckboxes.push($wrapperCheckbox.get(0));
                        }
                    });
                    return $filteredCheckboxes;
                };
                $messageList.countCheckboxes = function($siblings) {
                    var countTrue = 0,
                        countFalse = 0,
                        isMarkedFalse = false,
                        isMarkedTrue = false;
                    $.each($siblings, function() {
                        if (this.checked) {
                            isMarkedTrue = true;
                            countTrue++;
                        } else {
                            isMarkedFalse = true;
                            countFalse++;
                        }
                    });

                    return {
                        countTrue: countTrue,
                        isMarkedTrue: isMarkedTrue,
                        countFalse: countFalse,
                        isMarkedFalse: isMarkedFalse
                    };
                };
                $messageList.getAllWrappers = function(searchVal) {
                    var filterVisible = '',
                        $rawItems = $(),
                        $filteredWrappers = [];
                    if (searchVal && searchVal !== '') {
                        filterVisible = ':not(.li-filtered)';
                    }
                    $rawItems = $messageList.findButNotInside('.message-item' + filterVisible);
                    $.each($rawItems, function(ind, _curItem) {
                        if ($(_curItem).hasClass('js-message-wrapper')) {
                            $filteredWrappers.push(_curItem);
                        }
                    });

                    return $filteredWrappers;
                };
                $messageList.checkWrappers = function($wrappers, searchVal) {
                    $.each($wrappers, function(ind, wrapper) {
                        var $wrapperContent = $(wrapper),
                            $wrapperItem = $wrapperContent.closest('.message-item'),
                            result;
                        result = $messageList.countCheckboxes($messageList.getAllCheckboxes(false, searchVal, $wrapperContent));
                        if (result.isMarkedFalse) {
                            // if at least one is not checked
                            $messageList.toggleItemCheckbox($wrapperItem.find('.js-message-wrapper-checkbox'));
                        } else if (result.isMarkedTrue && !result.isMarkedFalse) {
                            // if all contact methods are checked
                            $messageList.toggleItemCheckbox($wrapperItem.find('.js-message-wrapper-checkbox'), true);
                        }
                    });
                };
                $messageList.selectReadUnreadMessages = function(selectUnread, searchVal) {
                    var count = 0,
                        checkboxes = $messageList.getAllCheckboxes(false, searchVal);
                    $.each(checkboxes, function() {
                        var $this = $(this),
                            $messageItem = $this.closest('.message-item'),
                            toCount = false;
                        if ($messageItem.hasClass('message-unread') && selectUnread) {
                            toCount = true;
                        } else if (!$messageItem.hasClass('message-unread') && !selectUnread) {
                            toCount = true;
                        }

                        if (toCount) {
                            $messageList.toggleItemCheckbox(this, true);
                            count++;
                        } else {
                            $messageList.toggleItemCheckbox(this, false);
                        }
                    });

                    return count;
                };
                $messageList.on('click', '.js-message-item', function(event) {
                    event.stopPropagation();
                    //open detail message window
                    var $this = $(this),
                        id = $this.attr('data-id'),
                        foundMessage = messageManager.getById(id);
                    if (foundMessage) {
                        if (foundMessage.artifactType === foundMessage.ArtifactTypeEnum.DRAFT) {
                            _this.setupMessageEditing({
                                id: id,
                                message: foundMessage,
                                action: 'edit'
                            });
                        } else {
                            _this.setupMessageDetail({
                                id: id,
                                foundMessage: foundMessage
                            });
                        }
                    }

                    // make as read
                    setTimeout(function() {

                        if (foundMessage && !foundMessage.isRead) {
                            foundMessage.isRead = true;
                            foundMessage.save(function() {
                                var $message = _this.$messageLayout.find('.message-item[data-id="' + id + '"]');
                                $message.removeClass('message-unread');

                                _this.updateCounters({
                                    inbox: {
                                        total: messageManager.countMessages(_this.inbox),
                                        unread: messageManager.countUnreadMessages(_this.inbox)
                                    },
                                    sent: {
                                        total: messageManager.countMessages(_this.sent),
                                        unread: ''
                                    },
                                    trash: {
                                        total: messageManager.countMessages(_this.trash),
                                        unread: messageManager.countUnreadMessages(_this.trash)
                                    },
                                    draft: {
                                        total: messageManager.countMessages(_this.draft),
                                        unread: messageManager.countUnreadMessages(_this.draft)
                                    }
                                });

                                eventEmitter.trigger(messageManager.events.RECORD_UPDATED_LOCALLY, foundMessage);
                            });
                        }

                    }, _this.minLoadingTime);
                });
                $messageList.on('click', '.js-message-wrapper', function(event) {
                    event.stopPropagation();
                    //open wrapped sms messages
                    var $this = $(this),
                        $messageContent = $this.find('.message-content'),
                        $iconWrapper = $this.find('[data-role="wrapper-icon"]');
                    $messageContent.toggleClass('hide');
                    $iconWrapper.toggleClass('icon-level-down');
                    $iconWrapper.toggleClass('icon-level-up');
                    $this.toggleClass('message-inline-open');
                });
                $messageList.on('click', '.js-message-item-checkbox', function(event) {
                    event.stopPropagation();
                    var $this = $(this),
                        $wrapperContent = $this.closest('.js-message-wrapper'),
                        $wrapperItem = $wrapperContent.closest('.message-item'),
                        result;

                    if ($wrapperContent.length) {

                        result = $messageList.countCheckboxes($messageList.getAllCheckboxes(false, $search.val(), $wrapperContent));
                        if (result.isMarkedFalse) {
                            // if at least one is not checked
                            $messageList.toggleItemCheckbox($wrapperItem.find('.js-message-wrapper-checkbox'));
                        } else if (result.isMarkedTrue && !result.isMarkedFalse) {
                            // if all contact methods are checked
                            $messageList.toggleItemCheckbox($wrapperItem.find('.js-message-wrapper-checkbox'), true);
                        }
                    }

                    $messageList.toggleItemCheckbox(this, this.checked);
                    $actionToolbar.display($messageList.getAllCheckboxes(true, $search.val()).length);

                    result = $messageList.countCheckboxes($messageList.getAllCheckboxes(false, $search.val()));
                    if (result.isMarkedFalse) {
                        // if at least one is not checked
                        $selectFilter.$parentSelect.toggle();
                    } else if (result.isMarkedTrue && !result.isMarkedFalse) {
                        // if all contact methods are checked
                        $selectFilter.$parentSelect.toggle(true);
                    }
                });
                $messageList.on('click', '.js-message-wrapper-checkbox', function(event) {
                    event.stopPropagation();
                    var $this = $(this),
                        $parent = $this.closest('.message-item'),
                        $children = $messageList.getAllCheckboxes(false, $search.val(), $parent),
                        result;

                    $messageList.toggleItemCheckbox(this, this.checked);
                    // toggle all children
                    $messageList.toggleItemsCheckboxes($children, this.checked);
                    $actionToolbar.display($messageList.getAllCheckboxes(true, $search.val()).length);

                    result = $messageList.countCheckboxes($messageList.getAllCheckboxes(false, $search.val()));
                    if (result.isMarkedFalse) {
                        // if at least one is not checked
                        $selectFilter.$parentSelect.toggle();
                    } else if (result.isMarkedTrue && !result.isMarkedFalse) {
                        // if all contact methods are checked
                        $selectFilter.$parentSelect.toggle(true);
                    }
                });

                var $selectFilter = $messagesView.find('.select-filter'),
                    checked = [],
                    all = [];
                $selectFilter.$parentSelect = $messagesView.find('.js-message-toggle-all');
                $selectFilter.$parentSelect.toggle = function(toggle) {
                    $selectFilter.$parentSelect.get(0).checked = toggle ? toggle : false;
                };
                $selectFilter.$parentSelect.on('click', function() {
                    $messageList.toggleItemsCheckboxes($messageList.getAllCheckboxes(false, $search.val()), this.checked);
                    $messageList.toggleItemsCheckboxes($messageList.getWrapperCheckboxes(false, $search.val()), this.checked);
                    $actionToolbar.display($messageList.getAllCheckboxes(true, $search.val()).length);
                });
                $selectFilter.$allSelect = $selectFilter.find('[data-select]');
                $selectFilter.$allSelect.on('click', function(event) {
                    var $target = $(event.currentTarget),
                        selectAttr = $target.attr('data-select'),
                        difference = false;
                    if (selectAttr && selectAttr.indexOf('select') > -1) {

                        selectAttr = selectAttr.substring(selectAttr.indexOf(':') + 1);

                        switch (selectAttr) {
                            case 'All':
                                $selectFilter.$parentSelect.toggle(true);
                                $messageList.toggleItemsCheckboxes($messageList.getAllCheckboxes(false, $search.val()), true);
                                $messageList.toggleItemsCheckboxes($messageList.getWrapperCheckboxes(false, $search.val()), true);
                                $actionToolbar.display($messageList.getAllCheckboxes(true).length);
                                break;
                            case 'None':
                                $selectFilter.$parentSelect.toggle();
                                $messageList.toggleItemsCheckboxes($messageList.getAllCheckboxes(), false);
                                $messageList.toggleItemsCheckboxes($messageList.getWrapperCheckboxes(), false);
                                $actionToolbar.display(0);
                                break;
                            case 'Unread':
                                $messageList.selectReadUnreadMessages(true, $search.val());
                                $messageList.checkWrappers($messageList.getAllWrappers($search.val()), $search.val());
                                checked = $messageList.getAllCheckboxes(true, $search.val());
                                all = $messageList.getAllCheckboxes(false, $search.val());
                                difference = checked.length === all.length;
                                $selectFilter.$parentSelect.toggle(difference);
                                $actionToolbar.display(checked.length);
                                break;
                            case 'Read':
                                $messageList.selectReadUnreadMessages(false, $search.val());
                                $messageList.checkWrappers($messageList.getAllWrappers($search.val()), $search.val());
                                checked = $messageList.getAllCheckboxes(true, $search.val());
                                all = $messageList.getAllCheckboxes(false, $search.val());
                                difference = checked.length === all.length;
                                $selectFilter.$parentSelect.toggle(difference);
                                $actionToolbar.display(checked.length);
                                break;
                        }

                    }
                });

                var $search = $messagesView.find('input.nav-search-input');
                $search.on('focus', function() {
                    // {{ invalidate
                    $actionToolbar.display();
                    $selectFilter.$parentSelect.toggle();
                    $messageList.toggleItemsCheckboxes($messageList.getAllCheckboxes(), false);
                    $messageList.toggleItemsCheckboxes($messageList.getWrapperCheckboxes(), false);
                    // }} invalidate
                });
                $search.on('keyup', function() {
                    var $this = $(this),
                        classFiltered = 'li-filtered',
                        $items = $messageList.getAllItems(),
                        $wrappers = $messageList.getAllWrappers(),
                        filterVal = $this.val(),
                        listId = $this.attr('data-list-id'),
                        $listCount = $('#' + listId + '-count'),
                        visible = 0;

                    // filter list
                    if (filterVal === '') {
                        $.each($items, function(ind, item) {
                            var $item = $(item);
                            $item.removeClass(classFiltered);
                        });
                        visible = $items.length;
                    } else {
                        $.each($items, function(ind, item) {
                            var $item = $(item);
                            if ($item.is(':contains(' + filterVal + ')')) {
                                $item.removeClass(classFiltered);
                                visible++;
                            } else {
                                $item.addClass(classFiltered);
                            }
                        });
                    }

                    $.each($wrappers, function(ind, wrapper) {
                        var $wrapper = $(wrapper),
                            $wrapperItem = $wrapper.closest('.message-item'),
                            filtered = $wrapper.find('.' + classFiltered),
                            items = $wrapper.find('.message-item');
                        if (filtered.length === items.length) {
                            $wrapperItem.addClass(classFiltered);
                        } else {
                            $wrapperItem.removeClass(classFiltered);
                        }
                    });

                    // set list count if necessary
                    if ($listCount.length) {
                        $listCount.html(visible == $items.length ? $items.length : visible + '/' + $items.length);
                    }
                });

                var $actionToolbar = $messagesView.find('.message-toolbar-actions');
                $actionToolbar.$reply = $actionToolbar.find('[data-action="action:reply"]');
                $actionToolbar.$forward = $actionToolbar.find('[data-action="action:forward"]');
                $actionToolbar.$restore = $actionToolbar.find('[data-action="action:restore"]');
                $actionToolbar.display = function(selectedMessages) {
                    if (!selectedMessages || selectedMessages === 0) {
                        $actionToolbar.addClass('hide');
                    } else if (selectedMessages === 1) {
                        $actionToolbar.removeClass('hide');
                        $actionToolbar.$reply.show();
                        $actionToolbar.$forward.show();
                    } else {
                        $actionToolbar.removeClass('hide');
                        $actionToolbar.$reply.hide();
                        $actionToolbar.$forward.hide();
                    }
                    if (_this.messageLocation === window.$wrtc.enums.messageLocation.trash) {
                        $actionToolbar.$restore.show();
                    } else {
                        $actionToolbar.$restore.hide();
                    }
                };
                $actionToolbar.$allActions = $actionToolbar.find('[data-action]');
                $actionToolbar.$allActions.on('click', function(event) {
                    var $target = $(event.currentTarget),
                        actionAttr = $target.attr('data-action'),
                        action, requests = [],
                        $checkboxes = $(),
                        confirmText = '',
                        callbackCountReal = 0, callbackCountToBe = 0,
                        checkCallbackCount = function() {
                            if (callbackCountReal >= callbackCountToBe) {

                                $actionToolbar.display();
                                $selectFilter.$parentSelect.toggle();
                                $search.val('');

                                _this.prepareData();
                                _this.reloadList();
                            }
                        },
                        id, foundMessage, options = {};
                    if (actionAttr && actionAttr.indexOf('action') > -1) {

                        action = actionAttr.substring(actionAttr.indexOf(':') + 1);

                        switch (action) {
                            case 'reply':
                                id = $($messageList.getAllCheckboxes(true, $search.val())).closest('.message-item').attr('data-id');
                                foundMessage = messageManager.getById(id);
                                options.id = id;
                                options.message = foundMessage;
                                options.action = action;
                                _this.setupMessageEditing(options);
                                break;
                            case 'forward':
                                id = $($messageList.getAllCheckboxes(true, $search.val())).closest('.message-item').attr('data-id');
                                foundMessage = messageManager.getById(id);
                                options.id = id;
                                options.message = foundMessage;
                                options.action = action;
                                _this.setupMessageEditing(options);
                                break;
                            case 'markread':
                                $checkboxes = $messageList.getAllCheckboxes(true, $search.val());
                                callbackCountReal = 0;
                                callbackCountToBe = $checkboxes.length;
                                $.each($checkboxes, function() {
                                    var $this = $(this),
                                        $message = $this.closest('.message-item'),
                                        id = $message.attr('data-id'),
                                        foundMessage = messageManager.getById(id);

                                    if (foundMessage) {
                                        requests.push(
                                            function() {
                                                foundMessage.isRead = true;
                                                var onSuccessSave = function() {
                                                    callbackCountReal++;
                                                    checkCallbackCount();
                                                };
                                                var onFailureSave = function() {
                                                    callbackCountReal++;
                                                    foundMessage.isRead = false; // restore message state
                                                    $.gritter.add({
                                                        text: 'Error update message:: ' + foundMessage.text,
                                                        class_name: 'gritter-error gritter-center'
                                                    });
                                                    checkCallbackCount();
                                                };
                                                return foundMessage.save(onSuccessSave, onFailureSave);
                                            }
                                        );
                                    }
                                });

                                if (requests.length) {
                                    _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                                        $.waitFor($.pipeline(requests));
                                    });
                                }
                                break;
                            case 'markunread':
                                $checkboxes = $messageList.getAllCheckboxes(true, $search.val());
                                callbackCountReal = 0;
                                callbackCountToBe = $checkboxes.length;
                                $.each($checkboxes, function() {
                                    var $this = $(this),
                                        $message = $this.closest('.message-item'),
                                        id = $message.attr('data-id'),
                                        foundMessage = messageManager.getById(id);

                                    if (foundMessage) {
                                        requests.push(
                                            function() {
                                                foundMessage.isRead = false;
                                                var onSuccessSave = function() {
                                                    callbackCountReal++;
                                                    checkCallbackCount();
                                                };
                                                var onFailureSave = function() {
                                                    callbackCountReal++;
                                                    foundMessage.isRead = true; // restore message state
                                                    $.gritter.add({
                                                        text: 'Error update message:: ' + foundMessage.text,
                                                        class_name: 'gritter-error gritter-center'
                                                    });
                                                    checkCallbackCount();
                                                };
                                                return foundMessage.save(onSuccessSave, onFailureSave);
                                            }
                                        );
                                    }
                                });

                                if (requests.length) {
                                    _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                                        $.waitFor($.pipeline(requests));
                                    });
                                }
                                break;
                            case 'restore':
                                $checkboxes = $messageList.getAllCheckboxes(true, $search.val());
                                callbackCountReal = 0;
                                callbackCountToBe = $checkboxes.length;
                                $.each($checkboxes, function() {
                                    var $this = $(this),
                                        $message = $this.closest('.message-item'),
                                        id = $message.attr('data-id'),
                                        foundMessage = messageManager.getById(id);

                                    if (foundMessage) {
                                        requests.push(
                                            function() {
                                                foundMessage.isMarkForDelete = false;
                                                var onSuccessSave = function() {
                                                    callbackCountReal++;
                                                    checkCallbackCount();
                                                };
                                                var onFailureSave = function() {
                                                    callbackCountReal++;
                                                    foundMessage.isMarkForDelete = true; // restore message state
                                                    $.gritter.add({
                                                        text: 'Error restore message:: ' + foundMessage.text,
                                                        class_name: 'gritter-error gritter-center'
                                                    });
                                                    checkCallbackCount();
                                                };
                                                return foundMessage.save(onSuccessSave, onFailureSave);
                                            }
                                        );
                                    }
                                });

                                if (requests.length) {
                                    _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                                        $.waitFor($.pipeline(requests));
                                    });
                                }
                                break;
                            case 'delete':
                                $checkboxes = $messageList.getAllCheckboxes(true);
                                callbackCountReal = 0;
                                callbackCountToBe = $checkboxes.length;
                                if ($checkboxes.length) {
                                    if (_this.messageLocation !== window.$wrtc.enums.messageLocation.trash) {
                                        confirmText = "Are you sure you want to delete " + ($checkboxes.length > 1 ? 'these' : 'this') + " " + ($checkboxes.length > 1 ? 'messages' : 'message') + "?";
                                    } else {
                                        confirmText = "Are you sure you want to permanently delete " + ($checkboxes.length > 1 ? 'these' : 'this') + " " + ($checkboxes.length > 1 ? 'messages' : 'message') + "?";
                                    }
                                    bootbox.confirm(confirmText, function(result) {
                                        if (result) {
                                            _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                                                $.each($checkboxes, function() {
                                                    var $this = $(this),
                                                        $message = $this.closest('.message-item'),
                                                        id = $message.attr('data-id'),
                                                        foundMessage = messageManager.getById(id);

                                                    if (foundMessage) {
                                                        requests.push(
                                                            function() {
                                                                var onSuccessDelete = function() {
                                                                    callbackCountReal++;
                                                                    checkCallbackCount();
                                                                };
                                                                var onFailureDelete = function() {
                                                                    callbackCountReal++;
                                                                    if (_this.messageLocation !== window.$wrtc.enums.messageLocation.trash) {
                                                                        foundMessage.isMarkForDelete = false; // restore message state
                                                                    }
                                                                    $.gritter.add({
                                                                        text: 'Error restore message:: ' + foundMessage.text,
                                                                        class_name: 'gritter-error gritter-center'
                                                                    });
                                                                    checkCallbackCount();
                                                                };
                                                                // hard or soft delete => define Arcus
                                                                return messageManager.deleteMessageRequest(foundMessage, onSuccessDelete, onFailureDelete);
                                                            }
                                                        );
                                                    }
                                                });
                                                $.waitFor($.pipeline(requests));
                                            });
                                        }
                                    });
                                }
                                break;
                        }

                    }
                });

                // from bootstrap to add handler on tab-switch
                $messagesView.find('.nav-tabs a[data-toggle]').on('shown.bs.tab', function(event) {
                    if (event.type === 'shown') {
                        var $target = $(event.delegateTarget),
                            targetAttr = $target.attr('data-target');

                        $selectFilter.$parentSelect.toggle();
                        $search.val('');
                        $actionToolbar.display();

                        _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                            _this.messageLocation = targetAttr;

                            _this.prepareData(targetAttr);

                            _this.renderMessageList({
                                renderData: {
                                    inbox: _this.inbox,
                                    sent: _this.sent,
                                    trash: _this.trash,
                                    draft: _this.draft,
                                    messageLocation: _this.messageLocation
                                }
                            });

                            $search.attr('placeholder', 'Search ' + targetAttr + ' ...');
                        });
                    }
                });

                //hide message list and display new message form
                $messagesView.find('.btn-new-mail').on('click', function() {
                    _this.setupMessageEditing({
                        message: {
                            artifactType: 'SMS'
                        }
                    });
                });

                $messagesView.find('.dropdown-new-message li').on('click', function(event) {
                    var $el = $(event.target).parent();
                    var messageType = ($el) ? $el.data('value') : null;
                    _this.setupMessageEditing({
                        message: {
                            artifactType: messageType
                        }
                    });
                });

                eventEmitter.off(messageManager.events.RECORD_ADDED_REMOTELY).on(messageManager.events.RECORD_ADDED_REMOTELY, function() {
                    if (_this.STATE !== 'BUSY') {
                        _this.reloadList();
                    }
                });
                eventEmitter.off(messageManager.events.RECORD_DELETED_REMOTELY).on(messageManager.events.RECORD_DELETED_REMOTELY, function() {
                    if (_this.STATE !== 'BUSY') {
                        _this.reloadList();
                    }
                });
                eventEmitter.off(messageManager.events.RECORD_DELETED_REMOTELY).on(messageManager.events.RECORD_UPDATED_REMOTELY, function() {
                    if (_this.STATE !== 'BUSY') {
                        _this.reloadList();
                    }
                });
            },
/* // TODO if perfomance need + handle with wrapped messages
            changeList : function(action, id, callbackOnMessage) {
                if (action) {
                    var $message = _this.$messageLayout.find('.message-item[data-id="' + id + '"]');
                    if ($message.length && callbackOnMessage && typeof callbackOnMessage === 'function') {
                        callbackOnMessage($message);
                    }
                    switch (action) {
                        case 'deleted' :
                            $message.remove();
                            break;
                        case 'updated' :
                            $message.remove();
                            break;
                    }
                }
                // if message-detail window is opened => refresh it with new data or close

                _this.updateCounters({
                    inbox: {
                        total: messageManager.countMessages(_this.inbox),
                        unread: messageManager.countUnreadMessages(_this.inbox)
                    },
                    sent: {
                        total: messageManager.countMessages(_this.sent),
                        unread: ''
                    },
                    trash: {
                        total: messageManager.countMessages(_this.trash),
                        unread: messageManager.countUnreadMessages(_this.trash)
                    }
                });
            },
*/
            reloadList : function () {
                _this.showLoadingSpinner(_this.$messageLayout).done(function() {
                    _this.prepareData();
                    _this.renderMessageList({
                        renderData: {
                            inbox: _this.inbox,
                            sent: _this.sent,
                            trash: _this.trash,
                            draft: _this.draft,
                            messageLocation: _this.messageLocation
                        }
                    });
                });
            },

            prepareData : function (targetToPrepare) {
                if (targetToPrepare) {
                    switch (targetToPrepare) {
                        case window.$wrtc.enums.messageLocation.inbox:
                            _this.inbox = messageManager.getMessagesByArtifactType(messageManager.data, _this.artifactTypes);
                            _this.inbox = messageManager.getMessagesByLocation(_this.inbox, window.$wrtc.enums.messageLocation.inbox);
                            if (_this.groupState) {
                                _this.inbox = messageManager.groupMessagesSentBy(_this.inbox, _this.sortBy);
                            } else {
                                _this.inbox = messageManager.getMessages(_this.inbox, _this.sortBy);
                            }
                            break;
                        case window.$wrtc.enums.messageLocation.sent:
                            _this.sent = messageManager.getMessagesByArtifactType(messageManager.data, _this.artifactTypes);
                            _this.sent = messageManager.getMessagesByLocation(_this.sent, window.$wrtc.enums.messageLocation.sent);
                            _this.sent = messageManager.getMessages(_this.sent, _this.sortBy);
                            break;
                        case window.$wrtc.enums.messageLocation.trash:
                            _this.trash = messageManager.getMessagesByArtifactType(messageManager.data, _this.artifactTypes);
                            _this.trash = messageManager.getMessagesByLocation(_this.trash, window.$wrtc.enums.messageLocation.trash);
                            _this.trash = messageManager.getMessages(_this.trash, _this.sortBy);
                            break;
                        case window.$wrtc.enums.messageLocation.draft:
                            _this.draft = messageManager.getMessagesByLocation(messageManager.data, window.$wrtc.enums.messageLocation.draft);
                            _this.draft = messageManager.getMessages(_this.draft, _this.sortBy);
                            break;
                    }
                } else {
                    _this.inbox = messageManager.getMessagesByArtifactType(messageManager.data, _this.artifactTypes);
                    _this.inbox = messageManager.getMessagesByLocation(_this.inbox, window.$wrtc.enums.messageLocation.inbox);
                    if (_this.groupState) {
                        _this.inbox = messageManager.groupMessagesSentBy(_this.inbox, _this.sortBy);
                    } else {
                        _this.inbox = messageManager.getMessages(_this.inbox, _this.sortBy);
                    }

                    _this.sent = messageManager.getMessagesByArtifactType(messageManager.data, _this.artifactTypes);
                    _this.sent = messageManager.getMessagesByLocation(_this.sent, window.$wrtc.enums.messageLocation.sent);
                    _this.sent = messageManager.getMessages(_this.sent, _this.sortBy);

                    _this.trash = messageManager.getMessagesByArtifactType(messageManager.data, _this.artifactTypes);
                    _this.trash = messageManager.getMessagesByLocation(_this.trash, window.$wrtc.enums.messageLocation.trash);
                    _this.trash = messageManager.getMessages(_this.trash, _this.sortBy);

                    _this.draft = messageManager.getMessagesByLocation(messageManager.data, window.$wrtc.enums.messageLocation.draft);
                    _this.draft = messageManager.getMessages(_this.draft, _this.sortBy);
                }
            },

            validateTags: function(tagsInput, validator, messageType) {

                var items, i, isValid = true,
                    tagFilterFunc = function() { return $(this).data('item') === items[i]; };

                messageType = messageType.toLowerCase();



                if(tagsInput) {

                    items = tagsInput.items();
                    if(items.length > 0) {

                        for(i = 0; i < items.length; i++) {
                            var tag = $('.tag', tagsInput.$container).filter(tagFilterFunc);

                            if(messageType === 'email') {

                                if(items[i].methodType !== 'EMAIL' || !validations.isValidEmail(items[i].methodValue)) {
                                    tag.removeClass('label-info').addClass('label-danger');
                                    isValid = false;
                                }
                                else {
                                    tag.removeClass('label-danger').addClass('label-info');
                                }
                            }
                            else if (messageType === 'sms') {

                                if(items[i].methodType !== 'PHONE' || !validations.isValidPhone(items[i].methodValue)) {
                                    tag.removeClass('label-info').addClass('label-danger');
                                    isValid = false;
                                }
                                else {
                                    tag.removeClass('label-danger').addClass('label-info');
                                }
                            }
                        }

                    }
                }

                return isValid;
            }
        };
    };
    return messageController().init();
});
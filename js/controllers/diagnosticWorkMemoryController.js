/**
 * Created by aa86862 on 6/2/2014.
 */
define('diagnosticWorkMemoryController',['Arcus'], function(arcus){

    var diagnosticWorkMemoryController =  function() {
        return{

        init : function(){
                var _this = this;
                return _this;
            },
            checkStatus : function(){
                var dataContent = "Service";
                if(arcus.diagnosticStatus.SMSMessageService===false||arcus.diagnosticStatus.MetaDataStorageService===false){
                    $("#diagnosticsUIMessage button").addClass('btn-red');
                    $("#diagnosticsUIMessage").click(function(){
                        $.gritter.add({
                            title:'WARNING',
                            text: dataContent+' is not configured',
                            class_name: 'gritter-red gritter-left'
                        });
                    });
                }
                else{
                    $("#diagnosticsUIMessage button").removeClass('btn-red');
                }
                if(arcus.diagnosticStatus.MMSMessageService===false){
                    dataContent = "Activity Service ";

                    $("#diagnosticsUIActivity button").addClass('btn-red');
                    $("#diagnosticsUIActivity button").click(function(){
                        $.gritter.add({
                            title:'WARNING',
                            text: dataContent+' is not configured',
                            class_name: 'gritter-error gritter-left'
                        });
                    });
                }
                else{
                    $("#diagnosticsUIActivity").removeClass('btn-red');
                }
            }

        };
    };
    return diagnosticWorkMemoryController().init();
});
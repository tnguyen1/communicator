define('callLogsController',
    [
        'template',
        'jquery',
        'Arcus',
        'config',
        'jqgrid'
    ],
    function (template, $, Arcus, config) {
        var CallLogsController = function () {
            this.loaded = false;
            this.resize = this.resize.bind(this);

            template.registerPartial('calllogs/calllogs');
            template.registerPartial('calllogs/calllogsFail');
        };

        CallLogsController.prototype = {
            title: "Call Logs",
            /**
             * Build main view of the settings view
             * @param  {Object} options
             */
            buildMainView: function(options) {
                if (!this.loaded) {
                    Arcus.CallLogs.retrieveCallLogs(config.config.ARCUS.CallLogs.maxLogs,
                        function success(data) {
                            this.loaded = true;
                            this.callLogCollection = data;
                            this.buildMainView(options);
                        }.bind(this),
                        function error(errMsg) {
                            var $view = $(template.buildTemplate('calllogs/calllogsFail', {
                                message: errMsg
                            }));
                            if (options.success) options.success($view);
                        }.bind(this));
                } else {
                    var logsPresentation = this.getCallLogPresentation();
                    var logsSortedByTime = this.sortLogsByTime(logsPresentation);

                    var $view = $(template.buildTemplate('calllogs/calllogs', {}));
                    if (options.success) options.success($view);
                    this.setupGrid(logsSortedByTime);
                }
            },

            resize: function() {
            },

            /**
             * builds table using jgGrid plugin
             * @param logsSortedByTime logsPresentations sorted by time
             */
            setupGrid: function (logsSortedByTime) {
                var $grid = $("#grid-table");
                if (!$grid) {
                    setTimeout(function() {
                        this.setupGrid(logsSortedByTime);
                    }, 100).bind(this);
                    return;
                }

                var height = $(window).height() - $("#navbar").height() - 130;

                $grid.jqGrid({
                    datatype: "local",
                    autowidth: true,
                    height: height,
                    sortname:"formattedTime",
                    colNames: ['Number', 'Duration', 'Time', 'Direction', 'Status'],
                    colModel: [
                        {name: 'callWith', index: 'callWith', sorttype: "string"},
                        {name: 'formattedDuration', index: 'formattedDuration', sorttype: "string"},
                        {name: 'formattedTime', index: 'formattedTime', sorttype: function(time){
                            for(var i = 0; i < logsSortedByTime.length; i++){
                                if(logsSortedByTime[i].formattedTime === time){
                                    return i;
                                }
                            }
                        }},
                        {name: 'direction', index: 'direction', sorttype: "string"},
                        {name: 'status', index: 'status', sorttype: "string"}
                    ],
                    rowNum: logsSortedByTime.length
                });

                for (var i = 0; i <= logsSortedByTime.length; i++){
                    $grid.jqGrid('addRowData', i + 1, logsSortedByTime[i]);
                }
            },

            /**
             * Generates additional parameters for the view that are based on data in the call log models
             * @returns CallLog[]
             */
            getCallLogPresentation: function () {
                var logPresentations = $.map(this.callLogCollection.logs, function (log) {
                    var logClone = $.extend({}, log); //clone to avoid mangling data.
                    logClone.formattedTime = this.formatTime(log.time);
                    if (log.callingNo === this.customerNo) {
                        logClone.callWith = log.calledNo;
                    } else if (log.calledNo === this.customerNo) {
                        logClone.callWith = log.callingNo;
                    } else {
                        logClone.callWith = log.callingNo + " >> " + log.calledNo;
                    }
                    logClone.formattedDuration = this.formatDuration(logClone.duration);

                    return logClone;
                }.bind(this));

                return logPresentations;
            },

            /**
             * sort logs based on time
             * @param callLogs
             * @returns {*}
             */
            sortLogsByTime: function (callLogs) {
                var sorted = callLogs.sort(function (a, b) {
                    return ((a.time > b.time) ? -1 : ((a.time < b.time) ? 1 : 0));
                });
                return sorted;
            },

            /**
             * Format date into am/pm format
             * @param {Date} date
             * @returns {string}
             */
            formatTime: function (date) {
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0' + minutes : minutes;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                return date.toLocaleDateString() + " - " + strTime;
            },

            /**
             * Format duration into hours, minutes, seconds
             * @param duration
             */
            formatDuration: function (duration) {
                var hours = Math.floor(duration / 3600);
                var mins = Math.floor(duration / 60 % 60);
                var seconds = Math.floor(duration % 60);

                return ("00" + hours).slice(-2) + ":" + ("00" + mins).slice(-2) + ":" + ("00" + seconds).slice(-2);
            }
        };

        return new CallLogsController();
    });
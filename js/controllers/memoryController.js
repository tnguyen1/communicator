define('memoryController',
            ['logger', 'template', 'memoryManager', 'eventEmitter', 'bootbox', /* hidden use => */ 'photobox'],
    function(logger, template, memoryManager, eventEmitter, bootbox) {
    var memoryController = function () {
        var _this;

        return {

            title: "Memories",

            init: function () {
                _this = this;

                _this.scrolls = {};
                _this.minLoadingTimeElapsed = false;
                _this.minLoadingTime = 600; // used for "perceived working" - avoids choppy screen flashes
                _this.pageLoadingDone = false;

                _this.isEdit = false;

                template.registerPartial('memory/gallery-item');

                return _this;
            },

            buildMainView: function(options) {

                _this.pageLoadingDone = false;
                _this.minLoadingTimeElapsed = false;

                if (options.success) options.success(template.buildTemplate('memory/gallery', {}));

                setTimeout(function() {
                    _this.minLoadingTimeElapsed = true;
                    if(_this.pageLoadingDone) {
                        _this.hideLoading();
                    }

                }, _this.minLoadingTime);

                _this.showLoading(true);

                memoryManager.load().done(function() {

                    var data = {
                        memories: memoryManager.filterMemories("Pic")
                    };

                    var $galleryView = $(template.buildTemplate('memory/gallery', data));

                    _this.setupHandlers($galleryView);

                    if (options.success) options.success($galleryView);

                    _this.pageLoadingDone = true;
                    if(_this.minLoadingTimeElapsed) {
                        _this.hideLoading();
                    }
                }).fail(function() {
                    var $sidebar = $('#memories-gallery');
                    var errorHTML = '<div class="bigger-110"><div class="alert alert-danger">Error loading memories!</div></div>';
                    $sidebar.html(errorHTML);
                    _this.hideLoading();
                });
            },

            setupHandlers: function($galleryView) {
                eventEmitter.off(eventEmitter.Event.ROUTER_AFTER_CHANGE, _this.routerSubscriber);
                eventEmitter.on(eventEmitter.Event.ROUTER_AFTER_CHANGE, _this.routerSubscriber);

                $galleryView.find('#memories-gallery').photobox('a', {
                    thumbs: true,
                    history: false,
                    loop: false
                });

                $($galleryView).find('img.memory-image').off('load').on('load', function() {
                    var $wrapper = $(this).closest('li');
                    $wrapper.addClass('loaded');
                    $.adjustImage($wrapper, true, true);
                });

                $($galleryView).find('.btn-upload-memory').click(function() {
                    _this.setupMemoryUpload();
                });

                $($galleryView).find('.btn-delete-memory').click(function() {
                    _this.deleteMemories();
                });

                $($galleryView).find('.btn-edit-memory').click(function() {
                    _this.toggleEditMode();
                });

                $($galleryView).find('.memory-item a').off('click').on('click', _this.memoryItemEditClickHandler);
            },
            memoryItemEditClickHandler: function(e) {
                if(_this.isEdit) {
                    _this.toggleMemoryCheck($(this).parent());
                    e.stopPropagation();
                    e.preventDefault();
                }
            },

            routerSubscriber: function(event, data) {
                if (data.routeName && data.routeName !== data.routing.pages.MEMORIES) {
                    eventEmitter.off(eventEmitter.Event.ROUTER_AFTER_CHANGE, _this.routerSubscriber);
                    _this.isEdit = false;
                }
            },

            deleteMemories: function() {
                var checkedMemoryIds = $('#memories-gallery').find('li.checked').map(function() {
                    return $(this).data('id');
                });

                if(checkedMemoryIds.length > 0) {
                    bootbox.confirm('Are you sure you want to remove selected memor' + (checkedMemoryIds.length > 1 ? 'ies': 'y') + '?', function(result) {
                        if (result) {
                            //_this.showLoading(true);

                            memoryManager.deleteMemories(checkedMemoryIds, function(deletedMemoryId) {
                                $('#memories-gallery').find('li.checked[data-id="'+deletedMemoryId+'"]').remove();

                                _this.toggleDeleteButton();
                            });
                        }
                    });
                }
            },

            toggleEditMode: function() {

                _this.isEdit = !_this.isEdit;

                if(_this.isEdit) {

                    $('.btn-edit-memory').attr('title', 'Done Editing Memory');
                    $('.btn-edit-memory span').text('Done');
                    $('.btn-edit-memory i').removeClass('icon-edit').addClass('icon-ok');
                }
                else {
                    $('#memories-gallery').find('li').removeClass('checked');

                    $('.btn-edit-memory').attr('title', 'Edit Memory');
                    $('.btn-edit-memory span').text('Edit');
                    $('.btn-edit-memory i').removeClass('icon-ok').addClass('icon-edit');

                    _this.toggleDeleteButton();
                }

                $('#memories-gallery').toggleClass('edit');
            },

            toggleMemoryCheck: function(memoryItem) {

                memoryItem.toggleClass('checked');
                _this.toggleDeleteButton();
            },

            toggleDeleteButton: function() {

                var checkedMemories = $('#memories-gallery').find('li.checked');
                if(checkedMemories.length > 0) {
                    $('.btn-delete-memory').show();
                }
                else {
                    $('.btn-delete-memory').hide();
                }
            },

            showLocalOverlay: function(done) {
                var secondaryOverlay = $('#main-container .widget-box-overlay-secondary');
                secondaryOverlay.show();
                secondaryOverlay.on('click', function(e){
                    e.preventDefault();
                    secondaryOverlay.off('click');
                    done();
                });
            },

            hideLocalOverlay: function() {
                $('#main-container .widget-box-overlay-secondary').hide();
            },

            setupMemoryUpload: function() {

                var memoryUploadHTML = template.buildTemplate('memory/memory-upload', {}),
                    memoryUploadContainer = $('.memory-upload-container');

                memoryUploadContainer.html(memoryUploadHTML);

                var uploadMemoryButton = memoryUploadContainer.find('.btn-upload-memory');
                uploadMemoryButton.removeClass('green').addClass('disabled light-grey');

                _this.transitionDetailIn(memoryUploadContainer).done(function() {
                    _this.showLocalOverlay(function(){
                        _this.transitionDetailOut(memoryUploadContainer);
                    });
                });

                memoryUploadContainer.off('click', '.btn-back-message-list').on('click', '.btn-back-message-list', function(e) {
                    e.preventDefault();
                    _this.transitionDetailOut(memoryUploadContainer);
                });

                $('#file-input-image').ace_file_input({
                    no_file: 'No File ...',
                    btn_choose: 'Choose',
                    btn_change: 'Change',
                    droppable: true,
                    thumbnail: true,
                    before_change: function(files) {
                        var allowed_files = []
                            , reader = new FileReader()
                            , onloadend = function(e) {

                                _this.mediaInfo = {};

                                _this.mediaInfo.size = file.size;
                                _this.mediaInfo.type = file.type;
                                _this.mediaInfo.name = file.name;

                                //if image show image preview
                                if(_this.mediaInfo.type.indexOf('image') > -1 && typeof e.target.result === "string") {
                                    $('#image-preview').attr('src', e.target.result);
                                    //adjustImage($image);
                                }

                                if(typeof e.target.result === "string") {
                                    _this.base64Img = e.target.result;
                                    reader.readAsArrayBuffer(file);
                                }
                                else {
                                    _this.mediaInfo.mediaData = e.target.result;
                                }
                            };

                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            if (typeof file === "string") {
                                //IE8 and browsers that don't support File Object
                                if (!(/\.(jpe?g|png|gif|bmp|mp3|mp4)$/i).test(file)) return false;
                            }
                            else {
                                var type = $.trim(file.type);
                                if (( type.length > 0 && !(/^image\/(jpe?g|png|gif|bmp|mp3|mp4)$/i).test(type) )
                                    || ( type.length === 0 && !(/\.(jpe?g|png|gif|bmp|mp3|mp4)$/i).test(file.name) )//for android's default browser which gives an empty string for file.type
                                    ) continue;//not an image so don't keep this file
                            }

                            allowed_files.push(file);
                            reader.onloadend = onloadend;

                            reader.readAsDataURL(file);

                        }
                        if (allowed_files.length === 0) return false;

                        uploadMemoryButton.addClass('green').removeClass('disabled light-grey');
                        return allowed_files;
                    },
                    before_remove: function() {
                        $('#image-preview').attr('src', '');
                        uploadMemoryButton.removeClass('green').addClass('disabled light-grey');
                        delete _this.mediaInfo;
                        return true;
                    }
                });

                memoryUploadContainer.find('.btn-upload-memory').click(function() {
                    if (!_this.mediaInfo) {
                        return;
                    }

                    var description = memoryUploadContainer.find('#form-field-description').val();

                    _this.showLoading(true);

                    memoryManager.uploadMemory(description, _this.mediaInfo).done(function(memory) {

                        //add memory to page
                        template.prependTemplate("memory/gallery-item", memory, "memories-gallery");
                        var $memoriesGallery = $('#memories-gallery');
                        $memoriesGallery.photobox('a', {
                            thumbs: true,
                            history: false
                        });

                        $memoriesGallery.find('img.memory-image').off('load').on('load', function() {
                            var $wrapper = $(this).closest('li');
                            $wrapper.addClass('loaded');
                            $.adjustImage($wrapper, true, true);
                        });

                        $memoriesGallery.find('.memory-item a').off('click').on('click', _this.memoryItemEditClickHandler);

                        _this.hideLoading();
                        delete _this.mediaInfo;
                        uploadMemoryButton.addClass('disabled');
                        _this.transitionDetailOut(memoryUploadContainer);

                    }).fail(function() {
                        _this.hideLoadingSpinner(memoryUploadContainer);
                    });
                });
            },

            transitionDetailIn: function(_$selector) {
                var $deferred = $.Deferred();

                _$selector.removeClass('off-tr');
                _$selector.addClass('on-tr');

                _$selector.addClass('on-scr');
                _$selector.removeClass('off-scr');
                
                _this.initScroll('memory-sidebar', $('#memory-upload-scroll'));
                _this.resizeScroll();
                eventEmitter.on(eventEmitter.Event.VIEW_RESIZE, function() {
                    _this.resizeScroll();
                });

                $deferred.resolve();

                return $deferred;
            },

            transitionDetailOut: function(_$selector) {
                _$selector.removeClass('off-tr');
                _$selector.addClass('on-tr');

                _$selector.addClass('off-scr');
                _$selector.removeClass('on-scr');
                _this.hideLocalOverlay();

                eventEmitter.off(eventEmitter.Event.VIEW_RESIZE, function() {
                    _this.resizeScroll();
                });
            },

            /*
            * Resize init for memory upload view to fit the screen height
            * @param  {String} scrollName - name of the scroll to store
            * @param  {jQuery function} scrollableElement - element to attach scroll
            * @param  {jQuery function} scrollViewport - element to set height. If undefined - search element '.scroll-viewport' inside scrollableElement 
            */
            initScroll: function(scrollName, scrollableElement, scrollViewport) {
                _this.scrolls[scrollName] = {
                    scrollName: scrollName,
                    $scrollableElement: $(scrollableElement),
                    $scrollViewport: scrollViewport ? $(scrollViewport) : $(scrollableElement).closest('.scroll-viewport'),
                    resize: function() {
                        var elem = _this.scrolls[scrollName]
                            , $this = elem.$scrollViewport
                            , offsetTop = $this.offset().top
                            , pageHeight = window.$wrtc.getPageDimensions().height
                            , resultHeight = pageHeight - offsetTop - 15
                            ;
                        $this.height(resultHeight);
                        return resultHeight;
                    }
                };
            },

            /*
            * Resize memory upload view to fit the screen height
            * @param  {String} scrollName - name of scroll to resize. If undefined - resize all scrolls
            * @param  {NUmber} _height - height to set for current scroll
            */
            resizeScroll: function(scrollName, _height) {
                if (scrollName) {
                    return _this.scrolls[scrollName].resize(_height);
                } else {
                    for (var key in _this.scrolls) {
                        if (_this.scrolls.hasOwnProperty(key)) {
                            _this.scrolls[key].resize();
                        }
                    }
                }
            },

            showLoadingSpinner: function($element) {
                var deferred = $.Deferred();

                $.blockUI.defaults.css.cursor = 'default';

                $element.block({
                    baseZ: 16,
                    css: {
                        border: 'none',
                        backgroundColor: 'transparent',
                        cursor: 'auto'
                    },
                    overlayCSS: {
                        backgroundColor: 'white',
                        opacity: 0.7,
                        cursor: 'auto'
                    },
                    onBlock: function() {
                        // when element is blocked
                        deferred.resolve();
                    },
                    message: '<i class="icon-spin icon-spinner icon-4x green"></i>'
                });

                return deferred;
            },

            hideLoadingSpinner: function($element) {
                $element.unblock();
            },

            //TODO: temp solution... need to removed this when minimum page load time will handle in one place
            showLoading: function(showSpinner) {
                // show or hide spinner depending
                var spinner = $('#main-container .widget-box-overlay .icon-spinner');

                if (showSpinner) {
                    spinner.show();
                }
                else {
                    spinner.hide();
                }

                // always show the overlay
                $('#main-container .widget-box-overlay').show();
            },

            hideLoading: function() {
                $('#main-container .widget-box-overlay').hide();
            }
        };
    };

    return memoryController().init();
});

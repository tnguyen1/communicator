define('contactController',
    ['logger', 'template', 'chatController', 'contactManager', 'groupManager', 'callController', 'list', 'eventEmitter', 'validations', 'base64Binary', 'messageController', 'bootbox', 'Arcus', /* hidden use => */ 'aceEdit', 'aceElements', 'jqueryvalidate', 'Jcrop', 'picker', 'pickadateDate',  'lib/vcard/vcard-utils'],
    function(logger, template, chatController, contactManager, groupManager, callController, list, eventEmitter, validations, base64Binary, messageController, bootbox, Arcus) {
        var contactController = function() {
            var _this;
            return {
                init: function() {
                    _this = this;
                    _this.scrolls = {};

                    template.registerPartial('contact/contact-group-list-item');
                    template.registerPartial('contact/contact-list-item');
                    template.registerPartial('contact/contact-phone-item');
                    template.registerPartial('contact/contact-email-item');
                    template.registerPartial('contact/contact-address-item');
                    template.registerPartial('contact/contact-imaddress-item');
                    template.registerPartial('contact/contact-note-item');
                    template.registerPartial('chat/conversation-list-item');
                    template.registerPartial('chat/conversation-list');

                    _this.minLoadingTime = 600; // used for "perceived working" - avoids choppy screen flashes
                    _this.fadeInStartingOpacity = 0.35;
                    _this.contactsSortBy = 'firstName';
                    _this.groupsSortBy = 'name';

                    //bind event handlers
                    this.renderChatList = this.renderChatList.bind(this);
                    $(chatController).on('ActiveChatClosed', this.renderChatList);

                    return _this;
                },

                formatModelForView: function(contact){
                    //replace enums with user facing text
                    contact.phoneNumbers = $.map(contact.phoneNumbers, function(phoneNumber){
                        var userFacingText = phoneNumber.contactType.charAt(0).toUpperCase() +
                            phoneNumber.contactType.slice(1).toLowerCase();
                        phoneNumber.contactTypeText = userFacingText;


                        //format us telephone numbers
                        if(phoneNumber.value.length === 10){
                            phoneNumber.displayPhoneNumber = phoneNumber.value.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
                        }else{
                            phoneNumber.displayPhoneNumber = phoneNumber.value;
                        }

                        return phoneNumber;
                    });
                    contact.emailAddress = $.map(contact.emailAddress, function(email){
                        var userFacingText = email.contactLocation.charAt(0).toUpperCase() +
                            email.contactLocation.slice(1).toLowerCase();
                        email.contactTypeText = userFacingText;
                        return email;
                    });

                    contact.onlineChatNumbers = $.grep(contact.phoneNumbers, function(phoneNumber){
                        if(phoneNumber.presence){
                            return phoneNumber.presence.primaryStatus == "ONLINE";
                        }
                    });

                    return contact;
                },


                loadAndRenderContactData: function(options) {
                    if (options.routing) {
                        _this.routing = options.routing;
                    }

                    contactManager.load()
                        .done(function() {
                            groupManager.load()
                                .done(function() {
                                    _this.renderContactWidget(options);
                                })
                                .fail(function() {
                                    var $container = $('#sidebar');
                                    var errorHTML = '<div class="bigger-110"><div class="alert alert-danger">Error loading contacts!</div></div>';
                                    $container.html(errorHTML);
                                });
                        })
                        .fail(function() {
                            var $container = $('#sidebar');
                            var errorHTML = '<div class="bigger-110"><div class="alert alert-danger">Error loading contact groups!</div></div>';
                            $container.html(errorHTML);
                        });

                    eventEmitter.on(eventEmitter.Event.VIEW_RESIZE, function() {
                        _this.resizeScroll();
                    });
                },

                renderContactWidget: function(options) {
                    var contactsSorted, chatsData;

                    contactsSorted = contactManager.getContactsSorted(_this.contactsSortBy);

                    contactsSorted = $.map(contactsSorted, this.formatModelForView);

                    chatsData = _this.generateChatsData();

                    // get data and really setup the page
                    var data = {
                        contacts: contactsSorted,
                        contactGroups: groupManager.getContactGroupsSorted(_this.groupsSortBy),
                        chatSessions: chatsData
                    };

                    _this.renderContactsAndGroupsList(data);

                    // resize and setup resizing handler
                    _this.initScroll('contact-list', $('.contact-list-scroll'));
                    _this.initScroll('group-list', $('.group-list-scroll'));
                    _this.initScroll('chat-list', $('.chat-list-scroll'));

                    // groups-tab is hidden => set height for hidden groups-tab according to the visible contacts-tab
                    _this.resizeScroll('contact-list');

                    if (options.success) options.success();
                },

                setupContactImage: function(event) {
                    var contact = event.data.contact
                        , canceled = false
                        , $view = $(template.buildTemplate('contact/contact-image', contact))
                        , $viewWrapper = $('#sidebar-image-widget')
                        , $contactImage = event.data.$contactImage
                        , contactImage = contact ? ( contact.images.length ? contact.images[0] : null ) : null
                        , $cropWrapper = $view.find('.crop-checkbox')
                        , showHideCropWrapper = function(showOrHide) {
                            $cropWrapper.toggle(showOrHide);
                        }
                        , showLoadingSpinner = function($element) {
                            var deferred = $.Deferred();

                            $.blockUI.defaults.css.cursor = 'default';

                            $element.block({
                                baseZ: 16,
                                css: {
                                    border: 'none',
                                    backgroundColor: 'transparent',
                                    cursor: 'auto'
                                },
                                overlayCSS: {
                                    backgroundColor: 'white',
                                    opacity: 0.7,
                                    cursor: 'auto'
                                },
                                onBlock: function() {
                                    // when element is blocked
                                    deferred.resolve();
                                },
                                message: '<i class="icon-spinner icon-spin icon-2x green"></i>'
                            });

                            return deferred;
                        }
                        , hideLoadingSpinner = function($element) {
                            $element.unblock();
                        }
                        , $checkboxCrop = $cropWrapper.find('input[type=checkbox]')
                        , $cropPreview = $view.find('.js-image-crop-view')
                        , $image = $view.find('.image-preview-wrapper img')
                        , $previewImage = $view.find('.canvas-wrapper img')
                        , $canvas = $view.find('.canvas-wrapper canvas')
                        , canvas = $canvas[0]
                        , setupJCrop = function() {
                            var JCropAPI = $image.data('Jcrop');
                            if (JCropAPI) {
                                JCropAPI.destroy();
                            }
                            // calc real size
                            var $imageBox = $image.parent()
                                , opts , fromX, fromY, toX, toY, fakeImage = new Image();

                            fakeImage.src = _this.base64Img;

                            opts = {
                                bgColor: 'transparent',
                                boxWidth: $imageBox.width(),
                                boxHeight: $imageBox.height(),
                                minSize: [$.em(1), $.em(1)],
                                trueSize: [fakeImage.width, fakeImage.height],
                                aspectRatio: 1, // TODO for this option we have to have square viewport
                                onSelect: function(_cropData) {

                                    if (parseInt(_cropData.w, 10) > 0) {
                                        canvas.width = parseInt(_cropData.w, 10);
                                        canvas.height = parseInt(_cropData.h, 10);
                                        // Show image preview
                                        var context = canvas.getContext("2d");
                                        if (context) {
                                            context.drawImage(fakeImage, _cropData.x, _cropData.y, _cropData.w, _cropData.h, 0, 0, canvas.width, canvas.height);
                                            _this.croppedBase64Img = canvas.toDataURL();
                                            $previewImage.attr('src', _this.croppedBase64Img);
                                        }
                                    }
                                }
                            };
                            fromX = fakeImage.width / 4;
                            fromY = fakeImage.height / 4;
                            toX = fakeImage.width - fromX; // TODO if aspectRatio != 1
                            toY = fakeImage.height - fromY; // TODO if aspectRatio != 1
                            $image.Jcrop(opts, function() {
                                JCropAPI = this;
                                $image.data('Jcrop', JCropAPI);
                                JCropAPI.animateTo([ fromX, fromY, toX, toY]);
                                JCropAPI.setSelect([ fromX, fromY, toX, toY]);
                            });
                        }
                        , destroyJCrop = function() {
                            $checkboxCrop.removeAttr('checked');
                            var JCropAPI = $image.data('Jcrop');
                            if (JCropAPI) {
                                JCropAPI.destroy();
                            }
                            // clean canvas
                            canvas.width = canvas.width;
                            _this.croppedBase64Img = '';
                            $previewImage.attr('src', '');
                        }
                        , adjustImage = function($wrapper) {
                            var $box = $wrapper.is('img') ? $wrapper.parent() : $wrapper
                                , boxWidth = $box.width() // TODO calc padding too
                                , $image = $wrapper.is('img') ? $wrapper : $box.find('img'),
                                _adjustImage = function() {
                                    var fakeImage = new Image()
                                        , ratio = 1;
                                    fakeImage.src = $image.attr('src');
                                    if (fakeImage.width > 0 && fakeImage.width < boxWidth) {
                                        // extend image by height
                                        ratio = fakeImage.width / boxWidth;
                                        $image.css({
                                            height: ratio * fakeImage.height,
                                            width: ratio * fakeImage.width,
                                            maxWidth: ratio * fakeImage.width
                                        });
                                    } else {
                                        $image.removeAttr('style');
                                    }
                                }
                                ;
                            _adjustImage();
                        }
                        ;

                    $checkboxCrop.removeAttr('checked').on('change', function() {
                        if (this.checked) {
                            setupJCrop();
                            $cropPreview.removeClass('hide');
                        } else {
                            destroyJCrop();
                            $cropPreview.addClass('hide');
                        }
                    });

                    // set the widget content and then transition the view in
                    _this.transitionDetailIn($view, true, $viewWrapper);

                    // entry point
                    // prepare image

                    if (_this.contactImageHandler === window.$wrtc.enums.contactImageHandler.has) {
                        showLoadingSpinner($image.parent()).done(function() {
                            $.convertImgToBase64(contactImage.imageUrl, function(base64Img) {
                                if (!canceled) {
                                    $image.attr('src', base64Img);
                                    adjustImage($image);
                                    _this.base64Img = base64Img;
                                    showHideCropWrapper(true);
                                }

                                hideLoadingSpinner($image.parent());

                            }, contactImage.imageMIMEType);
                        });
                    } else if (_this.contactImageHandler === window.$wrtc.enums.contactImageHandler.none) {
                        $image.attr('src', '');
                        showHideCropWrapper(false);
                    } else {
                        var base64data = $contactImage.attr('src');
                        $image.attr('src', base64data);
                        adjustImage($image);
                        showHideCropWrapper(true);
                        if (base64data.indexOf('base64') > -1) {
                            _this.base64Img = $contactImage.attr('src');
                        }
                    }

                    $('#file-input-image').ace_file_input({
                        no_file: 'No File ...',
                        btn_choose: 'Choose',
                        btn_change: 'Change',
                        droppable: true,
                        thumbnail: true,
                        before_change: function(files) {
                            var allowed_files = []
                                , reader = new FileReader()
                                , onloadend = function(e) {
                                    destroyJCrop();
                                    $image.attr('src', e.target.result);
                                    adjustImage($image);
                                    _this.base64Img = e.target.result;
                                    showHideCropWrapper(true);
                                    _this.imageInfo.size = file.size;
                                    _this.imageInfo.type = file.type;
                                    _this.imageInfo.name = file.name;
                                }
                                ;
                            for (var i = 0; i < files.length; i++) {
                                var file = files[i];
                                if (typeof file === "string") {
                                    //IE8 and browsers that don't support File Object
                                    if (!(/\.(jpe?g|png|gif|bmp)$/i).test(file)) return false;
                                }
                                else {
                                    var type = $.trim(file.type);
                                    if (( type.length > 0 && !(/^image\/(jpe?g|png|gif|bmp)$/i).test(type) )
                                        || ( type.length === 0 && !(/\.(jpe?g|png|gif|bmp)$/i).test(file.name) )//for android's default browser which gives an empty string for file.type
                                        ) continue;//not an image so don't keep this file
                                }

                                allowed_files.push(file);
                                reader.onloadend = onloadend;
                                reader.readAsDataURL(file);
                                canceled = true;
                                hideLoadingSpinner($image.parent());
                            }
                            if (allowed_files.length === 0) return false;

                            return allowed_files;
                        },
                        before_remove: function() {
                            showHideCropWrapper(false);
                            $image.attr('src', '');
                            destroyJCrop();
                            return true;
                        }
                    });

                    $view.on('click', '.data-action', function() {
                        var val = $(this).attr('data-val');

                        switch (val) {
                            case 'ok' :
                                if (_this.croppedBase64Img && $checkboxCrop.is(':checked')) {
                                    $contactImage.attr('src', _this.croppedBase64Img);
                                    _this.imageInfo.imageRawData = base64Binary.decodeArrayBuffer(_this.croppedBase64Img.split(',')[1]);
                                    _this.imageInfo.size = _this.imageInfo.imageRawData.byteLength;
                                    _this.contactImageHandler = window.$wrtc.enums.contactImageHandler.new;
                                } else if (_this.base64Img) {
                                    $contactImage.attr('src', _this.base64Img);
                                    _this.imageInfo.imageRawData = base64Binary.decodeArrayBuffer(_this.base64Img.split(',')[1]);
                                    _this.imageInfo.size = _this.imageInfo.imageRawData.byteLength;
                                    _this.contactImageHandler = window.$wrtc.enums.contactImageHandler.new;
                                }
                                $('.remove-avatar').show();
                                _this.transitionDetailOut($viewWrapper);
                                break;
                            case 'cancel' :
                                _this.transitionDetailOut($viewWrapper);
                                break;
                        }
                    });

                    _this.initScroll('contact-image', $('.contact-image-scroll'));
                    _this.resizeScroll('contact-image');
                },

                initScroll: function(scrollName, scrollableElement, scrollViewport) {
                    _this.scrolls[scrollName] = {
                        scrollName: scrollName,
                        $scrollableElement: $(scrollableElement),
                        $scrollViewport: scrollViewport ? $(scrollViewport) : $(scrollableElement).closest('.scroll-viewport'),
                        resize: function() {
                            var elem = _this.scrolls[scrollName]
                                , $this = elem.$scrollViewport
                                , offsetTop = $this.offset().top
                                , pageHeight = window.$wrtc.getPageDimensions().height
                                , resultHeight = pageHeight - offsetTop - 15
                                ;
                            $this.height(resultHeight);
                            return resultHeight;
                        }
                    };
                },

                resizeScroll: function(scrollName, _height) {
                    if (scrollName) {
                        return _this.scrolls[scrollName].resize(_height);
                    } else {
                        for (var key in _this.scrolls) {
                            if (_this.scrolls.hasOwnProperty(key)) {
                                _this.scrolls[key].resize();
                            }
                        }
                    }
                },

                renderContactsAndGroupsList: function(data) {
                    // render sidebar - has contacts and contactGroups in the list
                    var content = template.buildTemplate('contact/contact-list', data);

                    // setup handlers
                    _this.setupMainHandlers();
                    _this.setupChatListListener();

                    // get sidebar
                    var $sidebar = $('#sidebar');

                    // set content
                    $sidebar.html(content);

                    // from bootstrap to add handler on tab-switch
                    $sidebar.find('.nav-tabs a').on('shown.bs.tab', function(event) {
                        if (event.type === 'shown') {
                            var $target = $(event.delegateTarget)
                                , targetAttr = $target.attr('data-target');
                            switch (targetAttr) {
                                case '#contacts-tab' :
                                    _this.resizeScroll('contact-list');
                                    break;
                                case '#groups-tab' :
                                    _this.resizeScroll('group-list');
                                    break;
                                case '#chats-tab' :
                                    _this.resizeScroll('chat-list');
                                    break;
                            }
                        }
                    });
                },

                generateChatsData: function() {
                    return $.map(Arcus.IMChat.imChatDictionaryInstance, function(imChatSession) {
                        var otherContact = imChatSession.receiverInfo;
                        if(otherContact){
                            otherContact.imageUrl = (otherContact.images && otherContact.images.length) ? otherContact.images[0].imageUrl: undefined;
                        }
                        return $.extend({
                            newMessageCount: imChatSession.getUnreadCount()
                        }, imChatSession);
                    });
                },

                setupNewChatEditing: function() {
                    var $view = $(template.buildTemplate('chat/new-chat-edit'));

                    _this.transitionDetailIn($view, true).done(function() {
                        _this.initScroll('new-chat-edit', $('.new-chat-edit-scroll'));
                        _this.resizeScroll('new-chat-edit');
                        $view.find('.focus-edit').focus();
                    });

                    // add handlers on toolbar
                    $view.find('.data-action').on('click', function() {
                        var val = $(this).attr('data-val');
                        switch (val) {
                            case 'newchat':
                                var $chatterUri = $('#chatterUri');
                                var chatterUri = $chatterUri.val();

                                chatController.displayChat(chatterUri, _this.routing);
                                break;
                            case 'cancel':
                                _this.showLoading();
                                _this.transitionEditingOut(null, 'chat');
                                break;
                        }
                    });

                    $view.find('#new-chat-form').submit(function() {
                        return false;
                    });

                },

                setupChatListListener: function() {
                    $(Arcus.IMChat.imChatDictionaryInstance).off(Arcus.EventsEnum.IMChatEventType.IMChatStart);
                    $(Arcus.IMChat.imChatDictionaryInstance).on(Arcus.EventsEnum.IMChatEventType.IMChatStart, this.renderChatList);
                    $(Arcus.IMChat.imChatDictionaryInstance).on(Arcus.EventsEnum.IMChatEventType.IMChatStart, function(){
                        ga('send', 'event', 'Chat', 'conversation started');
                    });
                    $(Arcus.IMChat.imChatDictionaryInstance).on(Arcus.EventsEnum.IMChatEventType.IMChatEnd, this.renderChatList);
                },

                renderChatList: function(){
                    var data = this.generateChatsData();
                    var conversationList = template.buildTemplate('chat/conversation-list', {chatSessions: data});
                    $("#chat-session-list").html(conversationList);
                    $("#chat-session-list-count").html(data.length);
                },

                setupMainHandlers: function() {
                    $(document).on('click', '.new-contact-request', function() {
                        _this.setupContactEditing();
                    });

                    $(document).on('click', '.new-contact-group-request', function() {
                        _this.setupContactGroupEditing();
                    });

                    $(document).on('click', '.new-chat-request', function() {
                        _this.setupNewChatEditing();
                    });

                    // add handler for contact details
                    $(document).on('click', '.contact-data-request-details', function() {
                        var id = $(this).closest('.list-item').attr('data-id');
                        _this.setupContactDetail(id, true);
                    });

                    // add handler for contact details
                    $(document).on('click', '.active-conversation-session', function(event) {
                        var activeSession = Arcus.IMChat.getActiveSession();
                        var phoneNo = $(event.target).closest('.list-item').attr('data-id');
                        chatController.displayChat(phoneNo, _this.routing);
                        if(!activeSession || phoneNo !== activeSession.chatterURI){
                            //re-render chat list to show new active state
                            this.renderChatList();
                        }
                    }.bind(this));

                    $(document).on('click', '.chat-list-action-close', function() {
                        var phoneNo = $(this).closest('.list-item').attr('data-id');
                        chatController.removeChat(phoneNo, _this.routing);
                    });

                    // add handler for group details
                    $(document).on('click', '.group-data-request-details', function() {
                        var id = $(this).closest('.list-item').attr('data-id');
                        _this.setupContactGroupDetail(id, true);
                    });

                    // add handler for user action calling
                    $(document).on('click', '.user-action-call', function() {
                        var a = $(this);
                        var number = a.attr('data-val');
                        callController.initCall(number);
                    });

                    // add handler for user action email to
                    $(document).on('click', '.user-action-email,.js-action-sms', function() {
                        if (_this.routing) {
                            _this.transitionMainContainerOut();
                            $('#main-container .widget-box-overlay').hide();
                            eventEmitter.one(eventEmitter.Event.ROUTER_AFTER_CHANGE, _this.createMessageCallback.bind(this));
                            _this.routing.navigateTo(_this.routing.pages.MESSAGES);
                        }
                    });

                    $(document).on('click', '.js-start-chat', function(){
                        var contactPhoneNumber = $(this).data('val').toString();
                        chatController.displayChat(contactPhoneNumber,  _this.routing);
                    });

                    // add handler for list filtering
                    // list filtering
                    $(document).on('keyup', 'input.filter-list', function() {
                        // get vars
                        var _this = $(this);
                        var filter = _this.val();
                        var listId = _this.attr('data-list-id');
                        var $list = $('#' + listId);
                        var $listCount = $('#' + listId + '-count');

                        // filter list
                        list.filterList({ $list: $list, $listCount: $listCount, val: filter});
                    });

                    eventEmitter.on('show:contact', function(event, data) {
                        _this.transitionMainContainerIn();
                        _this.setupContactDetail(data.contactId, true, true);

                        eventEmitter.on('hide:contacts', function() {
                            _this.transitionDetailOut();
                            _this.transitionMainContainerOut();
                        });
                    });

                    //contact dropdown options init
                    $(document).on('click', '.js-dropdown-toggle', function(e) {

                        e.preventDefault();
                        e.stopPropagation();

                        var $this = $(this),
                            $menu = $this.nextAll('ul'),
                            $container = $('#contacts-tab .scroll-viewport'),
                            $menuButtonGroup = $this.parent(),
                            menuButtonHeight,
                            menuButtonOffset,
                            menuHeight,
                            conrainerOffset,
                            containerHeight,
                            menuBottomPosition,
                            containerBottomPosition;

                        if(!$this.parent().hasClass('open')) {
                            //close all submenu
                            $menu.find('.open').children('ul').hide();
                            $menu.find('.open').removeClass('open');
                        }

                        menuButtonHeight = $this.height();
                        menuButtonOffset = $this.offset();
                        menuHeight = $menu.height();
                        conrainerOffset = $container.offset();
                        containerHeight = $container.height();
                        menuBottomPosition = menuButtonOffset.top + menuButtonHeight + menuHeight;
                        containerBottomPosition = conrainerOffset.top + containerHeight;

                        if(menuBottomPosition > containerBottomPosition) {
                            $menu.css('bottom', $menuButtonGroup.height() + 'px');
                            $menu.css('top', 'auto');
                        }
                        else {
                            $menu.removeAttr('style');
                        }

                        $(this).next('a').dropdown('toggle');
                    });
                },

                /**
                 * callback to execute after routing to #/messages/ when sending a sms or email to contact
                 */
                createMessageCallback: function() {
                    var $this = $(this);
                    var id = $this.attr('data-id')
                        , val = $this.attr('data-val')
                        , recipient = $this.attr('data-recipient')
                        , predefinedArtifactType = $this.attr('data-artifactType');

                    switch (recipient) {
                        case 'Contact':
                            var result = contactManager.getContactByContactTypeValue(val)
                                , contact = result.contact
                                , emailOrPhone = result.foundContactMethod
                                , autocompleteMethod = {};
                            switch (predefinedArtifactType) {
                                case 'SMS':
                                case 'EMail':
                                    autocompleteMethod = contactManager.wrapAutocompleteContactMethod(contact, emailOrPhone);
                                    break;
                            }

                            messageController.setupMessageEditing({
                                action: 'writeTo',
                                message: { artifactType: predefinedArtifactType },
                                autocompleteMethods: [autocompleteMethod]
                            });
                            break;
                        case 'Group':
                            var foundGroup = groupManager.getById(id)
                                , groupContactMethods = groupManager.getGroupContactMethods(foundGroup)
                                , autocompleteMethods = [];
                            if (foundGroup && groupContactMethods) {
                                $.each(groupContactMethods, function(ind, method) {
                                    var foundContact = contactManager.getContactByMethodId(method.id);
                                    if (foundContact) {
                                        autocompleteMethods.push(contactManager.wrapAutocompleteContactMethod(foundContact, method));
                                    }
                                });

                                messageController.setupMessageEditing({
                                    action: 'writeTo',
                                    message: { artifactType: predefinedArtifactType },
                                    autocompleteMethods: autocompleteMethods
                                });
                            }
                            break;
                    }
                },

                setupContactDetail: function(id, initial, shouldCloseMainContainer) {
                    // get contact and build view
                    var contact = contactManager.getById(id);
                    contact.prepareContactNotesPriorities();
                    var $view = $(template.buildTemplate('contact/contact-detail', contact));

                    // add handlers to view for the toolbar
                    // add handlers on toolbar
                    $view.on('click', '.data-action', function() {
                        var val = $(this).attr('data-val');
                        var _id = $(this).closest('.data-container').attr('data-id');

                        switch (val) {
                            case 'edit' :
                                _this.setupContactEditing(_id);
                                break;
                            case 'export' :
                                contactManager.getById(_id).exportVCard();
                                break;
                            case 'cancel' :
                                _this.transitionDetailOut();
                                if (shouldCloseMainContainer) {
                                    _this.transitionMainContainerOut();
                                }
                                break;
                        }
                    });

                    // set the widget content and then transition the view in
                    _this.transitionDetailIn($view, initial).done(function() {
                        _this.initScroll('contact-detail', $('.contact-detail-scroll'));
                        _this.resizeScroll('contact-detail');
                    });
                },

                setupContactGroupDetail: function(id, initial) {
                    // get contact group and build view
                    var contactGroup = groupManager.getById(id);
                    var $view = $(template.buildTemplate('contact/contact-group-detail', contactGroup));

                    // add handlers to view for the toolbar
                    // add handlers on toolbar
                    $view.on('click', '.data-action', function() {
                        var val = $(this).attr('data-val');
                        var _id = $(this).closest('.data-container').attr('data-id');

                        switch (val) {
                            case 'edit' :
                                _this.setupContactGroupEditing(_id);
                                break;
                            case 'cancel' :
                                _this.transitionDetailOut();
                                break;
                        }
                    });

                    // setup the tree view in view mode
                    _this.setupContactGroupTreeView($view, false, contactGroup);

                    // set the widget content and then transition the view in
                    _this.transitionDetailIn($view, initial).done(function() {
                        _this.initScroll('group-detail', $('.group-detail-scroll'));
                        _this.resizeScroll('group-detail');
                    });
                },

                setupContactEditing: function(_id) {
                    if (_id) {
                        _this.showLoading();
                    }

                    // swap this view for the edit view
                    var contact = contactManager.getById(_id);
                    contact = contactManager.defineContactSelect(contact);
                    var $view = $(template.buildTemplate('contact/contact-edit', contact));
                    var $contactImage = $view.find('.profile-picture img');
                    // before enter into image-functionality
                    if (contact.images.length) {
                        _this.contactImageHandler = window.$wrtc.enums.contactImageHandler.has;
                    } else {
                        _this.contactImageHandler = window.$wrtc.enums.contactImageHandler.none;
                    }
                    _this.imageInfo = {};
                    $contactImage.on('click', { contact: contact, $view: $view, $contactImage: $contactImage }, _this.setupContactImage);

                    if (!_id) { // new contact
                        // set the widget content and then transition the view in
                        _this.transitionDetailIn($view, true).done(function() {
                            _this.initScroll('contact-edit', $('.contact-edit-scroll'));
                            _this.resizeScroll('contact-edit');
                            $view.find('.focus-edit').focus();
                        });
                    } else {
                        _this.transitionEditingIn($view).done(function() {
                            _this.initScroll('contact-edit', $('.contact-edit-scroll'));
                            _this.resizeScroll('contact-edit');
                            $view.find('.focus-edit').focus();
                        });
                    }

                    // add handlers on toolbar
                    $view.find('.data-action').on('click', function(event) {
                        var val = $(this).attr('data-val');
                        var id = $(this).closest('.data-container').attr('data-id');
                        switch (val) {
                            case 'save' :
                                //$('#contact-form').submit();
                                break;
                            case 'import' :
                                $('.vcard-upload-group').toggle();
                                _this.importVCard();
                                break;
                            case 'remove' :
                                event.stopPropagation();
                                event.preventDefault();
                                bootbox.confirm("Are you sure you want to remove this contact?", function(result) {
                                    if (result) {
                                        _this.showLoading();
                                        $.waitFor(contactManager.removeContactRequest(_id)).done(function() {

                                            _this.hideLoading();
                                            _this.transitionDetailOut();

                                            _this.updateContactList(contact.id, 'contact', true);
                                        });
                                    }
                                });
                                break;
                            case 'cancel' :
                                _this.showLoading();
                                _this.transitionEditingOut(id, 'contact');
                                _this.file = null;
                                break;
                        }
                    });

                    $view.find('.remove-avatar').on('click', function() {
                        $(this).hide();
                        $contactImage.attr('src', 'images/no_image_.jpg');
                        _this.contactImageHandler = window.$wrtc.enums.contactImageHandler.none;
                    });

                    // store all ids
                    var ids = (function(_contact) {
                        var sumArray = _contact.phoneNumbers
                                .concat(_contact.emailAddress)
                                .concat(_contact.addresses)
                                .concat(_contact.imAddresses)
                                .concat(_contact.notes)
                            , result = $.map(sumArray, function(elementOfArray) {
                                return elementOfArray.id;
                            });

                        return result;
                    })(contact);
                    // new item handler
                    $(document).off('click', '.js-child-data-action').on('click', '.js-child-data-action', function() {
                        var $this = $(this)
                            , val = $this.attr('data-val')
                            , $formGroup = $this.closest('.form-group')
                            , data_id = $formGroup.attr('data-id');

                        if (val == 'new-child-row') {
                            var generateUniqueId = function(oldIdsArray) {
                                    var newId = (Math.floor(Math.random() * (100000 - 1 + 1)) + 1).toString()
                                        , found = false;
                                    $.each(oldIdsArray, function(ind, element) {
                                        if (element === newId) {
                                            found = true;
                                            return false;
                                        }
                                        return true;
                                    });
                                    if (!found) {
                                        oldIdsArray.push(newId);
                                        return newId;
                                    } else {
                                        return generateUniqueId(oldIdsArray);
                                    }
                                }
                                , newObject = {
                                    id: generateUniqueId(ids),
                                    newItem: true,
                                    contactTypes: contact.contactTypes,
                                    contactLocations: contact.contactLocations,
                                    locationTypes: contact.locationTypes,
                                    states: contact.states,
                                    imTypes: contact.imTypes,
                                    priorities: contact.priorities
                                }
                                , HTML = template.buildTemplate($this.attr('data-templ-path'), newObject);


                            // append and show the row
                            $(HTML).insertBefore($formGroup);
                            $formGroup.prev().find('.focus-new').focus();
                        } else if (val == 'remove-child-row') {
                            // remove the nearest row
                            $formGroup.remove();

                            $.each(ids, function(ind, element) {
                                if (element === data_id) {
                                    ids.splice(ind, 1);
                                    return false;
                                }
                                return true;
                            });
                        }
                    });

                    _this.setupContactPostHandler($view);
                    $('#firstName').focus();
                },

                setupContactGroupEditing: function(_id) {
                    if (_id) {
                        _this.showLoading();
                    }

                    // swap this view for the edit view
                    var contactGroup = groupManager.getById(_id);
                    var $view = $(template.buildTemplate('contact/contact-group-edit', contactGroup));

                    if (!_id) { // new contact group
                        // set the widget content and then transition the view in
                        _this.transitionDetailIn($view, true).done(function() {
                            _this.initScroll('group-edit', $('.group-edit-scroll'));
                            _this.resizeScroll('group-edit');
                            $view.find('.focus-edit').focus();
                        });

                    } else {
                        _this.transitionEditingIn($view).done(function() {
                            _this.initScroll('group-edit', $('.group-edit-scroll'));
                            _this.resizeScroll('group-edit');
                            $view.find('.focus-edit').focus();
                        });
                    }

                    // add handlers on toolbar
                    $view.find('.data-action').on('click', function(event) {
                        var val = $(this).attr('data-val');
                        var id = $(this).closest('.data-container').attr('data-id');
                        switch (val) {
                            case 'save' :
                                _this.showAllTree($('#chooseMethodsThree'));
                                //$('#contact-group-form').submit();
                                break;
                            case 'remove' :
                                event.stopPropagation();
                                event.preventDefault();
                                bootbox.confirm("Are you sure you want to remove this group?", function(result) {
                                    if (result) {
                                        _this.showLoading();
                                        $.waitFor(groupManager.removeContactGroupRequest(_id)).done(function() {

                                            _this.hideLoading();
                                            _this.transitionDetailOut();

                                            _this.updateContactList(contactGroup.id, 'contact-group', true);
                                        });
                                    }
                                });
                                break;
                            case 'cancel' :
                                _this.showLoading();
                                _this.transitionEditingOut(id, 'contact-group');
                                break;
                        }
                    });

                    _this.setupContactGroupPostHandler($view);
                    _this.setupContactGroupTreeView($view, true, contactGroup);
                },

                showAllTree: function($tree) {
                    // show all three
                    var treeData = $tree.data('tree');
                    if (treeData) {
                        $.each($tree.find('.tree-folder-header'), function(ind, element) {
                            if (ind > 0) { // first element is main header
                                treeData.selectFolder(element);
                                // open nested elements
                                var $children = $(element).parent().find('.tree-folder-header');
                                $.each($children, function(ind, element) {
                                    if (ind > 0) { // first element is main header
                                        treeData.selectFolder(element);
                                    }
                                });
                            }
                        });
                    }
                },

                setupContactGroupTreeView: function($view, editable, contactGroup) {
                    var $tree = $();
                    if (editable) {
                        // setup tree for editing
                        $tree = $view.find('#chooseMethodsThree');
                        $tree.ace_tree({
                            dataSource: contactGroup ? contactManager.getContactsAsData(contactManager.data, contactGroup, true, groupManager) : contactManager.getContactsAsData(contactManager.data),
                            loadingHTML: '<div class="tree-loading"><i class="icon-refresh icon-spin blue"></i></div>',
                            multiSelect: true,
                            'open-icon': 'icon-minus',
                            'close-icon': 'icon-plus',
                            'selectable': true,
                            'selected-icon': 'icon-ok',
                            'unselected-icon': 'icon-remove'
                        });
                    } else {
                        // setup tree for viewing
                        $tree = $view.find('#includedMethodsThree');
                        $tree.ace_tree({
                            dataSource: contactManager.getContactsAsData(groupManager.getUniqueContactGroupMembers(contactGroup, contactManager), undefined, true),
                            loadingHTML: '<div class="tree-loading"><i class="icon-refresh icon-spin blue"></i></div>',
                            'open-icon': 'icon-minus',
                            'close-icon': 'icon-plus',
                            'selectable': false,
                            'selected-icon': 'icon-ok',
                            'unselected-icon': 'icon-remove'
                        });
                        // open tree view
                        _this.showAllTree($tree);
                    }
                },

                transitionMainContainerIn: function() {
                    $('#sidebar').removeClass('off-screen');
                    $('.widget-box-overlay').on('click', function() {
                        $('#toggle-contacts').trigger('click');
                    });
                    _this.resizeScroll();
                },

                transitionMainContainerOut: function() {
                    $('#sidebar').addClass('off-screen');
                    $('.widget-box-overlay').off('click');
                },

                transitionDetailIn: function($view, initial, _$selector) {
                    var $deferred = $.Deferred()
                        , $selector = _$selector || $('#sidebar-detail-widget');
                    if (initial) {
                        var tr3d = 'translate3d(408px,0,0)';
                        $selector.html($view).css('-webkit-transform', tr3d)
                            .css('-moz-transform', tr3d)
                            .css('-ms-transform', tr3d)
                            .css('-o-transform', tr3d)
                            .css('transform', tr3d);
                        //.removeClass('off-screen');
                        $deferred.resolve();
                    } else {
                        // fade the detail view back in
                        setTimeout(function() {
                            $view.css('opacity', _this.fadeInStartingOpacity);

                            // transition the content in
                            $selector.html($view);

                            $view.animate({
                                opacity: 1
                            });

                            $deferred.resolve();

                        }, _this.minLoadingTime);
                    }
                    return $deferred;
                },

                transitionDetailOut: function(_$selector) {
                    var tr3d = 'translate3d(0,0,0)'
                        , $selector = _$selector || $('#sidebar-detail-widget');
                    $selector.css('-webkit-transform', tr3d)
                        .css('-moz-transform', tr3d)
                        .css('-ms-transform', tr3d)
                        .css('-o-transform', tr3d)
                        .css('transform', tr3d);
                    //.addClass('off-screen');

                },

                transitionEditingIn: function($view) {
                    var $deferred = $.Deferred();
                    // wait for a few seconds to transition in smoothly
                    setTimeout(function() {
                        $view.css('opacity', _this.fadeInStartingOpacity);

                        // transition the content in
                        $('#sidebar-detail-widget').html($view);

                        // focus control
                        $view.find('.control-focus').focus();

                        // animate to visible
                        $view.animate({
                            opacity: 1
                        });

                        $deferred.resolve();
                    }, _this.minLoadingTime);

                    return $deferred;
                },

                transitionEditingOut: function(id, key) {
                    if (id) {
                        if (key === 'contact') {
                            _this.setupContactDetail(id, false);

                        } else if (key === 'contact-group') {
                            _this.setupContactGroupDetail(id, false);
                        }
                    } else {
                        _this.transitionDetailOut();
                    }
                },
                importVCard: function(){
                    var fileInput = document.getElementById('vcardFileUpload');
                    fileInput.addEventListener('change', function () {
                        var file = fileInput.files[0];
                        var textType = /text.*/;
                        if (file.type.match(textType)) {
                            var reader = new FileReader();
                            reader.onload = function () {
                                console.log(reader.result);
                                VCF.parse(reader.result, function(vc) {
                                    console.log(vc);
                                    $('#firstName').val(vc.n.givenName);
                                    $('#lastName').val(vc.n.familyName);
                                    //Loop through phone numbers.
                                    for(var i=0; i < vc.tel.length; i++) {
                                        var p = vc.tel[i];
                                        $('a#new-phone').click();
                                        $("input[id^='Phone']").last().val(p.value);
                                        $("select[name^='contactType']").last().val(p.type[0].toUpperCase());
                                    }
                                    //Loop through email addresses
                                    for (i=0; i < vc.email.length;i++){
                                        $('a#new-email').click();
                                        var e = vc.email[i];
                                        $("input[id^='Email']").last().val(e.value);
                                        $("select[name^='contactLocation']").last().val(e.type[0].toUpperCase());
                                    }
                                    //Load address. May need to revisit to add multiple addresses.
                                    if (typeof vc.adr !== "undefined") {
                                        var a = vc.adr.value.replace(/;/g,'').split('\\n');
                                        $('a#new-address').click();

                                        $("input[name^='streetLine']").last().val(a[0]);
                                        $("input[name^='city']").last().val(a[a.length-1].split('\\,')[0]);
                                        $("select[name^='state']").last().val(a[a.length-1].split('\\,')[1].split(' ')[1]);
                                        $("input[name^='zip']").last().val(a[a.length-1].split('\\,')[1].split(' ')[2]);
                                        console.log(a[a.length-1].split('\\,')[1].split(' ')[0]);
                                    }
                                    //Loop through notes.
                                    for (i=0; i < vc.note.length;i++){
                                        $('a#new-note').click();
                                        var n = vc.note[i];
                                        $("select[name^='priority']").last().val(2); //Set to "Normal" by default.
                                        $("input[name^='subject']").last().val("No Subject"); //Default to this message.
                                        $("input[name^='description']").last().val(n);
                                    }
                                    $('input#firstName').focus();
                                });
                            };
                            reader.readAsText(file);
                            $('.vcard-upload-group').hide();
                        } else {
                            //fileDisplayArea.innerText = 'File not supported!';
                            console.log("File type is not supported");
                        }
                    });
                },

                showLoading: function() {
                    $.blockUI.defaults.css.cursor = 'default';
                    $('#sidebar-detail-widget .spinner-viewport').block({
                        css: {
                            marginTop: '-40%',
                            border: 'none',
                            backgroundColor: 'transparent',
                            cursor: 'auto'
                        },
                        overlayCSS: {
                            backgroundColor: 'white',
                            opacity: 0.7,
                            cursor: 'auto'
                        },
                        message: '<i class="icon-spinner icon-spin icon-2x green"></i>'
                    });
                },

                hideLoading: function() {
                    $('#contact-detail-widget').unblock();
                },

                setupContactPostHandler: function($view) {
                    validations.init();

                    $.validator.addClassRules({
                        phoneVal: {
                            //phoneUS: true,
                            required: true
                        },
                        emailVal: {
                            email: true,
                            required: true
                        },
                        streetLine1Val: {
                            required: true
                        },
                        cityVal: {
                            required: true
                        },
                        zipVal: {
                            digits: true,
                            required: true
                        },
                        imAddressVal: {
                            required: true
                        },
                        subjectVal: {
                            required: true
                        },
                        descriptionVal: {
                            required: true
                        }
                    });

                    $view.find('#contact-form').validate({
                        errorClass: 'help-block',
                        highlight: function(e) {
                            $(e).closest('.form-group').addClass('has-error');
                        },
                        unhighlight: function(e) {
                            $(e).closest('.form-group').removeClass('has-error');
                        },
                        submitHandler: function(form) {
                            var $form = $(form)
                                , id = $form.find('input[id="id"]').val()
                                , contact = contactManager.getById(id)
                                , newRecordHandler = function(event, data) {

                                    _this.updateContactList(data ? data.id : id, 'contact');
                                    _this.transitionEditingOut(data ? data.id : id, 'contact');
                                    eventEmitter.off(groupManager.events.ADD_RECORD, newRecordHandler);
                                    eventEmitter.off(contactManager.events.ERROR);
                                };

                            _this.showLoading();

                            eventEmitter.off(groupManager.events.ADD_RECORD, newRecordHandler);
                            eventEmitter.on(groupManager.events.ADD_RECORD, newRecordHandler);

                            eventEmitter.off(contactManager.events.ERROR).on(contactManager.events.ERROR, function(event, data) {
                                _this.hideLoading();
                                eventEmitter.off(groupManager.events.ADD_RECORD, newRecordHandler);
                                if (data) {
                                    $.gritter.add({
                                        text: data.text,
                                        class_name: 'gritter-error gritter-center'
                                    });
                                }
                            });

                            $.waitFor(contactManager.saveContact(
                                {
                                    contact: contact,
                                    contactManager: contactManager,
                                    groupManager: groupManager,
                                    imageInfo: _this.imageInfo,
                                    contactImageHandler: _this.contactImageHandler,
                                    serializedItems: _this.serializeContact()
                                }));
                        },
                        rules: {
                            firstName: { required: true },
                            lastName: { required: true }
                        },
                        messages: {
                            firstName: { required: 'First name is required' },
                            lastName: { required: 'Last name is required' }
                        }
                    });
                },

                setupContactGroupPostHandler: function($view) {
                    $.validator.addClassRules({
                        contactGroupName: {
                            required: true
                        }
                    });

                    $view.find('#contact-group-form').validate({
                        errorClass: 'help-block',
                        highlight: function(e) {
                            $(e).closest('.form-group').addClass('has-error');
                        },
                        unhighlight: function(e) {
                            $(e).closest('.form-group').removeClass('has-error');
                        },
                        submitHandler: function(form) {
                            var $form = $(form)
                                , id = $form.find('input[id="id"]').val()
                                , contactGroup = groupManager.getById(id)
                                , newRecord
                                , newRecordHandler = function(event, data) {
                                    newRecord = data;
                                };

                            _this.showLoading();

                            eventEmitter.off(groupManager.events.ADD_RECORD, newRecordHandler);
                            eventEmitter.on(groupManager.events.ADD_RECORD, newRecordHandler);

                            $.waitFor(groupManager.saveContactGroup(
                                    {
                                        contactGroup: contactGroup,
                                        contactManager: contactManager,
                                        groupManager: groupManager
                                    })).done(function() {

                                    _this.updateContactList(newRecord ? newRecord.id : id, 'contact-group');
                                    _this.transitionEditingOut(newRecord ? newRecord.id : id, 'contact-group');
                                }).fail(function() {

                                $('#sidebar-detail-widget .spinner-viewport').unblock();

                                $.gritter.add({
                                    text: "Failed to update group",
                                    class_name: 'gritter-error gritter-center'
                                });
                            });
                        },
                        rules: {
                            name: 'required'
                        },
                        messages: {
                            name: 'Group name is required'
                        }
                    });
                },

                updateContactList: function(id, dataType, remove) {
                    var $element = $('.list-item[data-type="' + dataType + '"][data-id="' + id + '"]'),
                        $list,
                        $listCount,
                        $filter,
                        element,
                        content,
                        index,
                        listId,
                        tabId,
                        insertPlace;

                    switch (dataType) {
                        case 'contact':
                            if(!remove) { // no need to build template for contact if we deleted it
                                element = this.formatModelForView(contactManager.getById(id));
                                index = contactManager.getContactsSorted(_this.contactsSortBy).indexOf(element);
                                content = template.buildTemplate('contact/contact-list-item', element);
                            }
                            listId = 'contact-list-count';
                            tabId = 'contacts-tab';
                            $list = $('#contact-list');
                            break;
                        case 'contact-group':
                            element = groupManager.getById(id);
                            index = groupManager.getContactGroupsSorted(_this.groupsSortBy).indexOf(element);
                            listId = 'contact-group-list-count';
                            tabId = 'groups-tab';
                            content = template.buildTemplate('contact/contact-group-list-item', element);
                            $list = $('#contact-group-list');
                            break;
                    }

                    if ($element.length) {
                        $element.remove();
                    }

                    if (!remove) {
                        if (index > 0) {
                            $list.find('> div:eq(' + (index - 1) + ')').after(content);
                        } else {
                            insertPlace = $list.find('> div:eq(0)');
                            if (insertPlace.length) {
                                insertPlace.before(content);
                            } else {
                                $list.html(content);
                                _this.resizeScroll();
                            }
                        }
                    }

                    // filter list
                    $listCount = $('#' + listId);
                    $filter = $('#' + tabId).find('input.filter-list');
                    list.filterList({ $list: $list, $listCount: $listCount, val: $filter.val()});
                },

                serializeContact: function() {
                    var $items = $('#sidebar-detail-widget').find('.form-group:not(.new-child-row)'), resultData = [];

                    $.each($items, function(ind, item) {
                        var $item = $(item)
                            , $inputOrSelect = $item.find('input,select')
                            , data_id = $item.attr('data-id')
                            , data_model = $item.attr('data-model')
                            , data = $inputOrSelect.serializeObject()
                            , rewriteByKeyName = function(_object, _str) {
                                var key;
                                for (key in _object) {
                                    if (_object.hasOwnProperty(key)) {
                                        if (key.indexOf(_str) > -1) {
                                            _object[_str] = _object[key];
                                            delete _object[key];
                                            return true;
                                        }
                                    }
                                }
                                return false;
                            }
                            ;
                        if (data_model) {
                            if (data_id) {
                                data.id = data_id;
                            }
                            switch (data_model) {
                                case 'Phone':
                                    rewriteByKeyName(data, 'contactType');
                                    rewriteByKeyName(data, 'value');
                                    break;
                                case 'Email':
                                    rewriteByKeyName(data, 'contactLocation');
                                    rewriteByKeyName(data, 'value');
                                    break;
                                case 'ContactAddress':
                                    rewriteByKeyName(data, 'locationType');
                                    rewriteByKeyName(data, 'streetLine1');
                                    rewriteByKeyName(data, 'city');
                                    rewriteByKeyName(data, 'state');
                                    rewriteByKeyName(data, 'zip');
                                    break;
                                case 'InstantMessengerAddress':
                                    rewriteByKeyName(data, 'contactType');
                                    rewriteByKeyName(data, 'value');
                                    break;
                                case 'ContactNote':
                                    rewriteByKeyName(data, 'priority');
                                    rewriteByKeyName(data, 'subject');
                                    rewriteByKeyName(data, 'description');
                                    break;
                            }

                            resultData.push({
                                $selector: $item,
                                model: data_model,
                                data: data
                            });
                        }
                    });

                    return resultData;
                }
            };
        };
        return contactController().init();
    });
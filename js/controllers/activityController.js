define('activityController', ['diagnosticWorkMemoryController', 'Arcus', 'config', 'logger', 'template', 'activityManager', 'contactManager', 'moment', 'eventEmitter', 'bootbox', 'fullCalendar', 'jqueryui', 'bootstrapTag', 'picker', 'pickadateDate', 'pickadateTime', 'enums'],
    function(diagnosticWorkMemoryController, arcus, config, logger, template, activityManager, contactManager, moment, eventEmitter, bootbox) {

    /**
     * activityController controller, does main UI interaction and
     * interaction with business logic, related to activities/calendar events
     * @return {[type]}
     */
    var activityController = function() {
        var _this;

        return {

            title: "Activities",

            init: function() {
                _this = this;
                _this.scrolls = {};
                _this.intervalToPickers = 15;
                _this.pickerTimeFormat = 'hh:i A';
                _this.pickerDateFormat = 'mmm dd, yyyy';
                _this.newActivityStartDate = new Date();
                _this.newActivityEndDate = new Date(_this.newActivityStartDate.getTime() + (_this.intervalToPickers * 60 * 1000));
                _this.newActivityAllDay = true;
                template.registerPartial('activity/day-of-month-number');
                template.registerPartial('activity/day-of-week-name');
                template.registerPartial('activity/day-of-week-number');
                return _this;
            },

            /**
             * return array of week days
             * @param _curDayOfWeek
             * @returns {{value: number, text: string}[]}
             */
            getDaysOfWeek: function(_curDayOfWeek) {
                var days = [{ value : 0, text : 'Sunday'},
                    { value : 1, text : 'Monday'},
                    { value : 2, text : 'Tuesday'},
                    { value : 3, text : 'Wednesday'},
                    { value : 4, text : 'Thursday'},
                    { value : 5, text : 'Friday'},
                    { value : 6, text : 'Saturday'}];

                if (_curDayOfWeek) {
                    days[_curDayOfWeek].selected = 'selected';
                }

                return days;
            },

            /**
             * return count of days in the current month
             * @param year
             * @param month
             * @returns {number}
             */
            getDaysInMonth: function(year, month) {
                return new Date(year, month + 1, 0).getDate();
            },

            /**
             * return array for select
             * @param _daysInMonth
             * @param _curDay
             * @returns {Array}
             */
            getOrdinalNumbersText: function(_daysInMonth, _curDay) {
                var numbers = [], txt = '';
                for (var i = 0; i < _daysInMonth; i++) {
                    switch (i+1) {
                        case 1:case 21:case 31: // first
                        txt = 'st';
                        break;
                        case 2:case 22:// second
                        txt = 'nd';
                        break;
                        case 3:case 23: // third
                        txt = 'rd';
                        break;
                        default :
                            txt = 'th';
                            break;
                    }
                    numbers.push({ value : i+1, text : (i+1).toString() + txt });
                }

                if (_curDay) {
                    numbers[_curDay].selected = 'selected';
                }

                return numbers;
            },

            /**
             * return count of current day of week per month
             * @param _curDate
             * @returns {{daysOfWeekInMonth: number, curDayOfWeekInMonth: number}}
             */
            countDayOfWeekInCurrentMonth: function(_curDate) {
                var changedDate = new Date(_curDate.getFullYear(), _curDate.getMonth(), _curDate.getDate());
                var backward = 0, forward = 0;

                changedDate.setDate(changedDate.getDate() - 7); // step 7 days before
                while (_curDate.getMonth() === changedDate.getMonth()) {
                    changedDate.setDate(changedDate.getDate() - 7); // step 7 days before
                    backward++;
                }
                changedDate = new Date(_curDate.getFullYear(), _curDate.getMonth(), _curDate.getDate());
                changedDate.setDate(changedDate.getDate() + 7); // step 7 days after
                while (_curDate.getMonth() === changedDate.getMonth()) {
                    changedDate.setDate(changedDate.getDate() + 7); // step 7 days before
                    forward++;
                }

                return { daysOfWeekInMonth: backward + 1 + forward, curDayOfWeekInMonth : backward === 0 ? 1 : backward + 1 };
            },

            /**
             * Build main view of the calendar view
             * @param  {Object} options
             */
            buildMainView: function(options) {
                var view = $(template.buildTemplate('activity/activity-calendar', {}));
                _this.setupHandlers(view);
                if (options.success) options.success(view);
                diagnosticWorkMemoryController.checkStatus();
            },

            /**
             * Setup calendar view handlers and init calendar
             * @param  {Object} view
             */
            setupHandlers: function(view) {

                view.find('.btn-new-activity').on('click', function() {
                    _this.openActivityCreate(_this.newActivityStartDate, _this.newActivityEndDate, _this.newActivityAllDay);
                });

                _this.calendar = view.find('#calendar');

                _this.calendar.fullCalendar({

                    buttonText: {
                        prev: '<i class="icon-chevron-left"></i>',
                        next: '<i class="icon-chevron-right"></i>'
                    },

                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },

                    columnFormat: {
                        month: 'ddd',    // Mon
                        week: 'ddd M/d', // Mon 9/7
                        day: 'dddd, MMMM d, yyyy'  // Wednesday, May 7, 2014
                    },

                    titleFormat: {
                        month: 'MMMM yyyy',                             // September 2009
                        week: "MMM d[ yyyy]{ '&#8212;'[ MMM] d yyyy}", // Sep 7 - 13 2009
                        day: ''                                         // none
                    },
                    contentHeight: 1125,
                    slotEventOverlap: false,

                    events: function(start, end, callback) {

                        var startDate = Date.parse(start),
                            endDate = Date.parse(end),
                            minStartDate = new Date(activityManager.getMinStartDate()),
                            maxStartDate = new Date(activityManager.getMaxStartDate());

                        // determine if we need to refetch activities from the service
                        if (start < minStartDate || end > maxStartDate) {
                            _this.isRefreshEvents = true;
                        }

                        _this.showLoading(true);
                        activityManager.load(_this.isRefreshEvents, {
                            startDate: startDate,
                            endDate: endDate
                        }).done(function() {
                            _this.isRefreshEvents = false;

                            var events = _this.prepareEvents(activityManager.getData());

                            _this.hideLoading();
                            callback(events);
                        }).fail(function() {
                            var $container = $('#calendar');
                            var errorHTML = '<div class="bigger-110"><div class="alert alert-danger">Error loading activities!</div></div>';
                            $container.html(errorHTML);
                            _this.hideLoading();
                        });

                    },

                    editable: true,

                    eventResize: function( event, dayDelta, minuteDelta, revertFunc) {
                        if (!dayDelta && !minuteDelta) {
                            return;
                        }

                        var allCalback = function(){
                            _this.showLoading(true);
                             activityManager.updateActivityDate(event.id, undefined, minuteDelta, undefined, true, function(arcusParentEvent){
                                var eventsToUpdate = _this.calendar.fullCalendar('clientEvents', function(item) {
                                    return item.parentId === arcusParentEvent.id && item.id !== event.id;
                                });
                                var dateToSetup = new Date(arcusParentEvent.activityEndTime);

                                for (var i = 0; i < eventsToUpdate.length; i++) {
                                    eventsToUpdate[i].end = dateToSetup;
                                    // TODO: figure out why this solution is'n working
                                    // _this.calendar.fullCalendar('updateEvent', eventsToUpdate[i]);
                                }

                                _this.refetchEvents();
                             });
                            
                        };
                        var onlyThisEventCallback = function(){
                            _this.showLoading(true);
                            activityManager.updateActivityDate(event.id, undefined, minuteDelta, undefined, false, function(){
                                _this.hideLoading();
                            });
                        };
                        
                        if (event.parentId) {
                            _this.changeRepeatedActivityDialog(allCalback, onlyThisEventCallback, revertFunc);
                        } else {
                            onlyThisEventCallback();
                        }
                    },

                    eventDrop: function( event, dayDelta, minuteDelta, allDay, revertFunc) {
                        if (!dayDelta && !minuteDelta && allDay !== true) {
                            return;
                        }

                        var onlyThisEventCallback = function(){
                            _this.showLoading(true);
                            activityManager.updateActivityDate(event.id, '' + dayDelta, minuteDelta, allDay, false, function(){
                                _this.hideLoading();
                            });
                        };
                        
                        if (event.parentId) {
                            bootbox.dialog({
                                message: '<span class="bigger-110">You can change only this occurrence of the event. <br/>For changing all occurrences, please, <a href="#" class="link-edit-event">edit event</a>.</span>',
                                buttons: {
                                    'cancel': {
                                        'label': 'Cancel',
                                        'className': 'btn-sm btn-success',
                                        'callback': revertFunc
                                    },
                                    'click': {
                                        'label': 'Change This Event',
                                        'className': 'btn-sm btn-primary',
                                        'callback': onlyThisEventCallback
                                    }
                                }
                            });
                            $('a.link-edit-event').on('click', function(e){
                                e.preventDefault();
                                e.stopPropagation();

                                $('a.link-edit-event').off('click');
                                bootbox.hideAll();
                                revertFunc();
                                _this.openActivityEdit(event.id, event.start, event.end, event.allDay);
                            });
                        } else {
                            onlyThisEventCallback();
                        }
                    },

                    // TODO: Make draggable/dropable working on desktop and mobile devices

                    //droppable: true, // this allows things to be dropped onto the calendar !!!
                    //drop: function (date, allDay) { // this function is called when something is dropped

                    //    // retrieve the dropped element's stored Event Object
                    //    var originalEventObject = $(this).data('eventObject');
                    //    var $extraEventClass = $(this).attr('data-class');


                    //    // we need to copy it, so that multiple events don't have a reference to the same object
                    //    var copiedEventObject = $.extend({}, originalEventObject);

                    //    // assign it the date that was reported
                    //    copiedEventObject.start = date;
                    //    copiedEventObject.allDay = allDay;
                    //    if ($extraEventClass) copiedEventObject['className'] = [$extraEventClass];

                    //    // render the event on the calendar
                    //    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    //    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    //    // is the "remove after drop" checkbox checked?
                    //    if ($('#drop-remove').is(':checked')) {
                    //        // if so, remove the element from the "Draggable Events" list
                    //        $(this).remove();
                    //    }

                    //},

                    selectable: true,
                    selectHelper: true,
                    select: _this.setNewActivityParams,
                    eventClick: _this.openActivityDetails
                });
            },

            /**
             * Prepare activities objects to be displayed on the calendar as events
             * @param  {[Object]} data
             * @return {[Object]}
             */
            prepareEvents: function(data) {

                var events = [],
                    className;

                /**
                 * Remove dummy data from the in-memory dataset
                 * @param  {Object} item
                 * @return {Boolean}
                 */
                activityManager.findAndRemove(function(item) {
                    var dummyData = (item.description ? item.description.indexOf('Description from') != -1 : false);
                    return dummyData;
                });

                $.each(data, function(i, arcusEvent) {

                    if (arcusEvent.priority == 3) {
                        className = 'label-important';
                    } else if (arcusEvent.priority == 2) {
                        className = 'label-success';
                    } else {
                        className = '';
                    }

                    events.push({
                        id: arcusEvent.id,
                        parentId: (arcusEvent.parentActivity) ? arcusEvent.parentActivity.id : null,
                        title: arcusEvent.description,
                        start: new Date(arcusEvent.activityStartTime),
                        end: new Date(arcusEvent.activityEndTime),
                        allDay: arcusEvent.allDay,
                        className: className
                    });

                });

                return events;
            },

            /**
             * Re-fetch events from the service
             */
            refetchEvents: function() {
                // TODO: Eliminate global variables usage
                _this.isRefreshEvents = true;
                _this.calendar.fullCalendar('refetchEvents');
            },

            /** 
            * Save values for new calendar event
            * @param  {Date} start date for the event
            * @param  {Date} end date for the event
            * @param  {Boolean} allDay flag for the event
            */
            setNewActivityParams: function(start, end, allDay) {
                var startTimeIsZero = start.getHours() === 0 && start.getMinutes() === 0 && start.getSeconds() === 0 && start.getMilliseconds() === 0;
                var endTimeIsZero = end.getHours() === 0 && end.getMinutes() === 0 && end.getSeconds() === 0 && end.getMilliseconds() === 0;
                var todayDate = new Date();
                if (startTimeIsZero && endTimeIsZero) {
                    _this.newActivityStartDate = new Date(start.getFullYear(), start.getMonth(), start.getDate(), todayDate.getHours(), todayDate.getMinutes(), todayDate.getSeconds());
                    _this.newActivityEndDate = new Date(_this.newActivityStartDate.getTime() + (_this.intervalToPickers * 60 * 1000));
                } else {
                    _this.newActivityStartDate = start;
                    _this.newActivityEndDate = end;
                }

                _this.newActivityAllDay = allDay;
            },

            /**
             * Open view to create calendar event
             * @param  {Date} start date for the event
             * @param  {Date} end date for the event
             * @param  {Boolean} allDay flag for the event
             */
            openActivityCreate: function(start, end, allDay) {
                _this.openActivityEdit(null, start, end, allDay);
                _this.calendar.fullCalendar('unselect');
            },

            /**
             * Open view to edit calendar event
             * @param  {Number} id of the calendar event
             * @param  {Date} start date for the event
             * @param  {Date} end date for the event
             * @param  {Boolean} allDay flag for the event
             */
            openActivityEdit: function(id, start, end, allDay) {
                var view = _this.setupActivityEdit(id, start, end, allDay);
                _this.transitionSidebarIn(view);
            },

            /**
             * Open details view for calendar event
             * @param  {Object} calEvent represents full-calendar event object
             * @param  {Object} jsEvent represents javascript event
             * @param  {Object} calView represents full-calendar event view
             */
            openActivityDetails: function(calEvent, jsEvent) {
                var view;

                // prevent js event default to avoid routing execution
                jsEvent.preventDefault();

                if (calEvent && calEvent.id) {
                    view = _this.setupActivityDetail(calEvent.id);
                    _this.selectedCalEvent = calEvent;
                }

                _this.transitionSidebarIn(view);
            },

            /**
             * Close event details view or edit view
             */
            closeActivity: function() {
                _this.transitionSidebarOut();
            },

            /**
             * Retrieve calendar event object from BL, render it in detail template and attache event handlers
             * @param  {Number} id of the calendar event
             * @return {Object} built view of the calendar event details
             */
            setupActivityDetail: function(id) {

                var viewModel = {},
                    activity = activityManager.getById(id);

                viewModel.activity = activity;

                if (activity.activityEndTime)
                    viewModel.timeAgo = activity.activityEndTime;
                else
                    viewModel.timeAgo = activity.activityStartTime;

                viewModel.activityStartDateFormatted = moment(activity.activityStartTime).format('MMM DD, YYYY');
                viewModel.activityStartTimeFormatted = moment(activity.activityStartTime).format('MMM DD, YYYY hh:mm A');
                viewModel.activityEndTimeFormatted = moment(activity.activityEndTime).format('MMM DD, YYYY hh:mm A');

                if (activity.recurringType > 1) {
                    viewModel.contacts = activity.parentActivity.contacts;
                } else {
                    viewModel.contacts = activity.contacts;
                }

                if (viewModel.contacts) {
                    viewModel.unknowncontacts = [];
                    viewModel.knowncontacts = [];
                    if (viewModel.contacts.length && viewModel.contacts.length === 1 && viewModel.contacts[0].id === '-1' &&  viewModel.contacts[0].type === "1") {
                        // if activity doesn't have any attendies server requests with one contactsreference && its id = -1
                        // delete array inside model
                        if (activity.recurringType > 1) {
                            activity.parentActivity.contacts = [];
                        } else {
                            activity.contacts = [];
                        }
                        viewModel.contacts = [];
                    }
                    for (var i = 0; i < viewModel.contacts.length; i++) {
                        if (viewModel.contacts[i].id !== '-1') {
                            var contact = contactManager.getById(viewModel.contacts[i].id);
                            if (contact && contact.images && contact.images.length > 0) {
                                viewModel.contacts[i].imageUrl = contact.images[0].imageUrl;
                            } else {
                                viewModel.contacts[i].imageUrl = config.getValue('noImage', null);
                            }
                            if (contact) {
                                viewModel.knowncontacts.push(viewModel.contacts[i]);
                            } else {
                                viewModel.unknowncontacts.push(viewModel.contacts[i]);
                            }
                        } else if ( viewModel.contacts[i].type !== '1' ) { // Arcus.model.ContactReference.TypeEnum.NA = '1'
                            viewModel.contacts[i].imageUrl = config.getValue('noImage', null);
                            viewModel.unknowncontacts.push(viewModel.contacts[i]);
                        }
                    }
                }

                viewModel.hasntAttendies = !(viewModel.unknowncontacts.length || viewModel.knowncontacts.length);

                // TODO: Use activity priority enums instead
                switch (activity.priority) {
                    case 1:
                        viewModel.priority = {
                            text: 'low',
                            uiclass: 'icon-arrow-down blue'
                        };
                        break;
                    case 2:
                        viewModel.priority = {
                            text: 'normal',
                            uiclass: 'icon-info green'
                        };
                        break;
                    case 3:
                        viewModel.priority = {
                            text: 'high',
                            uiclass: 'icon-exclamation red'
                        };
                        break;

                }

                var $view = $(template.buildTemplate('activity/activity-detail', viewModel));

                // add handlers to view
                $view.on('click', '.data-action', function(evt) {
                    evt.preventDefault();
                    var val = $(this).attr('data-val');
                    var _id = $(this).closest('.data-container').attr('data-id');

                    switch (val) {
                        case 'edit':
                            _this.openActivityEdit(_id);
                            break;
                        case 'cancel':
                            _this.closeActivity();
                            break;
                    }

                });

                $view.on('click', '.attendee', function(e) {
                    e.preventDefault();
                    var contactId = $(this).attr('data-id');
                    eventEmitter.trigger('show:contact', {contactId: contactId});
                });

                $view.on('click', '.no-click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                });

                return $view;
            },

            /**
             * Set selects for more clearly UI
             * help to see each repeat day: day per month/per week
             */
            setUIRepeat: function(_dateToSet, _$view) {
                var curDate;
                if (_dateToSet) {
                    if (typeof _dateToSet === 'string') {
                        curDate = Date.parse(_dateToSet);
                    } else {
                        curDate = new Date(_dateToSet.year, _dateToSet.month, _dateToSet.date);
                    }
                } else {
                    curDate = new Date();
                }
                var curDayOfWeek = curDate.getDay();
                var curDayOfMonth = curDate.getDate();
                var daysInCurrentMonth = _this.getDaysInMonth(curDate.getFullYear(), curDate.getMonth());
                var daysOfWeekInMonth = _this.countDayOfWeekInCurrentMonth(curDate).daysOfWeekInMonth;
                var curDayOfWeekInMonth = _this.countDayOfWeekInCurrentMonth(curDate).curDayOfWeekInMonth;
                var selectOptions, selectHTML;

                switch (_$view.find('#repeat').val()) { // directly pass from selected option in dropdown
                    case '5': // MonthlyDate
                        selectOptions = _this.getOrdinalNumbersText(daysInCurrentMonth, curDayOfMonth - 1);
                        selectHTML = template.buildTemplate('activity/day-of-month-number', {options : selectOptions});
                        _$view.find('#day-of-month-number').replaceWith($(selectHTML));
                        // specific UI
                        _$view.find('#day-of-week-number').addClass('hide');
                        _$view.find('#day-of-week-name').addClass('hide');
                        _$view.find('#day-of-month-number').removeClass('hide');
                        break;
                    case '6': // MonthlyDay
                        selectOptions = _this.getOrdinalNumbersText(daysOfWeekInMonth, curDayOfWeekInMonth - 1);
                        selectHTML = template.buildTemplate('activity/day-of-week-number', {options : selectOptions});
                        _$view.find('#day-of-week-number').replaceWith($(selectHTML));
                        selectHTML = template.buildTemplate('activity/day-of-week-name', {options : _this.getDaysOfWeek(curDayOfWeek)});
                        _$view.find('#day-of-week-name').replaceWith($(selectHTML));
                        // specific UI
                        _$view.find('#day-of-week-number').removeClass('hide');
                        _$view.find('#day-of-week-name').removeClass('hide');
                        _$view.find('#day-of-month-number').addClass('hide');
                        break;
                }
            },

            /**
             * Retrieve calendar event object from BL, render it in edit template and attache event handlers
             * @param  {Number} id
             * @param  {Date} start
             * @param  {Date} end
             * @param  {Boolean} allDay
             * @return {Object} built view of the calendar event edit
             */
            setupActivityEdit: function(id, start, end, allDay) {

                var viewModel = {}, activity;

                // TODO: Use activity priority enums instead
                viewModel.priorities = [{
                    value: 1,
                    text: 'low'
                }, {
                    value: 2,
                    text: 'normal'
                }, {
                    value: 3,
                    text: 'high'
                }];

                // TODO: Use activity recurrence enums instead
                viewModel.recurrence = [{
                    value: 1,
                    text: 'Never'
                }, {
                    value: 2,
                    text: 'Daily'
                }, {
                    value: 3,
                    text: 'Weekdays'
                }, {
                    value: 4,
                    text: 'Weekly'
                }, {
                    value: 5,
                    text: 'Monthly (day of the month)'
                }, {
                    value: 6,
                    text: 'Monthly (day of the week)'
                }, {
                    value: 7,
                    text: 'Yearly'
                }];

                if (id) {
                    activity = activityManager.getById(id);
                } else if (start || end || allDay) {
                    activity = {
                        activityStartTime: start,
                        activityEndTime: end,
                        allDay: allDay
                    };
                }

                if (activity.priority)
                    for (var i = 0; i < viewModel.priorities.length; i++) {
                        if (viewModel.priorities[i].value == activity.priority) viewModel.priorities[i].selected = 'selected';
                    }

                if (activity.recurringType)
                    for (var k = 0; k < viewModel.recurrence.length; k++) {
                        if (viewModel.recurrence[k].value == activity.recurringType) viewModel.recurrence[k].selected = 'selected';
                    }

                viewModel.activity = activity;

                if (activity) {
                    viewModel.activityStartDateFormatted = moment(activity.activityStartTime).format('MMM DD, YYYY');
                    viewModel.activityEndDateFormatted = moment(activity.activityEndTime).format('MMM DD, YYYY');

                    viewModel.activityStartTimeFormatted = moment(activity.activityStartTime).format('hh:mm A');
                    viewModel.activityEndTimeFormatted = moment(activity.activityEndTime).format('hh:mm A');

                    if (activity.recurringType && activity.recurringType > 1 && activity.parentActivity.recurringStopDate) {
                        viewModel.activityStopDateFormatted = moment(activity.parentActivity.recurringStopDate).format('MMM DD, YYYY');
                    }
                } else {
                    viewModel.activityStartDateFormatted = moment().format('MMM DD, YYYY');
                    viewModel.activityEndDateFormatted = moment().format('MMM DD, YYYY');
                    viewModel.activityStopDateFormatted = moment().format('MMM DD, YYYY');

                    viewModel.activityStartTimeFormatted = moment().format('hh:mm A');
                    viewModel.activityEndTimeFormatted = moment().add('h', 1).format('hh:mm A');
                }

                viewModel.contacts = contactManager.getData();

                var contacts = [];
                var j, m, contactFullName;
                var formatInfo = function (name, value) {
                    var formatted = name;
                    if (value && value.length) {
                        formatted += ' \r\n(' + value + ')';
                    }

                    return formatted;
                };

                if (viewModel.contacts && viewModel.contacts.length > 0) {
                    for (j = 0; j < viewModel.contacts.length; j++) {
                        contactFullName = viewModel.contacts[j].fullName();

                        contacts.push({
                            value: viewModel.contacts[j].id + '_' + window.$wrtc.enums.modelTypes.Contact,
                            text: formatInfo(contactFullName),
                            type : window.$wrtc.enums.modelTypes.Contact
                        });

                        if (viewModel.contacts[j].hasPhones()) {
                            for (m = 0; m < viewModel.contacts[j].phoneNumbers.length; m++) {
                                contacts.push({
                                    value: viewModel.contacts[j].id + '_' + window.$wrtc.enums.modelTypes.ContactPhone + '_' + viewModel.contacts[j].phoneNumbers[m].id,
                                    text:  formatInfo(contactFullName, viewModel.contacts[j].phoneNumbers[m].value),
                                    type : window.$wrtc.enums.modelTypes.ContactPhone
                                });
                            }
                        }

                        if (viewModel.contacts[j].hasEmails()) {
                            for (m = 0; m < viewModel.contacts[j].emailAddress.length; m++) {
                                contacts.push({
                                    value: viewModel.contacts[j].id + '_' + window.$wrtc.enums.modelTypes.ContactEmail + '_' + viewModel.contacts[j].emailAddress[m].id,
                                    text: formatInfo(contactFullName, viewModel.contacts[j].emailAddress[m].value),
                                    type : window.$wrtc.enums.modelTypes.ContactEmail
                                });
                            }
                        }

                    }
                }

                //var $view = $(template.buildTemplate('activity/activity-edit', viewModel));
                var $view;
                if (arcus.diagnosticStatus.MetaDataStorageService === false) {
                    $view = $(template.buildTemplate('activity/activity-edit-error'));
                }

                else {
                    $view = $(template.buildTemplate('activity/activity-edit', viewModel));

                // add handlers

                var attendiesInput = $view.find('#attendies');
                var tagsinputData = attendiesInput.webrtc_tagsinput({
                    tagClass: function () {
                        return 'label label-info attendees-label';
                    },
                    itemValue: 'value', // should be unique for each tagsinput element
                    itemText: 'text',
                    typeahead: {
                        source: contacts
                    }
                });
                if (tagsinputData && tagsinputData.length) {
                    var $bootstrapInput = tagsinputData[0].$input;
                    $bootstrapInput.attr('name', 'bootstrapAttendies');
                    $bootstrapInput.on('paste', function (event) {
                        // hack to do native handler
                        // then resize the field
                        var clipboardData = event.originalEvent.clipboardData || window.clipboardData
                            , data = clipboardData.getData('Text')
                            , pasteLength = Math.max(data.length, $bootstrapInput.val().length);
                        $bootstrapInput.get(0).style.setProperty('width', (pasteLength < 3 ? 3 : pasteLength) + 'em', 'important');
                    });
                }

                $view.find('.bootstrap-tagsinput input').on('keypress', function(e){
                    if (e.keyCode === 13) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                });

                $view.find('.bootstrap-tagsinput input').on('keyup', function(e){
                    e.stopPropagation();
                    var value = $(this).val();
                    if (!value.length 
                        || e.keyCode === 38   // up arrow  
                        || e.keyCode === 40) { // down arrow
                         return;
                    }

                    if (value.indexOf(',') !== -1) {
                        var arr = value.split(',');
                        for (var i = 0; i < arr.length; i++) {
                            if (arr[i].length > 0) {
                                attendiesInput.webrtc_tagsinput('add', {
                                    value: arr[i] + '_' + window.$wrtc.enums.modelTypes.NA,
                                    text: arr[i],
                                    type : window.$wrtc.enums.modelTypes.NA
                                });
                            }
                        }
                        $(this).val('');
                    } else {
                        if (e.keyCode === 13) {
                            attendiesInput.webrtc_tagsinput('add', {
                                value: value + '_' + window.$wrtc.enums.modelTypes.NA,
                                text: value,
                                type : window.$wrtc.enums.modelTypes.NA
                            });
                            $(this).val('');
                        }
                        
                    }
                });

                var ct;
                if (activity.recurringType && activity.recurringType > 1) {
                    ct = activity.parentActivity.contacts;
                } else {
                    ct = activity.contacts;
                }

                if (ct && ct.length > 0) {
                    for (var c = 0; c < ct.length; c++) {
                        if (ct[c].id) {
                            if (ct[c].id !== '-1') {
                                var text = formatInfo(ct[c].name, ct[c].number),
                                    type = window.$wrtc.enums.modelTypes.NA,
                                    value = ct[c].number + '_' + window.$wrtc.enums.modelTypes.NA;

                                if (ct[c].number) {
                                    var result = contactManager.getContactByContactTypeValue(ct[c].number);
                                    if (result.contact) {
                                        switch (result.foundContactMethod.methodType) {
                                            case "PHONE":
                                                type = window.$wrtc.enums.modelTypes.ContactPhone;
                                                break;
                                            case "EMAIL":
                                                type = window.$wrtc.enums.modelTypes.ContactEmail;
                                                break;
                                        }
                                        value = result.contact.id + '_' + type + '_' + result.foundContactMethod.id;
                                        text = formatInfo(result.contact.fullName(), result.foundContactMethod.value);
                                    }
                                } else {
                                    var foundContact = contactManager.getById(ct[c].id);
                                    if (foundContact) {
                                        value = foundContact.id + '_' + window.$wrtc.enums.modelTypes.Contact;
                                        text = formatInfo(foundContact.fullName());
                                        type = window.$wrtc.enums.modelTypes.Contact;
                                    }
                                }

                                attendiesInput.webrtc_tagsinput('add', {
                                    value: value,
                                    text: text,
                                    type : type
                                });
                            } else {
                                attendiesInput.webrtc_tagsinput('add', {
                                    value: ct[c].number + '_' + window.$wrtc.enums.modelTypes.NA,
                                    text: ct[c].number,
                                    type : window.$wrtc.enums.modelTypes.NA
                                });
                            }
                        } 
                    }
                }

                $view.find('.data-action').on('click', function(event) {
                    // event.preventDefault();

                    var val = $(this).attr('data-val');
                    var id = $(this).closest('.data-container').attr('data-id');


                    switch (val) {
                        case 'save' :
                            //$('#activity-form').submit();
                            break;
                        case 'cancel':
                            event.preventDefault();
                            _this.closeActivity();
                            break;
                        case 'remove':
                            event.preventDefault();
                            if (activity.recurringType && activity.recurringType > 1) {
                                bootbox.dialog({
                                    message: '<span class="bigger-110">Do you want to delete all occurrences of this event, or only selected occurrence?</span>',
                                    buttons: {
                                        'cancel': {
                                            'label': 'Cancel',
                                            'className': 'btn-sm btn-success',
                                            'callback': function() {

                                            }
                                        },
                                        'danger': {
                                            'label': 'Delete All',
                                            'className': 'btn-sm btn-danger',
                                            'callback': function() {
                                                _this.showSidebarLoading();
                                                activityManager.removeActivity(id, true, function() {

                                                    _this.calendar.fullCalendar('removeEvents', function(evt) {
                                                        return evt.parentId == activity.parentActivity.id;
                                                    });
                                                    logger.log('Activity: ' + activity.description + ' with id: ' + id + ' has been removed');

                                                    _this.hideSidebarLoading();
                                                    _this.closeActivity();

                                                });

                                            }
                                        },
                                        'click': {
                                            'label': 'Delete Only This Event',
                                            'className': 'btn-sm btn-primary',
                                            'callback': function() {
                                                _this.showSidebarLoading();
                                                activityManager.removeActivity(id, false, function() {

                                                    _this.calendar.fullCalendar('removeEvents', id);
                                                    logger.log('Activity: ' + activity.description + ' with id: ' + id + ' has been removed');

                                                    _this.hideSidebarLoading();
                                                    _this.closeActivity();

                                                });

                                            }
                                        }
                                    }
                                });

                            } else {
                                bootbox.confirm('Are you sure you want to remove this event?', function(result) {
                                    if (result) {
                                        _this.showSidebarLoading();
                                        activityManager.removeActivity(id, false, function() {

                                            _this.calendar.fullCalendar('removeEvents', id);
                                            logger.log('Activity: ' + activity.description + ' with id: ' + id + ' has been removed');

                                            _this.hideSidebarLoading();
                                            _this.closeActivity();

                                        });
                                    }
                                });
                            }
                            break;
                    }

                });

                if (activity.allDay) {
                    $view.find('#not-all-day-data-container').addClass('hide');
                    $view.find('#all-day-data-container').removeClass('hide');
                } else {
                    $view.find('#all-day-data-container').addClass('hide');
                    $view.find('#not-all-day-data-container').removeClass('hide');
                }

                $view.find('#allDay').on('change', function(evt) {
                    if (evt.currentTarget.checked) {
                        $view.find('#not-all-day-data-container').addClass('hide');
                        $view.find('#all-day-data-container').removeClass('hide');
                        $view.find('#monthly-repeat-from-to').addClass('hide');
                        $view.find('#monthly-repeat-all-day').removeClass('hide');
                    } else {
                        $view.find('#all-day-data-container').addClass('hide');
                        $view.find('#not-all-day-data-container').removeClass('hide');
                        $view.find('#monthly-repeat-from-to').removeClass('hide');
                        $view.find('#monthly-repeat-all-day').addClass('hide');
                    }
                });

                if (activity.recurringType > 1) {
                    $view.find('#repeat-container').removeClass('hide');
                }

                // add validation rules
                var validatorEditForm;
                
                $.validator.addMethod('time', function(value) {
                    return value.match(/^(0?[1-9]|1[0-2]){1}:([0-5][0-9]) ([ap]m)$/i); // hh:mm AM/PM - pickerTimeFormat 
                });

                $.validator.addMethod('date', function(value) {
                    if(!value) return true;
                    return value.match(/^(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)\s+\d{1,2}\s*,?\s*\d{4}$/i); // mmm dd, yyyy - pickerDateFormat
                });

                $.validator.addMethod('eventDurationEnd', function() {
                    var timeStart = moment($('#timePickerFrom').val(), 'hh:mm A');
                    var timeEnd = moment($('#timePickerTo').val(), 'hh:mm A');
                    return timeEnd.isAfter(timeStart);
                }, 'Time End should be after then time Start');

                validatorEditForm = $view.find('#activity-form').validate({
                        errorClass: 'help-block',
                        errorElement: 'div',
                        highlight: function(e) {
                            $(e).closest('.validation-holder').addClass('has-error');
                        },
                        unhighlight: function(e) {
                            $(e).closest('.validation-holder').removeClass('has-error');
                        },
                        errorPlacement: function(error, element) {
                            error.insertAfter(element.closest('.validation-placement'));
                        },
                        submitHandler: function() {
                            if (this.valid()) {
                                _this.submitActivityForm();
                            } else {
                                this.form();
                            }
                        },
                        rules: {
                            activityName: { required: true },
                            timePickerFrom: { required: true, time: true},
                            timePickerTo: { required: true, time: true, eventDurationEnd: true },
                            datePickerFrom: { required: true, date: true },
                            datePickerTo: { required: true, date: true },
                            datePicker: { required: true, date: true }
                        },
                        messages: {
                            activityName: { required: 'Event name is required' },
                            timePickerFrom: { required: 'Time from is required', time: 'Enter time in format: hh:mm AM/PM'},
                            timePickerTo: { required: 'Time to is required', time: 'Enter time in format: hh:mm AM/PM' },
                            datePickerFrom: { required: 'Date from is required', date: 'Enter date in format: mmm dd, yyyy' },
                            datePickerTo: { required: 'Date to is required', date: 'Enter date in format: mmm dd, yyyy' },
                            datePicker: { required: 'Date is required', date: 'Enter date in format: mmm dd, yyyy' }
                        }
                });
                
                // init date and time pickers after details view transition in
                var datePicker, datePickerTo, datePickerFrom, timePickerFrom, timePickerTo, datePickerStop, datePickerHelper;
                var pickerContainer = $view.find('#id-activity-content');

                $view.find('.date-picker').pickadate({
                    format: _this.pickerDateFormat,
                    container: pickerContainer,
                    klass: {
                        active: '',
                        focused: ''
                    },
                    onStart: function() {
                        datePicker = this;
                        datePicker.set('select', viewModel.activityStartDateFormatted, { muted: true, format: _this.pickerDateFormat, fromValue: 'some fake text' }); // fromValue to parse month correctly
                    },
                    onOpen: function() {
                        [datePickerFrom, datePickerTo, timePickerFrom, timePickerTo, datePickerHelper].map(function(picker){ picker.close();});
                    },
                    onSet: function() {

                        $view.find('#datePicker').val(this.get('select', _this.pickerDateFormat));
                        validatorEditForm.element('#datePicker');

                        var selectedDate = this.get('select');

                        if (Date.parse(datePickerStop.get('select').obj) < Date.parse(selectedDate.obj)) {
                            datePickerStop.set('select', selectedDate.obj);
                        }
                        // set min date every time
                        datePickerStop.set('min', selectedDate.obj);

                        _this.setUIRepeat(datePicker.get('select') , $view);
                        // muted to avoid recursion
                        datePickerHelper.set('select', selectedDate.obj, {muted : true});
                    }
                });

                $view.find('.date-picker-from').pickadate({
                    format: _this.pickerDateFormat,
                    container: pickerContainer,
                    klass: {
                        active: '',
                        focused: ''
                    },
                    onStart: function() {
                        datePickerFrom = this;
                        datePickerFrom.set('select', viewModel.activityStartDateFormatted, { muted: true, format: _this.pickerDateFormat, fromValue: 'some fake text' }); // fromValue to parse month correctly
                    },
                    onOpen: function() {
                        [datePickerTo, timePickerFrom, timePickerTo, datePickerHelper].map(function(picker){ picker.close();});
                    },
                    onSet: function() {
                        var dateToInputValue = $view.find('#datePickerTo').val();
                        $view.find('#datePickerFrom').val(this.get('select', _this.pickerDateFormat));
                        validatorEditForm.element('#datePickerFrom');

                        var selectedDate = this.get('select');
                        if (Date.parse(dateToInputValue) < Date.parse(selectedDate.obj)) {
                            datePickerTo.set('select', selectedDate);
                        }
                        // set min date every time
                        datePickerTo.set('min', selectedDate);

                        if (Date.parse(datePickerStop.get('select').obj) < Date.parse(selectedDate.obj)) {
                            datePickerStop.set('select', selectedDate.obj);
                        }
                        // set min date every time
                        datePickerStop.set('min', selectedDate.obj);

                        _this.setUIRepeat(datePickerFrom.get('select') , $view);
                        // muted to avoid recursion
                        datePickerHelper.set('select', selectedDate.obj, {muted : true});
                    }
                });
                
                $view.find('.date-picker-to').pickadate({
                    format: _this.pickerDateFormat,
                    min: activity.activityStartTime,
                    container: pickerContainer,
                    klass: {
                        active: '',
                        focused: ''
                    },
                    onStart: function() {
                        datePickerTo = this;
                        datePickerTo.set('select', viewModel.activityEndDateFormatted, { muted: true, format: _this.pickerDateFormat, fromValue: 'some fake text' }); // fromValue to parse month correctly
                    },
                    onOpen: function() {
                        [datePickerFrom, timePickerFrom, timePickerTo, datePickerStop, datePickerHelper].map(function(picker){ picker.close();});
                    },
                    onSet: function() {
                        if (this.get('select')) {
                            var element = $view.find('#datePickerTo');
                            element.val(this.get('select', _this.pickerDateFormat));
                            validatorEditForm.element('#datePickerTo');
                        }
                    }
                });

                $view.find('#datePickerTo').on('keyup', function(e){
                    e.preventDefault();
                    var value = $(this).val();
                    if (moment(value, _this.pickerDateFormat).isValid()) {
                        datePickerTo.set('select', value, { muted: true, format: _this.pickerDateFormat });
                    } else {
                        datePickerTo.set('select', _this.newActivityEndDate, { muted: true });
                    }
                });

                $view.find('.date-picker-stop').pickadate({
                    format: _this.pickerDateFormat,
                    min: activity.activityStartTime,
                    container: pickerContainer,
                    klass: {
                        active: '',
                        focused: ''
                    },
                    onStart: function() {
                        datePickerStop = this;
                        datePickerStop.set('select', viewModel.activityStopDateFormatted, { muted: true, format: _this.pickerDateFormat, fromValue: 'some fake text' }); // fromValue to parse month correctly
                    },
                    onOpen: function() {
                        [datePickerTo, datePickerFrom, timePickerFrom, timePickerTo, datePickerHelper].map(function(picker){ picker.close();});
                    },
                    onSet: function() {
                        if (this.get('select')) {
                            var element = $view.find('#datePickerStop');
                            element.val(this.get('select', _this.pickerDateFormat));
                            validatorEditForm.element('#datePickerStop');
                        }
                    }
                });

                $view.find('#datePickerStop').on('keyup', function(e){
                    e.preventDefault();
                    var value = $(this).val();
                    if (moment(value, _this.pickerDateFormat).isValid()) {
                        datePickerStop.set('select', value, { muted: true, format: _this.pickerDateFormat });
                    } else {
                        datePickerStop.set('select', datePickerFrom.get('select'), { muted: true });
                    }
                });

                $view.find('.time-picker-from').pickatime({
                    format: _this.pickerTimeFormat,
                    interval: _this.intervalToPickers,
                    container: pickerContainer,
                    klass: {
                        active: '',
                        focused: ''
                    },
                    onStart: function() {
                        timePickerFrom = this;
                    },
                    onOpen: function() {
                        [datePickerTo, datePickerFrom, timePickerTo, datePickerStop, datePickerHelper].map(function(picker){ picker.close();});
                    },
                    onSet: function() {
                        var selectedTime = this.get('select');

                        $view.find('#timePickerFrom').val(this.get('select', _this.pickerTimeFormat));

                        if (!timePickerTo.get('select')  || timePickerTo.get('select').pick < selectedTime.pick) {
                            timePickerTo.set('select', selectedTime.pick + _this.intervalToPickers);
                        }
                        selectedTime.pick += _this.intervalToPickers;
                        timePickerTo.set('min', selectedTime);

                        validatorEditForm.element('#timePickerFrom');
                    }
                });

                $view.find('#timePickerFrom').on('keyup', function(e){
                    e.preventDefault();
                    var value = $(this).val();
                    var momentDate = moment(value, 'hh:mm A');
                    var newTime = _this.newActivityStartDate;
                    if ($(this).valid() && momentDate.isValid()) {
                        newTime = momentDate.toDate();
                    }

                    timePickerFrom.set('select', newTime , { muted: true });
                    timePickerFrom.set('highlight', newTime , { muted: true });

                    validatorEditForm.element('#timePickerTo');
                });

                $view.find('.time-picker-to').pickatime({
                    format: _this.pickerTimeFormat,
                    interval: _this.intervalToPickers,
                    container: pickerContainer,
                    onStart: function() {
                        timePickerTo = this;
                    },
                    onOpen: function() {
                        [datePickerTo, datePickerFrom, timePickerFrom, datePickerStop, datePickerHelper].map(function(picker){ picker.close();});
                    },
                    onSet: function() {
                        $view.find('#timePickerTo').val(this.get('select', _this.pickerTimeFormat));
                        validatorEditForm.element('#timePickerTo');
                    }
                });

                $view.find('#timePickerTo').on('keyup', function(e){
                    e.preventDefault();
                    var value = $(this).val();
                    var momentDate = moment(value, 'hh:mm A');
                    var newTime = _this.newActivityEndDate;
                    if (momentDate.isValid()) {
                        newTime = momentDate.toDate();
                    }

                    timePickerTo.set('select', newTime, { muted: true });
                    timePickerTo.set('highlight', newTime, { muted: true });
                });

                $view.find('.js-date-picker-helper').pickadate({
                    format: _this.pickerDateFormat,
                    container: pickerContainer,
                    klass: {
                        active: '',
                        focused: ''
                    },
                    onStart: function() {
                        datePickerHelper = this;
                        datePickerHelper.set('select', viewModel.activityStartDateFormatted, { muted: true, format: _this.pickerDateFormat, fromValue: 'some fake text' }); // fromValue to parse month correctly
                        _this.setUIRepeat(datePickerHelper.get('select'), $view);
                    },
                    onOpen: function() {
                        [datePickerTo, datePickerFrom, timePickerFrom, timePickerTo, datePickerStop].map(function(picker){ picker.close();});
                    },
                    onSet: function() {
                        _this.setUIRepeat(this.get('select'), $view);
                        datePickerFrom.set('select', this.get('select'));
                    }
                });

                var setMainRepeatUI = function(value) {
                    switch (value) {
                        case '2':case '3':case '4':case '7':
                        // common UI
                        $view.find('#repeat-container').removeClass('hide');
                        $view.find('#datePickerStop').rules( 'add', {
                            required: true,
                            date: true,
                            messages: { required: 'End repeat date is required', date: 'Enter valid date' }
                        });
                        $view.find('#monthly-repeat').addClass('hide');
                        break;
                        case '5': // MonthlyDate
                            // common UI

                            $view.find('#repeat-container').removeClass('hide');
//                            $('#datePickerStop').rules('remove', 'required date');
                            $view.find('#monthly-repeat').removeClass('hide');
                            // specific UI preparation
                            if (datePickerFrom) {
                                _this.setUIRepeat(datePickerFrom.get('select'), $view);
                            }
                            $view.find('#datePickerStop').rules( 'add', {
                                required: true,
                                date: true,
                                messages: { required: 'End repeat date is required', date: 'Enter valid date' }
                            });
                            break;
                        case '6': // MonthlyDay
                            // common UI

                            $view.find('#repeat-container').removeClass('hide');
//                            $('#datePickerStop').rules('remove', 'required date');
                            $view.find('#monthly-repeat').removeClass('hide');
                            // specific UI preparation
                            if (datePickerFrom) {
                                _this.setUIRepeat(datePickerFrom.get('select'), $view);
                            }
                            $view.find('#datePickerStop').rules( 'add', {
                                required: true,
                                date: true,
                                messages: { required: 'End repeat date is required', date: 'Enter valid date' }
                            });
                            break;
                        default : // 1
                            // common UI
                            $view.find('#repeat-container').addClass('hide');
                            $view.find('#datePickerStop').rules('remove', 'required date');
                            $view.find('#monthly-repeat').addClass('hide');
                            break;
                    }
                };

                $view.find('#repeat').on('change', function(evt) {
                    setMainRepeatUI(evt.currentTarget.value);
                });

                $view.find('#allDay').trigger('change');
                // init on edit existing activity
                if (viewModel.activity.recurringType === '5' || viewModel.activity.recurringType === '6') {
                    setMainRepeatUI(viewModel.activity.recurringType);
                }
            }
                
                return $view;
            },

            changeRepeatedActivityDialog: function(allCalback, onlyThisEventCallback, cancelCallback) {
                bootbox.dialog({
                    message: '<span class="bigger-110">Do you want to change only this occurrence of the event, or all occurrences?</span>',
                    buttons: {
                        'cancel': {
                            'label': 'Cancel',
                            'className': 'btn-sm btn-success',
                            'callback': cancelCallback || function(){}
                        },
                        'danger': {
                            'label': 'All',
                            'className': 'btn-sm btn-danger',
                            'callback': allCalback
                        },
                        'click': {
                            'label': 'Only This Event',
                            'className': 'btn-sm btn-primary',
                            'callback': onlyThisEventCallback
                        }
                    }
                });
            },

            /**
             * Submit activity form action
             */
            submitActivityForm: function() {
                var data = $('#activity-form').serializeObject();

                data.attendies = $('#attendies').webrtc_tagsinput('items');

                if (data.bootstrapAttendies) {
                    var checkStr = data.bootstrapAttendies.replace(/\s/g, '');
                    if (checkStr.length) {
                        data.attendies.push({
                            value: data.bootstrapAttendies + '_' + window.$wrtc.enums.modelTypes.NA,
                            text: data.bootstrapAttendies,
                            type : window.$wrtc.enums.modelTypes.NA
                        });
                    }
                }
                // prepare attendies ids
                data.attendies = data.attendies.map( function(item) {
                    var result = {}, contact, lastIndex = item.value.lastIndexOf('_'), contactMethod;

                    switch (item.type) {
                        case window.$wrtc.enums.modelTypes.Contact:
                            result.id = parseInt(item.value, 10); // from string "contactId_Contact"
                            contact = contactManager.getById(result.id);
                            if (contact) {
                                result.name = contact.fullName();
                            }
                            result.number = "";
                            break;
                        case window.$wrtc.enums.modelTypes.ContactPhone:
                            result.id = parseInt(item.value, 10); // from string "contactId_ContactPhone_ContactMethodId"
                            contact = contactManager.getById(result.id);
                            if (contact) {
                                result.name = contact.fullName();
                                contactMethod = contactManager.getContactMethod(contact, item.value.substring(lastIndex + 1));
                                if (contactMethod) {
                                    result.number = contactMethod.value;
                                }
                            }
                            break;
                        case window.$wrtc.enums.modelTypes.ContactEmail:
                            result.id = parseInt(item.value, 10); // from string "contactId_ContactEmail_ContactMethodId"
                            contact = contactManager.getById(result.id);
                            if (contact) {
                                result.name = contact.fullName();
                                contactMethod = contactManager.getContactMethod(contact, item.value.substring(lastIndex + 1));
                                if (contactMethod) {
                                    result.number = contactMethod.value;
                                }
                            }
                            break;
                        case window.$wrtc.enums.modelTypes.NA:
                            result.id = "-1";
                            result.name = "";
                            result.number = item.value.substring(0,lastIndex);
                            break;
                    }

                    return result;
                });

//                    .filter(onlyUnique);

                // if it is update action for recurring activity
                if (data.id && data.repeat && data.repeat > 1) {
                    var allCalback = function() {
                        _this.showSidebarLoading();
                        activityManager.saveActivity(data, true, function() {
                            _this.refetchEvents();
                            _this.hideSidebarLoading();
                            _this.closeActivity();
                        });
                    };

                    var onlyThisEventCallback = function() {
                        _this.showSidebarLoading();
                        activityManager.saveActivity(data, false, function(activity) {
                            var className;
                            if (activity.priority == 3) {
                                className = 'label-important';
                            } else if (activity.priority == 2) {
                                className = 'label-success';
                            } else {
                                className = '';
                            }

                            var evt = _this.selectedCalEvent;
                            evt.id = activity.id;
                            evt.title = activity.description;
                            evt.start = new Date(activity.activityStartTime);
                            evt.end = new Date(activity.activityEndTime);
                            evt.allDay = activity.allDay;
                            evt.className = className;

                            _this.calendar.fullCalendar('updateEvent', evt);

                            _this.hideSidebarLoading();
                            _this.closeActivity();

                        });
                    };
                    _this.changeRepeatedActivityDialog(allCalback, onlyThisEventCallback);
                } else {

                    _this.showSidebarLoading();

                    activityManager.saveActivity(data, false, function(activity) {

                        if (data.repeat && data.repeat > 1) {
                            _this.refetchEvents();
                        } else {

                            var className;
                            if (activity.priority == 3) {
                                className = 'label-important';
                            } else if (activity.priority == 2) {
                                className = 'label-success';
                            } else {
                                className = '';
                            }

                            if (data.id) {

                                var evt = _this.selectedCalEvent;
                                evt.id = activity.id;
                                evt.title = activity.description;
                                evt.start = new Date(activity.activityStartTime);
                                evt.end = new Date(activity.activityEndTime);
                                evt.allDay = activity.allDay;
                                evt.className = className;

                                _this.calendar.fullCalendar('updateEvent', evt);
                            } else {
                                _this.calendar.fullCalendar('renderEvent', {
                                        id: activity.id,
                                        title: activity.description,
                                        start: new Date(activity.activityStartTime),
                                        end: new Date(activity.activityEndTime),
                                        allDay: activity.allDay,
                                        className: className
                                    },
                                    true
                                );
                            }

                        }

                        _this.hideSidebarLoading();
                        _this.closeActivity();

                    });
                }
            },

            /**
             * Helper functions
             */

            /*
            * Resize init for calendar details view to fit the screen height
            * @param  {String} scrollName - name of the scroll to store
            * @param  {jQuery function} scrollableElement - element to attach scroll
            * @param  {jQuery function} scrollViewport - element to set height. If undefined - search element '.scroll-viewport' inside scrollableElement 
            */
            initScroll: function(scrollName, scrollableElement, scrollViewport) {
                _this.scrolls[scrollName] = {
                    scrollName: scrollName,
                    $scrollableElement: $(scrollableElement),
                    $scrollViewport: scrollViewport ? $(scrollViewport) : $(scrollableElement).closest('.scroll-viewport'),
                    resize: function() {
                        var elem = _this.scrolls[scrollName]
                            , $this = elem.$scrollViewport
                            , offsetTop = $this.offset().top
                            , pageHeight = window.$wrtc.getPageDimensions().height
                            , resultHeight = pageHeight - offsetTop - 15
                            ;
                        $this.height(resultHeight);
                        return resultHeight;
                    }
                };
            },

            /*
            * Resize calendar details view to fit the screen height
            * @param  {String} scrollName - name of scroll to resize. If undefined - resize all scrolls
            * @param  {NUmber} _height - height to set for current scroll
            */
            resizeScroll: function(scrollName, _height) {
                if (scrollName) {
                    return _this.scrolls[scrollName].resize(_height);
                } else {
                    for (var key in _this.scrolls) {
                        if (_this.scrolls.hasOwnProperty(key)) {
                            _this.scrolls[key].resize();
                        }
                    }
                }
            },

            /**
             * Insert calendar event detail/edit view into container and show it
             * @param  {[type]} $view of the calendar event
             */
            transitionSidebarIn: function($view) {
                $('#activity-details').html($view);
                $('#activityName').focus();
                $('#activity-details').removeClass('off-screen');
                _this.showLocalOverlay(function(){                    
                    _this.transitionSidebarOut();
                });
                _this.initScroll('activity-sidebar', $('#id-activity-content'));
                _this.resizeScroll();

                eventEmitter.on(eventEmitter.Event.VIEW_RESIZE, function() {
                    _this.resizeScroll();
                });
            },

            /**
             * Hide calendar event detail/edit container
             */
            transitionSidebarOut: function() {
                $('#activity-details').addClass('off-screen');
                _this.hideLocalOverlay();
                eventEmitter.off(eventEmitter.Event.VIEW_RESIZE, function() {
                    _this.resizeScroll();
                });
            },

            /**
            * Show overlay for calendar view
            * @param  {Function} done callback for closing overlay
            */
            showLocalOverlay: function(done) {
                var secondaryOverlay = $('#main-container .widget-box-overlay-secondary');
                secondaryOverlay.show();
                secondaryOverlay.on('click', function(e){
                    var sidebar = $('#sidebar');
                    e.preventDefault();
                    secondaryOverlay.off('click');
                    if (!sidebar.hasClass('off-screen')) {
                        eventEmitter.trigger('hide:contacts');
                        sidebar.addClass('off-screen');
                    }
                    if (done) {
                        done();
                    }
                });
            },

            /**
            * Hide overlay for calendar view
            */
            hideLocalOverlay: function() {
                $('#main-container .widget-box-overlay-secondary').hide();
            },


            /**
             * Fade out event details/edit view and show loading spinner
             */
            showSidebarLoading: function() {
                $.blockUI.defaults.css.cursor = 'default';
                $('#activity-details .widget-body').block({
                    css: {
                        marginTop: '-40%',
                        border: 'none',
                        backgroundColor: 'transparent',
                        cursor: 'auto'
                    },
                    overlayCSS: {
                        backgroundColor: 'white',
                        opacity: 0.7,
                        cursor: 'auto'
                    },
                    message: '<i class="icon-spinner icon-spin icon-2x green"></i>'
                });
            },

            /**
             * Fade in event details/edit view and hide loading spinner
             */
            hideSidebarLoading: function() {
                $('#activity-details').unblock();
            },

            /**
             * Display loading state for main callendar container
             * @param  {Boolean} showSpinner flag to show spinner or not
             */
            showLoading: function(showSpinner) {
                var spinner = $('#main-container .widget-box-overlay .icon-spinner');

                if (showSpinner) {
                    spinner.show();
                } else {
                    spinner.hide();
                }

                $('#main-container .widget-box-overlay').show();
            },

            /**
             * Hide loading state for main callendar container
             */
            hideLoading: function() {
                $('#main-container .widget-box-overlay').hide();
            }

        };
    };

    return activityController().init();
});
define('defaultController', ['logger', 'template', 'router', 'activityController', 'messageController', 'contactController', 'callController', 'notificationController', 'eventEmitter', 'diagnosticWorkMemoryController'],
    function(logger, template, router, activityController, messageController, contactController, callController, notificationController, EventBus, diagnosticWorkMemoryController) {
        var defaultController = function() {
            var _this;
            return {
                init: function() {
                    _this = this;
                    return _this;
                },

                /**
                 * Entry point for app to render any UI
                 * @param {Object} data -
                 */
                initApplicationUI: function() {
                    // set user info and show nav item
                    $('#user-name').text(localStorage.VoipTn);
                    $('#user-main-menu').removeClass('hidden');

                    // kick off main route handler - start with messages as main view
                    //_this.handleRoute('message', true);
                    diagnosticWorkMemoryController.checkStatus();
                    notificationController.bind();
                    // kick off phone loading last - it's very lightweight and should load as fast or faster than the other async events before it
                    callController.initCallWidget();

                    if (window.localStorage.getItem('RTCServiceEnabled') === 'true') {
                        // setup handlers
                        _this.setupHandlers();
                        _this.firstControllerIsLoaded = $.Deferred();
                        router.navigate();
                        // kick off contacts list - it's the longest loading
                        contactController.loadAndRenderContactData({routing : router, success : function () {
                            if (router.route && router.route.default) {

                                // we dont know when messages will be loaded, messages count could be 1 or 100000!!!
                                $.when(_this.firstControllerIsLoaded).then(function() {
                                    // slide the contact list in
                                    contactController.transitionMainContainerIn();

                                    // TEMP: add a transition for showing and hiding this with css
                                    setTimeout(function() {
                                        _this.showLoading(false);
                                    }, 450);
                                });
                            }
                        }});
                    } else {
                        _this.hideLoading();
                    }
                },

                setupHandlers: function() {

                    EventBus.on('LOADED', function(event, data) {
                        if (window.localStorage.getItem('RTCServiceEnabled') === 'true') {
                            if (data.title === 'Messages') {
                                _this.firstControllerIsLoaded.resolve();
                            }
                        }
                    });

                    //setup handlers when page is changes
                    EventBus.on(EventBus.Event.ROUTER_CHANGE, function() {
                        if (window.localStorage.getItem('RTCServiceEnabled') === 'true') {
                            _this.showLoading(true);
                        }
                    });

                    EventBus.on(EventBus.Event.ROUTER_AFTER_CHANGE, function(event, options) {
                        if (window.localStorage.getItem('RTCServiceEnabled') === 'true') {
                            $('#page-title').html(options.routeInfo.controller.title || 'Unknown page');

                            _this.swapSlidingContainerIn(options.view, options.initial || false);
                            _this.closeSlidingContainers();
                            _this.hideLoading();

                            if (options.routeName === router.pages.ACTIVITIES) {
                                $(window).trigger('resize');
                            }
                        }
                    });

                    // setup toggle handler for contacs in harmony with page
                    $('#toggle-contacts').on('click', function(e) {
                        var sidebar = $('#sidebar');
                        if (sidebar.hasClass('off-screen')) {
                            // show it
                            contactController.transitionMainContainerIn();

                            // TEMP: add a transition for showing and hiding this with css
                            setTimeout(function() {
                                _this.showLoading(false);
                            }, 450);
                        } else {
                            // hide it
                            contactController.transitionMainContainerOut();

                            // TEMP: add a transition for showing and hiding this with css
                            setTimeout(function() {
                                _this.hideLoading();
                            }, 500);
                        }
                        e.preventDefault();
                    });

                    window.onkeydown = function(event) {
                        messageController.scrollListByKeyCode(event.keyCode);
                    };
                },

                swapSlidingContainerIn: function(view) {
                    // show nav bar - TODO: move this upstream
                    $('#main-container').find('.header-nav').removeClass('app-hidden');

                    // slide content in
                    var body = $('#body-content');
                    body.find('.sliding-container').removeClass('active-container');

                    $(view).addClass('active-container');
                    body.html(view);
                },

                showLoading: function(showSpinner) {
                    // show or hide spinner depending
                    var spinner = $('#main-container .widget-box-overlay .icon-spinner');

                    if (showSpinner) {
                        spinner.show();
                    } else {
                        spinner.hide();
                    }

                    // always show the overlay
                    $('#main-container .widget-box-overlay').show();
                },

                hideLoading: function() {
                    $('#main-container .widget-box-overlay').hide();
                },

                hideSpinner: function() {
                    $('#main-container .widget-box-overlay .icon-spinner').hide();
                },

                closeSlidingContainers: function() {

                    //hiding contact list and phone widget
                    if ($('#phone-widget-btn').hasClass('open')) {
                        $('#phone-widget-btn').trigger(ace.click_event);
                    }

                    if (!$('#sidebar').hasClass('off-screen')) {
                        $('#toggle-contacts').trigger('click');
                    }

                    //hiding edit/details containers
                    activityController.transitionSidebarOut();

                    if (messageController.state.activeLayouts) {
                        messageController.hideSecondaryLayouts();
                    }

                    contactController.transitionDetailOut();
                }
            };
        };
        return defaultController().init();
    });
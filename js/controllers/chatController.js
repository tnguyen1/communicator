/*jshint unused:true */
define('chatController', ['logger', 'template', 'Arcus', 'audiotones'], function(logger, template, Arcus, audiotones) {
    var chatController = function() {
        var _this;
        return {

            title: "Chat",


            init: function() {
                if (!_this) {
                    _this = this;
                    _this.minLoadingTimeElapsed = false;
                    _this.minLoadingTime = 600; // used for "perceived working" - avoids choppy screen flashes
                    _this.pageLoadingDone = false;

                    template.registerPartial('chat/chat-conversation');
                    template.registerPartial('chat/chat-message-show');
                    template.registerPartial('chat/chat-new-message-notification');

                    _this.isTypingSendIntervalMs = 5000;
                    _this.isTypingShowIntervalMs = 6000;

                    //bind event handlers
                    this.handleNewChatMessageNotification = this.handleNewChatMessageNotification.bind(this);


                    $(Arcus.IMChat.imChatDictionaryInstance).on(Arcus.EventsEnum.IMChatEventType.IMChatJiggle,
                        this.handleNewChatMessageNotification);
                    $(Arcus.IMChat.imChatDictionaryInstance).on(Arcus.EventsEnum.IMChatEventType.IMChatNewMessage,
                        function() {
                            audiotones.play(audiotones.CHAT_IN);
                        });
                    $("#user-main-menu a[href='#/chat/']").click(this.handleChatTabClick);
                }

                return _this;
            },

            /**
             * handle nav bar chat tab navigation
             */
            handleChatTabClick: function(){
                var $chatTab = $("#chat-list-tab");
                var isSidebarClosed = $("#sidebar").hasClass('off-screen');
                var isSidebarChatTabActive = $chatTab.parent().hasClass('active');
                var activeConversation = Arcus.IMChat.getActiveSession();
                var unreadMessageCount = Arcus.IMChat.getTotalUnread();
                var activeConversationUnreadMessageCount = activeConversation ? activeConversation.getUnreadCount() : 0;

                if(unreadMessageCount - activeConversationUnreadMessageCount > 0){

                    if(isSidebarClosed) {
                        //open the contact list
                        $('#toggle-contacts').click();
                    }

                    if(!isSidebarChatTabActive){
                        //change focus to chat list
                        $chatTab.click();
                    }

                    //don't navigate to chat page
                    event.preventDefault();
                }

                if(!isSidebarClosed && isSidebarChatTabActive){
                    //close chat list
                    $('#toggle-contacts').click();
                }

            },

            /**
             * Update unread message count in top nav and set jiggle animation
             */
            handleNewChatMessageNotification: function(){
                ga('send', 'event', 'Chat', 'received');

                var activeSession = Arcus.IMChat.getActiveSession();
                if(!activeSession){
                    var keys = Object.keys(Arcus.IMChat.imChatDictionaryInstance);
                    if(keys.length === 1){
                        activeSession = Arcus.IMChat.imChatDictionaryInstance[keys[0]];
                    }
                }

                if($(".noconversation").length && activeSession){
                    //we are on the chat page currently with no open messages
                    this.setActiveSession(activeSession);
                    this.render();
                }else{
                    this.updateChatSessionNumber();
                    this.updateChatTabUnreadCount();
                }
            },

            /**
             * Update the number of unread chat messages displayed on chat tab in nav
             */
            updateChatTabUnreadCount:function(){
                var $newMessageBadge, $chatJiggle, unreadCount;

                $newMessageBadge = $(".conversation-new-message-badge");
                $chatJiggle = $(".chat-jiggle-locator");
                unreadCount = Arcus.IMChat.getTotalUnread();

                $chatJiggle.removeClass("jiggle");
                if (unreadCount > 0) {
                    $newMessageBadge.html(unreadCount);
                    $chatJiggle.addClass("jiggle");
                } else {
                    $newMessageBadge.html("");
                }
            },

            updateChatSessionNumber: function() {
                var $chatSessionList, $chatSessions, phoneNo, chatSession, $newMessageCount;

                $chatSessionList = $("#chat-session-list");
                $chatSessions = $chatSessionList.find(".list-item");
                $.each($chatSessions, function(index, $chatSession) {
                    phoneNo = $($chatSession).data("id");
                    chatSession = Arcus.IMChat.getChatSession(phoneNo);
                    $newMessageCount = $($chatSession).find(".new-message-count-locator");
                    $newMessageCount.html(chatSession.getUnreadCount());
                });
            },

            getViewHtml: function(){
                var view;
                var activeChatSession = Arcus.IMChat.getActiveSession();
                if (activeChatSession) {
                    var chatMessages = _this.getChatMessages(activeChatSession);

                    view = template.buildTemplate('chat/chat-conversation',
                        {
                            chatterUri: activeChatSession.chatterURI,
                            chatMessages: chatMessages
                        }
                    );
                } else {
                    view = template.buildTemplate('chat/chat-conversation',
                        {
                            chatterUri: _this.chatterUri
                        }
                    );
                }
                return view;
            },

            render: function(){
                var view = this.getViewHtml();
                var content = $('#body-content');
                content.html(view);
                this.setupHandlers(content);
            },

            buildMainView: function(options) {

                _this.pageLoadingDone = false;
                _this.minLoadingTimeElapsed = false;
                if (options.routing) {
                    _this.routing = options.routing;
                }

                var $view = $(this.getViewHtml());
                _this.setupHandlers($view);

                if (options && options.success) options.success($view);

                _this.hideLoading();
            },

            scrollToBottom: function() {
                var $dialogs = $(".dialogs");
                var dialogsElement = $dialogs[0];
                dialogsElement.scrollTop = dialogsElement.scrollHeight - $dialogs.height();
            },



            setupHandlers: function($chatView) {

                $chatView.find('#chatMessageSend').on('click', function() {
                    _this.sendMessage();
                });
                $chatView.find('#startChatContacts').on('click', function() {
                    $('#toggle-contacts').trigger('click');
                });

                $chatView.find('#chat-form').submit(function() {
                    _this.sendMessage();
                    return false;
                });

                $chatView.find('.icon-remove').on('click', function() {
                    this.setActiveSession(null, true);
                    $(this).trigger("ActiveChatClosed");
                }.bind(this));

                $chatView.find('#chat-message-text').on('input', _this.sendIsTyping);
            },

            /**
             * Display a chat session
             * @param {String} receiverUri phone number of the contact
             * @param {Object} [routing] router object
             */
            displayChat: function(receiverUri, routing) {
                //create new chat session if one doesn't exist
                var chatSession = Arcus.IMChat.getChatSession(receiverUri);
                if (!chatSession) {
                    chatSession = Arcus.IMChat.startChatSession(receiverUri, true);
                }

                _this.setActiveSession(chatSession);
                _this.chatterUri = receiverUri;

                if (_this.routing) {
                    _this.routing.navigateTo(routing.pages.CHAT);
                } else if (routing) {
                    routing.navigateTo(routing.pages.CHAT);
                }
                //focus chat input after render
                setTimeout(function() {
                    document.getElementById('chat-message-text').select();
                    _this.scrollToBottom();
                }, 0);
            },

            /***
             * Remove active conversation if it is clicked from active window
             *
             * @param phoneNo
             * @param routing
             */
            removeChat: function(phoneNo, routing) {
                var chatSession = Arcus.IMChat.getChatSession(phoneNo);
                if (chatSession === _this.currentChatSession) {
                    delete _this.currentChatSession;
                    delete _this.chatterUri;
                }


                if (chatSession) {
                    Arcus.IMChat.deleteChatSession(phoneNo);
                }
                this.updateChatTabUnreadCount();
                routing.navigate();
            },

            /**
             * Redraw a message in the active conversation
             * @param {Arcus.model.IMChatSession} chatSession
             * @param message
             */
            updateUISendMessage: function(chatSession, message) {
                if (_this.currentChatSession === chatSession) {
                    var $message = $("#message" + message.id);
                    if ($message) {
                        var chatMessage = _this.getChatMessage(chatSession, message);
                        var messageHtml = template.buildTemplate('chat/chat-message-show', chatMessage);
                        $message.replaceWith(messageHtml);
                    }
                }
            },

            /***
             * Append a new message in the conversation GUI
             * @param {Arcus.model.IMChatSession} imChatSession
             * @param message
             */
            renderNewMessage: function(imChatSession, message) {
                var $chatMessages = $('#chatMessages');

                if ($chatMessages && $chatMessages.length > 0) {
                    var chatMessage = _this.getChatMessage(imChatSession, message);
                    var $messageHtml = $(template.buildTemplate('chat/chat-message-show', chatMessage));

                    $chatMessages.append($messageHtml);
                    _this.scrollToBottom();
                }
            },

            /***
             * Send a new message to the other party
             */
            sendMessage: function() {
                var $chatMessageText, messageText, chatContainer, chatterPhoneNo;

                $chatMessageText = $('#chat-message-text');
                messageText = $chatMessageText.val();
                $chatMessageText.val('');

                chatContainer = $("#chat-container");
                chatterPhoneNo = chatContainer.data("chatter-uri");

                if (messageText) {
                    var chatSession = Arcus.IMChat.getChatSession(chatterPhoneNo);

                    this.setActiveSession(chatSession);

                    Arcus.IMChat.sendChat(chatterPhoneNo,
                        function() {
                            //chat sent sound
                            audiotones.play(audiotones.CHAT_OUT);
                            ga('send', 'event', 'Chat', 'send');
                        }, function() {
                            //chat failure sound
                            audiotones.play(audiotones.MSG_FAIL);
                            ga('send', 'event', 'Chat', 'error', 'failed to send');
                        }, messageText);


                    this.renderNewMessage(chatSession, chatSession.chatMessages[chatSession.chatMessages.length - 1]);
                }
            },

            /***
             * Set the active session
             *
             * @param imChatSession
             * @param renderRequired
             */
            setActiveSession: function(imChatSession, renderRequired) {
                Arcus.IMChat.setActiveSession(imChatSession);

                if (_this.currentChatSession !== imChatSession) {
                    if (_this.currentChatSession) {
                        $(_this.currentChatSession).off();
                    }

                    _this.currentChatSession = imChatSession;
                    if (_this.currentChatSession) {
                        $(_this.currentChatSession).on(Arcus.EventsEnum.IMChatEventType.IMChatNewMessage, function(event, message) {
                            _this.renderNewMessage(_this.currentChatSession, message);
                            _this.showLastMessageReceivedDT();
                        });
                        $(_this.currentChatSession).on(Arcus.EventsEnum.IMChatEventType.IMChatTyping, function() {
                            _this.showIsTyping();
                        });
                        $(_this.currentChatSession).on(Arcus.EventsEnum.IMChatEventType.IMChatSendStatus, function(event, chatSession, updatedMessage) {
                            //Update the message as success or failure
                            _this.updateUISendMessage(chatSession, updatedMessage);
                        });
                    }
                }

                if (renderRequired) {
                    delete _this.chatterUri;
                    _this.routing.navigate();
                }
            },

            showLoading: function(showSpinner) {
                // show or hide spinner depending
                var spinner = $('#main-container').find('.widget-box-overlay .icon-spinner');

                if (showSpinner) {
                    spinner.show();
                }
                else {
                    spinner.hide();
                }

                // always show the overlay
                $('#main-container .widget-box-overlay').show();
            },

            hideLoading: function() {
                $('#main-container .widget-box-overlay').hide();
            },

            /***
             * Return an array of unread chat messages for GUI to display
             *
             * @param imChatSession
             * @param [unread] if true, only returns new messages
             * @returns {Array}
             */
            getChatMessages: function(imChatSession, unread) {
                var messageTexts, chatMessage, chatMessages = [];

                // getChatMessage function will set isRead = true on all messages, this var tracks how many were unread
                var unreadMessageCount = imChatSession.getUnreadCount();
                messageTexts = imChatSession.getMessages(unread);
                if (messageTexts.length > 0) {
                    $.each(messageTexts, function(index, message) {
                        chatMessage = this.getChatMessage(imChatSession, message);
                        chatMessages.push(chatMessage);
                    }.bind(this));
                }

                if(unreadMessageCount){
                    this.updateChatTabUnreadCount();
                    this.updateChatSessionNumber();
                }
                return chatMessages;
            },

            /***
             * Convert a IMChatMessageTextWithStamp to a message for GUI to display
             *
             * @param {Arcus.model.IMChatSession} imChatSession
             * @param message
             * @returns {Object}
             */
            getChatMessage: function(imChatSession, message) {
                var chatMessage, messageSender;

                message.isRead = true;
                if (message.chatDirection === Arcus.model.IMChatMessageTextWithStamp.ChatDirection.INBOUND) {
                    messageSender = imChatSession.receiverInfo;
                } else {
                    messageSender = imChatSession.meInfo;
                }
                chatMessage = $.extend({
                    messageTime: message.timeStamp.toLocaleString(),
                    messageSender: messageSender,
                    messageSenderPhoneNo: imChatSession.chatterURI,
                    chatterAvatar: (messageSender && messageSender.images && messageSender.images.length) ? messageSender.images[0].imageUrl: undefined
                }, message);

                return chatMessage;
            },

            sendIsTyping: function() {
                var now, $chatContainer, chatterPhoneNo;

                //If nothing has been sent and no message received so far, it is not needed to send IsTyping
                if (_this.currentChatSession.lastAnyMessageSentDT || _this.currentChatSession.lastMessageReceivedDT) {
                    if (_this.sendIsTypingTimer) {
                        clearTimeout(_this.sendIsTypingTimer);
                        _this.sendIsTypingTimer = null;
                    }

                    now = new Date();
                    if (_this.currentChatSession.lastAnyMessageSentDT &&
                        (now.getTime() - _this.currentChatSession.lastAnyMessageSentDT.getTime() < _this.isTypingSendIntervalMs)) {
                        _this.sendIsTypingTimer = setTimeout(function() {
                            _this.sendIsTypingTimer = null;
                            _this.sendIsTyping();
                        }, _this.isTypingSendIntervalMs - (now.getTime() - _this.currentChatSession.lastAnyMessageSentDT.getTime()));
                    } else {
                        $chatContainer = $("#chat-container");
                        chatterPhoneNo = $chatContainer.data("chatter-uri");

                        Arcus.IMChat.sendIsTyping(chatterPhoneNo,
                            function() {
                            }, function() {
                            });
                    }
                }
            },

            showIsTyping: function() {
                if (_this.clearIsTypingTimer) {
                    clearTimeout(_this.clearIsTypingTimer);
                    _this.clearIsTypingTimer = null;
                }

                //If no message has been received and nothing has been sent to that person so far, it is not needed to show IsTyping
                if (_this.currentChatSession &&
                    (_this.currentChatSession.lastMessageReceivedDT || _this.currentChatSession.lastAnyMessageSentDT)) {
                    _this.displayMessageStatus(Arcus.model.IMChat.ChatType.IS_COMPOSING);
                    _this.clearIsTypingTimer = setTimeout(function() {
                        _this.clearIsTypingTimer = null;
                        _this.displayMessageStatus(Arcus.model.IMChat.ChatType.SEND_CHAT);
                    }, _this.isTypingShowIntervalMs);
                }
            },

            showLastMessageReceivedDT: function() {
                if (_this.clearIsTypingTimer) {
                    clearTimeout(_this.clearIsTypingTimer);
                    _this.clearIsTypingTimer = null;
                }

                _this.displayMessageStatus(Arcus.model.IMChat.ChatType.SEND_CHAT);
            },

            displayMessageStatus: function(chatType) {
                var statusMessage;

                switch (chatType) {
                    case Arcus.model.IMChat.ChatType.SEND_CHAT:
                        if (_this.currentChatSession.lastMessageReceivedDT) {
                            statusMessage = "Last message received on " + _this.currentChatSession.lastMessageReceivedDT.toLocaleString();
                        } else {
                            statusMessage = "&nbsp;";
                        }
                        break;
                    case Arcus.model.IMChat.ChatType.IS_COMPOSING:
                        if(_this.currentChatSession.receiverInfo){
                            statusMessage = _this.currentChatSession.receiverInfo.firstName + " " +
                                _this.currentChatSession.receiverInfo.lastName;
                        }else if(_this.currentChatSession.chatterURI){
                            statusMessage = _this.currentChatSession.chatterURI;
                        }else{
                            statusMessage = "Unknown";
                        }
                        statusMessage += " is typing a message...";
                        break;
                }

                $("#chatMessageStatus").html(statusMessage);
            }
        };
    };
    return chatController().init();
});

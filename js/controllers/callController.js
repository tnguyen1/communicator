define('callController',
            ['Arcus', 'logger', 'template', 'config', 'audiotones', 'contactManager', 'bootbox', /* hidden use => */ 'blockUI', 'utils', 'transit'],
    function(Arcus, logger, template, config, audiotones, contactManager, bootbox) {
    //var URLPlugin = 'http://137.107.64.101:8580/gencomweb-plugin/GCFWEnabler.exe?v=0.8108910122680175';
    var callController = function() {
        var _this,
            _callErrors = {
                1: "Network failure",
                2: "Authentication / Authorization failure",
                3: "State failure",
                4: "Privilege failure",
                9: "Unknown failure"
            };

        return {
            intraframe: null,

            /**
             * Controller initialization
             * @returns {callController}
             */
            init: function() {
                _this = this;
                _this.currentCalls = {};
                _this.numberOfCalls = 0;
                _this.activeCall = null;
                _this.incomingCall = null;
                _this.pluginNotPresent = false;
                _this.errorObject = { errorType : 'info' }; // info || warning || danger
                _this.audioContext = window.AudioContext || window.webkitAudioContext ||
                    window.mozAudioContext || window.oAudioContext || window.msAudioContext;

                _this.videoQuality = "320x240";

                template.registerPartial('call/call-dialpad');
                template.registerPartial('call/call-active');
                template.registerPartial('call/call-incoming');
                template.registerPartial('call/call-outgoing');
                template.registerPartial('call/call-settings');
                template.registerPartial('call/call-error');
                template.registerPartial('call/call-download-plugin');

                //init block ui
                $.blockUI.defaults.css.border = 'none';
                $.blockUI.defaults.css.backgroundColor = 'rgba(0,0,0,0)';
                $.blockUI.defaults.overlayCSS.opacity = 0.1;
                $.blockUI.defaults.message = '<i class="icon-spinner icon-spin icon-2x green"></i>';

                return _this;
            },

            /**
             * Do additional initialization for call widget (render template)
             */
            initCallWidget: function() {

                // setup fcs config
                fcs.setup(config.fcsApiSettings());
                logger.log('initCallWidget --> fcs.setup');

                // render template
                var callHTML = template.buildTemplate('call/call-widget', {})
                    , $callPlace = $('#phone-widget-content');

                if (callHTML && $callPlace.length) {
                    $callPlace.html(callHTML);

                    // init media
                    _this.initMedia();

                    _this.initHandlers();

                    //init scroll
                    //$('.active-call-list').slimScroll({height: 239});
                }
            },

            /**
             * Setup handlers for user event
             */
            initHandlers: function() {
                // setup tab listeners
                $('#phone-widget-content').find('.phone-tab-btn').on('click', function() {
                    // get radio value
                    var target = $(this).find('input[type=radio]');
                    var which = target.val();

                    // swap visibilities
                    $('#phone-widget-content').find('.phone-tab').hide();
                    $('#phone-widget-content').find('#phone-tab-' + which).show();
                });

                // setup call button handlers
                $('.phone-dialpad-content').find('.btn-voice-call').on('click', function() {
                    ga('send', 'event', 'call', 'dialed', 'voice');
                    _this.makeCall(false);
                });

                $('.phone-dialpad-content').find('.btn-video-call').on('click', function() {
                    ga('send', 'event', 'call', 'dialed', 'video');
                    _this.makeCall(true);
                });

                $(document).on('click', '.btn-stop-call', function() {
                    ga('send', 'event', 'call', 'ended', 'hangup', _this.getCallDurationSeconds());
                    _this.endCall();
                });

                $(document).on('click', '.btn-answer-call', function() {
                    _this.answer(false);
                    ga('send', 'event', 'call', 'answer', 'voice');
                });

                $(document).on('click', '.btn-video-answer-call', function() {
                    _this.answer(true);
                    ga('send', 'event', 'call', 'answer', 'video');
                });

                $(document).on('click', '.btn-decline-call', function() {
                    _this.reject();
                    ga('send', 'event', 'call', 'decline');
                });

                $(document).on('click', '.btn-call-hold', function() {

                    var $holdBtn = $(this)
                        , $holdBtnIcon = $holdBtn.find('i')
                        , callOnHold = $holdBtn.data('hold')
                        , $muteBtn = $('#phone-widget-call-toolbar').find('.btn-call-mute')
                        , $muteBtnIcon = $muteBtn.find('i');

                    if (_this.localMediaStream) {
                        var $activeCallWidget = $('#call-widget-active')
                            , remoteVideo = $activeCallWidget.find('.remoteVideo').get(0)
                            , localVideo = $activeCallWidget.find('.localVideo').get(0);

                        if (remoteVideo.paused) {
                            $holdBtnIcon.removeClass('icon-play').addClass('icon-pause');
                            remoteVideo.play();
                            localVideo.play();
                        } else {
                            $holdBtnIcon.removeClass('icon-pause').addClass('icon-play');
                            remoteVideo.pause();
                            localVideo.pause();

                            $muteBtnIcon.removeClass('icon-microphone').addClass('icon-microphone-off');
                            remoteVideo.muted = true;
                            localVideo.muted = true;
                            if (_this.volumeNode) {
                                _this.volumeNode.gain.value = 0;
                            }
                        }
                    } else {
                        if (callOnHold) {
                            //resume call
                            _this.hold(!callOnHold, function() {
                                $holdBtn.data('hold', !callOnHold);
                                $holdBtnIcon.removeClass('icon-play').addClass('icon-pause');
                                $holdBtn.removeClass('active');
                                $('.active-call-list-header').text('Currently Speaking');
                            });

                        } else {
                            //place call on hold
                            _this.hold(!callOnHold, function() {
                                $holdBtn.data('hold', !callOnHold);
                                $holdBtnIcon.removeClass('icon-pause').addClass('icon-play');
                                $holdBtn.addClass('active');
                                $('.active-call-list-header').text('ON HOLD');
                            });
                        }
                    }
                });

                $(document).on('click', '.btn-call-mute', function() {

                    var $muteBtn = $(this)
                        , $muteBtnIcon = $muteBtn.find('i')
                        , callOnMute = $muteBtn.data('mute');

                    if (_this.localMediaStream) {

                        var $activeCallWidget = $('#call-widget-active')
                            , remoteVideo = $activeCallWidget.find('.remoteVideo').get(0)
                            , localVideo = $activeCallWidget.find('.localVideo').get(0);

                        if (remoteVideo.muted) {
                            $muteBtnIcon.removeClass('icon-microphone-off').addClass('icon-microphone');
                            remoteVideo.muted = false;
                            localVideo.muted = false;
                            if (_this.volumeNode) {
                                _this.volumeNode.gain.value = 1;
                            }
                        } else {
                            $muteBtnIcon.removeClass('icon-microphone').addClass('icon-microphone-off');
                            remoteVideo.muted = true;
                            localVideo.muted = true;
                            if (_this.volumeNode) {
                                _this.volumeNode.gain.value = 0;
                            }
                        }
                    } else {
                        if (_this.activeCall && _this.activeCall.mute && _this.activeCall.unmute) {
                            if (callOnMute) {
                                //unmute call
                                _this.mute(false);
                                $muteBtn.data('mute', !callOnMute);
                                $muteBtnIcon.removeClass('icon-microphone').addClass('icon-microphone-off');
                                $muteBtn.removeClass('active');
                            }
                            else {
                                //mute call
                                _this.mute(true);
                                $muteBtn.data('mute', !callOnMute);
                                $muteBtnIcon.removeClass('icon-microphone-off').addClass('icon-microphone');
                                $muteBtn.addClass('active');
                            }
                        }
                    }

                });

                $(document).on('click', '.btn-call-toggle-video', function() {
                    var $videoBtn = $(this)
                        , videoIsShown = $videoBtn.data('video');

                    if (_this.localMediaStream) {
                        var $icon = $videoBtn.find('i')
                            , localVideoData = $videoBtn.data('localVideo');

                        if (localVideoData === undefined) {
                            $videoBtn.data('localVideo', true);
                            localVideoData = $videoBtn.data('localVideo');
                        }

                        var $activeCallWidget = $('#call-widget-active')
                            , remoteVideo = $activeCallWidget.find('.remoteVideo').get(0)
                            , localVideo = $activeCallWidget.find('.localVideo').get(0);

                        if (!localVideoData) {
                            $icon.addClass('active');
                            remoteVideo.src = _this.localSource;
                            localVideo.src = _this.localSource;
                            $videoBtn.data('localVideo', true);
                        } else {
                            $icon.removeClass('active');
                            remoteVideo.src = "";
                            localVideo.src = "";
                            $videoBtn.data('localVideo', false);
                        }
                    } else {
                        _this.setVideo(videoIsShown, function() {

                            if (videoIsShown) {
                                $videoBtn.addClass('active');
                            }
                            else {
                                $videoBtn.removeClass('active');
                            }

                            $videoBtn.data('video', !videoIsShown);
                        });
                    }

                });

                $(document).on('click', '.btn-close-error', function() {
                    _this.toggleError(false);
                });

                $(document).on('click', '.call-item', function() {

                    var callId = $(this).data('id');
                    _this.switchTo(callId);
                });

                $(document).on('click', '.js-keypad', function() {
                    var $this = $(this),
                        dialData = $this.data('dial-tone-frq'),
                        key = $this.text(),
                        phoneNumber = $('.dialpad-input').val();

                    phoneNumber += key;
                    if (_this.activeCall) {
                        _this.activeCall.sendDTMF(key);
                    }
                    if (dialData) {
                        var frequencies = dialData.split(',').map(function(strFrq) { return strFrq.trim();}) || [];
                        if (frequencies.length > 1) {
                            audiotones.dialTonePlay(frequencies[0], frequencies[1]);
                        }
                    }

                    $('.dialpad-input').val(phoneNumber);
                });

                $(document).on('keypress', '.dialpad-input', function(event) {
                    var theEvent = event || window.event;
                    var keyCode = theEvent.keyCode || theEvent.which;
                    var keyStr = String.fromCharCode( keyCode );
                    var regexKey = /[0-9]||[#]||[*]]/;
                    var $phoneWidget = $(this).closest('.phone-widget-screen');
                    if (regexKey.test(keyStr) && $phoneWidget.length) {
                        if (_this.activeCall) {
                            _this.activeCall.sendDTMF(keyStr);
                        }
                        var $dialButton = $phoneWidget.find('.js-keypad[title="' + keyStr + '"]')
                            , dialData = $dialButton.data('dial-tone-frq');
                        if (dialData) {
                            var frequencies = dialData.split(',').map(function(strFrq) { return strFrq.trim();}) || [];
                            if (frequencies.length > 1) {
                                audiotones.dialTonePlay(frequencies[0], frequencies[1]);
                            }
                        }
                    }
                });

                $(document).on('click', '.btn-keypad-backspace', function() {
                    var phoneNumber = $('.dialpad-input').val();
                    if ($.isNotNullOrEmpty(phoneNumber)) {
                        $('.dialpad-input').val(phoneNumber.slice(0, -1));
                    }
                    return false;
                });

                $(document).on('click', '.btn-keypad-clear', function() {
                    $('.dialpad-input').val('');
                    return false;
                });

                $(document).on('click', '.remoteVideo', function() {

                    if($(this).data("maximize")) {
                        _this.minimizeVideo();
                    }
                    else {
                        _this.maximizeVideo();
                    }
                });

                $(window).resize(function() {
                    _this.videoResize();
                });
            },

            /**
             * Prepare call widget to make call (if we choose call from contact list)
             * @param {string} number - The number to be called
             */
            initCall: function(number) {
                // make sure the phone widget is open
                var window = $('#phone-widget-container');
                if (!window.hasClass('open')) {
                    window.find('#phone-widget-btn').trigger(ace.click_event);
                }

                // set dialpad to be the active tab
                $('.phone-tab-btn.js-dialpad-tab').trigger('click');

                // set the number to the number requested
                $('#phone-widget-content').find('.dialpad-input').val(number);
            },

            /**
             * Toggle phone ringing animation for incoming calls
             * @param {boolean} isRinging - Indicate do we need turn on|off animation
             */
            togglePhoneRinging: function(isRinging) {

                logger.log('togglePhoneRinging --> ring ring...');

                // TODO: do more than animate the phone icon - make it more prominent

                if (isRinging) {
                    $('#phone-widget-btn > .icon-phone').addClass('icon-animated-bell');
                }
                else {
                    $('#phone-widget-btn > .icon-phone').removeClass('icon-animated-bell');
                }
            },

            /**
             * Preparing to do call
             * @param {boolean} isVideoCall - Indicate should we start video call or not
             */
            makeCall: function(isVideoCall) {

                if (_this.activeCall !== null && !_this.activeCall.onHold) {

                    _this.hold(true, function() {
                        _this.doCall(isVideoCall);
                    });
                }
                else {
                    _this.doCall(isVideoCall);
                }
            },

            /**
             * Doing call
             * @param {boolean} isVideoCall - Indicate should we start video call or not
             */
            doCall: function(isVideoCall) {

                _this.toggleLoading(true);

                // get all necessary vars
                var currentUser = fcs.getUser(),
                    userParts = currentUser.split("@"),
                    phoneNumber = $('.phone-dialpad-content').find('input').val(),
                    phoneNumberToCall = _this.stripCallId(phoneNumber),
                    phoneNumberToFindContact = phoneNumber.replace(/-/g, "");

                if ((phoneNumberToCall.indexOf("@") === -1) && userParts.length === 2) {
                    phoneNumberToCall = phoneNumberToCall + "@" + userParts[1];
                }

                var contact = contactManager.getContactByPhoneNumber(phoneNumberToFindContact);
                var useVideo = true;

                logger.log('About to call ' + phoneNumber);

                var domain = localStorage.getItem("Domain") ? localStorage.getItem("Domain"): "";
                var numToCall = !/@/.test(phoneNumberToCall) ?  phoneNumberToCall + "@" + domain : phoneNumberToCall;

                fcs.call.startCall(
                    currentUser,
                    null,
                    numToCall,

                    function(outgoingCall) {
                        var callId = outgoingCall.getId();

                        outgoingCall.callerName = "";
                        outgoingCall.callerNumber = phoneNumber;

                        outgoingCall.preCall = true;
                        outgoingCall.incoming = false;

                        outgoingCall.contact = contact;
                        outgoingCall.startTime = null;
                        outgoingCall.onHold = false;
                        outgoingCall.isVideoCall = isVideoCall;

                        outgoingCall.onStateChange = function(state, statusCode) {
                            //Add statusCode that returned from the server property to the call
                            outgoingCall.statusCode = statusCode;
                            logger.log('outgoingCall status changed' + statusCode);
                            _this.onStateChange(outgoingCall, state);
                        };

                        outgoingCall.onStreamAdded = function(streamURL) {

                            //the remote video stream are added for current call

                            logger.log("Outgoing call remote stream added: " + streamURL);

                            if( streamURL ){
                                outgoingCall.remoteStreamURL = streamURL;
                                _this.setRemoteStream(streamURL);
                                if (_this.activeCall.isVideoCall) {
                                    _this.toggleLocalVideo(true);
                                }
                            }
                        };

                        _this.currentCalls[callId] = outgoingCall;
                        _this.numberOfCalls++;
                        _this.activeCall = outgoingCall;

                        if (isVideoCall) {
                            _this.setLocalStream();
                        }

                        _this.toggleLoading(false);
                        _this.toggleOutgoingCall(true);

                    },
                    function(errorMessage) {
                        _this.toggleLoading(false);
                        if (errorMessage === 2) {
                            logger.log("CREATE_PEER_FAILED", "error");

                        }
                        else {
                            logger.log("CALL_FAILED", "error");
                            ga('send', 'event', 'call', 'error', 'start call failed');
                        }

                        _this.displayError("Call failed", errorMessage);
                    },
                    useVideo,
                    isVideoCall,
                    _this.videoQuality
                );
            },

            /**
             * Preparing to answering to the incoming call
             * @param {boolean} isVideoCall - Indicate should we start video call or not
             */
            answer: function(isVideoCall) {

                if (_this.activeCall !== null && !_this.activeCall.onHold) {

                    _this.hold(true, function() {
                        _this.doAnswer(isVideoCall);
                    });
                }
                else {
                    _this.doAnswer(isVideoCall);
                }
            },

            /**
             * Do answer to the incoming call
             * @param {boolean} isVideoCall - Indicate should we start video call or not
             */
            doAnswer: function(isVideoCall) {
                // stop ringing and show loading
                audiotones.stop(audiotones.RING_IN);
                _this.toggleLoading(true);

                if (_this.incomingCall) {
                    _this.incomingCall.answer(
                        function() {
                            _this.activeCall = _this.incomingCall;
                            _this.activeCall.preCall = false;
                            _this.currentCalls[_this.activeCall.getId()] = _this.activeCall;
                            _this.numberOfCalls++;

                            _this.incomingCall = null;

                            _this.activeCall.isVideoCall = isVideoCall;

                            if (isVideoCall) {
                                _this.setLocalStream();
                            }

                            _this.startTimer();
                            _this.togglePhoneRinging(false);
                            _this.toggleIncomingCall(false);
                            _this.updateCallView();

                            _this.toggleLoading(false);
                            // uncomment if we need to show video from remote client in audio answer
//                            if (_this.activeCall.canReceivingVideo()) {
//                                _this.activeCall.videoStart();
//                            }
                        },
                        function(error) {
                            _this.toggleLoading(false);
                            _this.displayError("Answering failed", error);
                            ga('send', 'event', 'call', 'error', 'answer failed');
                        },
                        isVideoCall,
                        _this.videoQuality
                    );
                }
            },

            /**
             * Ending the active call
             */
            endCall: function() {

                if (_this.activeCall && _this.activeCall.end) {

                    _this.toggleLoading(true);

                    audiotones.stop(audiotones.RING_OUT);

                    _this.activeCall.end(
                        function() {
                            logger.log('Call ended successfully');

                            _this.toggleLoading(false);
                            _this.deleteCall(_this.activeCall.getId());
                            _this.toggleOutgoingCall(false);
                        },
                        function() {
                            logger.log('Error ending call');
                            ga('send', 'event', 'call', 'error', 'end call failed');
                            _this.toggleLoading(false);
                        });
                } else {
                    if (_this.localMediaStream) {

                        _this.toggleLoading(true).done(function() {
                            _this.activeCall = null;
                            _this.updateCallView();

                            if (_this.localMediaStream.stop) {
                                _this.localMediaStream.onended = function() {
                                    _this.toggleLoading(false);
                                    _this.toggleOutgoingCall(false);
                                    _this.localMediaStream = null;
                                    _this.volumeNode = null;
                                };
                                _this.localMediaStream.stop();
                            } else {
                                _this.localMediaStream = null;
                                _this.volumeNode = null;
                            }
                        });
                    } else {
                        // clean up just in case
                        _this.toggleLoading(false);
                        _this.toggleOutgoingCall(false);
                    }
                }
            },

            /**
             * Handles when the user wants to reject/decline incoming call
             */
            reject: function() {

                if (_this.incomingCall) {

                    // stop ringing and show loading
                    audiotones.stop(audiotones.RING_IN);
                    _this.toggleLoading(true);

                    _this.incomingCall.reject(
                        function() {
                            _this.incomingCall = null;
                            _this.toggleIncomingCall(false);
                            _this.toggleLoading(false);
                        },
                        function(error) {
                            _this.toggleIncomingCall(false);
                            _this.toggleLoading(false);
                            _this.displayError("Decline call failed", error);
                            ga('send', 'event', 'call', 'error', 'decline failed');
                        }
                    );
                }
            },

            /**
             * Handle if user need mute/unmute the currrent call
             * @param mute - indicate should we mute or unmute call
             * @returns {boolean}
             */
            mute: function(mute) {

                if (_this.activeCall && _this.activeCall.mute && _this.activeCall.unmute) {
                    if (mute) {
                        _this.activeCall.mute();
                        _this.activeCall.muted = true;
                    } else {
                        _this.activeCall.unmute();
                        _this.activeCall.muted = false;
                    }
                    return true;
                }
                return false;
            },

            /**
             * Delete call from current calls and make previous call active
             * @param {string } callId - The id of deleted call
             */
            deleteCall: function(callId) {

                if (_this.incomingCall !== null) {
                    if (callId === _this.incomingCall.getId()) {
                        _this.incomingCall = null;
                    }
                }
                else {
                    var activeCallId = _this.activeCall.getId();

                    if (activeCallId === callId) {
                        _this.stopTimer();
                        _this.stopIntraFrame();
                        _this.activeCall = null;
                    }

                    delete _this.currentCalls[callId];
                    _this.numberOfCalls--;

                    //make previous call active
                    for (var id in _this.currentCalls) {
                        _this.activeCall = _this.currentCalls[id];
                        break;
                    }

                    if (_this.activeCall !== null) {
                        if (_this.activeCall.onHold) {
                            _this.toggleVideo(false);
                        }
                        else {
                            if (_this.activeCall.isVideoCall) {
                                _this.toggleLocalVideo(true);
                            }
                            _this.toggleRemoteVideo(true);
                        }
                    }
                }

                _this.updateCallView();
            },

            /**
             * Turning on/off video for current call
             * @param {boolean} isVideoStop - Indicate should we start or stop video
             * @param {function} onFinish - Function will be called after successful starting/stopping video
             */
            setVideo: function(isVideoStop, onFinish) {

                if (_this.activeCall && _this.activeCall.videoStop && _this.activeCall.videoStart) {
                    _this.toggleLoading(true);

                    if (isVideoStop) {
                        _this.activeCall.videoStop(function() {

                                _this.activeCall.isVideoCall = false;
                                _this.toggleLocalVideo(false);
                                _this.stopIntraFrame();

                                if (onFinish) {
                                    onFinish();
                                }

                                _this.toggleLoading(false);

                            },
                            function() {
                                logger.log("Could not stop video");
                                _this.toggleLoading(false);
                            });
                    }
                    else {
                        _this.activeCall.videoStart(function() {

                                if (fcs.call.hasVideoDevice()) {
                                    _this.toggleLocalVideo(true);
                                }
                                _this.activeCall.isVideoCall = true;

                                if (onFinish) {
                                    onFinish();
                                }

                                _this.toggleLoading(false);
                            },
                            function() {
                                logger.log("Could not start video");
                                _this.toggleLoading(false);
                            });
                    }
                }
            },

            /**
             * Hold or unhold current active call
             * @param {boolean} hold - Indicate do we need hold or unhold call
             * @param {function} onFinish - Function will be called after successful hold/unhold call
             */
            hold: function(hold, onFinish) {

                if (_this.activeCall && _this.activeCall.hold && _this.activeCall.unhold) {

                    _this.toggleLoading(true);

                    if (hold) {
                        _this.activeCall.hold(function() {

                                _this.stopTimer();

                                _this.stopIntraFrame();
                                _this.toggleVideo(false);

                                _this.activeCall.onHold = true;

                                if (onFinish) {
                                    onFinish();
                                }

                                _this.updateCallView();
                                _this.toggleLoading(false);
                            },
                            function(error) {

                                logger.log("Could not hold");
                                _this.toggleLoading(false);
                                _this.displayError("Call hold failed", error);
                            });
                    } else {
                        _this.activeCall.unhold(function() {

                                _this.startTimer();
                                _this.activeCall.onHold = false;

                                if (onFinish) {
                                    onFinish();
                                }

                                _this.toggleRemoteVideo(true);
                                if (_this.activeCall.isVideoCall) {
                                    _this.toggleLocalVideo(true);
                                }
                                if(_this.activeCall.muted){
                                    _this.activeCall.mute(true);
                                }
                                _this.updateCallView();
                                _this.toggleLoading(false);
                            },
                            function(error) {

                                logger.log("Could not unhold");
                                _this.toggleLoading(false);
                                _this.displayError("Call unhold failed", error);
                            });
                    }
                }
            },

            //TODO: finish implementing join functionality when Spider service will be ready
//            join: function() {
//
//                _this.activeCall.hold(
//                    function(){
//                        callView.holdEvent(activeCall,true);
//                        activeCall.setJoin(true);
//                        activeCall.onJoin = function() {
//                            activeCall.join(anotherCall,function(newCall){
//                                    newCall.onStateChange = function(state){
//                                        self.onStateChange(newCall, state);
//                                    };
//                                    activeCall.setJoin(false);
//                                    logger.trace("(callControl) Merge Success");
//                                },
//                                function(){
//                                    activeCall.setJoin(false);
//                                    dialog.showDialog( "MERGE_FAILED", "error");
//                                    logger.error("(callControl) Merge Failed");
//                                });
//                        };
//
//
//                    },
//                    function(){
//                        logger.error("Could not hold active call for join");
//                    },
//                    callView.getcurrentWindowCalls()[activeCall.getId()].video);
//            },

            /**
             * Called after successful subscribing to notifications from Spider service
             */
            notificationReady: function() {
                logger.log('callController.notificationReady');
            },

            /**
             * Handler for incoming call
             * @param call info object
             */
            onNotification: function(call) {

                var callerName = call.callerName,
                    callerNumber = call.callerNumber;

                logger.log('callController.onNotification - for ' + callerName + ', ' + callerNumber);

                call.contact = contactManager.getContactByPhoneNumber(callerNumber);
                call.startTime = null;

                call.preCall = true;
                call.incoming = true;
                call.onHold = false;

                call.onStateChange = function(state) {
                    _this.onStateChange(call, state);
                };
                audiotones.play(audiotones.RING_IN);
                ga('send', 'event', 'call', 'ringing', 'incoming');

                call.onStreamAdded = function(streamURL) {

                    logger.log("Incoming call remote stream added: " + streamURL);
                    ga('send', 'event', 'call', 'in call', 'incoming');

                    if(typeof(streamURL) !== 'undefined') {
                        call.remoteStreamURL = streamURL;
                        _this.setRemoteStream(streamURL);
                    }
                };

                _this.incomingCall = call;
                _this.togglePhoneRinging(true);
                _this.toggleIncomingCall(true);

                if (!$('#phone-widget-btn').hasClass('open')) {
                    $('#phone-widget-btn').trigger(ace.click_event);
                }
            },

            /**
             * Called after status of the call changed
             * @param {object} call - The call what status has been changed
             * @param {Number} state - The new state of the call
             */
            onStateChange: function(call, state) {
                logger.log("onStateChange: " + state);

                var fcsCallStates = fcs.call.States,
                    callId = call.getId(),
                    currentCall = _this.currentCalls[callId];

                //Play the ring out when we receive the 180
                if (state === fcsCallStates.RINGING && call.statusCode !== "183") {
                    audiotones.play(audiotones.RING_OUT);
                    ga('send', 'event', 'call', 'ringing', 'outgoing');
                }
                else if (state === fcsCallStates.INCOMING) {
                    audiotones.play(audiotones.RING_IN);
                }
                else {
                    audiotones.stop(audiotones.RING_OUT);
                    audiotones.stop(audiotones.RING_IN);
                }

                if (state === fcsCallStates.IN_CALL) {

                    logger.log("The call state changed: IN_CALL");
                    ga('send', 'event', 'call', 'in call', 'outgoing');

                    currentCall.preCall = false;
                    currentCall.onHold = false;

                    if (call.getHold()) {
                        logger.log("the call currently on hold");
                        return;
                    }

                    if (_this.activeCall !== null && !_this.activeCall.onHold && callId !== _this.activeCall.getId()) {
                        //hold active call
                        logger.log("hold active call ");

                        _this.hold(true, function() {
                            _this.activeCall = call;

                            _this.updateCallView();
                            _this.startTimer();
                            if (_this.activeCall.isVideoCall) {
                                _this.toggleLocalVideo(true);
                            }
                            _this.toggleRemoteVideo(true);
                        });
                    }
                    else {
                        logger.log("new call");

                        _this.toggleOutgoingCall(false);

                        _this.startTimer();
                        if (_this.activeCall.isVideoCall) {
                            _this.toggleLocalVideo(true);
                        }
                        _this.updateCallView();

                    }

                    $('.active-call-list-header').text('Currently Speaking');
                    $('.btn-call-hold').removeClass('disabled');
                }
                else if (state === fcsCallStates.RENEGOTIATION) {

                    logger.log("The call state changed: RENEGOTIATION");
                    ga('send', 'event', 'call', 'renegotiation');

                }
                else if (state === fcsCallStates.ON_HOLD) {

                    logger.log("The call state changed: ON_HOLD");
                    ga('send', 'event', 'call', 'on hold');

                    call.onHold = true;

                    if (_this.activeCall.getId() === callId) {
                        _this.stopTimer();
                        _this.toggleVideo(false);
                        _this.updateCallControls(true);
                    }
                    _this.updateCallView();
                    $('.active-call-list-header').text('ON HOLD');
                }
                else if (state === fcsCallStates.ON_REMOTE_HOLD) {

                    logger.log("The call state changed: ON_REMOTE_HOLD");
                    ga('send', 'event', 'call', 'on remote hold');

                    call.onHold = true;

                    if (_this.activeCall.getId() === callId) {
                        _this.stopTimer();
                        _this.toggleVideo(false);
                        _this.updateCallControls(true);
                    }
                    _this.updateCallView();
                    $('.active-call-list-header').text('ON REMOTE HOLD');
                    $('.btn-call-hold').addClass('disabled');
                }
                else if (state === fcsCallStates.RINGING) {

                    logger.log("The call state changed: RINGING");

                }
                else if (state === fcsCallStates.ENDED) {

                    logger.log("The call state changed: ENDED");
                    ga('send', 'event', 'call', 'ended', 'remote', _this.getCallDurationSeconds());

                    if (call) {
                        //callView.removeCallAudio();

                        if (_this.incomingCall !== null) {
                            _this.toggleIncomingCall(false);
                            _this.togglePhoneRinging(false);
                        }

                        if (_this.activeCall && callId === _this.activeCall.getId() && _this.activeCall.preCall) {
                            _this.toggleOutgoingCall(false);
                        }

                        if (_this.activeCall) {
                            if (callId === _this.activeCall.getId()) {
                                _this.stopIntraFrame();
                            }
                        }

                        //End scenario has two options if status code is 0,call ended normally,else server returned an error
                        if (call.statusCode === 0 || call.statusCode === undefined) {
                            _this.deleteCall(callId);
                        }
                        else {
                            //if ((call.statusCode >= 100 && call.statusCode <= 300)) {
                            //callView.setCallStatusWithCallId(callId, window.lang.StatusCodes.webRTCError1 );
                            //}
                            //else {
                            //callView.setCallStatusWithCallId(callId, window.lang.StatusCodes[call.statusCode] );
                            //}
                            audiotones.play(audiotones.BUSY);
                            setTimeout(function() {

                                _this.deleteCall(callId);
                                audiotones.stop(audiotones.BUSY);
                            }, 1500);
                        }
                    }
                }
                else if (state === fcsCallStates.REJECTED) {

                    logger.log("The call state changed: REJECTED");

                    setTimeout(function() {
                        _this.deleteCall(callId);
                    }, 1500);
                }
                else if (state === fcsCallStates.OUTGOING) {
                    logger.log("The call state changed: OUTGOING");
                }
                else if (state === fcsCallStates.INCOMING) {
                    logger.log("The call state changed: INCOMING");
                }
                else if (state === fcsCallStates.JOINED) {
                    logger.log("The call state changed: JOINED");
                }
            },
            /**
             * Show or hide loading
             * @param {boolean} isLoading - Indicating should we show or hide loading
             */
            toggleLoading: function(isLoading) {

                var deferred = $.Deferred();

                if (isLoading) {
                    $('#phone-widget-content').block({
                        onBlock : function() {
                            deferred.resolve();
                        }
                    });
                }
                else {
                    $('#phone-widget-content').unblock({
                        onUnblock : function() {
                            deferred.resolve();
                        }
                    });
                }

                return deferred;
            },

            /**
             * Show or hide call tab
             * @param {boolean} show - Indicating should we show or hide call tab
             */
            toggleCallTab: function(isShow) {

                var phoneTab = $('#phone-widget-content').find('.js-call-tab');

                if (isShow) {

                    $('.no-active-calls').hide();
                    $('.active-call-content').removeClass('app-hidden');

                    phoneTab.trigger('click');
                }
                else {
                    $('.active-call-content').addClass('app-hidden');
                    $('.no-active-calls').show();
                }
            },

            /**
             * Show or hide outgoing call window
             * @param {boolean} isShow - Indicating should we show or hide outgoing call window
             */
            toggleOutgoingCall: function(isShow) {
                logger.log("toggleOutgoingCall " + isShow);

                var $view = $('#call-widget-outgoing');

                if (isShow) {

                    if ($.isNotNull(_this.activeCall)) {
                        var $name = $view.find("#name");
                        var $avatar = $view.find('#avatar');

                        if ($.isNotNull(_this.activeCall.contact)) {
                            $name.text(_this.activeCall.contact.fullName());

                            if (_this.activeCall.contact.images.length > 0) {
                                $avatar.attr("src", _this.activeCall.contact.images[0].imageUrl);
                            } else {
                                $avatar.attr("src", 'images/avatars/avatar-blank.png');
                            }

                        } else if ($.isNotNull(_this.activeCall.callerNumber)) {
                            $name.text(_this.activeCall.callerNumber);
                            $avatar.attr("src", 'images/avatars/avatar-blank.png');
                        } else {
                            $name.text('Unknown Caller');
                            $avatar.attr("src", 'images/avatars/avatar-blank.png');
                        }
                    }

                    $view.show();
                }
                else {
                    $view.hide();
                }
            },

            /**
             * Show or hide incoming call window
             * @param {boolean} isShow - Indicating should we show or hide incoming call window
             */
            toggleIncomingCall: function(isShow) {
                logger.log("toggleIncomingCall " + isShow);

                var view = $('#call-widget-incoming');

                if (isShow) {
                    //updating view
                    if ($.isNotNull(_this.incomingCall.contact)) {

                        view.find("#name").text(_this.incomingCall.contact.fullName());

                        if (_this.incomingCall.contact.images.length > 0) {
                            view.find("#avatar").attr("src", _this.incomingCall.contact.images[0].imageUrl);
                        }
                    }
                    else {
                        view.find("#name").text(_this.incomingCall.callerNumber);
                    }

                    view.show();
                }
                else {
                    view.hide();
                }
            },

            /**
             * Show or hide error window
             * @param {boolean} isShow - Indicating should we show or hide error window
             * @param {string} errorDescription - Error description to show
             */
            toggleError: function(isShow, errorDescription) {

                var view = $('#call-widget-error');

                if (isShow) {
                    view.find("#errorDesc").text(errorDescription);
                    view.show();
                }
                else {
                    view.hide();
                }
            },

            /**
             * Switching call to another
             * @param {string} callId - The call id to switch to
             */
            switchTo: function(callId) {

                var activeCallId = _this.activeCall.getId(),
                    callToSwitch = _this.currentCalls[callId];

                if (callId !== activeCallId) {

                    //hold active call
                    if (!_this.activeCall.onHold) {

                        _this.hold(true, function() {

                            _this.activeCall = callToSwitch;

                            if (_this.activeCall.onHold) {

                                _this.hold(false, function() {
                                    _this.updateCallView();
                                });
                            }
                        });
                    }
                    else {
                        _this.activeCall = callToSwitch;

                        if (_this.activeCall.onHold) {

                            _this.hold(false, function() {
                                _this.updateCallView();
                            });
                        }
                    }
                }
            },

            /**
             * Refreshing active call widget
             */
            updateCallView: function() {
                logger.log("updateCallView ");

                var callView = $('#call-widget-active'),
                    activeCallContainer = callView.find('#active-call-container'),
                    currentCallsContainer = callView.find('#active-call-list');

                if (_this.activeCall !== null) {

                    if ($.isNotNull(_this.activeCall.contact)) {

                        activeCallContainer.find(".contact-name").text(_this.activeCall.contact.fullName());

                        if (_this.activeCall.contact.images.length > 0) {
                            activeCallContainer.find("#avatar").attr("src", _this.activeCall.contact.images[0].imageUrl);
                        }
                    }
                    else {
                        activeCallContainer.find(".contact-name").text(_this.activeCall.callerNumber);
                    }

                    _this.toggleCallTab(true);


                }

                currentCallsContainer.html("");
                for (var callId in _this.currentCalls) {
                    var call = _this.currentCalls[callId];
                    template.appendTemplate("call/call-item", call, "active-call-list");
                }

                if (_this.activeCall === null) {

                    _this.toggleCallTab(false);

                    //removing video
                    $('.active-call-content .localVideo').remove();
                    $('.active-call-content .remoteVideo').remove();

                    //hide call controls
                    _this.updateCallControls(false);
                }
                else {
                    _this.updateCallControls(true);
                }

                if (_this.numberOfCalls > 1) { //move active call content and remote video to the left

                    activeCallContainer.removeClass('col-xs-12').addClass('col-xs-9');
                    $('.remoteVideo').css('left', 0);
                    $('#current-calls-container').show();
                }
                else if (_this.numberOfCalls === 1) { //move active call content and remote video to the center

                    activeCallContainer.removeClass('col-xs-9').addClass('col-xs-12');
                    $('.remoteVideo').css('left', '35px');
                    $('#current-calls-container').hide();
                }
            },

            /**
             * Show/hide and updating call toolbar buttons states
             * @param {boolean} isShow - Indicating should we show or hide call toolbar buttons
             */
            updateCallControls: function(isShow) {

                var callControlsToolbar = $("#phone-widget-call-toolbar");

                if (isShow) {
                    callControlsToolbar.show();

                    if (_this.activeCall !== null) {

                        var videoBtn = callControlsToolbar.find(".btn-call-toggle-video"),
                            muteBtn = callControlsToolbar.find(".btn-call-mute"),
                            holdBtn = callControlsToolbar.find(".btn-call-hold");

                        if (_this.activeCall.isVideoCall) {
                            videoBtn.data("video", true);
                            videoBtn.addClass("active");
                        }
                        else {
                            videoBtn.data("video", false);
                            videoBtn.removeClass("active");
                        }

                        if (_this.activeCall.muted) {
                            muteBtn.data("mute", true);
                            muteBtn.find('i').removeClass('icon-microphone-off').addClass('icon-microphone');
                            muteBtn.addClass("active");
                        }
                        else {
                            muteBtn.data("mute", false);
                            muteBtn.find('i').removeClass('icon-microphone').addClass('icon-microphone-off');
                            muteBtn.removeClass("active");
                        }

                        if (_this.activeCall.onHold) {
                            holdBtn.data("hold", true);
                            holdBtn.find('i').removeClass('icon-pause').addClass('icon-play');
                            holdBtn.addClass("active");

                            //make video and mute buttons disabled
                            videoBtn.addClass('disabled');
                            muteBtn.addClass('disabled');
                        }
                        else {
                            holdBtn.data("hold", false);
                            holdBtn.find('i').removeClass('icon-play').addClass('icon-pause');
                            holdBtn.removeClass("active");

                            //make video and mute buttons enabled
                            videoBtn.removeClass('disabled');
                            muteBtn.removeClass('disabled');
                        }
                    }
                }
                else {
                    callControlsToolbar.hide();
                }

            },

            /**
             * Show/hide video for current call
             * @param {boolean} showVideo - Need show or hide video
             */
            toggleVideo: function(showVideo) {
                _this.toggleLocalVideo(showVideo);
                _this.toggleRemoteVideo(showVideo);
            },

            /**
             * Show or hide local video
             * @param {boolean} showVideo - Need show or hide local video
             */
            toggleLocalVideo: function(showVideo) {
                logger.log("toggleLocalVideo " + showVideo);

                var localVideo = $('.active-call-content .localVideo');
                logger.log("toggleLocalVideo " + showVideo + " " + localVideo);

                if (showVideo) {

                    if (localVideo.length === 0) {
                        _this.setLocalStream();
                    }
                    else {
                        localVideo.show();
                    }
                    _this.startIntraFrame();
                }
                else {
                    _this.stopIntraFrame();
                    localVideo.hide();
                }
            },

            /**
             * Show or hide remote video
             * @param {boolean} showVideo - Need show or hide remote video
             */
            toggleRemoteVideo: function(showVideo) {

                var remoteVideo = $('.active-call-content .remoteVideo');
                logger.log("toggleRemoteVideo " + showVideo + " " + remoteVideo);

                if (showVideo) {

                    if (remoteVideo.length === 0) {
                        _this.setRemoteStream(_this.activeCall.remoteStreamURL);
                    }
                    else {
                        remoteVideo.show();
                    }
                }
                else {
                    remoteVideo.hide();
                }
            },

            maximizeVideo: function() {
                var remoteVideo = $('.remoteVideo'),
                    localVideo = $('.localVideo'),
                    windowHeight = $(window).height(),
                    windowWidth = $(window).width(),
                    videoContainerOffset = $('.call-content').offset();

                remoteVideo.css("top", "-7px");
                remoteVideo.css("left", "-" + videoContainerOffset.left + "px");
                remoteVideo.width(windowWidth);
                remoteVideo.height(windowHeight - 45);

//                remoteVideo.transition(
//                    {
//                        top: -7,
//                        left: -videoContainerOffset.left,
//                        width: windowWidth,
//                        height: windowHeight - 45
//                    });
//
//                localVideo.transition(
//                    {
//                        top: windowHeight - 200 - videoContainerOffset.top,
//                        left: windowHeight - 150 - videoContainerOffset.left,
//                        width: 200,
//                        height: 150
//                    });

                remoteVideo.data("maximize", true);

                localVideo.width(200);
                localVideo.height(150);

                localVideo.css("top", windowHeight - localVideo.height() - videoContainerOffset.top + "px");
                localVideo.css("left", windowWidth - localVideo.width() - videoContainerOffset.left + "px");
            },

            minimizeVideo: function() {
                var remoteVideo = $('.remoteVideo'),
                    localVideo = $('.localVideo');

                remoteVideo.removeAttr('style');
                localVideo.removeAttr('style');

                if (_this.numberOfCalls === 1) {
                    remoteVideo.css('left', '35px');
                }

                //remoteVideo.transition({ top: 55, left: 0, width: 186, height: 139 });
                //localVideo.transition({ left: 0, bottom: 0, width: 80, height: 60 });

                remoteVideo.data("maximize", false);
            },

            videoResize: function() {

                var remoteVideo = $('.remoteVideo'),
                    localVideo = $('.localVideo'),
                    windowHeight = $(window).height(),
                    windowWidth = $(window).width(),
                    videoContainerOffset = $('.call-content').offset();

                if(remoteVideo.length === 1 && remoteVideo.data("maximize")) {

                    remoteVideo.css("left", "-" + videoContainerOffset.left + "px");
                    remoteVideo.width(windowWidth);
                    remoteVideo.height(windowHeight - 45);

                    localVideo.css("top", windowHeight - localVideo.height() - videoContainerOffset.top + "px");
                    localVideo.css("left", windowWidth - localVideo.width() - videoContainerOffset.left + "px");
                }
            },

            /**
             *
             * @returns {*}
             */
            getMediaSource: function() {

                var mediaSources = {
                        audio: [],
                        video: []
                    },
                    deferred = $.Deferred(),
                    sourceInfo, i;

                var MediaStreamTrack = window.MediaStreamTrack;

                if (_this.rtcPlugin) {
                    var audioInDeviceNames = _this.rtcPlugin.getAudioInDeviceNames()
                        , videoDeviceNames = _this.rtcPlugin.getVideoDeviceNames();

                    for (i = 0; i != audioInDeviceNames.length; ++i) {
                        sourceInfo = audioInDeviceNames[i];
                        logger.log(sourceInfo || 'undefined audio device');
                        mediaSources.audio.push({ label : sourceInfo, id : i });
                    }

                    for (i = 0; i != videoDeviceNames.length; ++i) {
                        sourceInfo = videoDeviceNames[i];
                        logger.log(sourceInfo || 'undefined video device');
                        mediaSources.video.push({ label : sourceInfo, id : i });
                    }

                    deferred.resolve(mediaSources);
                } else if (MediaStreamTrack && MediaStreamTrack.getSources) {
                    MediaStreamTrack.getSources(function(sourceInfos) {

                        for (i = 0; i != sourceInfos.length; ++i) {
                            sourceInfo = sourceInfos[i];
                            if (sourceInfo.kind === 'audio') {
                                logger.log(sourceInfo.id, sourceInfo.label || 'microphone');
                                mediaSources.audio.push(sourceInfo);

                            } else if (sourceInfo.kind === 'video') {
                                logger.log(sourceInfo.id, sourceInfo.label || 'camera');
                                mediaSources.video.push(sourceInfo);

                            } else {
                                logger.log('Some other kind of source: ', sourceInfo);
                            }
                        }

                        deferred.resolve(mediaSources);
                    });
                } else {
                    deferred.resolve(mediaSources);
                }

                return deferred;
            },

            /**
             * Place local video to the page
             */
            setLocalStream: function() {
                var content,
                    callView = $('.active-call-content'),
                    localVideo = $('.active-call-content .localVideo');

                localVideo.remove();
                if (_this.activeCall && _this.activeCall.localStreamURL) {

                    logger.log('Set local stream: ' + _this.activeCall.localStreamURL);

                    content = '<video class="localVideo" autoplay="autoplay" muted="true" src="' + _this.activeCall.localStreamURL + '" />';
                    callView.append(content);
                }
            },

            /**
             * Place remote video to the page
             * @param {string} streamURL - The url to the remote stream
             */
            setRemoteStream: function(streamURL) {

                var content,
                    callView = $('.active-call-content'),
                    remoteVideo = $('.active-call-content .remoteVideo');

                logger.log('Set remote stream: ' + streamURL);

                remoteVideo.remove();
                if (streamURL) {
                    content = '<video class="remoteVideo" style="left: ' + (_this.numberOfCalls === 1 ? '35px' : '0px') + '" autoplay="autoplay" src="' + streamURL + '" />';
                    callView.append(content);
                }
            },

            /**
             * Update call duration
             * @param {string} duration - Call duration
             */
            setCallDuration: function(duration) {
                $('.call-duration').text(duration);
            },
            /*
             *  Prompts the user to download correct plugin for repective operating system
             * */
            getAppVersion : function(){
                if ( window.navigator.appVersion.indexOf("Mac") !== -1){
                    window.open(config.getValue("macOsMediaPluginLink", null));
                }
                else if(window.navigator.userAgent.indexOf("Win64") !== -1
                    && window.navigator.userAgent.indexOf("x64") !== -1){
                    window.open(config.getValue("windowsMediaPluginLink_x64", null));
                }
                else {
                    window.open(config.getValue("windowsMediaPluginLink", null));
                }
            },
            /**
             * populate list of available audio & video devices on the settings page
             */
            populateMediaSource : function(mediaSource) {
                var $audioSelect = $('#call-audio-source'), $videoSelect = $('#call-video-source');

                if (mediaSource.audio && mediaSource.audio.length) {
                    $.each(mediaSource.audio, function(i, source) {
                        var label = (source.label === "" ? ("Audio " + (i + 1)) : source.label)
                            , optionToAppend = '<option value="' + source.id + '">' + label + '</option>';
                        $audioSelect.append(optionToAppend);
                    });
                } else {
                    $audioSelect.attr("disabled", "");
                }

                if (mediaSource.video && mediaSource.video.length) {
                    $.each(mediaSource.video, function(i, source) {
                        var label = (source.label === "" ? ("Video " + (i + 1)) : source.label)
                            , optionToAppend = '<option value="' + source.id + '">' + label + '</option>';
                        $videoSelect.append(optionToAppend);
                    });
                } else {
                    $videoSelect.attr("disabled", "");
                }
            },

            getSelectedMediaSource: function() {
                var $audioSelect = $('#call-audio-source'), $videoSelect = $('#call-video-source');

                return {
                    audio : $audioSelect.val(),
                    video : $videoSelect.val()
                };
            },

            /**
             * Initialize media for calling
             * @param {Object} options
             */
            initMedia: function(options) {
                var deferred = $.Deferred(),
                    videoContainer = $("#plug-in-container");

                function removeContainerStyle () {
                    videoContainer.removeAttr("style");
                }

                // fcs init media
                Arcus.initMedia(
                    function() {
                        // success
                        if (options && options.success) options.success();
                        deferred.resolve();
                        removeContainerStyle();
                        $.each($('object'), function(ind, elem) {
                            if (elem.type === 'application/x-gcfwenabler') {
                                _this.rtcPlugin = elem;
                                videoContainer.addClass('disabled');
                            }
                        });

                        _this.getMediaSource().done(function(mediaSource) {
                            _this.populateMediaSource(mediaSource);
                        });

                        _this.initCallButton();
                    },
                    function(error) {
                        //error
                        logger.log(error);
                        $.each($('object'), function(ind, elem) {
                            if (elem.type === 'application/x-gcfwenabler') {
                                _this.rtcPlugin = elem;
                                videoContainer.addClass('disabled');
                            }
                        });
                        function confirmGetMessage() {
                            _this.errorObject = { errorType : 'info' }; // info || warning || danger

                            switch (error){
//                                case fcs.call.MediaErrors.NEW_VERSION_WARNING: // need to handle
//                                case fcs.call.MediaErrors.WRONG_VERSION: // need to handle
                                default:
                                    _this.errorObject.message = "Please download the latest Plugin." + _this.rtcPlugin ? (" Your current plugin version: " + _this.rtcPlugin.version) : '';
                                    _this.errorObject.errorType = "danger";
                            }

                            _this.pluginNotPresent = true;
                            _this.askDownloadPluguin(_this.errorObject);
                        }

                        confirmGetMessage();
                        _this.initCallButton();
                        deferred.resolve();

                        removeContainerStyle();
                    },
                    {

                         "pluginLogLevel" : config.getValue("pluginLogLevel","auto"),
                         "ice" : config.getValue("ice",null),
                         "videoContainer" : videoContainer.get(0),
                         "pluginMode" : config.getValue("pluginMode",null),
                         "iceserver" : config.getValue("iceserver",null),
                         "webrtcdtls" : config.getValue("webrtcdtls",false)

                    }
                );

                return deferred;
            },
            
            /**
            * Display a confirmation box asking the visitor if they want to download the plugin or not
            */
            askDownloadPluguin: function(errorObject) {
                var _errorObject = errorObject || {}
                    , templateDownload = template.buildTemplate('call/call-download-plugin', _errorObject);

                bootbox.dialog({
                    message: templateDownload,
                    buttons: {
                        'ok': {
                            'label': 'OK',
                            'className': 'btn-sm btn-primary',
                            'callback': function() {
                            }
                        }
                    }
                });

                $('a.download-plugin').on('click', function(event){
                    event.preventDefault();
                    event.stopPropagation();

                    _this.getAppVersion();
                });
            },

            /*
            * Setup phone widget main button handler
            */
            initCallButton: function() {
                $(document).on(ace.click_event, '#phone-widget-btn', function() {
                    if (_this.pluginNotPresent) {
                        _this.askDownloadPluguin(_this.errorObject);
                        return;
                    }

                    var $btn = $(this);
                    var $widget = $('#phone-widget-container');
                    var $content = $('#phone-widget-content');
                    var $toolbar = $('#phone-widget-call-toolbar');
                    var open = $widget.hasClass('open');

                    if (open) {
                        //$widget.css('z-index', '');
                        setTimeout(function() {
                            $content.css('opacity', 0);
                        }, 500);
                    } else {
                        //$widget.css('z-index', 999);
                        $content.css('opacity', 1);
                    }

                    $widget.toggleClass('open');
                    $btn.toggleClass('open');
                    $toolbar.toggleClass('app-hidden');
                });
            },

            /**
             * Remove special characters from phone number
             * @param {string} phoneNumber
             * @returns {string} Stripped phone number
             */
            stripCallId: function(phoneNumber) {

                /*
                 * If callId is DN we are removing special characters
                 * from callId thoses may causes on call server.
                 * Removed special characters: -,.,(,),+ space
                 */
                var bInterNat,
                    regExp = new RegExp(/[.),(+\- ]/g),
                    callId_split = phoneNumber.split('@');

                if (!(/[a-zA-Z]/.test(phoneNumber[0]))) {
                    if (phoneNumber.indexOf('+') === 0) {
                        bInterNat = true;
                    }
                    if (phoneNumber.indexOf('@') < 0) {
                        phoneNumber = phoneNumber.replace(regExp, '');
                    }
                    else {
                        phoneNumber = callId_split[0].replace(regExp, '') + "@" + callId_split[1];
                    }

                    if (bInterNat) {
                        phoneNumber = '+' + phoneNumber;
                    }
                }

                return phoneNumber;
            },

            /**
             * Started call timer to calculate call duration
             */
            startTimer: function() {

                if (_this.activeCall) {

                    //stopping all other timers
                    for (var callId in _this.currentCalls) {
                        var call = _this.currentCalls[callId];
                        if(call.interval) {
                            clearInterval(call.interval);
                        }
                    }

                    if (_this.activeCall.startTime === null) {
                        _this.activeCall.startTime = new Date();
                        _this.activeCall.pauseDuration = 0;
                        _this.activeCall.pauseStartTime = null;
                    }
                    else {
                        //update pauseDuration if was pause
                        if(_this.activeCall.pauseStartTime !== null) {
                            _this.activeCall.pauseDuration += $.getTimeDuration(_this.activeCall.pauseStartTime, new Date());
                            _this.activeCall.pauseStartTime = null;
                        }
                    }

                    // Update the duration once per second
                    _this.activeCall.interval = setInterval(function() {
                        if (_this.activeCall) {

                            var durationText = $.formatPeriod(_this.activeCall.startTime, new Date(), _this.activeCall.pauseDuration);
                            _this.setCallDuration(durationText);
                        }
                    }, 1000);
                }
            },

            getCallDurationSeconds: function(){
                if (_this.activeCall && _this.activeCall.startTime) {
                    var durationMs = new Date().getTime() - _this.activeCall.startTime.getTime();
                    return Math.floor(durationMs / 1000);
                } else {
                    return 0;
                }
            },

            /**
             * Stop call timer
             */
            stopTimer: function() {
                if (_this.activeCall) {
                    var interval = _this.activeCall.interval;
                    _this.activeCall.pauseStartTime = new Date();
                    if ($.isNotNull(interval)) {
                        clearInterval(interval);
                    }
                }
            },

            stopIntraFrame: function() {
                if (_this.intraframe) {
                    logger.log('clearInterval for intraframe');
                    clearInterval(_this.intraframe);
                }
            },

            startIntraFrame: function() {
                _this.intraframe = setInterval(function() {
                    if (_this.activeCall) {
                        _this.activeCall.sendIntraFrame();
                        logger.log('sendIntraFrame');
                    }
                    else {
                        logger.log('stopIntraFrame');
                        _this.stopIntraFrame();
                    }
                }, 5000);
            },

            /**
             * Display error
             * @param {string} error - Error details
             * @param {string} detailError - Additional error details
             */
            displayError: function(error, detailError) {

                var messageText = error,
                    errorNumber = parseInt(detailError, 10);

                if (!isNaN(errorNumber)) {
                    //we have error state number, so get description for it
                    detailError = _callErrors[errorNumber];
                }

                if (detailError && window.$wrtc.isDebug()) {
                    messageText += [" (", detailError, ")"].join("");
                }

                _this.toggleError(true, messageText);
            }
        };
    };

    return callController().init();
});

define('notificationController', [ 'logger', 'callController' ],
    function(logger, callController) {

        var notificationController = function() {

            var _this;

            return {
                init: function() {
                    _this = this;
                    return _this;
                },

                bind: function() {

                    fcs.call.onReceived = $.proxy(callController.onNotification, callController);

                    fcs.notification.start(function() {
                        callController.notificationReady();
                    }, function() {
                        $.gritter.add({
                            text: "Failed to establish websocket. You won't be able to Call, Chat, or receive new message notifications.",
                            class_name: 'gritter-error gritter-center'
                        });
                    });

                    fcs.notification.setOnConnectionLost(function() {
                        $.gritter.add({
                            text: "Lost websocket connection. You won't be able to Call, Chat, or receive new message notifications.",
                            class_name: 'gritter-error gritter-center'
                        });
                    });

                    fcs.notification.setOnConnectionEstablished(function() {
                        var stmp = window.cache.getItem('SubscriptionStamp');
                        if (stmp > new Date().getTime()) {
                            fcs.notification.extend(function() {
                                callController.notificationReady();
                            }, function() {
                                //TODO: handle errors;
                            });
                        }
                        else {
                            fcs.notification.start(function() {
                                callController.notificationReady();

                            }, function() {
                                //TODO: handle errors;
                            });
                        }

                    });
                }
            };
        };

        return notificationController().init();
    }
);


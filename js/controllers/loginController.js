define('loginController', ['logger', 'config', 'template', 'defaultController', 'Arcus', 'blockUI'], function (logger, config, template, defaultController, arcus) {
    var loginController = function () {
        var _this,
            _loginValidator;
        return {
            init: function () {
                _this = this;

                _this.setupHandlers();

                return _this;
            },

            configArcus: function () {
                //arcus config
                var arcusCfg = config.arcusSettings();
                arcusCfg.afClient = config.afClientSettings();
                arcus.init(arcusCfg);
            },

            initView: function () {

                defaultController.hideLoading();

                $('body').addClass('login-layout');

                template.appendTemplate("login/login",
                    {
                        lastLoginSelection: localStorage.lastLoginSelection
                    }, "login-container");

                _this.setupLoginPostHandler();
                $("#login-container").show();
                $('#username').focus();
            },

            setupHandlers: function () {
                $(document).on('click', '#logout', function () {
                    _this.logout();
                });
            },

            setupLoginPostHandler: function () {

                _loginValidator = $('#login-form').validate({
                    errorClass: 'help-block',
                    highlight: function (e) {
                        $(e).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-error');
                    },
                    submitHandler: function (form) {
                        var $form = $(form),
                            login = $form.find('#username').val(),
                            pass = $form.find('#password').val();

                        localStorage.lastLoginSelection = login;
                        _this.login(login, pass);
                    },
                    rules: {
                        username: { required: true },
                        password: { required: true }

                    },
                    messages: {
                        username: { required: 'Username is required' },
                        password: { required: 'Password is required' }
                    }
                });

            },

            /**
             * Will call CTLFacade interface for login
             * @param username -- Required
             * @param pass -- Not used in this situation
             */
            login: function (username) {

                $('#loginForm').block();

                arcus.ctlIDFacadeLogin(username,
                    function (sessionId, authenticationkey) {
                        arcus.preRegister(sessionId, authenticationkey, 'facade',
                            _this.onLoginSuccess,
                            _this.onLoginFailure);
                    },
                    function () {
                        _this.onLoginFailure();
                    }
                );
            },

            onLoginSuccess: function () {
                $('#loginForm').unblock();

                $("#login-container").html("");

                $('body').removeClass('login-layout');

                $("#login-container").hide();

                if (window.localStorage.getItem('RTCServiceEnabled') !== 'true') {
                    _this.showError('RTCService is disabled. The Contacts, Messages, Activities and Memories capabilities are not provided');
                }
                defaultController.showLoading(true);
                defaultController.initApplicationUI();
            },

            onLoginFailure: function () {
                logger.log('error logging in');

                $('#loginForm').unblock();
                $('#loginForm').find('#username').focus();
                $('#loginForm').find('#password').val('');

                var errors = { username: "The username you entered is incorrect" };
                if (_loginValidator)
                    _loginValidator.showErrors(errors);
            },

            /**
             * Main login entry point
             */
            autologin: function () {
                // setup arcus
                _this.configArcus();

                //If we have network creds cached, no need to interact with CTLID
                if (!arcus.isLoginCached()) {
                    if (arcus.UseCtlIdLogin === "true") {
                        var queryString = window.location.search;
                        var params = {};
                        //Test out on multiple scenarios -- Question.  Does '?' apply to group or '*'
                        queryString.replace(new RegExp("([^?=&]+)(=([^&]*))?", "g"), function (whole, name, valueextra, value) {
                            params[name] = value;
                        });

                        // If query params are from CTLID, we were redirected.  Give values to Arcus.
                        if (params.s && params.k && params.pct === '100') {
                            arcus.preRegister(params.s, params.k, params.dc,
                                _this.onLoginSuccess,
                                function() {
                                    _this.showError('There was an error logging in');
                                });
                        } else {
                            //Goto CTLID to authenticate the user
                            var newUrl = window.location.href;
                            if (newUrl.indexOf('#') > -1) {
                                newUrl = newUrl.substr(0, newUrl.indexOf('#')) + 'index.html';
                            }
                            newUrl = arcus.CtlIDLoginUrl + newUrl;
                            window.location.replace(newUrl);
                        }
                    } else {
                        //show login form
                        _this.initView();
                    }
                } else {
                    logger.log('logging in from cache');
                    arcus.loginWithCachedCredentials(function () {
                        if (window.localStorage.getItem('RTCServiceEnabled') !== 'true') {
                            _this.showError('RTCService is disabled. The Contacts, Messages, Activities and Memories capabilities are not provided');
                        }
                        defaultController.showLoading(true);
                        defaultController.initApplicationUI();
                    }, function () {
                        _this.showError('There was an error logging in from cache');
                    });
                }
            },

            showError: function(errorMessage) {
                template.appendTemplate("login/login-error", { errorMessage : errorMessage }, "body-content");

                var $container = $('#body-content');

                $container.on('click', '[data-action]', function() {
                    var $this = $(this), action = $this.data('action');
                    switch (action) {
                        case 'try-again':
                            window.location.reload();
                            break;
                        case 'new-login':
                            _this.logout();
                            break;
                    }
                });

                window.console.error(errorMessage);
                defaultController.hideLoading();
            },

            logout: function () {
                logger.log('will logout');

                var confirmLogout = true, synchronous = true;
                //TODO Tolga: Chrome disconects takes up to 5-6 seconds lower it down
//                    if($.browser.chrome) {
//                        synchronous = false;
//                    }

                //TODO: check if we are currently on a call
                //if(window.callCtrl.hasUserGotAnyCall()){
                //    confirmLogout = window.confirm('You are currently on a call, navigating away from this page will hang out the call');
                //}
                if (confirmLogout) {
                    $(window).unbind('beforeunload');
                    $(window).unbind('unload');

                    //window.callCtrl.endAllCalls();

                    arcus.logout(function () {
                        //onsuccess
                        _this.populateRedirectUrl();
                    }, function () {
                        //onfailure, can be used in the future
                        _this.populateRedirectUrl();
                    }, synchronous, false);

                } else {
                    return;
                }
            },

            populateRedirectUrl: function() {
                //detect if we use debug.html and redirect to debug.html
                if (window.location.href.indexOf("debug") >= 0) {
                    window.location.href = "debug.html";
                }
                else {
                    window.location.href = "index.html";
                }
            }
        };
    };

    return loginController().init();
});

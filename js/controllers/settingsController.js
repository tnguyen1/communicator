define('settingsController',
    ['template', 'eventEmitter', 'contactManager', 'settingsManager', 'logger', 'base64Binary', 'bootbox', 'validations', 'hbs!templates/settings/about', /*globals ->*/ 'blockUI', 'enums', 'jqueryvalidate', 'Jcrop', 'webrtc_tagsinput' ],
    function(template, eventEmitter, contactManager, settingsManager, logger, base64Binary, bootbox, validations, aboutTemplate) {
        var settingsController = function() {
            var _this;
            return {

                title: "Settings",

                init: function() {
                    _this = this;
                    _this.max911Addresses = 3;
                    _this.pendingSettingsChanges = {};

                    $.fn.reverse = [].reverse;

                    template.registerPartial('settings/contact-911address-show');
                    template.registerPartial('settings/contact-911address-edit');
                    template.registerPartial('contact/contact-phone-item');
                    template.registerPartial('contact/contact-email-item');
                    template.registerPartial('contact/contact-address-item');
                    template.registerPartial('contact/contact-note-item');

                    return _this;
                },

                /**
                 * Build main view of the settings view
                 * @param  {Object} options
                 */
                buildMainView: function(options) {
                    var $view = $(template.buildTemplate('settings/settings-layout', {}));
                    if (options.success) options.success($view);
                    _this.$sidebar = $('#setting-details');
                    _this.setupLocalRouting($view);
                },

                findElement: function(oldIdsArray, idToFind) {
                    var found = null;
                    $.each(oldIdsArray, function(ind, element) {
                        if (element === idToFind) {
                            found = ind;
                            return false;
                        }
                        return true;
                    });
                    return found;
                },

                /**
                 * is used for generating unique id for jquery validation plugin
                 * each dynamic added field should have unique name "name"+"id"
                 *
                 * @param oldIdsArray
                 * @param addToArray
                 * @returns {*}
                 */
                generateUniqueId: function(oldIdsArray, addToArray) {
                    var newId = (Math.floor(Math.random() * (100000 - 1 + 1)) + 1).toString()
                        , found = _this.findElement(oldIdsArray, newId);
                    if (found === null) {
                        if (addToArray) {
                            oldIdsArray.push(newId);
                        }
                        return newId;
                    } else {
                        return _this.generateUniqueId(oldIdsArray, addToArray);
                    }
                },

                isExactMatch: function(xmlData, nodeName, nodeText, returnNode) {
                    var $xmlData = $(xmlData)
                        , $found = $xmlData.find(nodeName)
                        , $exactFound = $found.filter(function() {
                            return $(this).text().toUpperCase() === nodeText.toUpperCase();
                        })
                        , isExactMatch = false;
                    if ($xmlData.length && $found.length && $exactFound.length) {
                        isExactMatch = true;
                        if (returnNode) {
                            isExactMatch = $exactFound;
                        }
                    }

                    return isExactMatch;
                },

                setSettingsFormActionState: function($view) {
                    _this.buildPendingChangesData($view);

                    // show / hide the save / cancel action buttons
                    var $actionContainers = $view.find('[data-role="all-action-wrapper"]');
                    if ($.isEmptyObject(_this.pendingSettingsChanges)) {
                        $actionContainers.addClass('hide');
                    }
                    else {
                        $actionContainers.removeClass('hide');
                    }
                },

                buildPendingChangesData: function($view) {
                    var changes = {};

                    // check incoming calls
                    var incomingCallsCurrentValue = $($view.find('input[name="IncomingCalls"]:checked')[0]).val();
                    if (settingsManager.data.IncomingCalls[incomingCallsCurrentValue] === false) {
                        changes.IncomingCalls = incomingCallsCurrentValue;
                    }

                    // check forward all calls number
                    var forwardAllCallsNumber = $view.find('input[name="ForwardAllCallsNumber"]')[0].value;
                    if (forwardAllCallsNumber !== settingsManager.data.cfu.targetInfo) {
                        changes.ForwardAllCallsNumber = forwardAllCallsNumber;
                    }

                    // check block anonymous calls
                    var anonymousCallsValue = $view.find('.js-checkbox-switch[name="BlockAnonymousCalls"]')[0].checked;
                    if (anonymousCallsValue !== settingsManager.data.ACR.checked) {
                        changes.ACR = anonymousCallsValue;
                    }

                    // check AnyEmails
                    var anyEmailsRaw = $view.find('input[name="AnyEmail"]')[0].value;
                    var anyEmails = (anyEmailsRaw ? _this.normalizeEmailAddresses(anyEmailsRaw) : []);
                    var serverEmails = settingsManager.data.AnyEmail ? settingsManager.data.AnyEmail.SMTPAddresses : [];
                    if (!_this.areEmailArraysEqual(anyEmails, serverEmails)) {
                        changes.AnyEmail = anyEmails;
                    }

                    // check email voicemails
                    var emailVoicemailMessages = $view.find('.js-checkbox-switch[name="ForwardVoicemailsToAnyEmail"]')[0].checked;
                    if (emailVoicemailMessages !== settingsManager.data.Notification.NotificationMappings_IncomingVoiceMail) {
                        changes.ForwardVoicemailsToAnyEmail = emailVoicemailMessages;
                    }

                    // check email voicemail capacity
                    var emailCapacityWarnings = $view.find('.js-checkbox-switch[name="ForwardVoicemailCapacityWarningsToAnyEmail"]')[0].checked;
                    if (emailCapacityWarnings !== settingsManager.data.Notification.NotificationMappings_VoiceMailboxSpace) {
                        changes.ForwardVoicemailCapacityWarningsToAnyEmail = emailCapacityWarnings;
                    }

                    // set local variable with changes
                    _this.pendingSettingsChanges = changes;
                },

                setupLocalRouting: function() {
                    settingsManager.init();

                    switch(window.location.hash){
                        case '#/settings/basic/':
                            // look for email param
                            var email = window.location.hash.indexOf('?email=true') >= 0;

                            // render basic with proper email param
                            _this.renderBasic(email);
                            $('.settings-tabs a[href="#/settings/basic/"]').closest('li').addClass('active');
                            break;
                        case '#/settings/911address/':
                            _this.render911Address();
                            $('.settings-tabs a[href="#/settings/911address/"]').closest('li').addClass('active');
                            break;
                        case '#/settings/profile/':
                            _this.renderProfile();
                            $('.settings-tabs a[href="#/settings/profile/"]').closest('li').addClass('active');
                            break;
                        case '#/settings/about/':
                            $('.settings-tabs a[href="#/settings/about/"]').closest('li').addClass('active');
                            $('#settings-list').html(aboutTemplate());
                            this.hideLoading();
                            break;
                        default:
                            _this.renderBasic();
                            $('.settings-tabs a[href="#/settings/basic/"]').closest('li').addClass('active');
                    }
                },

                renderBasic: function(setFocusToEmail) {
                    settingsManager.getSettings({
                        onSuccess: function(response) {
                            settingsManager.filterLoadSettings(response); // TODO replace this with settingsManager.load() ??
                            // settingsManager will store all services information

                            // TODO replace inside data on init
                            settingsManager.data.NOTSMSEnabled = settingsManager.NOTSMSEnabled;
                            settingsManager.data.NOTVoicemailEnabled = settingsManager.NOTVoicemailEnabled;

                            var $view = _this.renderTemplate('settings/basic', settingsManager.data);

                            _this.setupSettingsHandlers($view);

                            _this.setupAnyEmailSettingsInput($view);

                            _this.hideLoading();

                            // this method is to make sure that the retrieved server settings
                            // are valid - it's possible they may not be
                            _this.checkBasicServerData($view);

                            // set email focus if necessary
                            if (setFocusToEmail === true) {
                                $view.find('input[name="AnyEmail"]').next().find('input').focus();
                            }
                        },
                        onError: function() {
                            $('#settings-list').html('<div class="bigger-110 alert alert-danger">There was an error retrieving User settings!</div>');
                            _this.hideLoading();
                        }
                    });
                },

                checkBasicServerData: function($view) {
                    var validationFailures = [],
                        callbacks = [];

                    // check email settings
                    if (settingsManager.data.AnyEmail.SMTPAddresses.length <= 0) {
                        if (settingsManager.data.Notification.NotificationMappings_IncomingVoiceMail) {
                            validationFailures.push({
                                message: '"Voicemail Messages" is enabled but "Send to Email Addresses" service does not contain any emails.',
                                resolution: '"Voicemail Messages" will be disabled.'
                            });
                            callbacks.push(function() {
                                $view.find('[name="ForwardVoicemailsToAnyEmail"]').get(0).checked = false;
                                _this.saveBasic($view);
                            });
                        }
                        if (settingsManager.data.Notification.NotificationMappings_VoiceMailboxSpace) {
                            validationFailures.push({
                                message: '"Voicemail Capacity Warnings" is enabled but "Send to Email Addresses" service does not contain any emails.',
                                resolution: '"Voicemail Capacity Warnings" will be disabled.'
                            });
                            callbacks.push(function() {
                                $view.find('[name="ForwardVoicemailCapacityWarningsToAnyEmail"]').get(0).checked = false;
                                _this.saveBasic($view);
                            });
                        }
                    }

                    // get all of the current values for the incoming calls services
                    var blockAllCalls = settingsManager.data.IncomingCalls.BlockAllCalls; // takes precedence over the rest (because it's in ICB)
                    var sendAllCallsToNumber = settingsManager.data.IncomingCalls.SendAllCallsToVoicemail; // 2nd precedence is CFU
                    var sendAllCallsToVoicemail = settingsManager.data.IncomingCalls.ForwardAllCallsToNumber; // last precedence is CFUVM

                    // check for and handle invalid data scenarios
                    // rules are to be enforced in the following order: BlockAllCalls, ForwardAllCallsToNumber, SendAllCallsToVoicemail
                    if (blockAllCalls && (sendAllCallsToNumber || sendAllCallsToVoicemail)) {
                        // needs to set block all calls to be the current setting
                        validationFailures.push({
                            message: '"Incoming Calls" contains more than one enabled service at the same time.',
                            resolution: '"Block All Calls" will be enabled since it takes precedence over the other services.'
                        });
                        callbacks.push(function() {
                            _this.showLoading(true);
                            _this.fixIncomingCommunicationRules($view, 'BlockAllCalls')
                                .done(function() {
                                    _this.renderBasic();
                                }).fail(function() {
                                    logger.log('Error fixing rules...');
                                });
                        });
                    } else if (sendAllCallsToNumber && sendAllCallsToVoicemail) {
                        // needs to set sendAllCallsToNumber to the current setting
                        validationFailures.push({
                            message: '"Incoming Calls" contains more than one enabled service at the same time.',
                            resolution: '"Forward All Calls to Number" will be enabled since it takes precedence over the other services.'
                        });
                        callbacks.push(function() {
                            _this.showLoading(true);
                            _this.fixIncomingCommunicationRules($view, 'ForwardAllCallsToNumber')
                                .done(function() {
                                    _this.renderBasic();
                                }).fail(function() {
                                    logger.log('Error fixing rules...');
                                });
                        });
                    }

                    // if there is invalid data then show the error message
                    if (validationFailures.length) {
                        bootbox.dialog({
                            title: '<div class="settings-error-header">Invalid Rules Detected</div>',
                            message: template.buildTemplate('settings/server-error-data', { validationFailures: validationFailures }),
                            buttons: {
                                'ok': {
                                    'label': 'OK',
                                    'className': 'btn-sm btn-primary',
                                    'callback': function() {
                                        $.each(callbacks, function(ind, callback) {
                                            if (callback && typeof callback === 'function') {
                                                callback();
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                },

                showFormGroupError: function($formGroup, errorMessage) {
                    if (!$formGroup.hasClass('has-error')) {
                        $formGroup.addClass('has-error').append(
                            '<div class="col-xs-12 form-group-error-row">' +
                            '<label class="help-block">' + errorMessage + '</label>' +
                            '</div>');
                    }
                },

                clearFormGroupError: function($formGroup) {
                    $formGroup.removeClass('has-error');
                    $formGroup.find('.form-group-error-row').remove();
                },

                saveBasic: function($view) {
                    _this.saveBasicExecute($view).done(function() {
                        _this.renderBasic();
                    }).fail(function(errors) {
                        if (errors && $.isArray(errors)) {
                            // we have array of deferred errors
                            $.each(errors, function(ind, error) {
                                $.gritter.add({
                                    text: error,
                                    class_name: 'gritter-error'
                                });
                            });
                        } else if (errors) {
                            // we have only one error
                            $.gritter.add({
                                text: errors,
                                class_name: 'gritter-error'
                            });
                        } else {
                            // generic error
                            $.gritter.add({
                                text: 'Something went wrong.',
                                class_name: 'gritter-error'
                            });
                        }

                        _this.hideLoading();
                    });
                },

                saveBasicExecute: function($view) {
                    // build pending changes from form data present
                    _this.buildPendingChangesData($view);

                    if ($.isEmptyObject(_this.pendingSettingsChanges)) {
                        logger.log('No pending changes were found to save.');
                    }
                    else {
                        var requests = [],
                            validated = true,
                            $incomingCallsDef = $.Deferred(),
                            $blockAnonymousCallsDef = $.Deferred(),
                            $anyEmailsDef = $.Deferred(),
                            $emailVoicemailMessagesDef = $.Deferred(),
                            $emailCapacityWarningsDef = $.Deferred(),
                            incomingCallsCurrentValue = $($view.find('input[name="IncomingCalls"]:checked')[0]).val();

                        // check for incoming calls changes
                        if ('IncomingCalls' in _this.pendingSettingsChanges || 'ForwardAllCallsNumber' in _this.pendingSettingsChanges) {
                            var input = $view.find('input[name="ForwardAllCallsNumber"]')[0];
                            var number = input.value;
                            var $input = $(input);
                            var sendNumber = false;

                            // if ForwardAllCallsToNumber is selected we need to validate the phone number
                            if (_this.pendingSettingsChanges.IncomingCalls === 'ForwardAllCallsToNumber' || incomingCallsCurrentValue === 'ForwardAllCallsToNumber') {
                                sendNumber = true;
                            }

                            if (sendNumber) {
                                if (!validations.isValidPhone(number)) {
                                    // set local variable
                                    validated = false;

                                    // set validation message on form if necessary
                                    var $formGroup = $input.closest('.form-group');
                                    if (!$formGroup.hasClass('has-error')) {
                                        $formGroup.addClass('has-error').append(
                                            '<div class="col-xs-12 form-group-error-row"><label class="help-block">' +
                                            validations.invalidPhoneNumberMessage() +
                                            '</label></div>');
                                    }
                                    $input.focus();
                                }
                            }

                            requests.push(function() {
                                    settingsManager.handleIncomingCalls({
                                        ForwardAllCallsToNumber: sendNumber ? number : '',
                                        Option: sendNumber ? 'ForwardAllCallsToNumber' : _this.pendingSettingsChanges.IncomingCalls

                                    }).done(function(response) {
                                        $incomingCallsDef.resolve(response);

                                    }).fail(function(response) {
                                        $incomingCallsDef.reject(response);
                                    });
                                }
                            );
                        }
                        else {
                            $incomingCallsDef.resolve();
                        }

                        // check for block anonymous calls changes
                        if ('ACR' in _this.pendingSettingsChanges) {
                            requests.push(function() {
                                settingsManager.updateBlockAllAnonymousCalls({
                                    ruleEnabled: _this.pendingSettingsChanges.ACR,
                                    onSuccess: function(response) {
                                        $blockAnonymousCallsDef.resolve(response);
                                    },
                                    onError: function(response) {
                                        $blockAnonymousCallsDef.reject(response);
                                    }
                                });
                            });
                        }
                        else {
                            $blockAnonymousCallsDef.resolve();
                        }

                        // check for AnyEmail changes
                        var emailIsValid = _this.validateEmailSettings($view);
                        if (!emailIsValid) {
                            validated = false;
                            $anyEmailsDef.resolve();
                        }
                        else if ('AnyEmail' in _this.pendingSettingsChanges) {
                            // validate the email addresses

                            requests.push(function() {
                                settingsManager.updateAnyEmailService({
                                    SMTPAddresses: _this.pendingSettingsChanges.AnyEmail,
                                    onSuccess: function(response) {
                                        $anyEmailsDef.resolve(response);
                                    },
                                    onError: function(response) {
                                        $anyEmailsDef.reject(response);
                                    }
                                });
                            });
                        }
                        else {
                            $anyEmailsDef.resolve();
                        }

                        // check for Email notification changes
                        // NOTE this request can be packaged and therefore checks for both
                        // settings changes first and then the individual settings
                        if ('ForwardVoicemailsToAnyEmail' in _this.pendingSettingsChanges && 'ForwardVoicemailCapacityWarningsToAnyEmail' in _this.pendingSettingsChanges) {
                            requests.push(function() {
                                settingsManager.updateForwardVoicemailToBoth({
                                    ruleEnabled: _this.pendingSettingsChanges.ForwardVoicemailsToAnyEmail && _this.pendingSettingsChanges.ForwardVoicemailCapacityWarningsToAnyEmail,
                                    onSuccess: function(response) {
                                        $emailVoicemailMessagesDef.resolve(response);
                                        $emailCapacityWarningsDef.resolve(response);
                                    },
                                    onError: function(response) {
                                        $emailVoicemailMessagesDef.reject(response);
                                        $emailCapacityWarningsDef.reject(response);
                                    }
                                });
                            });
                        }
                        else if ('ForwardVoicemailsToAnyEmail' in _this.pendingSettingsChanges) {
                            requests.push(function() {
                                settingsManager.updateForwardVoicemailToAnyEmail({
                                    ruleEnabled: _this.pendingSettingsChanges.ForwardVoicemailsToAnyEmail,
                                    ruleId: 'IncomingVoiceMail',
                                    onSuccess: function(response) {
                                        $emailVoicemailMessagesDef.resolve(response);
                                        $emailCapacityWarningsDef.resolve(response);
                                    },
                                    onError: function(response) {
                                        $emailVoicemailMessagesDef.reject(response);
                                        $emailCapacityWarningsDef.reject(response);
                                    }
                                });
                            });
                        }
                        else if ('ForwardVoicemailCapacityWarningsToAnyEmail' in _this.pendingSettingsChanges) {
                            requests.push(function() {
                                settingsManager.updateForwardVoicemailToAnyEmail({
                                    ruleEnabled: _this.pendingSettingsChanges.ForwardVoicemailCapacityWarningsToAnyEmail,
                                    ruleId: 'VoiceMailboxSpace',
                                    onSuccess: function(response) {
                                        $emailVoicemailMessagesDef.resolve(response);
                                        $emailCapacityWarningsDef.resolve(response);
                                    },
                                    onError: function(response) {
                                        $emailVoicemailMessagesDef.reject(response);
                                        $emailCapacityWarningsDef.reject(response);
                                    }
                                });
                            });
                        }
                        else {
                            $emailVoicemailMessagesDef.resolve();
                            $emailCapacityWarningsDef.resolve();
                        }

                        if (!validated) {
                            return $.Deferred().reject('Invalid data has been detected. Please fix the errors on the page and try again.');
                        }
                        else {
                            // show loading
                            _this.showLoading(true);

                            // run each of the queued up requests
                            $.each(requests, function(ind, req) {
                                req();
                            });

                            // wrap up all the requests and return them as a single pipeline
                            return $.when($incomingCallsDef, $blockAnonymousCallsDef,
                                $anyEmailsDef, $emailVoicemailMessagesDef, $emailCapacityWarningsDef);
                        }
                    }

                    return $.Deferred().resolve();
                },

                fixIncomingCommunicationRules: function($view, ruleToEnable) {
                    var number = '';
                    if (ruleToEnable === 'ForwardAllCallsToNumber') {
                        number = $view.find('input[name="ForwardAllCallsNumber"]')[0].value;
                    }
                    return settingsManager.handleIncomingCalls({
                        ForwardAllCallsToNumber: number,
                        Option: ruleToEnable
                    });
                },

                setupSettingsHandlers: function($view) {
                    // If settings are modified/changed on a page, a user should receive a pop-up box
                    $('.settings-tabs.nav-tabs a[href]').off('click').on('click', function(event, additionalData) {
                        if (!additionalData) {
                            if (!$(this).closest('li').hasClass('active')) {
                                if (!$('[data-role="all-action-wrapper"]').hasClass('hide')) {
                                    event.preventDefault();
                                    event.stopPropagation();
                                    var messageHTML = '<div>There are currently <b>Pending Changes</b> to your Settings.&nbsp;&nbsp;Would you like to Save these Changes or Cancel?</div>';

                                    bootbox.dialog({
                                        title: 'Changes Pending',
                                        message: messageHTML,
                                        buttons: {
                                            'ok': {
                                                'label': 'Save',
                                                'className': 'btn-sm btn-success',
                                                'callback': function() {
                                                    _this.saveBasic($view);
                                                }
                                            },
                                            'cancel': {
                                                'label': 'Cancel',
                                                'className': 'btn-sm btn-primary',
                                                'callback': function() {
                                                    window.location.hash = event.currentTarget.hash;
                                                }
                                            }
                                        }
                                    });
                                }
                            } else {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                        }
                    });

                    // setup generic listener for showing title
                    $view.on('click', '[data-action="show-title"]', function() {
                        var $this = $(this),
                            $formGroup = $this.closest('.form-group'),
                            title = $this.attr('title'),
                            disabled = $this.is('.disabled');

                        if (title) {
                            // toggle tip row
                            var $tipRow = $formGroup.next('.setting-row-help-tip');
                            if ($tipRow.length > 0) {
                                $tipRow.remove();
                            } else {
                                // create row
                                $tipRow = $('<div class="row setting-row-help-tip' + (disabled ? ' disabled' : '') + '"><div class="col-xs-12">' + title + '<i class="icon icon-remove"></i></div></div>');

                                // append row after the form-group row
                                $formGroup.after($tipRow);

                                // set close handler
                                $tipRow.on('click', '.icon-remove', function() {
                                    $(this).closest('.setting-row-help-tip').remove();
                                });
                            }
                        }
                    });

                    // handle the radio button group switching
                    $view.find('input[type="radio"][name="IncomingCalls"]').change(function() {
                        // get value
                        var val = $(this).val();
                        var $input = $('input[name="ForwardAllCallsNumber"]');

                        // set focus if necessary
                        if (val === "ForwardAllCallsToNumber") {
                            $input.removeClass('disabled').attr('disabled', null).focus();
                        }
                        else {
                            $input.addClass('disabled').attr('disabled', 'disabled');

                            // just in case remove any validation errors on the form
                            var $formGroup = $input.closest('.form-group');
                            $formGroup.removeClass('has-error');
                            $formGroup.find('.form-group-error-row').remove();
                        }

                        _this.setSettingsFormActionState($view);
                    });

                    // handle all of the check box switches
                    $view.find('.js-checkbox-switch').change(function() {
                        _this.setSettingsFormActionState($view);
                    });

                    // setup basic settings data actions (save, cancel)
                    $view.find('.js-data-action').on('click', function() {
                        var dataAction = $(this).attr('data-action');
                        switch (dataAction) {
                            case 'save':
                                _this.saveBasic($view);
                                break;

                            case 'cancel':
                                _this.renderBasic();
                                break;
                        }
                    });

                    // setup phone number change handlers
                    $view.find('input[name="ForwardAllCallsNumber"]').on('input', function() {
                        _this.setSettingsFormActionState($view);
                    });

                    // setup email tags input listener
                    $view.find('input[name="AnyEmail"]').change(function() {
                        _this.setSettingsFormActionState($view);
                    });
                },

                setupAnyEmailSettingsInput: function($view) {
                    // setup basic tags input control
                    var $emailsInput = $view.find('input[name="AnyEmail"]');
                    $emailsInput.webrtc_tagsinput({
                        tagClass: function() {
                            return 'label label-info attendees-label';
                        },
                        confirmKeys: [13, 32, 186, 188], // http://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
                        maxTags: 6,
                        addOnBlur: false,
                        freeInput: true
                    });

                    // add event handlers
                    $emailsInput.on('beforeItemAdd', function(event) {
                        if (!validations.isValidEmail(event.item)) {
                            event.cancel = true;
                            _this.showFormGroupError($emailsInput.closest('.form-group'), 'Please enter a valid Email Address');
                            $emailsInput.focus();
                        }
                        else {
                            _this.clearFormGroupError($emailsInput.closest('.form-group'));
                        }
                    });
                    $emailsInput.on('itemAdded itemRemoved', function() {
                        _this.validateEmailSettings($view);
                    });
                    $emailsInput.siblings('.bootstrap-tagsinput').on('focusout', function() {
                        _this.validateEmailSettings($view);
                    });
                },

                validateEmailSettings: function($view) {
                    var valid = true;
                    var $emailsInput = $view.find('input[name="AnyEmail"]');

                    // first check email addresses
                    var emails = _this.getAnyEmailAddresses($view);
                    $.each(emails, function(i, val) {
                        if (!validations.isValidEmail(val)) {
                            valid = false;
                            _this.showFormGroupError($emailsInput.closest('.form-group'), 'Please enter a valid Email Address');
                        }
                    });
                    if (valid) _this.clearFormGroupError($emailsInput.closest('.form-group'));

                    // if there aren't any email addresses then make sure neither of the email forwarding settings are true
                    if ($emailsInput.val() === '') {
                        // check forward voicemails setting
                        var $forwardVoicemails = $view.find('input[name="ForwardVoicemailsToAnyEmail"]');
                        if ($forwardVoicemails.get(0).checked) {
                            valid = false;
                            _this.showFormGroupError($forwardVoicemails.closest('.form-group'), 'Valid email address is required');
                        }
                        else {
                            _this.clearFormGroupError($forwardVoicemails.closest('.form-group'));
                        }

                        // check voicemail capacity settings
                        var $forwardVoicemailCapacityWarnings = $view.find('input[name="ForwardVoicemailCapacityWarningsToAnyEmail"]');
                        if ($forwardVoicemailCapacityWarnings.get(0).checked) {
                            valid = false;
                            _this.showFormGroupError($forwardVoicemailCapacityWarnings.closest('.form-group'), 'Valid email address is required');
                        }
                        else {
                            _this.clearFormGroupError($forwardVoicemailCapacityWarnings.closest('.form-group'));
                        }
                    }

                    return valid;
                },

                setup911AddressHandlers: function($view) {
                    var ids = [];
                    $.validator.addClassRules({
                        AddressLine1_Validation: {
                            required: true
                        },
                        City_Validation: {
                            required: true
                        },
                        ZipCode_Validation: {
                            digits: true,
                            required: true
                        }
                    });

                    $view.on('click', '.js-action', function() {
                        var $button = $(this)
                            , dataAction = $button.attr('data-action')
                            , $formGroup = $button.closest('.form-group')
                            , ContactId = $formGroup.attr('data-id');

                        switch (dataAction) {
                            case 'add-address':
                                var newAddress = contactManager.defineContactAddressSelect();
                                newAddress.uniqueId = _this.generateUniqueId(ids, true);
                                var $html = $(template.buildTemplate('settings/contact-911address-edit', newAddress));

                                // append and show the row
                                $html.insertBefore($formGroup).find('.focus-new').focus();
                                _this.set911AddressControlsState($view, 1);

                                $html.find('form').validate({
                                    errorClass: 'help-block',
                                    highlight: function(e) {
                                        $(e).closest('.form-group').addClass('has-error');
                                    },
                                    unhighlight: function(e) {
                                        $(e).closest('.form-group').removeClass('has-error');
                                    },
                                    submitHandler: function(form) {
                                        var $formGroup = $(form).closest('.form-group')
                                            , $button = $formGroup.find('button[type="submit"]');

                                        _this.setActionUI($button, window.$wrtc.enums.updateStatus.Updating).done(function() {
                                            var serializedData = _this.serializeProfileContact($formGroup, true)[0].data;

                                            settingsManager.add911SubscriberInformation({
                                                ContactAddress: serializedData,
                                                onSuccess: function() {
                                                    logger.log('Successfully added new 911 Address');
                                                    _this.render911Address();
                                                },
                                                onError: function(errorText) {
                                                    logger.log('Error adding new 911 Address');
                                                    _this.setActionUI($button, window.$wrtc.enums.updateStatus.NotUpdated);
                                                    $.gritter.add({
                                                        text: errorText,
                                                        class_name: 'gritter-error gritter-center'
                                                    });
                                                }
                                            });
                                        });
                                    }
                                });
                                break;

                            case 'edit-address': // update existing contact 911 address
                                if (ContactId !== undefined) {
                                    $.each(settingsManager.addresses911, function(ind, address) {
                                        if (address.ContactId === ContactId) {
                                            address.uniqueId = _this.generateUniqueId(ids, true);
                                            var $html = $(template.buildTemplate('settings/contact-911address-edit', contactManager.defineContactAddressSelect(address)));
                                            $formGroup.replaceWith($html);
                                            $html.find('.focus-edit').focus();

                                            $html.find('form').validate({
                                                errorClass: 'help-block',
                                                highlight: function(e) {
                                                    $(e).closest('.form-group').addClass('has-error');
                                                },
                                                unhighlight: function(e) {
                                                    $(e).closest('.form-group').removeClass('has-error');
                                                },
                                                submitHandler: function(form) {
                                                    var $formGroup = $(form).closest('.form-group')
                                                        , ContactId = $formGroup.attr('data-id')
                                                        , $button = $formGroup.find('button[type="submit"]');

                                                    if (ContactId !== undefined) {
                                                        _this.setActionUI($button, window.$wrtc.enums.updateStatus.Updating).done(function() {
                                                            var serializedData = _this.serializeProfileContact($formGroup, true)[0].data;
                                                            serializedData.AddressId = address.ContactAddress.AddressId;
                                                            settingsManager.update911SubscriberInformation({
                                                                ContactAddress: serializedData,
                                                                oldContactAddress: address.ContactAddress,
                                                                ContactId: address.ContactId,
                                                                onSuccess: function() {
                                                                    logger.log('Successfully updated 911 Address');
                                                                    _this.render911Address();
                                                                },
                                                                onError: function(errorText) {
                                                                    logger.log('Error updating 911 Address');
                                                                    _this.setActionUI($button, window.$wrtc.enums.updateStatus.NotUpdated);
                                                                    $.gritter.add({
                                                                        text: errorText,
                                                                        class_name: 'gritter-error gritter-center'
                                                                    });
                                                                }
                                                            });
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                                break;

                            case 'cancel-edit-address':
                                _this.render911Address();
                                break;

                            case 'delete-address':
                                bootbox.confirm("Are you sure you want to delete this Address?", function(result) {
                                    if (result) {
                                        // get id and address from local array
                                        var address = $.grep(settingsManager.addresses911, function(obj) {
                                            return obj.ContactId !== null && obj.ContactId !== undefined && obj.ContactId === ContactId;
                                        });
                                        _this.setActionUI($button, window.$wrtc.enums.updateStatus.Updating).done(function() {
                                            settingsManager.delete911SubscriberInformation({
                                                ContactId: address[0].ContactId,
                                                ContactAddress: address[0].ContactAddress,
                                                onSuccess: function() {
                                                    logger.log('Successfully deleted 911 Address');
                                                    _this.render911Address();
                                                },
                                                onError: function(errorText) {
                                                    logger.log('Error deleting 911 Address');
                                                    _this.setActionUI($button, window.$wrtc.enums.updateStatus.NotUpdated);
                                                    $.gritter.add({
                                                        text: errorText,
                                                        class_name: 'gritter-error gritter-center'
                                                    });
                                                }
                                            });
                                        });
                                    }
                                });

                                break;

                            case 'make-current-911':
                                // get the list of any emails
                                var emails = [];
                                if (settingsManager.data && settingsManager.data.AnyEmail) {
                                    emails = settingsManager.data.AnyEmail.SMTPAddresses;
                                }

                                var message = '';
                                if (emails && emails.length > 0) {
                                    // get the address text to display
                                    var address = $formGroup.find('.js-911-address').text().trim();

                                    // build the emails select html
                                    var emailsSelectHtml = '<select class="any-email-select">';
                                    $.each(emails, function() {
                                        emailsSelectHtml += '<option value="' + this + '">' + this + '</option>';
                                    });
                                    emailsSelectHtml += '</select>';

                                    message = '<div class="margin-10-bottom">Your new 911 Address will be "' + address + '."</div>' +
                                    '<div class="margin-10-bottom">Please select an email address so we can confirm when your 911 address is updated:</div>' +
                                    '<div class="margin-10-bottom">' + emailsSelectHtml + '</div>' +
                                    '<div><a href="javascript:void(0);" class="js-manage-emails">Add or Change</a> your Email Notification Addresses on the Settings tab.</div>';
                                }
                                else {
                                    message = '<div class="margin-10-bottom">To change your 911 Address you must provide a valid email address.</div>' +
                                    '<div><a href="javascript:void(0);" class="js-manage-emails">Add or Change</a> your Email Notification Addresses on the Settings tab.</div>';
                                }

                                bootbox.dialog({
                                    title: emails && emails.length > 0 ? 'Confirm 911 Address Change' : 'Email Address Required',
                                    message: message,
                                    buttons: {
                                        success: {
                                            label: emails && emails.length > 0 ? 'Submit' : 'Ok',
                                            className: 'btn-success btn-sm',
                                            callback: function(e) {
                                                if (emails && emails.length > 0) {
                                                    if (ContactId !== undefined) {
                                                        var $modal = $(e.target).closest('.bootbox.modal');
                                                        var email = $modal.find('.any-email-select').val();

                                                        var address = $.grep(settingsManager.addresses911, function(obj) {
                                                            return obj.ContactId !== null && obj.ContactId !== undefined && obj.ContactId === ContactId;
                                                        });

                                                        _this.showLoading(true);
                                                        _this.getContactData()
                                                            .done(function(contact) {
                                                                settingsManager.profileContact = contact;
                                                                settingsManager.set911AddressAsMain({
                                                                    ContactAddress: address[0].ContactAddress,
                                                                    ContactId: address[0].ContactId,
                                                                    ContactEmail: email,
                                                                    onSuccess: function() {
                                                                        logger.log('Successfully updated current 911 Address');
                                                                        _this.render911Address();
                                                                    },
                                                                    onError: function(errorText) {
                                                                        _this.hideLoading();
                                                                        window.console.error('Error ocurred while setting current 911 Address');
                                                                        $.gritter.add({
                                                                            text: errorText,
                                                                            class_name: 'gritter-error gritter-center'
                                                                        });
                                                                    }
                                                                });
                                                            })
                                                            .fail(function() {
                                                                _this.hideLoading();
                                                                var errorText = 'Error ocurred while get profile info';
                                                                window.console.error(errorText);
                                                                $.gritter.add({
                                                                    text: errorText,
                                                                    class_name: 'gritter-error gritter-center'
                                                                });
                                                            });
                                                    }
                                                }
                                                else {
                                                    window.location.hash = '#/settings/basic/?email=true';
                                                }
                                            }
                                        },
                                        main: {
                                            label: 'Cancel',
                                            className: 'btn-sm'
                                        }
                                    }
                                }).off('shown.bs.modal').on('shown.bs.modal', function(e) {
                                    // setup handler for changing email addresses
                                    $(e.target).find('.js-manage-emails').off('click').on('click', function() {
                                        bootbox.hideAll();
                                        window.location.hash = '#/settings/basic/?email=true';
                                    });
                                });
                                break;

                            case 'disclamer-911':
                                _this.setupDisclaimerWarning();
                                break;
                        }
                    });
                },

                set911AddressControlsState: function($view, increment) {
                    increment = increment || 0;

                    var compareValue = settingsManager.addresses911.length + increment;
                    var buttonAddAddress = $view.find('button[data-action="add-address"]');
                    if (compareValue >= _this.max911Addresses) {
                        buttonAddAddress.hide();
                    } else {
                        buttonAddAddress.show();
                    }
                },

                serializeProfileContact: function($view, forceElement) {
                    var $items, resultData = [];
                    if (forceElement) {
                        $items = $view;
                    } else {
                        $items = $view.find('.form-group:not(.new-child-row)');
                    }

                    $.each($items, function(ind, item) {
                        var $item = $(item)
                            , $inputOrSelect = $item.find('input,select')
                            , data_id = $item.attr('data-id')
                            , data_model = $item.attr('data-model')
                            , data = $inputOrSelect.serializeObject()
                            , rewriteByKeyName = function(_object, _str) {
                                var key;
                                for (key in _object) {
                                    if (_object.hasOwnProperty(key)) {
                                        if (key.indexOf(_str) > -1 && key !== _str) {
                                            _object[_str] = _object[key];
                                            delete _object[key];
                                            return true;
                                        }
                                    }
                                }
                                return false;
                            }
                            ;
                        if (data_model) {
                            if (data_id) {
                                data.id = data_id;
                            }
                            switch (data_model) {
                                case 'Phone':
                                    rewriteByKeyName(data, 'contactType');
                                    rewriteByKeyName(data, 'value');
                                    break;
                                case 'Email':
                                    rewriteByKeyName(data, 'contactLocation');
                                    rewriteByKeyName(data, 'value');
                                    break;
                                case 'ContactAddress':
                                    rewriteByKeyName(data, 'locationType');
                                    rewriteByKeyName(data, 'streetLine1');
                                    rewriteByKeyName(data, 'city');
                                    rewriteByKeyName(data, 'state');
                                    rewriteByKeyName(data, 'zip');
                                    break;
                                case 'Contact911Address':
                                    rewriteByKeyName(data, 'AddressLine1');
                                    rewriteByKeyName(data, 'City');
                                    rewriteByKeyName(data, 'State');
                                    rewriteByKeyName(data, 'ZipCode');
                                    break;
                                case 'InstantMessengerAddress':
                                    rewriteByKeyName(data, 'contactType');
                                    rewriteByKeyName(data, 'value');
                                    break;
                                case 'ContactNote':
                                    rewriteByKeyName(data, 'priority');
                                    rewriteByKeyName(data, 'subject');
                                    rewriteByKeyName(data, 'description');
                                    break;
                            }

                            resultData.push({
                                $selector: $item,
                                model: data_model,
                                data: data
                            });
                        }
                    });

                    return resultData;
                },

                setupContactSubmitHandler: function($view) {
                    $.validator.addMethod('zipField', function(value) {
                        return value.match(/^\d{5}$|^\d{5}-\d{4}$/);
                    }, 'Zip must be in the valid 5-digit or ZIP+4 format (e.g. XXXXX, XXXXXXXXX, XXXXX-XXXX)');

                    $.validator.addMethod('phoneNumber', function(value) {
                        return validations.isValidPhone(value);
                    }, validations.invalidPhoneNumberMessage());

                    $.validator.addMethod('emailAddress', function(value) {
                        return validations.isValidEmail(value);
                    }, validations.invalidEmailAddressMessage());

                    $.validator.addMethod('alphaCommaPeriodHyphen', function(value) {
                        return value.match(/^[a-z0-9, .\-]*$/i); ///^[A-Z|,|.|\-| |0-9]*$/i);
                    }, 'Only alphanumeric (a-z, A-Z, 0-9), comma (,), period (.), and hyphen (-) characters allowed');

                    $.validator.addMethod('alphaCommaPeriodHyphenSlash', function(value) {
                        return value.match(/^[a-z0-9, .\-\/ ]*$/i);
                    }, 'Only alphanumeric (a-z, A-Z, 0-9), comma (,), period (.), hyphen (-), and slash (/) characters allowed');

                    $.validator.addClassRules({
                        phoneVal: {
                            required: true,
                            phoneNumber: true
                        },
                        emailVal: {
                            required: true,
                            emailAddress: true
                        },
                        streetLine1Val: {
                            required: true,
                            maxlength: 50,
                            alphaCommaPeriodHyphenSlash: true
                        },
                        cityVal: {
                            required: true,
                            maxlength: 50,
                            alphaCommaPeriodHyphenSlash: true
                        },
                        zipVal: {
                            required: true,
                            zipField: true
                        },
                        imAddressVal: {
                            required: true
                        },
                        subjectVal: {
                            required: true,
                            maxlength: 50
                        },
                        descriptionVal: {
                            required: true,
                            maxlength: 225
                        }
                    });

                    $view.find('form').validate({
                        errorClass: 'help-block',
                        highlight: function(e) {
                            $(e).closest('.form-group').addClass('has-error');
                        },
                        unhighlight: function(e) {
                            $(e).closest('.form-group').removeClass('has-error');
                        },
                        submitHandler: function() {
                            var contact = contactManager.getProfileContact()
                                , newRecordHandler = function() {
                                    eventEmitter.off(contactManager.events.ADD_RECORD, newRecordHandler);
                                    _this.renderProfile();
                                };

                            _this.showLoading(true);

                            eventEmitter.off(contactManager.events.ADD_RECORD, newRecordHandler);
                            eventEmitter.on(contactManager.events.ADD_RECORD, newRecordHandler);

                            $.waitFor(contactManager.saveContact(
                                {
                                    isSelf: true,
                                    contact: contact,
                                    contactManager: contactManager,
                                    imageInfo: _this.imageInfo,
                                    contactImageHandler: _this.contactImageHandler,
                                    serializedItems: _this.serializeProfileContact($view)
                                }));
                        },
                        rules: {
                            firstName: {
                                required: true,
                                maxlength: 50,
                                alphaCommaPeriodHyphen: true
                            },
                            lastName: {
                                required: true,
                                maxlength: 50,
                                alphaCommaPeriodHyphen: true
                            }
                        }
                    });
                },

                setupDisclaimerWarning: function() {
                    var $disclamerView = $(template.buildTemplate('settings/disclamer', {}));

                    $disclamerView.on('click', '.data-action', function() {
                        var val = $(this).attr('data-val');

                        switch (val) {
                            case 'gotIt' :
                                _this.transitionSidebarOut();
                                break;
                        }
                    });

                    _this.transitionSidebarIn($disclamerView);
                },

                renderProfile: function() {
                    _this.showLoading(true);
                    _this.getContactData().done(function(contact) {
                        var $profileView = _this.renderTemplate('settings/profile-view', contact);
                        _this.hideLoading();
                        // before enter into image-functionality
                        if (contact.images.length) {
                            _this.contactImageHandler = window.$wrtc.enums.contactImageHandler.has;
                        } else {
                            _this.contactImageHandler = window.$wrtc.enums.contactImageHandler.none;
                        }
                        _this.imageInfo = {};

                        $profileView.on('click', '.data-action', function() {
                            var val = $(this).attr('data-val');

                            switch (val) {
                                case 'edit' :
                                    var $profileEditView = $(_this.renderTemplate('settings/profile-edit', contact));

                                    _this.setupContactSubmitHandler($profileEditView);
                                    $profileEditView.find('.remove-avatar').on('click', function() {
                                        $(this).hide();
                                        var $contactImage = $(this).closest('.profile-picture').find('img');
                                        $contactImage.attr('src', 'images/no_image_.jpg');
                                        _this.contactImageHandler = window.$wrtc.enums.contactImageHandler.none;
                                    });

                                    $profileEditView.on('click', '.data-action', function(event) {
                                        event.stopPropagation();
                                        var $this = $(this);
                                        var val = $this.attr('data-val');

                                        switch (val) {
                                            case 'cancel' :
                                                _this.renderProfile();
                                                break;

                                            case 'picture' :
                                                _this.setupContactImage({ contact: contact, $contactImage: $this });
                                                break;
                                        }
                                    });

                                    var ids = (function(_contact) {
                                        var sumArray = _contact.phoneNumbers
                                                .concat(_contact.emailAddress)
                                                .concat(_contact.addresses)
                                                .concat(_contact.imAddresses)
                                                .concat(_contact.notes)
                                            , result = $.map(sumArray, function(elementOfArray) {
                                                return elementOfArray.id;
                                            });

                                        return result;
                                    })(contact);

                                    // new item handler
                                    $profileEditView.on('click', '.js-child-data-action', function() {
                                        var $this = $(this)
                                            , val = $this.attr('data-val')
                                            , $formGroup = $this.closest('.form-group')
                                            , data_id = $formGroup.attr('data-id');

                                        if (val == 'new-child-row') {
                                            var newObject = {
                                                    id: _this.generateUniqueId(ids, true),
                                                    newItem: true,
                                                    contactTypes: contact.contactTypes,
                                                    contactLocations: contact.contactLocations,
                                                    locationTypes: contact.locationTypes,
                                                    states: contact.states,
                                                    imTypes: contact.imTypes,
                                                    priorities: contact.priorities
                                                }
                                                , HTML = template.buildTemplate($this.attr('data-templ-path'), newObject);

                                            // append and show the row
                                            $(HTML).insertBefore($formGroup);
                                            $formGroup.prev().find('.focus-new').focus();

                                        } else if (val == 'remove-child-row') {
                                            // remove the nearest row
                                            $formGroup.remove();

                                            $.each(ids, function(ind, element) {
                                                if (element === data_id) {
                                                    ids.splice(ind, 1);
                                                    return false;
                                                }
                                                return true;
                                            });
                                        }
                                    });
                                    break;
                            }
                        });
                    }).fail(function() {
                        $('#settings-list').html('<div class="alert alert-danger bigger-110">There was an error retrieving profile data!</div>');
                        _this.hideLoading();
                    });
                },

                render911Address: function() {
                    settingsManager.get911SubscriberInformation({
                        onSuccess: function() {
                            var viewParams = {
                                addresses911: settingsManager.addresses911.slice(0, _this.max911Addresses),
                                are911AddressesPending: settingsManager.are911AddressesPending()
                            };

                            var $view = _this.renderTemplate('settings/advanced', viewParams);
                            _this.setup911AddressHandlers($view);
                            _this.set911AddressControlsState($view);
                            _this.hideLoading();

                            if (!settingsManager.initial911) {
                                _this.setupDisclaimerWarning();
                            }
                        },
                        onError: function() {
                            $('#settings-list').html('<div class="alert alert-danger bigger-110">There was an error retrieving 911 addresses!</div>');
                            _this.hideLoading();
                        }
                    });
                },

                getContactData: function() {
                    var $deferred = $.Deferred();
                    var afterLoad = function() {
                        var contact = contactManager.getProfileContact();
                        if (contact) {
                            contact.prepareContactNotesPriorities();
                            contact = contactManager.defineContactSelect(contact);
                        }

                        $deferred.resolve(contact);
                    };

                    if (!contactManager.getData()) {
                        contactManager.load().done(afterLoad).fail(function() {
                            $deferred.reject();
                        });
                    } else {
                        afterLoad();
                    }

                    return $deferred;
                },

                renderTemplate: function(viewName, data) {
                    var HTML = template.buildTemplate(viewName, data),
                        $viewport = $('#settings-list');
                    $viewport.html(HTML);
                    return $viewport.children();
                },

                parseObjectFromXML: function($children) {
                    var object = {};
                    if ($children && $children.length) {
                        $.each($children, function(ind, child) {
                            var $child = $(child);
                            var nodeName = $child.prop("tagName") ? $child.prop("tagName") : 'undefined';
                            nodeName = nodeName.replace('m:', '');
                            var nodeValue;
                            if ($child.children().length > 0) {
                                nodeValue = _this.parseObjectFromXML($child.children());
                            } else {
                                nodeValue = $child.html() ? $child.html() : 'undefined';
                            }

                            if (!object[nodeName]) {
                                object[nodeName] = nodeValue;
                            } else {
                                if (typeof nodeValue === 'object') {
                                    for (var key in nodeValue) {
                                        if (nodeValue.hasOwnProperty(key)) {
                                            object[nodeName][key] = nodeValue[key];
                                        }
                                    }
                                } else {
                                    object[nodeName] = nodeValue;
                                }
                            }

                        });
                    }
                    return object;
                },

                /**
                 * Display loading state for main settings container
                 * @param  {Boolean} showSpinner flag to show spinner or not
                 */
                showLoading: function(showSpinner) {
                    var spinner = $('#main-container .widget-box-overlay .icon-spinner');

                    if (showSpinner) {
                        spinner.show();
                    } else {
                        spinner.hide();
                    }

                    $('#main-container .widget-box-overlay').show();
                },

                /**
                 * Hide loading state for main callendar container
                 */
                hideLoading: function() {
                    $('#main-container .widget-box-overlay').hide();
                },

                /**
                 * Insert calendar event detail/edit view into container and show it
                 * @param  {[type]} $view of the calendar event
                 */
                transitionSidebarIn: function($view) {
                    _this.$sidebar.find('.clearfix:first').html($view);
                    _this.$sidebar.removeClass('off-screen');
                    _this.showLocalOverlay(function() {
                        _this.transitionSidebarOut();
                    });
                },

                /**
                 * Hide calendar event detail/edit container
                 */
                transitionSidebarOut: function() {
                    _this.$sidebar.addClass('off-screen');
                    _this.hideLocalOverlay();
                },

                /**
                 * Show overlay for calendar view
                 * @param  {Function} closeCallback callback for closing overlay
                 */
                showLocalOverlay: function(closeCallback) {
                    var secondaryOverlay = $('#main-container .widget-box-overlay-secondary');
                    secondaryOverlay.show();
                    secondaryOverlay.one('click', function(e) {
                        var sidebar = $('#sidebar');
                        e.preventDefault();
                        if (!sidebar.hasClass('off-screen')) {
                            eventEmitter.trigger('hide:contacts');
                        }
                        if (closeCallback) {
                            closeCallback();
                        }
                    });
                },

                /**
                 * Hide overlay for calendar view
                 */
                hideLocalOverlay: function() {
                    $('#main-container .widget-box-overlay-secondary').hide();
                },

                setupContactImage: function(data) {
                    var contact = data.contact
                        , canceled = false
                        , $imageEditView = $(template.buildTemplate('settings/profile-image', contact))
                        , $contactImage = data.$contactImage
                        , contactImage = contact ? ( contact.images.length ? contact.images[0] : null ) : null
                        , $cropWrapper = $imageEditView.find('.crop-checkbox')
                        , showHideCropWrapper = function(showOrHide) {
                            $cropWrapper.toggle(showOrHide);
                        }
                        , showLoadingSpinner = function($element) {
                            var deferred = $.Deferred();

                            $element.block({
                                baseZ: 16,
                                css: {
                                    border: 'none',
                                    backgroundColor: 'transparent',
                                    cursor: 'auto'
                                },
                                overlayCSS: {
                                    backgroundColor: 'white',
                                    opacity: 0.7,
                                    cursor: 'auto'
                                },
                                onBlock: function() {
                                    // when element is blocked
                                    deferred.resolve();
                                },
                                message: '<i class="icon-spinner icon-spin icon-2x green"></i>'
                            });

                            return deferred;
                        }
                        , hideLoadingSpinner = function($element) {
                            $element.unblock();
                        }
                        , $checkboxCrop = $cropWrapper.find('input[type=checkbox]')
                        , $cropPreview = $imageEditView.find('.js-image-crop-view')
                        , $image = $imageEditView.find('.image-preview-wrapper img')
                        , $previewImage = $imageEditView.find('.canvas-wrapper img')
                        , $canvas = $imageEditView.find('.canvas-wrapper canvas')
                        , canvas = $canvas[0]
                        , setupJCrop = function() {
                            var JCropAPI = $image.data('Jcrop');
                            if (JCropAPI) {
                                JCropAPI.destroy();
                            }
                            // calc real size
                            var $imageBox = $image.parent()
                                , opts , fromX, fromY, toX, toY, fakeImage = new Image();

                            fakeImage.src = _this.base64Img;

                            opts = {
                                bgColor: 'transparent',
                                boxWidth: $imageBox.width(),
                                boxHeight: $imageBox.height(),
                                minSize: [$.em(1), $.em(1)],
                                trueSize: [fakeImage.width, fakeImage.height],
                                aspectRatio: 1, // TODO for this option we have to have square viewport
                                onSelect: function(_cropData) {

                                    if (parseInt(_cropData.w, 10) > 0) {
                                        canvas.width = parseInt(_cropData.w, 10);
                                        canvas.height = parseInt(_cropData.h, 10);
                                        // Show image preview
                                        var context = canvas.getContext("2d");
                                        if (context) {
                                            context.drawImage(fakeImage, _cropData.x, _cropData.y, _cropData.w, _cropData.h, 0, 0, canvas.width, canvas.height);
                                            _this.croppedBase64Img = canvas.toDataURL();
                                            $previewImage.attr('src', _this.croppedBase64Img);
                                        }
                                    }
                                }
                            };
                            fromX = fakeImage.width / 4;
                            fromY = fakeImage.height / 4;
                            toX = fakeImage.width - fromX; // TODO if aspectRatio != 1
                            toY = fakeImage.height - fromY; // TODO if aspectRatio != 1
                            $image.Jcrop(opts, function() {
                                JCropAPI = this;
                                $image.data('Jcrop', JCropAPI);
                                JCropAPI.animateTo([ fromX, fromY, toX, toY]);
                                JCropAPI.setSelect([ fromX, fromY, toX, toY]);
                            });
                        }
                        , destroyJCrop = function() {
                            $checkboxCrop.removeAttr('checked');
                            var JCropAPI = $image.data('Jcrop');
                            if (JCropAPI) {
                                JCropAPI.destroy();
                            }
                            // clean canvas
                            canvas.width = canvas.width;
                            _this.croppedBase64Img = '';
                            $previewImage.attr('src', '');
                        }
                        , adjustImage = function($wrapper) {
                            var $box = $wrapper.is('img') ? $wrapper.parent() : $wrapper
                                , boxWidth = $box.width() // TODO calc padding too
                                , $image = $wrapper.is('img') ? $wrapper : $box.find('img'),
                                _adjustImage = function() {
                                    var fakeImage = new Image()
                                        , ratio = 1;
                                    fakeImage.src = $image.attr('src');
                                    if (fakeImage.width > 0 && fakeImage.width < boxWidth) {
                                        // extend image by height
                                        ratio = fakeImage.width / boxWidth;
                                        $image.css({
                                            height: ratio * fakeImage.height,
                                            width: ratio * fakeImage.width,
                                            maxWidth: ratio * fakeImage.width
                                        });
                                    } else {
                                        $image.removeAttr('style');
                                    }
                                }
                                ;
                            _adjustImage();
                        }
                        ;

                    $checkboxCrop.removeAttr('checked').on('change', function() {
                        if (this.checked) {
                            setupJCrop();
                            $cropPreview.removeClass('hide');
                        } else {
                            destroyJCrop();
                            $cropPreview.addClass('hide');
                        }
                    });

                    // set the widget content and then transition the view in
                    _this.transitionSidebarIn($imageEditView);

                    // entry point
                    // prepare image

                    if (_this.contactImageHandler === window.$wrtc.enums.contactImageHandler.has) {
                        showLoadingSpinner($image.parent()).done(function() {
                            $.convertImgToBase64(contactImage.imageUrl, function(base64Img) {
                                if (!canceled) {
                                    $image.attr('src', base64Img);
                                    adjustImage($image);
                                    _this.base64Img = base64Img;
                                    showHideCropWrapper(true);
                                }

                                hideLoadingSpinner($image.parent());

                            }, contactImage.imageMIMEType);
                        });
                    } else if (_this.contactImageHandler === window.$wrtc.enums.contactImageHandler.none) {
                        $image.attr('src', '');
                        showHideCropWrapper(false);
                    } else {
                        var base64data = $contactImage.attr('src');
                        $image.attr('src', base64data);
                        adjustImage($image);
                        showHideCropWrapper(true);
                        if (base64data.indexOf('base64') > -1) {
                            _this.base64Img = $contactImage.attr('src');
                        }
                    }

                    $imageEditView.find('#file-input-image').ace_file_input({
                        no_file: 'No File ...',
                        btn_choose: 'Choose',
                        btn_change: 'Change',
                        droppable: true,
                        thumbnail: true,
                        before_change: function(files) {
                            var allowed_files = []
                                , reader = new FileReader()
                                , onloadend = function(e) {
                                    destroyJCrop();
                                    $image.attr('src', e.target.result);
                                    adjustImage($image);
                                    _this.base64Img = e.target.result;
                                    showHideCropWrapper(true);
                                    _this.imageInfo.size = file.size;
                                    _this.imageInfo.type = file.type;
                                    _this.imageInfo.name = file.name;
                                }
                                ;
                            for (var i = 0; i < files.length; i++) {
                                var file = files[i];
                                if (typeof file === "string") {
                                    //IE8 and browsers that don't support File Object
                                    if (!(/\.(jpe?g|png|gif|bmp)$/i).test(file)) return false;
                                }
                                else {
                                    var type = $.trim(file.type);
                                    if (( type.length > 0 && !(/^image\/(jpe?g|png|gif|bmp)$/i).test(type) )
                                        || ( type.length === 0 && !(/\.(jpe?g|png|gif|bmp)$/i).test(file.name) )//for android's default browser which gives an empty string for file.type
                                        ) continue;//not an image so don't keep this file
                                }

                                allowed_files.push(file);
                                reader.onloadend = onloadend;
                                reader.readAsDataURL(file);
                                canceled = true;
                                hideLoadingSpinner($image.parent());
                            }
                            if (allowed_files.length === 0) return false;

                            return allowed_files;
                        },
                        before_remove: function() {
                            showHideCropWrapper(false);
                            $image.attr('src', '');
                            destroyJCrop();
                            return true;
                        }
                    });

                    $imageEditView.on('click', '.data-action', function() {
                        var val = $(this).attr('data-val');

                        switch (val) {
                            case 'image-ok' :
                                if (_this.croppedBase64Img && $checkboxCrop.is(':checked')) {
                                    $contactImage.attr('src', _this.croppedBase64Img);
                                    _this.imageInfo.imageRawData = base64Binary.decodeArrayBuffer(_this.croppedBase64Img.split(',')[1]);
                                    _this.imageInfo.size = _this.imageInfo.imageRawData.byteLength;
                                    _this.contactImageHandler = window.$wrtc.enums.contactImageHandler.new;
                                } else if (_this.base64Img) {
                                    $contactImage.attr('src', _this.base64Img);
                                    _this.imageInfo.imageRawData = base64Binary.decodeArrayBuffer(_this.base64Img.split(',')[1]);
                                    _this.imageInfo.size = _this.imageInfo.imageRawData.byteLength;
                                    _this.contactImageHandler = window.$wrtc.enums.contactImageHandler.new;
                                }
                                $imageEditView.find('.remove-avatar').removeClass('hide');
                                _this.transitionSidebarOut();
                                break;
                            case 'image-cancel':
                                _this.transitionSidebarOut();
                                break;
                        }
                    });
                },

                getAnyEmailAddresses: function($view) {
                    // get controls needed
                    var $anyEmailInput = $view.find('input[name="AnyEmail"]');
                    var $pendingInput = $anyEmailInput.siblings('.bootstrap-tagsinput').find('input');

                    // extract emails
                    var anyEmailsRaw = $anyEmailInput[0].value + ',' + $pendingInput.val();
                    var anyEmails = (anyEmailsRaw ? _this.normalizeEmailAddresses(anyEmailsRaw) : []);

                    return anyEmails;
                },

                setActionUI: function($button, newUpdateStatus) {
                    var $wrapper = $button.closest('.js-action-wrapper')
                        , $changes = $wrapper.find('.js-action-change')
                        , deferred = $.Deferred();

                    switch (newUpdateStatus) {
                        case window.$wrtc.enums.updateStatus.readyToUpdate:
                            $wrapper.unblock({
                                onUnblock: function() {
                                    deferred.resolve();
                                }
                            });
                            break;

                        case window.$wrtc.enums.updateStatus.Updating:
                            if ($wrapper.data('blockUI.isBlocked')) {
                                deferred.resolve();
                            } else {
                                $wrapper.block({
                                    ignoreIfBlocked: true,
                                    css: {
                                        border: 'none',
                                        backgroundColor: 'transparent',
                                        cursor: 'auto'
                                    },
                                    overlayCSS: {
                                        backgroundColor: 'white',
                                        opacity: 0.5,
                                        cursor: 'auto'
                                    },
                                    onBlock: function() {
                                        $button.parent().append('<i class="btn-icon icon-refresh icon-spin"></i>');
                                        deferred.resolve();
                                    },
                                    message: ''
                                });
                            }
                            break;

                        case window.$wrtc.enums.updateStatus.Updated:
                            $wrapper.unblock({
                                onUnblock: function() {
                                    $button.parent().find('.icon-spin').remove();
                                    var $btnsDIV = $wrapper.find('.js-btns');
                                    $btnsDIV.remove();
                                    deferred.resolve();
                                    $.each($changes, function(ind, change) {
                                        var $change = $(change);
                                        switch ($change.prop("nodeName")) {
                                            case 'INPUT':
                                                switch ($change.attr("type")) {
                                                    case 'text':
                                                        $change.data('init', $change.val());
                                                        break;
                                                    case 'checkbox':
                                                        $change.data('init', $change.get(0).checked ? 'checked' : '');
                                                        break;
                                                }
                                                break;
                                            case 'SELECT':
                                                $change.data('init', $change.val());
                                                break;
                                        }
                                    });
                                }
                            });
                            break;

                        case window.$wrtc.enums.updateStatus.NotUpdated:
                            $wrapper.unblock({
                                onUnblock: function() {
                                    $button.parent().find('.icon-spin').remove();
                                    deferred.resolve();
                                }
                            });
                            break;
                    }

                    return deferred;
                },

                normalizeEmailAddresses: function(_strValue) {
                    return _strValue.split(',').filter(function(item) {
                        return item !== '';
                    });
                },

                areEmailArraysEqual: function(array1, array2) {
                    // check obvious failures
                    if (!array1 && array2) return false;
                    if (array1 && !array2) return false;
                    if (array1.length !== array2.length) return false;

                    // compare value by value now since these still could be the same
                    for (var i = 0; i < array1.length; i++) {
                        if ($.inArray(array1[i], array2) === -1) return false;
                    }

                    // if we get this far it means they are equal
                    return true;
                }
            };
        };
        return settingsController().init();
    });

define('ContactGroupModel', [], function() {

    var ContactGroupModel = function(ArcusContactGroup) {

        var _this = this;
        return $.extend(ArcusContactGroup, _this);
    };

    ContactGroupModel.prototype.printGroupMembers = function() {
        var _curContactGroup = this, count = _curContactGroup.groupMembers.length, toPrint = 'member';

        if (count !== 1) {
            toPrint = 'members';
        }

        return count + ' ' + toPrint;
    };

    return ContactGroupModel;
});

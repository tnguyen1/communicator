define('ContactModel', ['Arcus', 'moment'], function(Arcus, moment) {

    var ContactModel = function(ArcusContact) {

        var _this = this;

        _this = $.extend(ArcusContact, _this);

        _this.init();

        return _this;
    };

    ContactModel.prototype.init = function() {
        this.parseContactPresence();
    };

    ContactModel.prototype.parseContactPresence = function() {
        var _curContact = this
            , definePhonesPresence = function(_curContact) {
                // parse contact-phones to sort by different presences
                var phonesByPresence = {};
                $.each(_curContact.phoneNumbers, function(i, _curPhone) {
                    if (_curPhone.presence) {
                        if (!phonesByPresence[_curPhone.presence.primaryStatus]) {
                            phonesByPresence[_curPhone.presence.primaryStatus] = [];
                        }
                        phonesByPresence[_curPhone.presence.primaryStatus].push(_curPhone);
                    }
                });
                return phonesByPresence;
            };
        _curContact.phonesByPresence = definePhonesPresence(_curContact);
        if (_curContact.phonesByPresence.ONLINE && _curContact.phonesByPresence.ONLINE.length) {
            _curContact.presence = 'ONLINE';
        } else if (_curContact.phonesByPresence.BUSY && _curContact.phonesByPresence.BUSY.length) {
            _curContact.presence = 'BUSY';
        } else {
            _curContact.presence = 'OFFLINE';
        }
    };

    ContactModel.prototype.prepareContactNotesPriorities = function() {
        var _curContact = this;
        $.each(_curContact.notes, function(i, _curNote) {
            _curNote.formattedDate = moment(_curNote.date).format('MMM DD, YYYY hh:mm A');

            if (typeof _curNote.priority === 'string') {
                var priority = {};

                switch (_curNote.priority) {
                    case '1':
                        priority = {
                            value: '1',
                            text: 'low',
                            uiclass: 'icon-arrow-down blue'
                        };
                        break;
                    case '2':
                        priority = {
                            value: '2',
                            text: 'normal',
                            uiclass: 'icon-info green'
                        };
                        break;
                    case '3':
                        priority = {
                            value: '3',
                            text: 'high',
                            uiclass: 'icon-exclamation red'
                        };
                        break;
                }
                _curNote.priority = priority;
            }
        });

        // after the priorities are all built sort the list
        this.sortNotes();
    };

    ContactModel.prototype.sortNotes = function() {
        if (this.notes) {
            this.notes.sort(function(a, b) {
                // sort is priority desc, date asc
                return Number(b.priority.value) - Number(a.priority.value) || new Date(a.date) - new Date(b.date);
            });
        }
    };

    ContactModel.prototype.hasMethodsToConnect = function() {
        return this.phoneNumbers.length || this.emailAddress.length;
    };

    return ContactModel;
});
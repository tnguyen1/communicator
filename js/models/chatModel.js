define('ArcusChatModel', function () {
    
    var ArcusChatModel = function (data) {
        if(!data){ data = {}; }

        this.id = data['@id'];
        this.type = data['@type'];
        this.contactReference = data.ContactReference;
        this.createDate = data.CreateDate;
        this.description = data.Description;
        this.lastUpdateDate = data.LastUpdateDate;
        this.createDate = data.MessageDate;
        this.messageDirection = data.MessageDirection;
        this.responseStatus = data.ResponseStatus;
        this.subject = data.Subject;
        this.version = data.Version;
        this.responseStatusCode = data.responseStatusCode;

    };

    return ArcusChatModel;
});

define('ChatModel', ['ArcusChatModel'], function (ArcusChatModel) {

    var ChatModel = function(data) {

        ArcusChatModel.apply(this, arguments);
        
        this.senderName = data.senderName;
        this.sendTime = data.sendTime;
        this.avatarURI = data.avatarURI;
        this.message = data.message;

    };

    ChatModel.prototype = new ArcusChatModel();

    return ChatModel;
});
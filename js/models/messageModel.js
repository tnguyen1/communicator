define('MessageModel', ['moment', 'Arcus', 'eventEmitter'], function(moment, Arcus, eventEmitter) {

    var MessageModel = function(ArcusMessage) {

        var _this = this;

        if (ArcusMessage) {
            // bind on Arcus AFSync events
            $(ArcusMessage).on(Arcus.EventsEnum.MessageEventType.MessageDeleted, function(event){
                if (event.target instanceof Arcus.model.Message) {
                    window.console.log('message deleted remotely!');
                    // trigger to manager
                    eventEmitter.trigger(MessageModel.prototype.events.DELETED_REMOTELY, event.target);
                }
            });
            $(ArcusMessage).on(Arcus.EventsEnum.MessageEventType.MessageUpdated, function(event){
                if (event.target instanceof Arcus.model.Message) {
                    window.console.log('message updated remotely!');
                    // trigger to manager
                    eventEmitter.trigger(MessageModel.prototype.events.UPDATED_REMOTELY, event.target);
                }
            });
            $(ArcusMessage).on(Arcus.EventsEnum.MessageEventType.SMSFailedToSend, function(){
                if(ArcusMessage.statusCode === Arcus.model.Message.statusCodeEnum.INVALID_SEND_TO){
                    window.alert('Error! Sms was sent to a phone number without SMS service.');
                }else{
                    window.alert('Error! Sms failed to send! Don\'t know why...');
                }
            });
            _this = $.extend({}, _this, ArcusMessage);

            _this.init();
        }

        return _this;
    };

    MessageModel.prototype.events = {
        DELETED_LOCALLY : 'DELETED_LOCALLY',
        DELETED_REMOTELY : 'DELETED_REMOTELY',
        UPDATED_LOCALLY : 'UPDATED_LOCALLY',
        UPDATED_REMOTELY : 'UPDATED_REMOTELY'
    };

    MessageModel.prototype.init = function() {
        this.resolveSentTime();
    };

    MessageModel.prototype.resolveContacts = function(options) {

        var curMessage = this
            , myNumber = options.myNumber
            , myName = options.myName
            , contactManager = options.contactManager
            , isSentByMyself = options.isSentByMyself
            , isSentToMe = options.isSentToMe
            ;

        var contact, result, method;

        if (!curMessage.sentBy) {
            curMessage.sentBy = {};
        }
        if (!curMessage.sentTo) {
            curMessage.sentTo = [];
        }

        // sentBy
        if (isSentByMyself(curMessage)) {
            curMessage.sentBy.contact = { id : -1 , firstName: myName };
            curMessage.sentBy.sentByMethod = myNumber;
        } else if ($.isNull(curMessage.sentBy.contact)) {
            if (curMessage.sentBy.id === "-1" || !curMessage.sentBy.id) {
                result = contactManager.getContactByContactTypeValue(curMessage.sentBy.number);

                contact = result.contact;
                method = result.foundContactMethod;
            } else {
                contact = contactManager.getContactByMethodId(curMessage.sentBy.id);
                if (contact === null) {
                    result = contactManager.getContactByContactTypeValue(curMessage.sentBy.number);

                    contact = result.contact;
                    method = result.foundContactMethod;
                }
                if (contact === null) {
                    contact = contactManager.getById(curMessage.sentBy.id);
                }
            }

            if (contact) {
                curMessage.sentBy.contact = contact;
                if (method) {
                    curMessage.sentBy.method = method;
                    if (method.methodType === 'PHONE') {
                        curMessage.sentBy.sentByMethod = method.contactType + ' '  + method.value;
                    } else if (method.methodType === 'EMAIL') {
                        curMessage.sentBy.sentByMethod = method.contactLocation + ' '  + method.value;
                    }
                }
            }
        }
        // sentTo
        $.each(curMessage.sentTo, function(ind, _curSentTo) {
            contact = result = method = null;
            if (isSentToMe(_curSentTo)) {
                _curSentTo.contact = { id : -1 , firstName: myName };
                _curSentTo.sentToMethod = myNumber;
            } else if ($.isNull(_curSentTo.contact)) {
                if (_curSentTo.id === "-1") {
                    result = contactManager.getContactByContactTypeValue(_curSentTo.number);

                    contact = result.contact;
                    method = result.foundContactMethod;
                } else {
                    contact = contactManager.getContactByMethodId(_curSentTo.id);
                    if (contact === null) {
                        result = contactManager.getContactByContactTypeValue(_curSentTo.number);

                        contact = result.contact;
                        method = result.foundContactMethod;
                    }
                    if (contact === null) {
                        contact = contactManager.getById(_curSentTo.id);
                    }
                }

                if (contact) {
                    _curSentTo.contact = contact;
                    if (method) {
                        _curSentTo.method = method;
                        if (method.methodType === 'PHONE') {
                            _curSentTo.sentToMethod = method.contactType + ' ' + method.value;
                        } else if (method.methodType === 'EMAIL') {
                            _curSentTo.sentToMethod = method.contactLocation + ' ' + method.value;
                        }
                    }
                }
            }
        });

        if (!curMessage.sentTo.length && curMessage.messageDirection === Arcus.model.Message.MessageDirectionEnum.INCOMING && curMessage.artifactType !== Arcus.model.Message.ArtifactTypeEnum.DRAFT) {
            curMessage.sentTo.push({
                contact : { id : -1 , firstName: myName },
                sentToMethod : myNumber
            });
        }
    };

    MessageModel.prototype.resolveSentTime = function() {
        var _curMessage = this;
        if (_curMessage.createDate) {
            _curMessage.formattedMessageDate = moment(_curMessage.createDate).format('MMM DD, YYYY hh:mm A');
        }
    };

    return MessageModel;
});
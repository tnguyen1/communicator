define('activityManager', ['logger', 'baseManager', 'contactManager', 'Arcus', 'collectionUtils', 'moment'], function(logger, baseManager, contactManager, Arcus, collectionUtils, moment) {

    var _this;

    /**
     * activityManager manager, represents work with Arcus library, in-memory storage and mock storage
     */
    var activityManager = function() {
        baseManager.apply(this, arguments);
    };

    activityManager.prototype = new baseManager();

    /**
     * Initialize manager with mock storage settings and loading function
     */
    activityManager.prototype.init = function() {

        var _this = this,
            config = {
                proxy: {
                    url: "data-mocks/activities-mock.json",
                    reader: {
                        rootProperty: ".Activity"
                    }
                },

                loadFunc: _this.getActivities,
                mockDataParserFunc: _this.mockDataParser
            };

        baseManager.prototype.init.call(this, config);
    };

    /**
     * Parses objects from mock storage into Arcus.model.Activity objects
     * @param  {[Object]} data array or retrieved from mock storage objects
     * @return {[Activity]} array of Arcus parsed Activity objects
     */
    activityManager.prototype.mockDataParser = function(data) {

        var result = [],
            activity;

        collectionUtils.forEach(data, function(item) {
            activity = new Arcus.model.Activity(item);
            result.push(activity);
        });

        return result;
    };

    /**
     * Retrieve activities from the service with Arcus library
     * @param  {Object} options for retrieving activities
     * @return {Deferred}
     */
    activityManager.prototype.getActivities = function(options) {

        var deferred = $.Deferred();

        Arcus.Activities.retrieveActivitiesByDate(options.startDate, options.endDate,
            function onSuccess(data) {

                logger.log('Retrieved activities!');

                var sorter = collectionUtils.getSorter('activityStartTime');
                data = data.sort(sorter);

                // TODO: Transform UTC to local time
                if (data)
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];
                        item.allDay = new Date(item.activityStartTime).getSeconds() !== 0;
                    }

                deferred.resolve(data);
            },
            function onFailure() {
                logger.log('Error retrieving activities.');
                deferred.reject();
            }
        );

        return deferred;
    };

    /**
     * Remove activity/activities from the service and in-memory storage
     * @param  {Number} id of the activity
     * @param  {Boolean} all flag to delete all occurences of the activity
     * @param  {function} onSuccess success callback
     * @param  {function} onFailure failure callback
     * @return {Object}
     */
    activityManager.prototype.removeActivity = function(id, all, onSuccess, onFailure) {

        var activity = _this.getById(id);

        var onSuccessDelete = function() {
            logger.log('activity removed succesfully!');

            if (all) {
                _this.findAndRemove(function(item) {
                    return (item.parentActivity && item.parentActivity.id === activity.parentActivity.id);
                });
            } else {
                _this.remove([_this.getById(id)]);
            }

            onSuccess();
        },
            onFailureDelete = function() {
                logger.log('failure to remove activity!');
                onFailure();
            };

        var res;

        if (all) {
            res = activity.destroySeries(onSuccessDelete, onFailureDelete);
        } else {
            res = activity.destroy(onSuccessDelete, onFailureDelete);
        }
        return res;

    };

    /**
     * Parse data into Activity Arcus object and save activity
     * @param  {Object} obj form data to parse and save
     * @param  {Boolean} all flag to update all occurences of the activity
     * @param  {function} onSuccess success callback
     * @param  {function} onFailure failure callback
     */
    activityManager.prototype.saveActivity = function(obj, all, onSuccess, onFailure) {

        var activity;

        var recurringType = parseInt(obj.repeat, 10);

        if (obj.id) {

            var act = _this.getById(obj.id);
            if (recurringType > 1 && all) {
                activity = act.parentActivity;
            } else {
                activity = act;
            }

        } else {
            activity = new Arcus.model.Activity();
        }

        activity.recurringType = recurringType;

        var activityDate = moment(obj.datePicker).format(); //date for all day event
        var activityStartTime = moment(obj.datePickerFrom + ' ' + obj.timePickerFrom).format();
        var activityEndTime = moment(obj.datePickerTo + ' ' + obj.timePickerTo).format();
        var activityStopTime = moment(obj.datePickerStop).add('d', 1).format();

        if (obj.allDay == 'on') {

            activity.allDay = true;

            var st = new Date(activityDate);

            if (activity.allDay)
                st.setSeconds(st.getSeconds() + 1);
            else
                st.setSeconds(0);

            activityStartTime = moment(st).format();
            // end date doesn't make sense
            // set end date just 15 minutes later start time
            st.setMinutes(st.getMinutes() + 15);
            activityEndTime = moment(st).format();
        } else {
            activity.allDay = false;
        }

        activity.activityStartTime = activityStartTime;
        activity.activityEndTime = activityEndTime;

        activity.description = obj.activityName;
        activity.priority = parseInt(obj.priority, 10);

        activity.recurringStopDate = activityStopTime;

        // add contact references to activity
        activity.contacts = [];
        var attendies = obj.attendies;
        if (attendies && attendies.length > 0) {
            for (var i = 0; i < attendies.length; i++) {
                var contactReference = new Arcus.model.ContactReference();
                contactReference.role = Arcus.model.ContactReference.RoleEnum.TAGGED;
                contactReference.type = Arcus.model.ContactReference.TypeEnum.PERSON;
                contactReference.id = attendies[i].id;
                contactReference.name = attendies[i].name;
                contactReference.number = attendies[i].number;

                activity.contacts.push(contactReference);
            }
        }

        var onSuccessAdd = function(response) {
            var newActivity;
            if (response && response.Artifact) {
                newActivity = new Arcus.model.Activity(response.Artifact.Activity);
                newActivity.allDay = new Date(newActivity.activityStartTime).getSeconds() !== 0;
                _this.add([newActivity]);
                logger.log('activity added succesfully!');
            } else {
                newActivity = activity;
                _this.add([newActivity]);
                logger.log('activity updated succesfully!');
            }
            onSuccess(newActivity);
        },
            onFailureAdd = function() {
                logger.log('failure to add activity!');
                onFailure();
            };

        if (_this.config.useMockData) {

            logger.log('activity added succesfully!');
            _this.add([activity]);
            onSuccess(activity);

        } else {
            activity.save(onSuccessAdd, onFailureAdd);
        }

    };

    /**
     * Change date in Activity Arcus object and update it
     * @param  {String} id Activity identifier
     * @param  {String} dayDelta Difference in days. Should be String for the reason when moving Activity in one day
     * @param  {Number} minuteDelta Difference in minutes. When dayDelta is not set, change only the end time of Activity
     * @param  {Boolean} allDay is Activity for all day
     * @param  {Boolean} updateAll flag to update all occurences of the activity
     * @param  {function} onSuccess success callback
     * @param  {function} onFailure failure callback
     */
    activityManager.prototype.updateActivityDate = function(id, dayDelta, minuteDelta, allDay,  updateAll, onSuccess, onFailure) {
        var addMinutes = function(date, minutes) {
            return new Date(new Date(date).getTime() + (minutes * 60000));
        };
        var addDays = function(date, days) {
            var curDate = new Date(date);
            return new Date(curDate.setDate(curDate.getDate() + days));
        };

        var activity;

        if (id) {
            var act = _this.getById(id);
            if (act.recurringType > 1 && updateAll) {
                activity = act.parentActivity;
            } else {
                activity = act;
            }

        } else {
            throw new Error('First argument should be Activity Id');
        }

        if (dayDelta) {
            if (dayDelta !== '0') {
                dayDelta = Number(dayDelta);
                activity.activityStartTime = moment(addDays(activity.activityStartTime, dayDelta)).format();
                activity.activityEndTime = moment(addDays(activity.activityEndTime, dayDelta)).format();
            }
            if (minuteDelta) {
                activity.activityStartTime = moment(addMinutes(activity.activityStartTime, minuteDelta)).format();
                activity.activityEndTime = moment(addMinutes(activity.activityEndTime, minuteDelta)).format();
            }
        } else {
            if (minuteDelta){
                activity.activityEndTime = moment(addMinutes(activity.activityEndTime, minuteDelta)).format();
            }
        }

        if (typeof allDay !== "undefined") {
            activity.allDay = allDay;
        }

        var st = new Date(activity.activityStartTime);

        if (activity.allDay) {
            st.setSeconds(st.getSeconds() + 1);
        }
        else {
            st.setSeconds(0);
        }

        activity.activityStartTime = moment(st).format();

        var onSuccessAdd = function() {
            // _this.replace(id, activity);
            _this.add([activity]);
            logger.log('activity updated succesfully!');
            if(onSuccess) {
                onSuccess(activity);
            }
        },
            onFailureAdd = function() {
                logger.log('failure to add activity!');
                if(onFailure) {
                    onFailure();
                }
            };

        if (_this.config.useMockData) {

            logger.log('activity updated succesfully!');
            _this.add([activity]);
            onSuccess(activity);

        } else {
            activity.save(onSuccessAdd, onFailureAdd);
        }
    };
    /**
     * Retrieve min start date of the activity from the in-memory storage
     * @return {String}
     */
    activityManager.prototype.getMinStartDate = function() {

        if (_this.data && _this.data.length > 0) {
            var activityStartTime = _this.data[0].activityStartTime;
            return activityStartTime;
        } else {
            return null;
        }

    };

    /**
     * * Retrieve max start date of the activity from the in-memory storage
     * @return {String}
     */
    activityManager.prototype.getMaxStartDate = function() {

        if (_this.data && _this.data.length > 0) {
            var activityStartTime = _this.data[_this.data.length - 1].activityStartTime;
            return activityStartTime;
        } else {
            return null;
        }

    };

    _this = new activityManager();
    _this.init();

    return _this;
});
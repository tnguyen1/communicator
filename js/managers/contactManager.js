define('contactManager', ['logger', 'baseManager', 'Arcus', 'collectionUtils', 'ContactModel', 'eventEmitter', 'utils'], function(logger, baseManager, Arcus, collectionUtils, ContactModel, eventEmitter) {

    var _this;

    var contactManager = function() {
        baseManager.apply(this, arguments);
    };

    contactManager.prototype = new baseManager();
    contactManager.prototype.init = function() {

        var _this = this,
            config = {
                storeId: "contacts",
                proxy: {

                    url: "data-mocks/persons-mock.json",
                    reader: {
                        rootProperty: ".persons"
                    }
                },

                loadFunc: _this.getContacts,
                mockDataParserFunc: _this.mockDataParser,
                loadParser: _this.wrapWithCustomModel
            };

        baseManager.prototype.init.call(this, config);
        contactManager.prototype.copyArcusEnums();
    };

    contactManager.prototype.copyArcusEnums = function() {

        var getArrayFromObject = function(object) {
                var retArray = [], key;
                for (key in object) {
                    if (object.hasOwnProperty(key)) {
                        retArray.push(object[key]);
                    }
                }
                return retArray;
            }
            , enumsArray = []
            ;

        // store Arcus enums
        if (!contactManager.prototype.ContactUserDescription) {

            // store enum as string
            enumsArray = getArrayFromObject(Arcus.model.Phone.ContactUserDescription);

            if (enumsArray.length) {
                contactManager.prototype.ContactUserDescription = $.defEnum(enumsArray);
            }
        }

        if (!contactManager.prototype.TypeEnum) {

            // store enum as string
            enumsArray = getArrayFromObject(Arcus.model.InstantMessengerAddress.TypeEnum);
            if (enumsArray.length) {
                contactManager.prototype.TypeEnum = $.defEnum(enumsArray);
            }
        }
    };

    contactManager.prototype.wrapWithCustomModel = function(data) {

        var result = [];

        collectionUtils.forEach(data, function(_curArcusContact) {
            result.push(new ContactModel(_curArcusContact));
        });

        return result;
    };

    contactManager.prototype.mockDataParser = function(data) {

        var result = [],
            contact;

        collectionUtils.forEach(data, function(item) {
            contact = new Arcus.model.Contact();
            contact.parse(item);
            result.push(new ContactModel(contact));
        });

        return result;
    };

    contactManager.prototype.getContactsPresence = function() {
        var deferred = $.Deferred();

        Arcus.Contacts.loadContactsPresence(
            function onSuccess(contactsPresence) {
                deferred.resolve(contactsPresence);
            },
            function onFailure() {
                logger.log('Error loading contacts presence');
                deferred.reject();
            });

        return deferred;
    };

    contactManager.prototype.getContacts = function() {
        var deferred = $.Deferred();

        Arcus.Contacts.loadContacts(
            function onSuccess(contacts) {
                deferred.resolve(contacts);
            },
            function onFailure() {
                logger.log('Error loading contacts!');
                deferred.reject();
            });

        return deferred;
    };

    contactManager.prototype.getContactsSorted = function(sortParameter) {
        var sorter = sortParameter ? collectionUtils.getSorter(sortParameter) : null;
        var contacts = _this.getData();

        // do not show contact with isSelf === 'true' inside contacts list
        contacts = contacts.filter(function(contact){
            return !contact.isSelf;
        });

        if (sorter) {
            contacts = contacts.sort(sorter);
        }
        
        var isOnline = function(contact){
            return contact.presence === "ONLINE";
        };
        var onlineContacts = $.grep(contacts, isOnline);
        var offlineContacts = $.grep(contacts, isOnline, true);

        return onlineContacts.concat(offlineContacts);
    };

    contactManager.prototype.getContactMethodsAsData = function(_curContact, _curGroup, _markSelected, __groupManager) {
        var contact_tree_data = {};
        var fullName = _curContact.fullName()
            , hasPhonesOnly = _curContact.hasPhones() && !_curContact.hasEmails()
            , hasEmailsOnly = !_curContact.hasPhones() && _curContact.hasEmails()
            , hasMethods = _curContact.hasPhones() && _curContact.hasEmails()
            , phoneChildren = {}
            , emailChildren = {}
            , groupMethods = _curGroup ? __groupManager.getGroupContactMethods(_curGroup) : []
            ;
        contact_tree_data = {
            name: '<span class="js-contact" data-id="' + _curContact.id + '">' + fullName + '</span>', // this for get UI snapshot
            type: 'folder'
        };
        if (hasPhonesOnly) {
            contact_tree_data.additionalParameters = {
                'children': {
                    'phones': {name: 'phones', type: 'folder'}
                }
            };
        } else if (hasEmailsOnly) {
            contact_tree_data.additionalParameters = {
                'children': {
                    'emails': {name: 'emails', type: 'folder'}
                }
            };
        } else if (hasMethods) {
            contact_tree_data.additionalParameters = {
                'children': {
                    'phones': {name: 'phones', type: 'folder'},
                    'emails': {name: 'emails', type: 'folder'}
                }
            };
        }
        if (_curContact.hasPhones()) {
            collectionUtils.forEach(_curContact.phoneNumbers, function(curPhone) {
                phoneChildren[curPhone.value] = {
                    name: '<span class="js-contact-method" data-id="' + curPhone.id + '">' + curPhone.value + '</span>', // this for get UI snapshot
                    type: 'item'
                };
                if (!_curGroup) {
                    phoneChildren[curPhone.value].additionalParameters = {
                        'item-selected': _markSelected ? true : false
                    };
                } else {
                    // in this case we can mark the existing item if it is in the group methods
                    phoneChildren[curPhone.value].additionalParameters = {
                        'item-selected': collectionUtils.findOne(groupMethods, collectionUtils.idGetter(curPhone.id)) !== null
                    };
                }
                contact_tree_data.additionalParameters.children.phones.additionalParameters = {
                    'children': phoneChildren
                };
            });
        }
        if (_curContact.hasEmails()) {
            collectionUtils.forEach(_curContact.emailAddress, function(curEmail) {
                emailChildren[curEmail.value] = {
                    name: '<span class="js-contact-method" data-id="' + curEmail.id + '">' + curEmail.value + '</span>', // this for get UI snapshot
                    type: 'item'
                };
                if (!_curGroup) {
                    emailChildren[curEmail.value].additionalParameters = {
                        'item-selected': _markSelected ? true : false
                    };
                } else {
                    // in this case we can mark the existing item if it is in the group methods
                    emailChildren[curEmail.value].additionalParameters = {
                        'item-selected': collectionUtils.findOne(groupMethods, collectionUtils.idGetter(curEmail.id)) !== null
                    };
                }
                contact_tree_data.additionalParameters.children.emails.additionalParameters = {
                    'children': emailChildren
                };
            });
        }
        return contact_tree_data;
    };

    contactManager.prototype.getContactsAsData = function(_contactsCollection, _curGroup, _markSelected, __groupManager) {
        var tree_data = {};
        collectionUtils.forEach(_contactsCollection, function(_curContact) {
            if (_curContact.hasPhones() || _curContact.hasEmails()) {
                tree_data[_curContact.fullName()] = _this.getContactMethodsAsData(_curContact, _curGroup, _markSelected, __groupManager);
            }
        });

        return new $.DataSourceTree({data: tree_data});
    };

    contactManager.prototype.getContactMethodsUISnapshot = function() {
        var contactsMethodsInUI = []
            , $tree = $('#chooseMethodsThree')
            ;
        // show all three

        var treeData = $tree.data('tree');
        if (treeData) {
            $.each($tree.find('.tree-folder-header'), function(ind, element) {
                if (ind > 0) { // first element is main header
                    treeData.selectFolder(element);
                    // open nested elements
                    var $children = $(element).parent().find('.tree-folder-header');
                    $.each($children, function(ind, element) {
                        if (ind > 0) { // first element is main header
                            treeData.selectFolder(element);
                        }
                    });
                }
            });
        }

        $.each($($tree.find('.js-contact-method')), function(ind, element) {
            var $element = $(element)
                , methodId = $element.attr('data-id')
                , $wrapper = $element.closest('.tree-item')
                , $phonesOrEmailsWrapper = $wrapper.closest('.tree-folder')
                , $contactWrapper = $phonesOrEmailsWrapper.parent().closest('.tree-folder')
                , contactId = $contactWrapper.find('.js-contact:first').attr('data-id')
                , methodItem
                ;
            methodItem = {
                id: methodId,
                contactId: contactId,
                marked: $wrapper.hasClass('tree-selected')
            };
            contactsMethodsInUI.push(methodItem);
        });

        return contactsMethodsInUI;
    };

    contactManager.prototype.getContactMethod = function(_curContact, contactMethodId, lookInAddresses) {

        var contactMethod = null;

        $.each(_curContact.phoneNumbers, function(index, item) {

            if (item.id === contactMethodId.toString()) {
                contactMethod = item;
                return false;
            }
            return true;

        });

        if (contactMethod === null) {
            $.each(_curContact.emailAddress, function(index, item) {

                if (item.id === contactMethodId.toString()) {
                    contactMethod = item;
                    return false;
                }
                return true;

            });
        }

        if (lookInAddresses && contactMethod === null) {
            $.each(_curContact.addresses, function(index, item) {

                if (item.id === contactMethodId.toString()) {
                    contactMethod = item;
                    return false;
                }
                return true;

            });
        }

        return contactMethod;
    };

    contactManager.prototype.removeContactRequest = function(contactId) {
        var onSuccessDelete = function() {
                window.console.log('contact removed succesfully!');

                collectionUtils.findAndRemove(_this.data, collectionUtils.idGetter(contactId));
            },
            onFailureDelete = function() {
                window.console.log('failure to remove contact!');
            };
        return Arcus.Contacts.deleteContact(contactId, onSuccessDelete, onFailureDelete);
    };

    contactManager.prototype.parseSerializedItems = function(_curContact, SerializedItems) {
        var newContact = new Arcus.model.Contact();

        $.each(SerializedItems, function(ind, item) {

            switch (item.model) {
                case 'Contact':
                    $.extend(newContact, item.data);
                    break;
                case 'Phone':
                    var newPhone = new Arcus.model.Phone(_curContact);
                    $.extend(newPhone, item.data);
                    newContact.phoneNumbers.push(newPhone);
                    break;
                case 'Email':
                    var newEmail = new Arcus.model.Email(_curContact);
                    $.extend(newEmail, item.data);
                    newContact.emailAddress.push(newEmail);
                    break;
                case 'ContactAddress':
                    var newAddress = new Arcus.model.ContactAddress(_curContact);
                    $.extend(newAddress, item.data);
                    newContact.addresses.push(newAddress);
                    break;
                case 'InstantMessengerAddress':
                    var newInstantMessengerAddress = new Arcus.model.InstantMessengerAddress(_curContact);
                    $.extend(newInstantMessengerAddress, item.data);
                    newContact.imAddresses.push(newInstantMessengerAddress);
                    break;
                case 'ContactNote':
                    var newContactNote = new Arcus.model.ContactNote(_curContact);
                    $.extend(newContactNote, item.data);
                    newContact.notes.push(newContactNote);
                    break;
            }
        });

        return newContact;
    };

    contactManager.prototype.saveContact = function(_options) {
        var serializedContact = _this.parseSerializedItems(_options.contact, _options.serializedItems)
            , requests = []
            , compareValues = function(newObject, property, oldObject) {
                if (!property && !newObject) {
                    return false;
                }
                if (!oldObject) {
                    return true;
                }

                return newObject[property] !== oldObject[property];
            }
            , deferred = $.Deferred()
            , savedContact
            ;

        if (_options.isSelf) {
            serializedContact.isSelf = _options.isSelf;
        }

        if (!_options.contact || !_options.contact.id) { // we are adding new contact

            requests.push(
                function() {
                    var onSuccessAdd = function(_savedContact) {
                            savedContact = _savedContact;
                            window.console.log('contact added succesfully!');
                        },
                        onFailureAdd = function() {
                            var text = 'failure to add contact!';
                            eventEmitter.trigger(_this.events.ERROR, {
                                text : text
                            });
                        };
                    return Arcus.Contacts.addContact(serializedContact, onSuccessAdd, onFailureAdd);
                }
            );

            requests.push(
                function() {
                    var onSuccessRefresh = function(_refreshedContact) {
                            // saved contact comes without id for contact-methods
                            savedContact = _refreshedContact;
                            window.console.log('contact refreshed succesfully!');
                            if (!_options.imageInfo.imageRawData) {
                                _this.add([new ContactModel(_refreshedContact)]);
                            }
                        },
                        onFailureRefresh = function() {
                            var text = 'failure to refreshed contact!';
                            eventEmitter.trigger(_this.events.ERROR, {
                                text : text
                            });
                        };


                    return savedContact.refresh(onSuccessRefresh, onFailureRefresh);
                }
            );

            if (_options.imageInfo && _options.imageInfo.imageRawData) {
                requests.push(
                    function() {
                        var onSuccessAddImage = function() {
                                window.console.log('contact image added succesfully!');
                                _this.add([new ContactModel(savedContact)]);
                            },
                            onFailureAddImage = function() {
                                var text = 'failure to add contact image!';
                                eventEmitter.trigger(_this.events.ERROR, {
                                    text : text
                                });
                            };

                        return savedContact.updateContactPhoto(_options.imageInfo, onSuccessAddImage, onFailureAddImage);
                    }
                );
            }

            return requests.length ? $.pipeline(requests) : deferred.resolve();

        } else {
            // we are updating the contact
            serializedContact.id = _options.contact.id;
            serializedContact.isSelf = _options.isSelf;

            // check contact is changed
            var isContactChanged = compareValues(serializedContact, 'firstName', _options.contact);
            if (!isContactChanged) {
                isContactChanged = compareValues(serializedContact, 'lastName', _options.contact);
            }
            if (serializedContact.id && isContactChanged) {
                requests.push(
                    function() {
                        var def = $.Deferred();
                        serializedContact.updateEntry(function(){def.resolve();}, function(){def.reject();});
                        return def;
                    }
                );
            }

            // check each phone is deleted
            collectionUtils.forEach(_options.contact.phoneNumbers, function(_curPhone) {
                var found = collectionUtils.findOne(serializedContact.phoneNumbers, collectionUtils.idGetter(_curPhone.id));

                if (!found) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            _curPhone.deleteEntry(function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                }
            });

            // check each phone is changed or added
            collectionUtils.forEach(serializedContact.phoneNumbers, function(_curPhone) {
                var oldPhone = collectionUtils.findOne(_options.contact.phoneNumbers, collectionUtils.idGetter(_curPhone.id))
                    , isChanged = compareValues(_curPhone, 'contactType', oldPhone);
                if (!isChanged) {
                    isChanged = compareValues(_curPhone, 'value', oldPhone);
                }

                if (!_curPhone.id && isChanged) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            serializedContact.addPhoneNumber(_curPhone, function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                } else if (_curPhone.id && isChanged) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            _curPhone.updateEntry(function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                }
            });

            // check each email is deleted
            collectionUtils.forEach(_options.contact.emailAddress, function(_curEmail) {
                var found = collectionUtils.findOne(serializedContact.emailAddress, collectionUtils.idGetter(_curEmail.id));

                if (!found) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            _curEmail.deleteEntry(function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                }
            });

            // check each email is changed or added
            collectionUtils.forEach(serializedContact.emailAddress, function(_curEmail) {
                var oldEmail = collectionUtils.findOne(_options.contact.emailAddress, collectionUtils.idGetter(_curEmail.id))
                    , isChanged = compareValues(_curEmail, 'contactLocation', oldEmail);
                if (!isChanged) {
                    isChanged = compareValues(_curEmail, 'value', oldEmail);
                }

                if (!_curEmail.id && isChanged) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            serializedContact.addEmail(_curEmail, function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                } else if (_curEmail.id && isChanged) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            _curEmail.updateEntry(function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                }
            });

            // check each address is deleted
            collectionUtils.forEach(_options.contact.addresses, function(_curAddress) {
                var found = collectionUtils.findOne(serializedContact.addresses, collectionUtils.idGetter(_curAddress.id));

                if (!found) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            _curAddress.deleteEntry(function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                }
            });

            // check each address is changed or added
            collectionUtils.forEach(serializedContact.addresses, function(_curAddress) {
                var oldAddress = collectionUtils.findOne(_options.contact.addresses, collectionUtils.idGetter(_curAddress.id))
                    , isChanged = compareValues(_curAddress, 'locationType', oldAddress);
                // check all value if at least one value was changed => update entry
                if (!isChanged) {
                    isChanged = compareValues(_curAddress, 'state', oldAddress);
                }
                if (!isChanged) {
                    isChanged = compareValues(_curAddress, 'streetLine1', oldAddress);
                }
                if (!isChanged) {
                    isChanged = compareValues(_curAddress, 'city', oldAddress);
                }
                if (!isChanged) {
                    isChanged = compareValues(_curAddress, 'zip', oldAddress);
                }

                if (!_curAddress.id && isChanged) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            serializedContact.addAddress(_curAddress, function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                } else if (_curAddress.id && isChanged) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            _curAddress.updateEntry(function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                }
            });

            // check each imAddress is deleted
            collectionUtils.forEach(_options.contact.imAddresses, function(_curImAddress) {
                var found = collectionUtils.findOne(serializedContact.imAddresses, collectionUtils.idGetter(_curImAddress.id));

                if (!found) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            var onSuccessDelete = function() {
                                    window.console.log('Instant message address deleted successfully!');
                                    def.resolve();
                                },
                                onFailureDelete = function() {
                                    window.console.log('failure to delete Instant message address!');
                                    def.reject();
                                };
                            _curImAddress.destroy(onSuccessDelete, onFailureDelete);
                            return def;
                        }
                    );
                }
            });

            // check each imAddress is changed or added
            collectionUtils.forEach(serializedContact.imAddresses, function(_curImAddress) {
                var oldImAddress = collectionUtils.findOne(_options.contact.imAddresses, collectionUtils.idGetter(_curImAddress.id))
                    , isChanged = compareValues(_curImAddress, 'contactType', oldImAddress);
                if (!isChanged) {
                    isChanged = compareValues(_curImAddress, 'value', oldImAddress);
                }

                if (!_curImAddress.id && isChanged) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            var onSuccessAdd = function() {
                                    window.console.log('Instant message address added successfully!');
                                    def.resolve();
                                },
                                onFailureAdd = function() {
                                    window.console.log('failure to add Instant message address!');
                                    def.reject();
                                };
                            serializedContact.addInstantMessengerAddress(_curImAddress.value, _curImAddress.contactType, onSuccessAdd, onFailureAdd);
                            return def;
                        }
                    );
                } else if (_curImAddress.id && isChanged) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            _curImAddress.updateEntry(function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                }
            });

            // check each note is deleted
            collectionUtils.forEach(_options.contact.notes, function(_curNote) {
                var found = collectionUtils.findOne(serializedContact.notes, collectionUtils.idGetter(_curNote.id));

                if (!found) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            var onSuccessDelete = function() {
                                    window.console.log('Contact note deleted successfully!');
                                    def.resolve();
                                },
                                onFailureDelete = function() {
                                    window.console.log('failure to delete Contact note!');
                                    def.reject();
                                };
                            _curNote.destroy(onSuccessDelete, onFailureDelete);
                            return def;
                        }
                    );
                }
            });

            // check each note is changed or added
            collectionUtils.forEach(serializedContact.notes, function(_curNote) {
                var oldNote = collectionUtils.findOne(_options.contact.notes, collectionUtils.idGetter(_curNote.id))
                    , isChanged = compareValues(_curNote, 'subject', oldNote);
                if (!isChanged) {
                    isChanged = _curNote.priority !== oldNote.priority.value;
                }
//                if (!isChanged) {
//                    isChanged = _curNote.date !== oldNote.formattedDate;
//                }
                if (!isChanged) {
                    isChanged = compareValues(_curNote, 'description', oldNote);
                }
//                if (isChanged) {
//                    _curNote.date = moment(_curNote.date).format();
//                }

                if (!_curNote.id && isChanged) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            var onSuccessAdd = function() {
                                    window.console.log('Contact note added successfully!');
                                    def.resolve();
                                },
                                onFailureAdd = function() {
                                    window.console.log('failure to add Contact note!');
                                    def.reject();
                                };
                            serializedContact.addNote(_curNote, onSuccessAdd, onFailureAdd);
                            return def;
                        }
                    );
                } else if (_curNote.id && isChanged) {
                    requests.push(
                        function() {
                            var def = $.Deferred();
                            _curNote.save(function(){def.resolve();}, function(){def.reject();});
                            return def;
                        }
                    );
                }
            });

            if (_options.contactImageHandler === window.$wrtc.enums.contactImageHandler.none && _options.contact.images.length) {
                requests.push(
                    function() {
                        var def = $.Deferred();
                        var onSuccessDeleteImage = function() {
                                window.console.log('contact image deleted successfully!');
                                def.resolve();
                            },
                            onFailureDeleteImage = function() {
                                window.console.log('failure to delete contact image!');
                                def.reject();
                            };

                        _options.contact.images[0].destroy(onSuccessDeleteImage, onFailureDeleteImage);
                        return def;
                    }
                );
            } else if (_options.imageInfo && _options.imageInfo.imageRawData && _options.contactImageHandler !== window.$wrtc.enums.contactImageHandler.none) {
                requests.push(
                    function() {
                        var def = $.Deferred();
                        var onSuccessAddImage = function() {
                                window.console.log('contact image added succesfully!');
                                def.resolve();
                            },
                            onFailureAddImage = function() {
                                window.console.log('failure to add contact image!');
                                def.reject();
                            };

                        _options.contact.updateContactPhoto(_options.imageInfo, onSuccessAddImage, onFailureAddImage);
                        return def;
                    }
                );
            }

            if (requests.length) {
                // after all requests we need replace old contact with the updated one
                requests.push(
                    function() {
                        // store images
                        var images = _options.contact.images;

                        return _options.contact.refresh(function(updatedContact) {
                            // replace contact images with local images - contact comes back w/o images from refresh
                            updatedContact.images = images;
                            _this.replace(updatedContact.id, new ContactModel(updatedContact));
                        });
                    }
                );
            } else {
                eventEmitter.trigger(_this.events.ADD_RECORD);
            }

            return requests.length ? $.pipeline(requests) : deferred.resolve();
        }
    };

    /**
     * prepare selects options for render with mustache
     * @param _curContact
     * @returns {*}
     */
    contactManager.prototype.defineContactSelect = function(_curContact) {
        function cloneObjectsArray(originalArray) {
            return $.map(originalArray, function(obj) {
                return $.extend({}, obj);
            });
        }

        var contactTypes = [], imTypes = [], key;

        for (key in contactManager.prototype.ContactUserDescription) {
            if (contactManager.prototype.ContactUserDescription.hasOwnProperty(key)) {
                contactTypes.push({ value : contactManager.prototype.ContactUserDescription[key], selected: false});
            }
        }

        for (key in contactManager.prototype.TypeEnum) {
            if (contactManager.prototype.TypeEnum.hasOwnProperty(key)) {
                imTypes.push({ value : contactManager.prototype.TypeEnum[key], selected: false});
            }
        }

        // TODO: Use arcus enums instead
        var contactLocations = [
                { value: "HOME", selected: false },
                { value: "WORK", selected: false },
                { value: "OTHER", selected: false }
            ]
            , states = [
                { value: "AL", text: "Alabama" },
                { value: "AK", text: "Alaska" },
                { value: "AZ", text: "Arizona" },
                { value: "AR", text: "Arkansas" },
                { value: "CA", text: "California" },
                { value: "CO", text: "Colorado" },
                { value: "CT", text: "Connecticut" },
                { value: "DE", text: "Delaware" },
                { value: "DC", text: "District of Columbia" },
                { value: "FL", text: "Florida" },
                { value: "GA", text: "Georgia" },
                { value: "HI", text: "Hawaii" },
                { value: "ID", text: "Idaho" },
                { value: "IL", text: "Illinois" },
                { value: "IN", text: "Indiana" },
                { value: "IA", text: "Iowa" },
                { value: "KS", text: "Kansas" },
                { value: "KY", text: "Kentucky" },
                { value: "LA", text: "Louisiana" },
                { value: "ME", text: "Maine" },
                { value: "MD", text: "Maryland" },
                { value: "MA", text: "Massachusetts" },
                { value: "MI", text: "Michigan" },
                { value: "MN", text: "Minnesota" },
                { value: "MS", text: "Mississippi" },
                { value: "MO", text: "Missouri" },
                { value: "MT", text: "Montana" },
                { value: "NE", text: "Nebraska" },
                { value: "NV", text: "Nevada" },
                { value: "NH", text: "New Hampshire" },
                { value: "NJ", text: "New Jersey" },
                { value: "NM", text: "New Mexico" },
                { value: "NY", text: "New York" },
                { value: "NC", text: "North Carolina" },
                { value: "ND", text: "North Dakota" },
                { value: "OH", text: "Ohio" },
                { value: "OK", text: "Oklahoma" },
                { value: "OR", text: "Oregon" },
                { value: "PA", text: "Pennsylvania" },
                { value: "RI", text: "Rhode Island" },
                { value: "SC", text: "South Carolina" },
                { value: "SD", text: "South Dakota" },
                { value: "TN", text: "Tennessee" },
                { value: "TX", text: "Texas" },
                { value: "UT", text: "Utah" },
                { value: "VT", text: "Vermont" },
                { value: "VA", text: "Virginia" },
                { value: "WA", text: "Washington" },
                { value: "WV", text: "West Virginia" },
                { value: "WI", text: "Wisconsin" },
                { value: "WY", text: "Wyoming" }
            ]
            , priorities = [
                { value: "1", text: "LOW", selected: false },
                { value: "2", text: "NORMAL", selected: false },
                { value: "3", text: "HIGH", selected: false }
            ]
            , curContact = _curContact ? _curContact : new Arcus.model.Contact()
            ;
        // use for new contact-items
        curContact.contactTypes = contactTypes;
        curContact.contactLocations = contactLocations;
        curContact.locationTypes = contactLocations;
        curContact.states = states;
        curContact.imTypes = imTypes;
        curContact.priorities = priorities;

        // phones
        $.each(curContact.phoneNumbers, function(ind_p, _curPhone) {
            _curPhone.contactTypes = cloneObjectsArray(contactTypes);
            var contactType = _curPhone.contactType ? _curPhone.contactType.toUpperCase() : '';
            $.each(_curPhone.contactTypes, function(ind_ct, _curContactType) {
                if (contactType === _curContactType.value) {
                    _curContactType.selected = true;
                } else {
                    _curContactType.selected = false;
                }
            });
        });
        // emails
        $.each(curContact.emailAddress, function(ind_e, _curEmail) {
            _curEmail.contactLocations = cloneObjectsArray(contactLocations);
            var contactLocation = _curEmail.contactLocation ? _curEmail.contactLocation.toUpperCase() : '';
            $.each(_curEmail.contactLocations, function(ind_cl, _curContactLocation) {
                if (contactLocation === _curContactLocation.value) {
                    _curContactLocation.selected = true;
                } else {
                    _curContactLocation.selected = false;
                }
            });
        });
        // addresses
        $.each(curContact.addresses, function(ind_a, _curAddress) {
            var locationType = _curAddress.locationType ? _curAddress.locationType.toUpperCase() : ''
                , state = _curAddress.state ? _curAddress.state.toUpperCase() : ''
                ;

            _curAddress.locationTypes = cloneObjectsArray(contactLocations);
            _curAddress.states = cloneObjectsArray(states);
            $.each(_curAddress.locationTypes, function(ind_lt, _curLocationType) {
                if (locationType === _curLocationType.value) {
                    _curLocationType.selected = true;
                } else {
                    _curLocationType.selected = false;
                }
            });

            $.each(_curAddress.states, function(ind_cm, _curState) {
                if (state === _curState.value) {
                    _curState.selected = true;
                } else {
                    _curState.selected = false;
                }
            });
        });
        // im addresses
        $.each(curContact.imAddresses, function(ind_im, _curImAddress) {
            _curImAddress.imTypes = cloneObjectsArray(imTypes);
            var contactType = _curImAddress.contactType ? _curImAddress.contactType.toUpperCase() : '';
            $.each(_curImAddress.imTypes, function(ind_ct, _curContactType) {
                if (contactType === _curContactType.value) {
                    _curContactType.selected = true;
                } else {
                    _curContactType.selected = false;
                }
            });
        });
        // notes
        $.each(curContact.notes, function(ind_n, _curNote) {
            _curNote.priorities = cloneObjectsArray(priorities);
            $.each(_curNote.priorities, function(ind_cp, _curPriority) {
                if (_curNote.priority.value === _curPriority.value) {
                    _curPriority.selected = true;
                } else {
                    _curPriority.selected = false;
                }
            });
        });

        return curContact;
    };

    contactManager.prototype.defineContactAddressSelect = function(_cur911Address) {
        var states = [
            { value: "AL", text: "Alabama" },
            { value: "AK", text: "Alaska" },
            { value: "AZ", text: "Arizona" },
            { value: "AR", text: "Arkansas" },
            { value: "CA", text: "California" },
            { value: "CO", text: "Colorado" },
            { value: "CT", text: "Connecticut" },
            { value: "DE", text: "Delaware" },
            { value: "DC", text: "District of Columbia" },
            { value: "FL", text: "Florida" },
            { value: "GA", text: "Georgia" },
            { value: "HI", text: "Hawaii" },
            { value: "ID", text: "Idaho" },
            { value: "IL", text: "Illinois" },
            { value: "IN", text: "Indiana" },
            { value: "IA", text: "Iowa" },
            { value: "KS", text: "Kansas" },
            { value: "KY", text: "Kentucky" },
            { value: "LA", text: "Louisiana" },
            { value: "ME", text: "Maine" },
            { value: "MD", text: "Maryland" },
            { value: "MA", text: "Massachusetts" },
            { value: "MI", text: "Michigan" },
            { value: "MN", text: "Minnesota" },
            { value: "MS", text: "Mississippi" },
            { value: "MO", text: "Missouri" },
            { value: "MT", text: "Montana" },
            { value: "NE", text: "Nebraska" },
            { value: "NV", text: "Nevada" },
            { value: "NH", text: "New Hampshire" },
            { value: "NJ", text: "New Jersey" },
            { value: "NM", text: "New Mexico" },
            { value: "NY", text: "New York" },
            { value: "NC", text: "North Carolina" },
            { value: "ND", text: "North Dakota" },
            { value: "OH", text: "Ohio" },
            { value: "OK", text: "Oklahoma" },
            { value: "OR", text: "Oregon" },
            { value: "PA", text: "Pennsylvania" },
            { value: "RI", text: "Rhode Island" },
            { value: "SC", text: "South Carolina" },
            { value: "SD", text: "South Dakota" },
            { value: "TN", text: "Tennessee" },
            { value: "TX", text: "Texas" },
            { value: "UT", text: "Utah" },
            { value: "VT", text: "Vermont" },
            { value: "VA", text: "Virginia" },
            { value: "WA", text: "Washington" },
            { value: "WV", text: "West Virginia" },
            { value: "WI", text: "Wisconsin" },
            { value: "WY", text: "Wyoming" }
        ];
        // use for new contact-items
        if (_cur911Address) {
            _cur911Address.states = states;

            $.each(_cur911Address.states, function(ind_cm, _curState) {
                if (_cur911Address.ContactAddress.State === _curState.value) {
                    _curState.selected = true;
                } else {
                    _curState.selected = false;
                }
            });
        } else {
            _cur911Address = {
                states : states
            };
        }

        return _cur911Address;
    };

    contactManager.prototype.getContactByPhoneNumber = function(phoneNumber) {
        var foundContact = null;

        if (_this.data) {
            collectionUtils.forEach(_this.data, function(_curContact) {

                var contactMethod = collectionUtils.findByAttrValue(_curContact.phoneNumbers, 'value', phoneNumber);
                if (contactMethod) {
                    foundContact = _curContact;
                    return false; // abort search
                }

                return true; // continue search
            });
        }

        return foundContact;
    };

    contactManager.prototype.getContactByEmailValue = function(emailValue) {
        var foundContact = null;

        collectionUtils.forEach(_this.data, function(_curContact) {

            foundContact = collectionUtils.findByAttrValue(_curContact.emailAddress, 'value', emailValue);
            if (foundContact) {
                return false; // abort search
            }

            return true; // continue search
        });

        return foundContact;
    };

    contactManager.prototype.getContactByContactTypeValue = function(contactTypeValue) {

        var foundContact = null,
            foundContactMethod = null;

        if (_this.data) {
            collectionUtils.forEach(_this.data, function (_curContact) {

                foundContactMethod = collectionUtils.findByAttrValue(_curContact.emailAddress, 'value', contactTypeValue);
                if (foundContactMethod) {
                    foundContact = _curContact;
                    return false; // abort search
                }

                foundContactMethod = collectionUtils.findByAttrValue(_curContact.phoneNumbers, 'value', contactTypeValue);
                if (foundContactMethod) {
                    foundContact = _curContact;
                    return false; // abort search
                }

                return true; // continue search
            });
        }

        return { contact: foundContact, foundContactMethod: foundContactMethod ? foundContactMethod[0] : null };
    };

    contactManager.prototype.getContactByMethodId = function(contactMethodId) {
        var foundContact = null,
            foundContactMethod = null;

        if (_this.data) {
            collectionUtils.forEach(_this.data, function (_curContact) {

                foundContactMethod = collectionUtils.findOne(_curContact.phoneNumbers, collectionUtils.idGetter(contactMethodId));
                if (foundContactMethod) {
                    foundContact = _curContact;
                    return false; // abort search
                }

                foundContactMethod = collectionUtils.findOne(_curContact.emailAddress, collectionUtils.idGetter(contactMethodId));
                if (foundContactMethod) {
                    foundContact = _curContact;
                    return false; // abort search
                }

                foundContactMethod = collectionUtils.findOne(_curContact.imAddresses, collectionUtils.idGetter(contactMethodId));
                if (foundContactMethod) {
                    foundContact = _curContact;
                    return false; // abort search
                }

                return true; // continue search
            });
        }

        return foundContact;
    };

    contactManager.prototype.wrapAutocompleteContactMethod = function(_contact, __method) {
        var autocompleteMethod = {
                id: 0,
                contactId: 0,
                groupId: 0,
                name: '',
                methodType: '',
                location: '',
                methodValue: '',
                image: null,
                imageUrl: null
            }
            , spaces = '      '
            , formatInfo = function (name, value) {
                var formatted = (name ? name : '');
                if (name !== '') {
                    formatted += spaces;
                }
                if (value && value.length) {
                    if (name !== '') {
                        formatted += '\r\n' + value + spaces;
                    } else {
                        formatted += value;
                    }
                }

                return formatted;
            }
            , _method = __method.methodType ? __method : (__method.method ? __method.method : __method);

        if (_method.methodType) {
            switch (_method.methodType) {
                case 'PHONE':
                    autocompleteMethod.id = _method.id;
                    autocompleteMethod.contactId = _contact.id;
                    autocompleteMethod.fullInfo = formatInfo(_contact.fullName(), _method.value);
                    autocompleteMethod.name = _contact.fullName();
                    autocompleteMethod.methodType = _method.methodType;
                    autocompleteMethod.location = _method.contactType;
                    autocompleteMethod.methodValue = _method.value;
                    autocompleteMethod.image = _contact.images.length > 0 ? _contact.images[0].imageData : null;
                    autocompleteMethod.imageUrl = _contact.images.length > 0 ? _contact.images[0].imageUrl : null;
                    break;
                case 'EMAIL':
                    autocompleteMethod.id = _method.id;
                    autocompleteMethod.contactId = _contact.id;
                    autocompleteMethod.fullInfo = formatInfo(_contact.fullName(), _method.value);
                    autocompleteMethod.name = _contact.fullName();
                    autocompleteMethod.methodType = _method.methodType;
                    autocompleteMethod.location = _method.contactLocation;
                    autocompleteMethod.methodValue = _method.value;
                    autocompleteMethod.image = _contact.images.length > 0 ? _contact.images[0].imageData : null;
                    autocompleteMethod.imageUrl = _contact.images.length > 0 ? _contact.images[0].imageUrl : null;
                    break;
            }
        } else if (_method.sentToMethod) {
            _method.name = _method.sentToMethod;
            _method.number = _method.sentToMethod;
            _method.id = _method.contact && _method.contact.id || "-1";
        }

        if ((!_contact || _contact.id === -1 ) && !_method.methodType) {
            // contact reference
            autocompleteMethod.id = (!_method.id || _method.id === -1) ? Math.floor(Math.random() * (999999 - 1 + 1)) + 1 : _method.id; // random id for bootstrap-tagsinput-typeahead
            autocompleteMethod.name = _method.name;
            autocompleteMethod.methodValue = _method.number;
            autocompleteMethod.contactId = _method.id;
            if (_method.name !== _method.number) {
                autocompleteMethod.fullInfo = formatInfo(_method.name, _method.number);
            } else {
                autocompleteMethod.fullInfo = formatInfo(_method.name);
            }
        }
        return autocompleteMethod;
    };

    contactManager.prototype.getAutocompleteContactsMethods = function(options) {

        var groupManager = options.groupManager,
            contacts = _this.getData(),
            groups = groupManager.getData(),
            contactsAndGroups = contacts.concat(groups),
            contactMethods = [],
            sortContactsAndGroups = function(a, b) {

                var aName, bName;

                if($.isNull(a.firstName) && $.isNull(a.lastName)) { // it's a group
                    aName = a.name.toLowerCase();
                }
                else {
                    aName = a.fullName().toLowerCase();
                }

                if($.isNull(b.firstName) && $.isNull(b.lastName)) { // it's a group
                    bName = b.name.toLowerCase();
                }
                else {
                    bName = b.fullName().toLowerCase();
                }

                if(aName < bName) {
                    return -1;
                }
                else if(aName === bName) {
                    return 0;
                }
                else {
                    return 1;
                }
            }
        ;

        contactsAndGroups.sort(sortContactsAndGroups);

        //getting all email and phone contact methods
        $.each(contactsAndGroups, function(index, contactOrGroup) {
            if(!($.isNull(contactOrGroup.firstName) && $.isNull(contactOrGroup.lastName))) {
                if(contactOrGroup.emailAddress !== null) {
                    $.each(contactOrGroup.emailAddress, function(index, emailAddress) {
                        contactMethods.push( _this.wrapAutocompleteContactMethod(contactOrGroup, emailAddress) );
                    });
                }

                if(contactOrGroup.phoneNumbers !== null) {
                    $.each(contactOrGroup.phoneNumbers, function(index, phoneNumber) {
                        contactMethods.push( _this.wrapAutocompleteContactMethod(contactOrGroup, phoneNumber) );
                    });
                }
            }
        });

        return contactMethods;
    };

    contactManager.prototype.getProfileContact = function() {
        var foundContact = null;

        collectionUtils.forEach(_this.data, function(_curContact) {

            if (_curContact.isSelf === true) {
                foundContact = _curContact;
                return false; // abort search
            }

            return true; // continue search
        });

        if (foundContact === null) {
            foundContact = new ContactModel(new Arcus.model.Contact());
        }

        return foundContact;
    };

    _this = new contactManager();
    _this.init();

    return _this;
});
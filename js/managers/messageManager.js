define('messageManager', ['logger', 'baseManager', 'Arcus', 'collectionUtils', 'contactManager', 'MessageModel', 'eventEmitter', 'audiotones'], function(logger, baseManager, Arcus, collectionUtils, contactManager, MessageModel, eventEmitter, audiotones) {

    var _this;

    var messageManager = function() {
        baseManager.apply(this, arguments);
    };

    messageManager.prototype = new baseManager();
    messageManager.prototype.init = function() {

        var _this = this,
            config = {
                storeId: "messages",
                proxy: {

                    url: "data-mocks/messages-mock.json",
                    reader: {
                        rootProperty: ".Message"
                    }
                },

                loadFunc: _this.loadMessages,
                mockDataParserFunc: _this.mockDataParser,
                loadParser: _this.filterTestMessages
            }
            , compareValues = function(newObject, property, oldObject) {
                if (!property && !newObject) {
                    return false;
                }
                if (!oldObject) {
                    return true;
                }

                return newObject[property] !== oldObject[property];
            };

        baseManager.prototype.init.call(this, config);
        // bind on model events
        eventEmitter.on(MessageModel.prototype.events.DELETED_REMOTELY, function(event, message) {
            var deletedMessage = _this.deleteMessage(message);
            if (deletedMessage) {
                // trigger to controller
                eventEmitter.trigger(_this.events.RECORD_DELETED_REMOTELY, deletedMessage);
            }
        });
        eventEmitter.on(MessageModel.prototype.events.UPDATED_REMOTELY, function(event, message) {
            var updatedMessage = new MessageModel(message);
            var oldMessage = _this.getById(message.id)
                , isChanged = false;
            if (!oldMessage) {
                _this.add([updatedMessage]);
                eventEmitter.trigger(_this.events.RECORD_ADDED_REMOTELY, updatedMessage);
            } else {
                // compare
                // check some values if at least one value was changed => update the entire message list
                isChanged = compareValues(updatedMessage, 'artifactType', oldMessage);
                if (!isChanged) {
                    isChanged = compareValues(updatedMessage, 'isRead', oldMessage);
                }
                if (!isChanged) {
                    isChanged = compareValues(updatedMessage, 'text', oldMessage);
                }
                if (!isChanged) {
                    isChanged = compareValues(updatedMessage, 'subject', oldMessage);
                }
                if (!isChanged) {
                    isChanged = compareValues(updatedMessage, 'isMarkForDelete', oldMessage);
                }
                if (isChanged && updatedMessage) {
                    updatedMessage.resolveContacts({
                        contactManager : contactManager,
                        isSentByMyself : _this.isSentByMyself,
                        isSentToMe : _this.isSentToMe,
                        myNumber : localStorage.getItem("VoipTn"),
                        myName : localStorage.getItem("USERNAME")
                    });
                    _this.replace(updatedMessage.id, updatedMessage);
                    // trigger to controller
                    eventEmitter.trigger(_this.events.RECORD_UPDATED_REMOTELY, updatedMessage);
                }
            }
        });
        messageManager.prototype.copyArcusEnums();
    };

    messageManager.prototype.copyArcusEnums = function() {

        var getArrayFromObject = function(object) {
                var retArray = [], key;
                for (key in object) {
                    if (object.hasOwnProperty(key)) {
                        retArray.push(object[key]);
                    }
                }
                return retArray;
            }
            , enumsArray = []
            ;

        // store Arcus enums
        if (!messageManager.prototype.ArtifactTypeEnum) {

            // store enum as string
            enumsArray = getArrayFromObject(Arcus.model.Message.ArtifactTypeEnum);

            if (enumsArray.length) {
                messageManager.prototype.ArtifactTypeEnum = $.defEnum(enumsArray);
            }
        }
    };

    messageManager.prototype.filterTestMessages = function(data) {

        var result = [];

        collectionUtils.forEach(data, function(_curMessage) {
            var newMessage = new MessageModel(_curMessage);
            newMessage.resolveContacts({
                contactManager : contactManager,
                isSentByMyself : _this.isSentByMyself,
                isSentToMe : _this.isSentToMe,
                myNumber : localStorage.getItem("VoipTn"),
                myName : localStorage.getItem("USERNAME")
            });
            result.push(newMessage);
        });

        return result;
    };

    messageManager.prototype.mockDataParser = function(data) {

        var result
            , dataToParse = { Artifacts: { Message: data } }
            ;

        result = new Arcus.model.Messages(dataToParse);

        return result;
    };

    messageManager.prototype.deleteMessage = function(messageToDelete) {
        return collectionUtils.findAndRemove(_this.data, collectionUtils.idGetter(messageToDelete.id));
    };

    messageManager.prototype.loadMessages = function() {
        var deferred = $.Deferred();

        Arcus.Messages.retrieveAllMessages(
            function onSuccessRetrieveUndeleted(Messages) {
                deferred.resolve(Messages.messageCollection);
                $(Arcus.Messages.messageCollectionInstance).on(Arcus.EventsEnum.MessageEventType.MessageAdded, function(event, message){
                    if (message instanceof Arcus.model.Message) {
                        window.console.log('new message received remotely!');
                        switch(message.artifactType){
                            case "EMail":
                                ga('send', 'event', 'Email', 'received');
                                break;
                            case "SMS":
                                ga('send', 'event', 'SMS', 'received');
                                break;
                        }
                        var found = _this.getById(message.id);
                        if (!found) {
                            _this.add([new MessageModel(message)]);
                            eventEmitter.trigger(_this.events.RECORD_ADDED_REMOTELY);
                        }
                        audiotones.play(audiotones.MSG_IN);
                    }
                });
            },
            function onFailureRetrieveUndeleted() {
                logger.log('Error loading messages!');
                deferred.reject();
            });

        return deferred;
    };

    messageManager.prototype.countUnreadMessages = function(_collection) {
        var count = 0;
        collectionUtils.forEach(_collection, function(_curMessage) {
            if (_curMessage.type && _curMessage.type === 'Conversation') {
                collectionUtils.forEach(_curMessage.messages, function(_curSubMessage) {
                    if (!_curSubMessage.isRead) {
                        count++;
                    }
                });
            } else {
                if (!_curMessage.isRead) {
                    count++;
                }
            }
        });
        return count;
    };

    messageManager.prototype.countMessages = function(_collection) {
        var count = 0;
        collectionUtils.forEach(_collection, function(_curMessage) {
            if (_curMessage.type && _curMessage.type === 'Conversation') {
                collectionUtils.forEach(_curMessage.messages, function() {
                    count++;
                });
            } else {
                count++;
            }
        });
        return count;
    };

    messageManager.prototype.getMessages = function(_collection, sortParameter, filterParameter) {
        var sorter = sortParameter ? collectionUtils.getSorter(sortParameter) : null
            , resultCollection = _collection ? _collection : _this.data;

        if (filterParameter) {
            resultCollection = $.grep(_collection, filterParameter);
        }

        if (sorter) {
            resultCollection = resultCollection.sort(sorter);
        }

        return resultCollection;
    };

    messageManager.prototype.getMessage = function(messageId, onSuccess, onFailure) {
        var newArcusMessage = new Arcus.model.Message()
            , onSuccessRetrieve = function() {
                window.console.log('id::' + messageId + '::' + 'message retrieved succesfully!');
                var newMessage = new MessageModel(newArcusMessage);
                if (newMessage) {
                    newMessage.resolveContacts({
                        contactManager : contactManager,
                        isSentByMyself : _this.isSentByMyself,
                        isSentToMe : _this.isSentToMe,
                        myNumber : localStorage.getItem("VoipTn"),
                        myName : localStorage.getItem("USERNAME")
                    });
                }
                onSuccess(newMessage);
            },
            onFailureRetrieve = function() {
                window.console.log('failure to retrieve message!');
                onFailure();
            };

        newArcusMessage.id = messageId;

        return newArcusMessage.fetch(onSuccessRetrieve, onFailureRetrieve);
    };

    messageManager.prototype.getMessagesByArtifactType = function(_collection, messageTypes) {
        var resultCollection = [];

        collectionUtils.forEach(_collection, function(_curMessage) {
            var approved;
            collectionUtils.forEach(messageTypes, function(_curType) {
                if (_curType === _curMessage.artifactType) {
                    approved = _curMessage;
                    return false;
                }
            });

            if (approved) {
                resultCollection.push(approved);
            }
        });

        return resultCollection;
    };

    messageManager.prototype.getMessagesByLocation = function(_collection, messageLocation) {
        var resultCollection = [];

        collectionUtils.forEach(_collection, function(_curMessage) {
            var approved;
            switch (messageLocation) {
                case window.$wrtc.enums.messageLocation.inbox:
                    if (_curMessage.artifactType !== Arcus.model.Message.ArtifactTypeEnum.DRAFT && !_curMessage.isMarkForDelete && _curMessage.messageDirection === Arcus.model.Message.MessageDirectionEnum.INCOMING) {
                        approved = true;
                    }
                    break;
                case window.$wrtc.enums.messageLocation.sent:
                    if (_curMessage.artifactType !== Arcus.model.Message.ArtifactTypeEnum.DRAFT && !_curMessage.isMarkForDelete && _curMessage.messageDirection === Arcus.model.Message.MessageDirectionEnum.OUTGOING) {
                        approved = true;
                    }
                    break;
                case window.$wrtc.enums.messageLocation.trash:
                    if (_curMessage.isMarkForDelete) {
                        approved = true;
                    }
                    break;
                case window.$wrtc.enums.messageLocation.draft:
                    if (_curMessage.artifactType === Arcus.model.Message.ArtifactTypeEnum.DRAFT && !_curMessage.isMarkForDelete) {
                        approved = true;
                    }
                    break;
            }

            if (approved) {
                resultCollection.push(_curMessage);
            }
        });

        return resultCollection;
    };

    messageManager.prototype.isSentToMe = function(_curSentTo) {
        var myNumber = window.afClient.getUserBtn()
            , result = false;
        //check if in sentTo has BTN of current user
        if ($.isNotNull(_curSentTo)) {
            if (_curSentTo.number === myNumber) {
                result = true;
            }
        }
        return result;
    };

    messageManager.prototype.isSentByMyself = function(_curMessage) {
        var myNumber = window.afClient.getUserBtn()
            , result = false;
        //check if in sentBy has BTN of current user
        if ($.isNotNull(_curMessage.sentBy)) {
            if (_curMessage.sentBy.number === myNumber) {
                result = true;
            }
        }
        return result;
    };

    messageManager.prototype.groupMessagesSentBy = function(messagesToGroup, sortParameter) {

        var groupedMessages = [],
            sorter = sortParameter ? collectionUtils.getSorter(sortParameter) : null,
        //first group by message types: sms, email, voicemail
            grouped_ArtifactType = collectionUtils.groupBy(messagesToGroup, function(message) {
                return message.artifactType;
            }),
            grouped_SentBy,
            result;

        //then for each group: group by sentBy contact
        function groupBySentBy(message) {
            if (message.sentBy) {
                return message.sentBy.contact ? message.sentBy.contact.id : message.sentBy.number;
            }
            else {
                return "";
            }
        }

        for (var type in grouped_ArtifactType) {
            grouped_SentBy = collectionUtils.groupBy(grouped_ArtifactType[type], groupBySentBy);
            grouped_ArtifactType[type] = grouped_SentBy;
        }

        //build result array
        for (type in grouped_ArtifactType) {
            grouped_SentBy = grouped_ArtifactType[type];
            for (var sentBy in grouped_SentBy) {
                if (grouped_SentBy[sentBy].length > 1) {
                    var conversation = { type : 'Conversation', messages : [], sendBy : sentBy };

                    conversation.messages = grouped_SentBy[sentBy];
                    conversation.messages = sorter ? conversation.messages.sort(sorter) : conversation.messages;

                    result = conversation;
                } else {
                    result = grouped_SentBy[sentBy][0];
                }

                groupedMessages.push(result);
            }
        }

        groupedMessages = sorter ? groupedMessages.sort(sorter) : groupedMessages;

        return groupedMessages;
    };

    messageManager.prototype.groupMessagesSentTo = function(messagesToGroup) {

        var groupedMessages = [],
        //first group by message types: sms, email, voicemail
            grouped_ArtifactType = collectionUtils.groupBy(messagesToGroup, function(message) {
                return message.artifactType;
            }),
            grouped_SentTo,
            result;

        //then for each group: group by sentBy contact
        function groupBySentBy(message) {
            if (message.sentTo) {
                return message.sentTo.contact ? message.sentTo.contact.id : message.sentTo.number;
            }
            else {
                return "";
            }
        }

        for (var type in grouped_ArtifactType) {
            grouped_SentTo = collectionUtils.groupBy(grouped_ArtifactType[type], groupBySentBy);
            grouped_ArtifactType[type] = grouped_SentTo;
        }

        //build result array
        for (type in grouped_ArtifactType) {
            grouped_SentTo = grouped_ArtifactType[type];
            for (var sentTo in grouped_SentTo) {
                if (grouped_SentTo[sentTo].length > 1) {
                    var conversation = { type : 'Conversation', messages : [], sendTo : sentTo };

                    conversation.messages = grouped_SentTo[sentTo];

                    result = conversation;
                } else {
                    result = grouped_SentTo[sentTo][0];
                }

                groupedMessages.push(result);
            }
        }

        return groupedMessages;
    };

    messageManager.prototype.deleteMessageRequest = function(messageToRemove, onSuccess, onFailure) {
        var onSuccessDelete = function() {
                window.console.log('message permanently deleted succesfully!');
                var deletedMessage = _this.deleteMessage(messageToRemove);
                if (deletedMessage) {
                    onSuccess();
                } else {
                    onFailure();
                }
            },
            onFailureDelete = function() {
                window.console.log('failure to permanently delete message!');
                onFailure();
            },
            onSuccessUpdate = function() {
                window.console.log('message deleted succesfully!');
                onSuccess();
            },
            onFailureUpdate = function() {
                window.console.log('failure to delete message!');
                onFailure();
            }
            ;
        if (messageToRemove.isMarkForDelete) {
            return messageToRemove.destroy(onSuccessDelete, onFailureDelete);
        } else {
            return messageToRemove.destroy(onSuccessUpdate, onFailureUpdate);
        }
    };

    messageManager.prototype.sendMessageRequest = function(data) {
        var newMessage = new Arcus.model.Message()
            , onSuccessSendSMS = function(sentId) {
                var onSuccessGet = function(newMessage) {
                    var found = _this.getById(newMessage.id);
                    if (!found) {
                        _this.add([newMessage]);
                        eventEmitter.trigger(_this.events.RECORD_ADDED_LOCALLY);
                    } else {
                        _this.replace(found.id, newMessage);
                        eventEmitter.trigger(_this.events.RECORD_ADDED_LOCALLY);
                    }
                }, onFailureGet = function() {
                    var text = 'failure to retrieve SMS!';
                    eventEmitter.trigger(_this.events.ERROR, {
                        text : text,
                        artifactType : messageManager.prototype.ArtifactTypeEnum.SMS
                    });
                };
                _this.getMessage(sentId, onSuccessGet, onFailureGet);

                window.console.log('SMS sent successfully!');
            }
            , onFailureSendSMS = function(statusCode) {
                var text;
                switch(statusCode) {
                    case Arcus.model.Message.statusCodeEnum.INVALID_SEND_TO:
                        text = "Failed to send SMS because  user doesn't have SMS enabled";
                        break;
                    default:
                        text = "Failed to send SMS for unknown reason";
                        break;
                }
                eventEmitter.trigger(_this.events.ERROR, {
                    statusCode : statusCode,
                    text : text,
                    artifactType : messageManager.prototype.ArtifactTypeEnum.SMS
                });
            }
            , onSuccessSendEMail = function(sentId) {
                var onSuccessGet = function(newMessage) {
                    var found = _this.getById(newMessage.id);
                    if (!found) {
                        _this.add([newMessage]);
                        eventEmitter.trigger(_this.events.RECORD_ADDED_LOCALLY);
                    } else {
                        _this.replace(found.id, newMessage);
                        eventEmitter.trigger(_this.events.RECORD_ADDED_LOCALLY);
                    }
                }, onFailureGet = function() {
                    var text = 'failure to retrieve EMail message!';
                    eventEmitter.trigger(_this.events.ERROR, {
                        text : text,
                        artifactType : messageManager.prototype.ArtifactTypeEnum.EMail
                    });
                };
                _this.getMessage(sentId, onSuccessGet, onFailureGet);

                window.console.log('EMail message sent successfully!');
            }
            , onFailureSendEMail = function() {
                var text = 'failure to send EMail message!';
                eventEmitter.trigger(_this.events.ERROR, {
                    text : text,
                    artifactType : messageManager.prototype.ArtifactTypeEnum.EMail
                });
            }
            , requests = []
            , deferred = $.Deferred()
            ;

        newMessage.subject = data.subject;
        newMessage.text = data.text;

        if (data.recipients) {
            collectionUtils.forEach(data.recipients, function(_curRecipie) {
                var contactReference = new Arcus.model.ContactReference();

                contactReference.name = _curRecipie.name;
                contactReference.role = contactReference.RoleEnum.SENT_TO;

                if (_curRecipie.contactId === 0) {
                    //sending message to group
                    contactReference.number = _curRecipie.name;
                    contactReference.id = _curRecipie.groupId;
                    contactReference.type = contactReference.TypeEnum.GROUP;
                } else {
                    contactReference.number = _curRecipie.methodValue;
                    contactReference.id = _curRecipie.contactId;
                    contactReference.type = contactReference.TypeEnum.PERSON;
                }

                newMessage.sentTo.push(contactReference);
            });
        }

        if (data.artifactTypeSnapshot === "Draft"){
            newMessage.artifactType =  data.artifactTypeSnapshot;
            newMessage.id =  data.id;
        }else{
            newMessage.artifactType =  data.artifactType;
        }
        newMessage.IsRead = true;

        switch (data.artifactType) {
            case messageManager.prototype.ArtifactTypeEnum.SMS:
                collectionUtils.forEach(newMessage.sentTo, function(_curSentTo) {
                    requests.push(
                        function() {
                            var messageInstance = newMessage
                                , successCallback = function() {
                                    onSuccessSendSMS(messageInstance.id);
                                };
                            return newMessage.sendSMS(_curSentTo.number, successCallback, onFailureSendSMS);
                        }
                    );
                });
                break;
            case messageManager.prototype.ArtifactTypeEnum.EMail:
                collectionUtils.forEach(newMessage.sentTo, function(_curSentTo) {
                    requests.push(
                        function() {
                            var messageInstance = newMessage
                                , successCallback = function() {
                                    onSuccessSendEMail(messageInstance.id);
                                };
                            return newMessage.sendEmail(_curSentTo.number, successCallback, onFailureSendEMail);
                        }
                    );
                });
                break;
        }

        return requests.length ? $.pipeline(requests) : deferred.resolve();
    };

    messageManager.prototype.saveMessageRequest = function(data, onSuccess, onFailure) {
        var editedMessage = data.id ? _this.getById(data.id) : null
            , onSuccessSave = function() {
                var _newArcusMessage = this
                    , found = _this.getById(_newArcusMessage.id)
                    , _newMessage = new MessageModel(_newArcusMessage);
                _newMessage.resolveContacts({
                    contactManager : contactManager,
                    isSentByMyself : _this.isSentByMyself,
                    isSentToMe : _this.isSentToMe,
                    myNumber : localStorage.getItem("VoipTn"),
                    myName : localStorage.getItem("USERNAME")
                });
                if (!found) {
                    _this.add([_newMessage]);
                    if (onSuccess && typeof onSuccess === 'function') {
                        onSuccess();
                    }
                } else {
                    _this.replace(found.id, _newMessage);
                    if (onSuccess && typeof onSuccess === 'function') {
                        onSuccess();
                    }
                }
            }
            , onFailureSave = function() {
                logger.log('failure to save message draft!');
                if (onFailure && typeof onFailure === 'function') {
                    onFailure();
                }
            }
            , requests = []
            , deferred = $.Deferred()
            ;
        if (!editedMessage) {
            editedMessage =  new Arcus.model.Message();
        } else if (editedMessage.artifactType !== Arcus.model.Message.ArtifactTypeEnum.DRAFT) {
            editedMessage = $.extend({}, editedMessage);
        }

        if (data.id) {
            // there is no way now => how to convert existing message into draft so
            // open existing message
            // click save
            // in this case save new draft
            if (editedMessage.artifactType === Arcus.model.Message.ArtifactTypeEnum.DRAFT) {
                editedMessage.id = data.id;
            } else {
                editedMessage.id = null;
            }
        }
        editedMessage.subject = data.subject;
        editedMessage.text = data.text;
        editedMessage.artifactType = Arcus.model.Message.ArtifactTypeEnum.DRAFT;
        editedMessage.draftType = data.artifactType;
        editedMessage.IsRead = true;

        if (data.recipients) {
            editedMessage.sentTo = [];
            collectionUtils.forEach(data.recipients, function(_curRecipie) {
                var contactReference = new Arcus.model.ContactReference();

                contactReference.name = _curRecipie.name;
                contactReference.role = Arcus.model.ContactReference.RoleEnum.SENT_TO;

                if (_curRecipie.contactId === 0) {
                    //sending message to group
                    contactReference.number = _curRecipie.name;
                    contactReference.id = _curRecipie.groupId;
                    contactReference.type = Arcus.model.ContactReference.TypeEnum.GROUP;
                } else {
                    contactReference.number = _curRecipie.methodValue;
                    contactReference.id = _curRecipie.contactId;
                    contactReference.type = Arcus.model.ContactReference.TypeEnum.PERSON;
                }

                editedMessage.sentTo.push(contactReference);
            });
        }

        requests.push(
            function() {
                return editedMessage.save(onSuccessSave.bind(editedMessage), onFailureSave);
            }
        );

        return requests.length ? $.pipeline(requests) : deferred.resolve();
    };

    _this = new messageManager();
    _this.init();

    return _this;
});

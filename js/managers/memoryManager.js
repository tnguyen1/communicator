define('memoryManager', ['logger', 'baseManager', 'Arcus', 'collectionUtils'], function(logger, baseManager, Arcus, collectionUtils) {

    var _this;

    var memoryManager = function () {
        baseManager.apply(this, arguments);
        this.init();
    };

    memoryManager.prototype = new baseManager();

    memoryManager.prototype.init = function () {

        var _this = this,
            config = {
                proxy: {

                    url: 'data-mocks/memories-mock.json',
                    reader: {
                        rootProperty: '.Media'
                    }
                },
                forceUseMockData: false,
                loadFunc: _this.getMemories,
                mockDataParserFunc: _this.mockDataParser
            };

        baseManager.prototype.init.call(this, config);
    };

    /**
     * Called when needed parse mock data
     * @param {Array} data - Array of objects to be parsed
     * @returns {Array} Array of parsed objects
     */
    memoryManager.prototype.mockDataParser = function (data) {

        var result = [];

        collectionUtils.forEach(data, function (item) {
            var chat = new Arcus.model.Memory(item);
            result.push(chat);
        });

        return result;
    };

    /**
     * Get all memories from service
     */
    memoryManager.prototype.getMemories = function () {

        var deferred = $.Deferred();

        Arcus.Memories.retrieveAllMemories(
            function onSuccess(memories) {
                deferred.resolve(memories.memoryCollection);
            },
            function onFailure() {
                logger.log('Error loading memories');
                deferred.reject();
            });

        return deferred;
    };

    /**
     * Delete memories
     * @param {Integer[]} ids - Array of memories ids to be deleted
     */
    memoryManager.prototype.deleteMemories = function(ids, onDeleteProgress) {

        var deferred = $.Deferred(), i, memoriesToDelete = [];

        ids = ids || [];

        for (i = 0; i < ids.length; i++) {
            memoriesToDelete[i] = _this.getById(ids[i]);
        }

        $.each(memoriesToDelete, function(index, memory) {
            memory.destroy(function() {

                if (onDeleteProgress) {
                    onDeleteProgress(memory.id);
                }
                _this.remove([memory]);

            }, function() {

            });
        });

        return deferred;
    };

    /**
     * Delete memory
     * @param {Integer} id - Memory id to be deleted
     */
    memoryManager.prototype.deleteMemory = function(id) {
        return _this.deleteMemories([id]);
    };

    /**
     * Filter memories by memory type
     * @param {string} memoryType - The type of memory
     * @returns {object[]} The array of filtered memories
     */
    memoryManager.prototype.filterMemories = function(memoryType) {

        var filteredMemories = $.grep(_this.data, function(memory) {
            return memory.artifactType === memoryType;
        });
        return filteredMemories;
    };

    /**
     * Upload memory and save it
     * @param {object} memory - Memory to upload
     * @returns {Object} Newly created memory object
     */
    memoryManager.prototype.uploadMemory = function(memoryDescription, mediaInfo) {

        var deferred = $.Deferred(),
            mediaType,
            memoryToSave;

        if(mediaInfo.type.indexOf('image') > -1) {
            mediaType = Arcus.Media.MediaTypeEnum.PIC;
        }
        else if(mediaInfo.type.indexOf('audio') > -1) {
            mediaType = Arcus.Media.MediaTypeEnum.AUDIO;
        }
        else if(mediaInfo.type.indexOf('video') > -1) {
            mediaType = Arcus.Media.MediaTypeEnum.VIDEO;
        }

        //uploading memory
        Arcus.Media.uploadMedia(mediaInfo.mediaData, mediaType, mediaInfo.type, function(mediaRef) {

            //saving memory
            memoryToSave = new Arcus.model.Memory();
            memoryToSave.id = 0;
            memoryToSave.description = memoryDescription;
            memoryToSave.artifactType = mediaType;
            memoryToSave.size = mediaInfo.size / 1024;
            memoryToSave.sizeUnit = memoryToSave.SizeUnitEnum.KB;
            memoryToSave.version = 1;
            memoryToSave.mediaRef = mediaRef;
            memoryToSave.fileName = mediaInfo.name;

            memoryToSave.save(function(memory) {
                _this.add(memory);
                deferred.resolve(memory);
            },
            function() {
                deferred.reject();
            });

        }, function() {
            deferred.reject();
        });

        return deferred;
    };

    _this = new memoryManager();
    return _this;
});
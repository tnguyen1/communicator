define('groupManager', ['logger', 'baseManager', 'Arcus', 'collectionUtils', 'ContactGroupModel'], function (logger, baseManager, Arcus, collectionUtils, ContactGroupModel) {

    var _this;

    var groupManager = function () {
        baseManager.apply(this, arguments);
        this.init();
    };

    groupManager.prototype = new baseManager();
    groupManager.prototype.init = function () {


        var _this = this,
            config = {
            proxy: {

                url: "data-mocks/groups-mock.json",
                reader: {
                    rootProperty: ".userGroup"
                }
            },

            loadFunc: _this.getContactGroups,
            loadParser: _this.wrapWithCustomGroup,
            mockDataParserFunc: _this.mockDataParser
        };

        baseManager.prototype.init.call(this, config);
    };

    groupManager.prototype.wrapWithCustomGroup = function(data) {

        var result = [];

        collectionUtils.forEach(data, function(_curArcusGroup) {
            result.push(new ContactGroupModel(_curArcusGroup));
        });

        return result;
    };

    groupManager.prototype.mockDataParser = function (data) {

        var result = [],
            group;

        collectionUtils.forEach(data, function (item) {
            group = new Arcus.model.ContactGroup();
            group.parse(item);
            result.push(group);
        });

        return result;
    };

    groupManager.prototype.getContactGroups = function () {
        var deferred = $.Deferred();

        Arcus.Contacts.loadContactGroups(
            function onSuccess(contactGroups) {
                deferred.resolve(contactGroups);
            },
            function onFailure() {
                logger.log('Error loading contact groups!');
                deferred.reject();
            });

        return deferred;
    };

    groupManager.prototype.getContactGroupsSorted = function (sortParameter) {
        var sorter = sortParameter ? collectionUtils.getSorter(sortParameter) : null,
            groups = _this.getData();

        if (sorter) {
            groups = groups.sort(sorter);
        }

        return groups; 
    };

    /**
     * filter contact methods from current contact if these methods are included in the group
     * @param _curContact
     * @param _curGroup
     * @returns {{phoneNumbers: Array, emailAddress: Array}}
     */
    groupManager.prototype.getContactMethodsByGroup = function (_curContact, _curGroup) {
        var filteredPhoneNumbers = [], filteredEmailAddress = [], groupContactMethods = [];
        groupContactMethods = _this.getGroupContactMethods(_curGroup);

        collectionUtils.forEach(_curContact.phoneNumbers, function (_curPhone) {
            collectionUtils.forEach(groupContactMethods, function (_curContactMethod) {
                if (_curPhone.id === _curContactMethod.id) {
                    filteredPhoneNumbers.push(_curPhone);
                }
            });
        });

        collectionUtils.forEach(_curContact.emailAddress, function (_curEmail) {
            collectionUtils.forEach(groupContactMethods, function (_curContactMethod) {
                if (_curEmail.id === _curContactMethod.id) {
                    filteredEmailAddress.push(_curEmail);
                }
            });
        });

        return {
            phoneNumbers: filteredPhoneNumbers,
            emailAddress: filteredEmailAddress
        };
    };

    groupManager.prototype.getGroupContactMethods = function (_curGroup) {
        var contactMethods = []
            ;

        collectionUtils.forEach(_curGroup.groupMembers, function (_curGroupMember) {
            collectionUtils.forEach(_curGroupMember.contactMethods, function (_curGroupMemberMethod) {
                _curGroupMemberMethod.contactId = _curGroupMember; // just to store parent id
                contactMethods.push(_curGroupMemberMethod);
            });
        });

        return contactMethods;
    };

    groupManager.prototype.getUniqueContactGroupMembers = function (_curGroup, __contactManager) { // TODO avoid include __contactManager
        // wrap group member as contact and store it as unique group member
        var uniqueGroupContactsMembers = []
            , found
            , filteredContactMethods
            , contactToPush
            , newContact
            ;
        collectionUtils.forEach(_curGroup.groupMembers, function (_curGroupMember) {
            found = false;
            collectionUtils.forEach(uniqueGroupContactsMembers, function (_curUniqueGroupMember) {
                if (_curGroupMember.id === _curUniqueGroupMember.id) {
                    found = true;
                }
            });
            if (!found) {
                newContact = new Arcus.model.Contact();
                contactToPush = __contactManager.getById(_curGroupMember.id);
                if (contactToPush) {
                    filteredContactMethods = _this.getContactMethodsByGroup(contactToPush, _curGroup);
                    newContact.id = contactToPush.id;
                    newContact.firstName = contactToPush.firstName;
                    newContact.lastName = contactToPush.lastName;
                    newContact.phoneNumbers = filteredContactMethods.phoneNumbers;
                    newContact.emailAddress = filteredContactMethods.emailAddress;
                    uniqueGroupContactsMembers.push(newContact);
                }
            }
        });

        return uniqueGroupContactsMembers;
    };

    groupManager.prototype.isGroupChanged = function (_curGroup, _newGroup) {
        if (_curGroup.name !== _newGroup.name) {
            return true;
        }
        return false;
    };

    groupManager.prototype.contactToMember = function (contactId, methodId, __contactManager) {
        var contact = __contactManager.getById(contactId)
            , member = new Arcus.model.ContactGroupMember()
            , method = __contactManager.getContactMethod(contact, methodId)
            ;
        if (contact && member && method) {
            member.id = contactId;
            member.firstName = contact.firstName;
            member.lastName = contact.lastName;
            member.contactMethods.push(method);
        }
        return member;
    };

    groupManager.prototype.getGroupMemberByMethodId = function (_curGroup, methodId) {
        var foundMember = null;
        $.grep(_curGroup.groupMembers, function (grMem) {
            $.each(grMem.contactMethods, function (index) {
                if (grMem.contactMethods[index].id === methodId) {
                    foundMember = grMem;
                    return false;
                }
                return true;
            });
        });
        return foundMember;
    };

    groupManager.prototype.removeContactMethods = function (_contactMethodsToRem, _curGroup) {
        var requests = []
            , grMemberMethod
            ;

        $.each(_contactMethodsToRem, function (idx, contactMethod) {
            var onSuccessDelete = function () {
                    window.console.log('contact method removed succesfully!');
                },
                onFailureDelete = function () {
                    window.console.log('failure to remove contact method!');
                };
            grMemberMethod = _this.getGroupMemberByMethodId(_curGroup, contactMethod.contactMethodId);
            if (grMemberMethod) {
                requests.push((function (grMemberMethod) {
                    return function () {
                        return grMemberMethod.deleteEntry(onSuccessDelete, onFailureDelete);
                    };
                }(grMemberMethod)));
            }
        });

        return function () {
            return $.pipeline(requests);
        };
    };

    groupManager.prototype.addContactMethods = function (_contactMethodsToAdd, _curGroup) {
        var requests = [];
        $.each(_contactMethodsToAdd, function (idx, contactMethod) {
            var onSuccessAdd = function () {
                    window.console.log('contact method added succesfully!');
                },
                onFailureAdd = function () {
                    window.console.log('failure to add contact method!');
                };

            requests.push(function () {
                return _curGroup.addContactGroupMember(contactMethod.contactMethodId, onSuccessAdd, onFailureAdd);
            });
        });
        return function () {
            return $.pipeline(requests);
        };
    };

    groupManager.prototype.serializeContactGroup = function () {
        var newContactGroup = new Arcus.model.ContactGroup()
            , $form = $('#contact-group-form')
            , $inputOrSelect = $form.find('input,select')
            , data = $inputOrSelect.serializeObject()
            ;
        if (data) {
            $.extend(newContactGroup, data);
        }
        return newContactGroup;
    };

    groupManager.prototype.saveContactGroup = function (options) {
        var _options  = options.groupManager.getContactGroupDifference(options)
            , serializedContactGroup = _this.serializeContactGroup(options.contactGroup)
            , updateGroup = function (_groupToUpdate) {
                var deferred = $.Deferred(),
                    origGroupName = _options.contactGroup.name;

                if (_this.isGroupChanged(_options.contactGroup, _groupToUpdate)) {
                    _options.contactGroup.name = _groupToUpdate.name;

                    _options.contactGroup.updateEntry(
                        function onSuccess(){
                            window.console.log('groupName updated succesfully!');
                            deferred.resolve();
                        }, function onFailure() {
                            window.console.log('failed to update groupName!');
                            //restore orig group
                            _options.contactGroup.name = origGroupName;
                            deferred.reject();
                        });
                } else {
                    deferred.resolve();
                }
                return deferred;
            }
            , request
            ;

        if(!_options.contactGroup) { // add new group

            collectionUtils.forEach(_options.contactMethodsToAdd, function(method) {
                serializedContactGroup.groupMembers.push( _this.contactToMember( method.contactId , method.contactMethodId, _options.contactManager ) );
            });

            var onSuccessAdd = function(savedContactGroup) {
                    window.console.log('contact-group added succesfully!');
                    _this.add([new ContactGroupModel(savedContactGroup)]);
                },
                onFailureAdd = function() {
                    window.console.log('failure to add contact-group!');
                };

            return Arcus.Contacts.addContactGroup(serializedContactGroup, onSuccessAdd, onFailureAdd);
        } else { // update current group

            request = $.pipeline(
                function () {
                    return updateGroup(serializedContactGroup);
                },
                _this.removeContactMethods(_options.contactMethodsToRem, _options.contactGroup),
                _this.addContactMethods(_options.contactMethodsToAdd, _options.contactGroup),
                // after all requests we need replace old group with the updated one
                function () {
                    return _options.contactGroup.refresh(function(updatedGroup){
                        _this.replace(_options.contactGroup.id, new ContactGroupModel(updatedGroup));
                    });
                }
            );

            return request;
        }
    };

    groupManager.prototype.removeContactGroupRequest = function(groupId) {
        var onSuccessDelete = function() {
                window.console.log('contact group removed succesfully!');
                //_this.remove([_this.getById(groupId)]); //doesn't work
                collectionUtils.findAndRemove(_this.data, collectionUtils.idGetter(groupId));
            },
            onFailureDelete = function() {
                window.console.log('failure to remove contact group!');
            };
        return Arcus.Contacts.deleteContactGroup(groupId, onSuccessDelete, onFailureDelete);
    };

    groupManager.prototype.getContactGroupDifference = function (_options) {
        var _contactsMethodsInUI = _options.contactManager.getContactMethodsUISnapshot()
            , groupMethods = _options.contactGroup ? _options.groupManager.getGroupContactMethods(_options.contactGroup) : []
            , foundGrMemberMethod
            , contactMethodsToAdd = []
            , contactMethodsToRem = []
            ;

        collectionUtils.forEach(_contactsMethodsInUI, function (_curUIContactMethod) {
            // iterate through checked contact-methods
            if (_curUIContactMethod.marked === true) {
                foundGrMemberMethod = collectionUtils.findOne(groupMethods, collectionUtils.idGetter(_curUIContactMethod.id));
                // if the group member was not found it means that the contact method was not found too
                if (!foundGrMemberMethod) {
                    contactMethodsToAdd.push({
                        contactId: _curUIContactMethod.contactId,
                        contactMethodId: _curUIContactMethod.id
                    });
                }
            } else if (_options.contactGroup) {
                // iterate through unchecked contact-methods
                foundGrMemberMethod = collectionUtils.findOne(groupMethods, collectionUtils.idGetter(_curUIContactMethod.id));
                // if the group member was found it means that the contact method was found too
                if (foundGrMemberMethod) {
                    contactMethodsToRem.push({
                        contactId: _curUIContactMethod.contactId,
                        contactMethodId: _curUIContactMethod.id
                    });
                }
            }
        });

        _options.contactMethodsToAdd = contactMethodsToAdd;
        _options.contactMethodsToRem = contactMethodsToRem;

        return _options;
    };

    _this = new groupManager();
    return _this;
});
define('settingsManager', ['logger', 'baseManager', 'Arcus'], function(logger, baseManager, Arcus) {

    var _this;

    var settingsManager = function() {
        baseManager.apply(this, arguments);
    };

    settingsManager.prototype = new baseManager();

    settingsManager.prototype.init = function() {
        var _this = this,
            config = {
                storeId: "settings",
                loadFunc: _this.loadSettings,
                loadParser: _this.filterLoadSettings
            };

        _this.basicInitial = false;
        _this.initial911 = false;

        if (localStorage.BAN !== undefined) {
            _this.PrivateUserIDName = localStorage.BAN;
        }
        if (localStorage.VoipTn !== undefined) {
            _this.PublicUserIDName = localStorage.VoipTn;
        }
        if (localStorage.SMSEnabled !== undefined) {
            if (localStorage.SMSEnabled === 'true' || localStorage.SMSEnabled === true) {
                _this.SMSEnabled = true;
            } else {
                _this.SMSEnabled = false;
            }
        } else {
            _this.SMSEnabled = false;
        }
        if (localStorage.VoicemailEnabled !== undefined) {
            if (localStorage.VoicemailEnabled === 'true' || localStorage.VoicemailEnabled === true) {
                _this.VoicemailEnabled = true;
            } else {
                _this.VoicemailEnabled = false;
            }
        } else {
            _this.VoicemailEnabled = false;
        }

        baseManager.prototype.init.call(this, config);
    };

    settingsManager.findNodeWithText = function(xmlData, nodeName, nodeText, returnNode) {
        var $xmlData = $(xmlData)
            , $found = $xmlData.find('m\\:' + nodeName + ', ' + nodeName)
            , $exactFound = $found.filter(function() {
                return $(this).text().toUpperCase() === nodeText.toUpperCase();
            })
            , isExactMatch = false;
        if ($xmlData.length && $found.length && $exactFound.length) {
            isExactMatch = true;
            if (returnNode) {
                isExactMatch = $exactFound;
            }
        }

        return isExactMatch;
    };

    settingsManager.prototype.handleIncomingCalls = function(incomingCalls) {
        var $def = $.Deferred();
        var callbacksToBe = 0,
            callbacksFail = 0,
            callbacksDone = 0;
        var errors = [];
        var checkCallbacks = function() {
            if (callbacksToBe === callbacksDone) {
                $def.resolve();
            } else if (callbacksToBe === callbacksFail + callbacksDone) {
                $def.reject(errors);
            }
        };
        var data = {
            PrivateUserIDName: _this.PrivateUserIDName,
            PublicUserIDName: _this.PublicUserIDName,
            target: incomingCalls.ForwardAllCallsToNumber,
            URI: 'TELURI',
            onSuccess: function() {
                callbacksDone++;
                checkCallbacks();
            },
            onError: function(error) {
                callbacksFail++;
                errors.push(error);
                checkCallbacks();
            }
        };

        switch (incomingCalls.Option) {
            case 'AcceptAllCalls':
                // disable all
                callbacksToBe = 2;
                Arcus.ServiceConfig.genericDeleteThenAddFlow('CommunicationDiversion', 'cfu', $.extend({}, data, { ruleEnabled: false, target: '' }));
                if (_this.VoicemailEnabled) {
                    callbacksToBe = 3;
                    Arcus.ServiceConfig.genericDeleteThenAddFlow('CommunicationDiversion', 'cfuvm', $.extend({}, data, { ruleEnabled: false }));
                }
                Arcus.ServiceConfig.genericAddOrUpdateFlow('IncomingCommunicationBarring', 'DNDCB', $.extend({}, data, { ruleEnabled: false }));
                break;

            case 'ForwardAllCallsToNumber':
                callbacksToBe = 2;
                Arcus.ServiceConfig.genericDeleteThenAddFlow('CommunicationDiversion', 'cfu', $.extend({}, data, { ruleEnabled: true }));
                if (_this.VoicemailEnabled) {
                    callbacksToBe = 3;
                    Arcus.ServiceConfig.genericDeleteThenAddFlow('CommunicationDiversion', 'cfuvm', $.extend({}, data, { ruleEnabled: false }));
                }
                Arcus.ServiceConfig.genericAddOrUpdateFlow('IncomingCommunicationBarring', 'DNDCB', $.extend({}, data, { ruleEnabled: false }));
                break;

            case 'SendAllCallsToVoicemail':
                callbacksToBe = 3;
                Arcus.ServiceConfig.genericDeleteThenAddFlow('CommunicationDiversion', 'cfu', $.extend({}, data, { ruleEnabled: false, target: '' }));
                Arcus.ServiceConfig.genericDeleteThenAddFlow('CommunicationDiversion', 'cfuvm', $.extend({}, data, { ruleEnabled: true }));
                Arcus.ServiceConfig.genericAddOrUpdateFlow('IncomingCommunicationBarring', 'DNDCB', $.extend({}, data, { ruleEnabled: false }));
                break;

            case 'BlockAllCalls':
                callbacksToBe = 2;
                Arcus.ServiceConfig.genericDeleteThenAddFlow('CommunicationDiversion', 'cfu', $.extend({}, data, { ruleEnabled: false, target: '' }));
                if (_this.VoicemailEnabled) {
                    callbacksToBe = 3;
                    Arcus.ServiceConfig.genericDeleteThenAddFlow('CommunicationDiversion', 'cfuvm', $.extend({}, data, { ruleEnabled: false }));
                }
                Arcus.ServiceConfig.genericAddOrUpdateFlow('IncomingCommunicationBarring', 'DNDCB', $.extend({}, data, { ruleEnabled: true }));
                break;
        }

        return $def;
    };

    settingsManager.prototype.filterLoadSettings = function(response) {

        var services = {}, $matchedNode, $Rule, ruleDeactivated, existsOnServer, $targetInfo, targetInfo, SMTPAddresses;

        // TODO make generic for services that we need
        // v1.3 => 1.7.1/1.7.2
        $matchedNode = $(response).find('m\\:AnyEmail, AnyEmail');
        if ($matchedNode.length) {
            existsOnServer = true;
            $targetInfo = $matchedNode.find('m\\:SMTPAddresses, SMTPAddresses');
            SMTPAddresses = $.map($targetInfo.find('m\\:SMTPAddress, SMTPAddress'), function(element) {
                return $(element).text();
            });
        } else {
            existsOnServer = false;
            SMTPAddresses = [];
        }

        services.AnyEmail = {
            existsOnServer: existsOnServer,
            serviceName: 'AnyEmail',
            SMTPAddresses: SMTPAddresses,
            SMTPAddressesCSV: SMTPAddresses.join(',')
        };

        // TODO make generic for services that we need
        // v1.3 => 1.7.3/1.7.4 or 1.7.5/1.7.6
        $matchedNode = $(response).find('m\\:Notification, Notification');
        if ($matchedNode.length) {
            existsOnServer = true;
        } else {
            existsOnServer = false;
        }

        services.Notification = {
            existsOnServer: existsOnServer,
            serviceName: 'Notification',
            NotificationMappings_IncomingVoiceMail: settingsManager.findNodeWithText($matchedNode, 'NotificationEvent', 'IncomingVoiceMail'),
            NotificationMappings_VoiceMailboxSpace: settingsManager.findNodeWithText($matchedNode, 'NotificationEvent', 'VoiceMailboxSpace')
        };

        // TODO make generic for services that we need
        // 1.15
        $matchedNode = settingsManager.findNodeWithText(response, 'Id', 'cfuvm', true);
        if ($matchedNode.length) {
            existsOnServer = true;
            $Rule = $matchedNode.parent();
            ruleDeactivated = settingsManager.findNodeWithText($Rule, 'RuleDeactivated', 'true');
        } else {
            existsOnServer = false;
            ruleDeactivated = false;
        }

        services.cfuvm = {
            existsOnServer: existsOnServer,
            serviceName: 'CommunicationDiversion',
            ruleDeactivated: ruleDeactivated,
            checked: existsOnServer && !ruleDeactivated ? true : false
        };

        // TODO make generic for services that we need
        // v1.3 => 1.11
        var URI = 'TELURI'; // 'SIPURI'
        $matchedNode = settingsManager.findNodeWithText(response, 'Id', 'cfu', true);
        if ($matchedNode.length) {
            existsOnServer = true;
            $Rule = $matchedNode.parent();
            ruleDeactivated = settingsManager.findNodeWithText($Rule, 'RuleDeactivated', 'true');

            $targetInfo = $Rule.find('m\\:target, target');
            targetInfo = $targetInfo.text();
            if (targetInfo.indexOf('sip:') >= 0) { // SIP URI
                targetInfo = targetInfo.replace('sip:+1', '');
                targetInfo = targetInfo.substring(0, 10);
                targetInfo = '(' + targetInfo.substring(0, 3) + ')' + ' ' + targetInfo.substring(3, 6) + '-' + targetInfo.substring(6, 10);
                URI = 'SIPURI';
            } else { // TEL URI
                targetInfo = targetInfo.replace('tel:+1', '');
                targetInfo = targetInfo.substring(0, 10);
                targetInfo = '(' + targetInfo.substring(0, 3) + ')' + ' ' + targetInfo.substring(3, 6) + '-' + targetInfo.substring(6, 10);
                URI = 'TELURI';
            }
            // check for the "default number" - clear it if so
            if (targetInfo === '9999999999') {
                targetInfo = '';
            }
            // show empty number if rule deactivated
            if (ruleDeactivated) {
                targetInfo = '';
            }
        } else {
            existsOnServer = false;
            ruleDeactivated = false;
            targetInfo = '';
        }

        services.cfu = {
            existsOnServer: existsOnServer,
            serviceName: 'CommunicationDiversion',
            ruleDeactivated: ruleDeactivated,
            targetInfo: targetInfo,
            URI: URI,
            checked: existsOnServer && !ruleDeactivated ? true : false
        };

        // TODO make generic for services that we need
        $matchedNode = settingsManager.findNodeWithText(response, 'Id', 'ACR', true);
        if ($matchedNode.length) {
            existsOnServer = true;
            $Rule = $matchedNode.parent();
            ruleDeactivated = settingsManager.findNodeWithText($Rule, 'RuleDeactivated', 'true');
        } else {
            existsOnServer = false;
            ruleDeactivated = false;
        }

        services.ACR = {
            existsOnServer: existsOnServer,
            serviceName: 'IncomingCommunicationBarring',
            ruleDeactivated: ruleDeactivated,
            checked: existsOnServer && !ruleDeactivated ? true : false
        };

        // TODO make generic for services that we need
        $matchedNode = settingsManager.findNodeWithText(response, 'Id', 'DNDCB', true);
        if ($matchedNode.length) {
            existsOnServer = true;
            $Rule = $matchedNode.parent();
            ruleDeactivated = settingsManager.findNodeWithText($Rule, 'RuleDeactivated', 'true');
        } else {
            existsOnServer = false;
            ruleDeactivated = false;
        }

        services.DNDCB = {
            existsOnServer: existsOnServer,
            serviceName: 'IncomingCommunicationBarring',
            ruleDeactivated: ruleDeactivated,
            checked: existsOnServer && !ruleDeactivated ? true : false
        };

        // TODO make generic for services that we need
        // 1.17
        $matchedNode = $(response).find('m\\:TelcoCommon, TelcoCommon');
        if ($matchedNode.length) {
            existsOnServer = true;
            $Rule = $matchedNode;
            ruleDeactivated = settingsManager.findNodeWithText($Rule, 'Provisioned', 'Suspended');
        } else {
            existsOnServer = false;
            ruleDeactivated = false;
        }

        services.TelcoCommon = {
            existsOnServer: existsOnServer,
            serviceName: 'TelcoCommon',
            ruleDeactivated: ruleDeactivated,
            checked: existsOnServer && ruleDeactivated ? true : false
        };

        services.IncomingCalls = {
            AcceptAllCalls: !services.cfu.checked && !services.cfuvm.checked && !services.DNDCB.checked,
            ForwardAllCallsToNumber: services.cfu.checked,
            SendAllCallsToVoicemail: services.cfuvm.checked,
            BlockAllCalls: services.DNDCB.checked
        };

        this.data = services; // TODO remove this if use load()

        return services;
    };

    settingsManager.prototype.getSettings = function(data) {
        // 1.5
        if (data.forceReload) {
            _this.basicInitial = false;
        }

        Arcus.ServiceConfig.getSettings({
            PublicUserIDName: _this.PublicUserIDName,
            FullSync: _this.basicInitial ? false : true,
            onSuccess: function(response) {
                var $response = $(response);
                var $PrivateUserIDName = $response.find('m\\:PrivateUserIDName, PrivateUserIDName');
                if ($PrivateUserIDName.length) {
                    _this.PrivateUserIDName = $PrivateUserIDName.text();
                }
                // invoke sync only once
                if (!_this.basicInitial) {
                    _this.basicInitial = true;
                }
                if (data.onSuccess && typeof data.onSuccess === 'function') {
                    data.onSuccess(response);
                }
            },
            onError: data.onError
        });
    };

    settingsManager.prototype.updateBlockAllAnonymousCalls = function(data) {
        // 1.11
        Arcus.ServiceConfig.updateBlockAllAnonymousCalls({
            ruleEnabled: data.ruleEnabled,
            PrivateUserIDName: _this.PrivateUserIDName,
            PublicUserIDName: _this.PublicUserIDName,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.updateSendAllCallsToVoicemail = function(data) {
        // 1.14
        Arcus.ServiceConfig.updateSendAllCallsToVoicemail({
            ruleEnabled: data.ruleEnabled,
            PrivateUserIDName: _this.PrivateUserIDName,
            PublicUserIDName: _this.PublicUserIDName,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.updateDoNotDisturbBlockAllCalls = function(data) {
        // 1.12
        Arcus.ServiceConfig.updateDoNotDisturbBlockAllCalls({
            ruleEnabled: data.ruleEnabled,
            PrivateUserIDName: _this.PrivateUserIDName,
            PublicUserIDName: _this.PublicUserIDName,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.parseObjectFromXML = function($children) {
        var object = {};
        if ($children && $children.length) {
            $.each($children, function(ind, child) {
                var $child = $(child);
                var nodeName = $child.prop("tagName") ? $child.prop("tagName") : 'undefined';
                nodeName = nodeName.replace('m:', '');
                var nodeValue;
                if ($child.children().length > 0) {
                    nodeValue = _this.parseObjectFromXML($child.children());
                } else {
                    nodeValue = $child.html() ? $child.html() : 'undefined';
                }

                if (!object[nodeName]) {
                    object[nodeName] = nodeValue;
                } else {
                    if (typeof nodeValue === 'object') {
                        for (var key in nodeValue) {
                            if (nodeValue.hasOwnProperty(key)) {
                                object[nodeName][key] = nodeValue[key];
                            }
                        }
                    } else {
                        object[nodeName] = nodeValue;
                    }
                }

            });
        }
        return object;
    };

    settingsManager.prototype.parse911AddressesAndCustomerInfo = function(xmlData) {
        var $xmlData = $(xmlData);
        _this.addresses911 = [];
        _this.previousAddress = null;
        _this.pendingAddress = null;
        _this.currentAddress = null;

        // parse out necessary local data
        _this.CustomerId = $xmlData.find('CustomerId').text();
        _this.CustomerAccountId = $xmlData.find('CustomerAccountId').text();
        _this.TelephoneNumberId = $xmlData.find('TelephoneNumberId').text();
        var serviceOrderStatus = $xmlData.find('OrderStatus').text();

        // get DN911Address - there will always be one of these
        var dn911AddressInfo = _this.parseObjectFromXML($xmlData.find('DN911ServiceAddress')).DN911ServiceAddress;

        // parse the data for a current and/or previous 911 address
        var current911AddressId = '';
        var pending911AddressId = '';

        if (dn911AddressInfo.Status === 'PENDING CHANGE') {
            current911AddressId = dn911AddressInfo.PreviousAddress.AddressId;
            pending911AddressId = dn911AddressInfo.AddressId;
        }
        else {
            current911AddressId = dn911AddressInfo.AddressId;
        }

        // iterate each address in the contact list
        var $addresses = $xmlData.find('SubContactList > Contact');
        $.each($addresses, function() {
            // parse the address
            var parsedAddress = _this.parseObjectFromXML($(this)).Contact;

            // check for base address
            if (parsedAddress.ContactName.LastName === 'BASE') {
                parsedAddress.isBase = true;
            }

            // set the status of the address
            if (parsedAddress.ContactAddress.AddressId === current911AddressId) {
                parsedAddress.Status = 'CURRENT';
            }
            else if (parsedAddress.ContactAddress.AddressId === pending911AddressId) {
                if (serviceOrderStatus === 'FAILED') {
                    parsedAddress.Status = 'FAILED';
                    parsedAddress.canMakeCurrent = true;
                }
                else {
                    parsedAddress.Status = 'PENDING';
                }
            }
            else {
                parsedAddress.Status = parsedAddress.isBase ? 'BASE' : 'TEMP';
                parsedAddress.canMakeCurrent = true;
            }

            // push the address into the local array
            _this.addresses911.push(parsedAddress);
        });

        // set the local variables for current and pending address
        _this.currentAddress = $.grep(_this.addresses911, function(obj) {
            return obj.ContactAddress.AddressId === current911AddressId;
        })[0];

        // set the pending address if necessary
        if (pending911AddressId !== '') {
            _this.pendingAddress = $.grep(_this.addresses911, function(obj) {
                return obj.ContactAddress.AddressId === pending911AddressId;
            })[0];
        }

        // if there are any pending addresses then there should be no make current
        if (serviceOrderStatus !== 'FAILED' && _this.pendingAddress !== null) {
            $.each(_this.addresses911, function() {
                this.canMakeCurrent = false;
            });
        }

        // sort array by BASE, CURRENT, PENDING, TEMP
        var base = $.grep(_this.addresses911, function(obj) {
            return obj.isBase;
        });

        var current = $.grep(_this.addresses911, function(obj) {
            return obj.Status === 'CURRENT' && !obj.isBase;
        });

        var pending = $.grep(_this.addresses911, function(obj) {
            return obj.Status === 'PENDING' && !obj.isBase;
        });

        var failed = $.grep(_this.addresses911, function(obj) {
            return obj.Status === 'FAILED' && !obj.isBase;
        });

        var temp = $.grep(_this.addresses911, function(obj) {
            return obj.Status === 'TEMP';
        });

        // set final sorted array
        var tempAddresses = $.merge(base, current);
        tempAddresses = $.merge(tempAddresses, pending);
        tempAddresses = $.merge(tempAddresses, failed);
        tempAddresses = $.merge(tempAddresses, temp);
        _this.addresses911 = tempAddresses;
    };

    settingsManager.prototype.are911AddressesPending = function() {
        // check for 911 addresses that have the pending state
        var result = false;
        if (_this.addresses911) {
            var pending = $.grep(_this.addresses911, function(obj) {
                return obj.Status === 'PENDING';
            });
            result = pending.length > 0;
        }
        return result;
    };

    settingsManager.prototype.get911SubscriberInformation = function(data) {
        // 1.20.1
        if (data.forceReload) {
            _this.initial911 = false;
        }
        Arcus.ServiceConfig.get911SubscriberInformation({
            TelephoneNumber: _this.PublicUserIDName,
            onSuccess: function(response) {
                _this.parse911AddressesAndCustomerInfo(response);
                if (data.onSuccess && typeof data.onSuccess === 'function') {
                    data.onSuccess(response);
                }
                if (!_this.initial911) {
                    _this.initial911 = true;
                }

                // need to also force any emails to be loaded
                if (!_this.basicInitial) {
                    _this.getSettings({
                        onSuccess: function(response) {
                            _this.filterLoadSettings(response);
                        },
                        onError: function() {
                            logger.log('Error loading settings from 911 address tab.');
                        }
                    });
                }
            },
            onError: data.onError
        });
    };

    settingsManager.prototype.add911SubscriberInformation = function(data) {
        // 1.20.2
        Arcus.ServiceConfig.add911SubscriberInformation({
            TelephoneNumber: _this.PublicUserIDName,
            ContactAddress: data.ContactAddress,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.update911SubscriberInformation = function(data) {
        // 1.20.3
        Arcus.ServiceConfig.update911SubscriberInformation({
            TelephoneNumber: _this.PublicUserIDName,
            ContactId: data.ContactId,
            ContactAddress: data.ContactAddress,
            oldContactAddress: data.oldContactAddress,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.delete911SubscriberInformation = function(data) {
        // 1.20.4
        Arcus.ServiceConfig.delete911SubscriberInformation({
            TelephoneNumber: _this.PublicUserIDName,
            ContactId: data.ContactId,
            oldContactAddress: data.ContactAddress,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.set911AddressAsMain = function(data) {
        // 1.20.5
        Arcus.ServiceConfig.set911AddressAsMain({
            FirstName: 'n/a', //_this.contact.firstName ? _this.contact.firstName : '',
            LastName: 'n/a', //_this.contact.lastName ? _this.contact.lastName : '',
            Email: data.ContactEmail,
            TelephoneNumber: _this.PublicUserIDName,
            ContactId: data.ContactId,
            AddressId: data.ContactAddress.AddressId,
            AddressLine1: data.ContactAddress.AddressLine1,
            City: data.ContactAddress.City,
            ZipCode: data.ContactAddress.ZipCode,
            State: data.ContactAddress.State,
            PreviousAddress: _this.currentAddress.ContactAddress,
            CustomerId: _this.CustomerId,
            CustomerAccountId: _this.CustomerAccountId,
            TelephoneNumberId: _this.TelephoneNumberId,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.updateAnyEmailService = function(data) {
        // v1.3 => 1.7.1/1.7.2
        Arcus.ServiceConfig.updateAnyEmailServiceList({
            SMTPAddresses: data.SMTPAddresses,
            PrivateUserIDName: _this.PrivateUserIDName,
            PublicUserIDName: _this.PublicUserIDName,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.updateCallerIdDisplayName = function(data) {
        // 1.10
        Arcus.ServiceConfig.updateCallerIdDisplayName({
            CommonDisplayName: data.CommonDisplayName,
            PrivateUserIDName: _this.PrivateUserIDName,
            PublicUserIDName: _this.PublicUserIDName,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.updateForwardVoicemailToAnyEmail = function(data) {
        // v1.3 => 1.7.3/1.7.4 or 1.7.5/1.7.6
        Arcus.ServiceConfig.updateForwardVMorVMBoxFullToAnyEmail({
            ruleEnabled: data.ruleEnabled,
            ruleId: data.ruleId,
            SMTPAddresses: data.SMTPAddresses,
            PrivateUserIDName: _this.PrivateUserIDName,
            PublicUserIDName: _this.PublicUserIDName,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.updateForwardVoicemailToBoth = function(data) {
        // v1.3 => 1.7.3/1.7.4 or 1.7.5/1.7.6
        Arcus.ServiceConfig.updateForwardVMandVMBoxFullToAnyEmail({
            ruleEnabled: data.ruleEnabled,
            SMTPAddresses: data.SMTPAddresses,
            PrivateUserIDName: _this.PrivateUserIDName,
            PublicUserIDName: _this.PublicUserIDName,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    settingsManager.prototype.updateForwardAllCallsToNumber = function(data) {
        // v1.3 => 1.11
        Arcus.ServiceConfig.updateForwardAllCallsToNumber({
            target: data.target,
            URI: data.URI,
            PrivateUserIDName: _this.PrivateUserIDName,
            PublicUserIDName: _this.PublicUserIDName,
            onSuccess: data.onSuccess,
            onError: data.onError
        });
    };

    _this = new settingsManager();
    _this.init();

    return _this;
});

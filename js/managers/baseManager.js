/*global JSONSelect*/
define("baseManager", ['logger', 'config', 'collectionUtils', 'eventEmitter', 'jsonSelect'], function(logger, projectConfig, utils, eventEmitter) {

    var baseManager = function() {

        this.config = {

            /**
             * @cfg {String/Object} proxy The Proxy to use for this Store. This can be either a string, a config
             * object or a Proxy instance - see {@link #setProxy} for details.
             */
            proxy: {
                type: "ajax",
                url: "",
                reader: {
                    type: "json",
                    rootProperty: null
                },
                getReader: function() {
                    return this.reader;
                }
            },

            /**
             * @cfg {boolean} forseUseMockData
             * Indicate that we need use mock data instead of global config option
             */
            forceUseMockData: false,

            /**
             * @cfg {Object[]} sorters
             * Array of {@link Sorter Sorters} for this store.
             */
            sorters: null,

            /**
             * @cfg {Number} pageSize
             * The number of records considered to form a 'page'.
             */
            pageSize: 25
        };

        this.events = {
            INIT: 'manager:init',
            CLEAR: 'manager:store:clear',
            ADD: 'manager:store:add',
            ADD_RECORD: 'manager:store:addRecord',
            REMOVE_RECORD: 'manager:store:removeRecord',
            REMOVE_AT: 'manager:store:removeAt',
            REMOVE_ALL: 'manager:store:removeAll',
            BEFORE_LOAD: 'manager:store:beforeLoad',
            LOAD: 'manager:store:load',
            BEFORE_SYNC: 'manager:store:beforeSync',
            SYNC: 'manager:store:sync',

            ERROR: 'ERROR',
            RECORD_ADDED_LOCALLY : 'RECORD_ADDED_LOCALLY',
            RECORD_ADDED_REMOTELY : 'RECORD_ADDED_REMOTELY',
            RECORD_DELETED_LOCALLY : 'RECORD_DELETED_LOCALLY',
            RECORD_DELETED_REMOTELY : 'RECORD_DELETED_REMOTELY',
            RECORD_UPDATED_LOCALLY : 'RECORD_UPDATED_LOCALLY',
            RECORD_UPDATED_REMOTELY : 'RECORD_UPDATED_REMOTELY'
        };

        this.data = null;

        this.loadDeferred = null;
    };

    baseManager.prototype = {

        /**
         * Manager initialization
         * @param {Object} config - The object with config setting to override default config
         */
        init: function(config) {

            this.config = $.extend(true, {}, this.config, config);

            eventEmitter.trigger(this.events.INIT);
        },

        /**
         * Getting proxy object from manager config
         * @returns {Object}
         */
        getProxy: function() {
            return this.config.proxy;
        },

        /**
         * Getting local manager data
         * @returns {Object[]}
         */
        getData: function() {
            return this.data;
        },

        /**
         * Setting local data
         * @param {Object[]} data
         * @returns {Object[]}
         */
        setData: function(data) {
            this.data = data;
            return this.data;
        },

        /**
         * Loading data from service or mock storage
         * @param {boolean} refresh - Indicate do we need load again all data from service|mock or return local data
         * @param {Object} options - Additional parameters pass to service call
         * @returns {Object} Deferred object that indicate when loading finished
         */
        load: function(refresh, options) {
            var _this = this,
                deferred = null,
                url = _this.getProxy().url,
                rootProperty = _this.getProxy().getReader().rootProperty,
                useMockData = projectConfig.useMockData;

            eventEmitter.trigger(_this.events.BEFORE_LOAD);
                
            if ( !this.loadDeferred || ( $.isNotActive( this.loadDeferred ) && refresh ) ) {
                // The list has never been loaded, or data *was* loaded and the caller is forcing a reload
                this.loadDeferred = deferred = $.Deferred();
                                
                if (_this.data === null || refresh) {
                    if (useMockData || _this.config.forceUseMockData) {
                        $.ajax({
                            url: url,
                            headers: {
                                "Accept": "application/json"
                            },
                            datatype: 'text/javascript',
                            success: function(response) {

                                var ar = JSONSelect.match(rootProperty, response);

                                if (_this.config.mockDataParserFunc) {
                                    _this.setData(_this.config.mockDataParserFunc(ar[0]));
                                } else {
                                    _this.setData(ar[0]);
                                }
                                _this.onLoad();
                                deferred.resolve(_this.data);
                            },
                            async: false
                        });
                    } else {
                        _this.config.loadFunc(options).done(function(data) {
                            if (_this.config.loadParser) {
                                _this.setData(_this.config.loadParser(data));
                            } else {
                                _this.setData(data);
                            }
                            _this.onLoad();
                            deferred.resolve(_this.data);
                        }).fail(function() {
                            deferred.reject();
                        });
                    }
                } else {
                    deferred.resolve(_this.data);
                }

                _this.loading = true;

            } else if ( $.isPending( this.loadDeferred ) ) {
                // Model data is already loading - return the deferred task currently in progress
                deferred = this.loadDeferred;

            } else {
                deferred = this.loadDeferred;
            }

            return deferred;
        },

        /**
         * Called after loading data finished
         */
        onLoad: function() {
            eventEmitter.trigger(this.events.LOAD);
            this.loading = false;
        },

        /**
         * Adds Model instance to the Store. This method accepts either:
         *
         * - An array of Model instances or Model configuration objects.
         * - Any number of Model instance or Model configuration object arguments.
         *
         * The new Model instances will be added at the end of the existing collection.
         *
         * Sample usage:
         *
         *     myStore.add({some: 'data2'}, {some: 'other data2'});
         *
         * @param {Model[]/Model...} model An array of Model instances
         * or Model configuration objects, or variable number of Model instance.
         * @return {Model[]} The model instances that were added.
         */
        add: function(records) {
            if (!(records instanceof Array)) {
                records = Array.prototype.slice.call(arguments);
            }

            var insertedRecords = this.insert(this.data.length, records);
            eventEmitter.trigger(this.events.ADD);
            return insertedRecords;
        },

        /**
         * Inserts Model instances into the Store at the given index and fires the {@link #add} event.
         * See also `{@link #add}`.
         * @param {Number} index The start index at which to insert the passed Records.
         * @param {Model[]} _records An Array of Model objects to add to the cache.
         * @return {Object}
         */
        insert: function(index, _records) {

            // TODO: Here we should detect what type of model we are going to insert
            if (!(_records instanceof Array)) {
                _records = Array.prototype.slice.call(arguments, 1);
            }

            var me = this,
                data = me.data,
                records = _records.slice();

            // Now we insert all these records in one go to the collection. Saves many function
            // calls to data.insert. Does however create two loops over the records we are adding.
            if (records.length === 1) {
                data.splice(index, 0, records[0]);
                eventEmitter.trigger(me.events.ADD_RECORD, records[0]);
            } else {
                data.splice(index, 0, records);
            }

            return records;
        },

        replace: function(idToReplace, newObject) {
            var self = this;
            $.each(self.data, function(ind, curRecord) {
                if (curRecord.id === idToReplace) {
                    self.data[ind] = newObject;
                    eventEmitter.trigger(self.events.ADD_RECORD, self.data[ind]); // TODO replace
                    return false;
                }
                return true;
            });
        },

        /**
         * Removes the given record from the Store, firing the `removerecords` event passing all the instances that are removed.
         * @param {Model/Model[]} records Model instance or array of instances to remove.
         */
        remove: function(records) {

            var me = this,
                i = 0,
                ln = records.length,
                indices = [],
                removed = [],
                record, index;

            for (; i < ln; i++) {
                record = records[i];
                // sometimes it does not work!
                // http://stackoverflow.com/questions/8668174/indexof-method-in-an-object-array
                if (me.data.indexOf(record)) {

                    index = me.data.indexOf(record);
                    if (index !== -1) {
                        removed.push(record);
                        indices.push(index);
                    }

                    me.data.splice(index, 1);
                    eventEmitter.trigger(me.events.REMOVE_RECORD);
                }
            }
        },

        /**
         * Get object from local storage by id
         * @param {Number} id
         * @returns {Object}
         */
        getById: function(id) {
            if (this.data) {
                return utils.findOne(this.data, utils.idGetter(id));
            } else {
                return null;
            }
        },

        /**
         * Find object in local store and delete it
         * @param {function} testFn - The function used to find object to remove
         * @returns {Object} The deleted object
         */
        findAndRemove: function(testFn) {
            return utils.findAndRemove(this.data, testFn);
        }
    };

    return baseManager;
});
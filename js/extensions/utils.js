define('utils', ['jquery'], function ($) {

    // declare $wrtc namespace
    if (!('$wrtc' in window)) window.$wrtc = {};

    // core methods for wrtc
    window.$wrtc.getPageDimensions = function () {
        var w; var h;
        // standard compliant browers
        if (typeof window.innerWidth != 'undefined') { w = window.innerWidth; h = window.innerHeight; }
            // IE6 in standards compliant mode
        else if (typeof document.documentElement !== 'undefined' && typeof document.documentElement.clientWidth !== 'undefined' && document.documentElement.clientWidth !== 0) { w = document.documentElement.clientWidth; h = document.documentElement.clientHeight; }
            // older versions of IE   
        else { w = document.getElementsByTagName('body')[0].clientWidth; h = document.getElementsByTagName('body')[0].clientHeight; }
        return { width: w, height: h };
    };

    window.$wrtc.fillPageHeight = function (options) {
        // extend options with defaults
        var defaultOptions = {
            elements: null,
            offset: { top : 0, left : 0 },
            minOffset : false,
            padding : { top : 0 , left : 0 }
        };

        options = $.extend({}, defaultOptions, options);

        if (options.elements !== null) {
            // get page height
            var pageHeight = window.$wrtc.getPageDimensions().height
                , toReturn = [];

            // set height on all elements
            $.each(options.elements, function () {
                var $this = $(this)
                    , minTopOffset = options.minOffset ? $this.offset().top : 0
                    , offsetTop = options.offset.top > 0 ? options.offset.top : minTopOffset
                    , paddingTop = options.padding.top > 0 ? options.padding.top : 0
                    ;
                if (options.height) {
                    $this.height(options.height);
                    toReturn.push(options.height);
                } else {
                    $this.height(pageHeight - offsetTop - paddingTop);
                    toReturn.push(pageHeight - offsetTop - paddingTop);
                }
            });

            return toReturn;
        }
    };

    window.$wrtc.isDebug = function() {
        return window.location.href.indexOf("debug.html") > -1;
    };

    // Returns the total non-content height for an element (in pixels). i.e.
    // (top margin + top padding + top border + bottom margin + bottom padding + bottom border)
    $.fn.extraHeight = function () {
        var $element = $(this[0]);

        return $element.outerHeight(true) - $element.height();
    };

    $.instantDeferred = function () {
        var deferral = $.Deferred();

        setTimeout(
            function () {
                deferral.resolve( /*args*/);
            },
            0
        );

        return deferral;
    };

    $.isNotActive = function (deferred) {
        return !this.loadDeferred || !$.isPending(deferred);
    };

    $.isPending = function (deferred) {
        return deferred && (deferred.state() === 'pending');
    };

    $.isNull = function (obj) {
        return typeof obj === 'undefined' || obj === null;
    };

    $.isNullOrEmpty = function (obj) {


        var isNull = typeof obj === 'undefined' || obj === null,
            isEmpty = true;

        if (!isNull) {

            if (typeof obj === 'string') {
                isEmpty = obj === '';
            }
        }

        return isNull || isEmpty;
    };

    $.isNotNull = function (obj) {
        return typeof obj !== 'undefined' && obj !== null;
    };

    $.isNotNullOrEmpty = function (obj) {

        var isNotNull = typeof obj !== 'undefined' && obj !== null,
            isNotEmpty = false;

        if (isNotNull) {
            isNotEmpty = obj !== '';
        }

        return isNotNull && isNotEmpty;
    };

    $.zeroFill = function (number, width) {
        width -= number.toString().length;

        if (width > 0) {
            return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
        }
        return number.toString(); // always return a string
    };

    $.formatPeriod = function (startTime, endTime, pauseDuration) {
        var duration = endTime.getTime() - startTime.getTime() - pauseDuration,
            minutes = Math.floor(duration / 60000),
            seconds = String(Math.floor((duration - minutes * 60000) / 1000));

        if (+seconds.length === 1) {
            seconds = "0" + seconds;
        }

        // TODO: add localizable format string
        return minutes + ":" + seconds;
    };

    $.getTimeDuration = function(startTime, endTime) {
        return endTime.getTime() - startTime.getTime();
    };

    $.fn.outerHTML = function (s) {
        return s
            ? this.before(s).remove()
            : $("<p>").append(this.eq(0).clone()).html();
    };

    $.defEnum = function (args) {
        var i, val = {};
        for (i = 0; i < args.length; i++) {
            val[args[i]] = args[i];
        }

        return val;
    };

    $.getAnchor = function (recalc, anchorVariable, jQuerySelect) {
        // this anchor will reduce time for resizing ???
        if (recalc || !anchorVariable) {
            anchorVariable = jQuerySelect;
        }
        return anchorVariable;
    };

    $.em = function (input) {
        var emSize = 0
            , fontSizeCSS = $("body").css("font-size");
        if (fontSizeCSS) {
            emSize = parseFloat(fontSizeCSS);
        }
        return (emSize * input);
    };

    $.pipeline =
        function pipeline() {
            var deferreds = arguments,
                pipe = $.Deferred();

            function wrapped(d) {
                var isPromise = function (val) {
                    return typeof val === 'object' && val.hasOwnProperty('then');
                };

                return isPromise(d) ?
                    function () {
                        return d;
                    } : d;
            }

            if (arguments.length === 1) {
                if (arguments[0] instanceof Array) {
                    deferreds = arguments[0];
                }
            }

            pipe.resolve();
            $.each(deferreds, function (idx, dfd) {
                pipe = pipe.pipe(wrapped(dfd));
            });
            return pipe;
        };

    $.rejectPromiseFn =
        function errorHandler(promise) {
            return function (code, error) {
                promise.reject(code, error);
            };
        };

    $.adjustImage =
        function adjustImage(wrappers) {
            $(wrappers).each(function (idx, wrapper) {
                var box = $(wrapper),
                    boxWidth = box.width(),
                    boxHeight = box.height(),
                    image = box.find('img'),
                    imageWidth = image.width(),
                    imageHeight = image.height(),
                    ratio = 1;
                //        console.log(boxWidth, boxHeight, imageWidth, imageHeight);
                if (imageHeight > 0 && boxHeight > imageHeight) {
                    // extend image by height
                    ratio = boxHeight / imageHeight;
                    $(image).css({
                        height: ratio * imageHeight,
                        width: ratio * imageWidth,
                        maxWidth: ratio * imageWidth,
                        left: (boxWidth - ratio * imageWidth) / 2
                    });
                }
            });
        };

    $.debounce = function (fn, timeout, ctx, immediate, callback) {
        var timer;

        return function () {
            var args = arguments,
                res;
            ctx = ctx || this;
            immediate = !timeout || immediate;
            callback = callback || $.noop;

            if (immediate) {
                callback(fn.apply(ctx, args));
            }

            clearTimeout(timer);

            timer = setTimeout(function () {
                if (!immediate) {
                    callback(fn.apply(ctx, args));
                }
                timer = null;
            }, timeout);

        };
    };

    $.debouncePromise = function (func, wait, ctx, immediate) {
        var timeout;
        var deferred = $.Deferred();
        return function () {
            var context = ctx || this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) {
                    $.when(func.apply(context, args))
                        .then(deferred.resolve, deferred.reject, deferred.notify);
                    deferred = $.Deferred();
                }
            };
            var callNow = immediate && !timeout;
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(later, wait);
            if (callNow) {
                deferred = $.Deferred();
                $.when(func.apply(context, args))
                    .then(deferred.resolve, deferred.reject, deferred.notify);
            }
            return deferred.promise();
        };
    };


    $.fn.allData = function () {
        var intID = $.data(this.get(0));
        return ($.cache[intID]);
    };

    // form serialization: http://stackoverflow.com/questions/1184624/convert-form-data-to-js-object-with-jquery
    // $.fn.serializeObject = function(o)
    // {
    //     o = o || {};
    //     var a = this.serializeArray();
    //     $.each(a, function() {
    //         if (o[this.name] !== undefined && $.isArray(o[this.name]) ) {
    //             o[this.name] = [o[this.name]];
    //             o[this.name].push(this.value || '');
    //         } else {
    //             o[this.name] = this.value || '';
    //         }
    //     });
    //     return o;
    // };

    // added new serialize with fix for arrays in one field
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $.extractId = function (id) {
        var result = "";
        if ($.isNotNullOrEmpty(id)) {
            result = id.substring(id.indexOf('_') + 1);
        }
        return result;
    };

    $.convertImgToBase64 = function (url, callback, outputFormat) {
        var canvas = document.createElement('CANVAS'),
            ctx = canvas.getContext('2d'),
            img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function () {
            canvas.height = img.height;
            canvas.width = img.width;
            ctx.drawImage(img, 0, 0);
            var dataURL = canvas.toDataURL(outputFormat || 'image/png');
            callback.call(this, dataURL);
            // Clean up
            canvas = null;
        };
        img.src = url;
    };

    $.isUrl = function (data) {
        return data.indexOf('http') === 0 || data.indexOf('https') === 0;
    };

    $.getProperty = function (obj, property) {
        return ((typeof obj[property]) === 'undefined') ? null : obj[property];
    };

    $.DataSourceTree = function(options) {
        this._data  = options.data;
        this._delay = options.delay;
    };

    $.DataSourceTree.prototype.data = function(options, callback) {
        var $data = null;

        if(!("name" in options) && !("type" in options)){
            $data = this._data;//the root tree
            callback({ data: $data });
            return;
        }
        else if("type" in options && options.type == "folder") {
            if("additionalParameters" in options && "children" in options.additionalParameters)
                $data = options.additionalParameters.children;
            else $data = {};//no data
        }

        if($data !== null) {
            callback({ data: $data });
        }
    };

    $.waitFor = function(_deferred) {
        var wrap = function(deferred) {
            var delayPromise = $.Deferred();
            (function delay(timeout) {
                setTimeout(function(){
                    delayPromise.resolve();
                }, timeout || 5000);
            }());

            return delayPromise.pipe(deferred);
            },

            wrappedDeffered = wrap(_deferred);

        if (wrappedDeffered) {
            $.when(wrappedDeffered).then(function onSuccess(){

            }, function onFailure(code, response) {

            });
        }

        return _deferred;
    };
});
define('enums', ['jquery', 'utils'], function($) {
    // declare $wrtc namespace
    if (!('$wrtc' in window)) window.$wrtc = {};

    //enums
    window.$wrtc.enums = {};

    // artifacts enums    
    window.$wrtc.enums.ArtifactTypes = $.defEnum(["VoiceMail", "SMS", "MMS", "Message", "Pic", "Video", "Sketch", "Audio", "Media", "Activity", "Reward", "Folder", "Setting", "Other", "All", "BuddyRequest"]);
    window.$wrtc.enums.SizeUnits = $.defEnum(["MIN", "CHAR", "KB", "POINT", "SEC"]);
    window.$wrtc.enums.EndpointTypes = $.defEnum(["Web", "Mobile", "Tablet"]);
    window.$wrtc.enums.ContactRoles = $.defEnum(["NA", "Tagged", "SentTo", "ReceivedFrom", "SentBy"]);
    // Activity Recurrence options
    window.$wrtc.enums.Recurrence = $.defEnum(['Never', 'Daily', 'Weekday', 'Weekly', 'MonthlyDate', 'MonthlyDay', 'Yearly']);

    //contacts enums
    window.$wrtc.enums.ContactUserTypes = $.defEnum(["WIRELESSTN", "PSTNTN", "VOIPTN", "EMAIL", "IM", "ITVMACID", "XTDM", "UNKNOWN", "PSI", "MOBILE", "SKYPE", "ICHAT", "GTALK"]);
    window.$wrtc.enums.ContactUserDescription = $.defEnum(["WORK", "HOME", "MOBILE", "OTHER", "PAGER", "HOMEFAX", "WORKFAX"]);
    window.$wrtc.enums.ContactGroupTypes = $.defEnum(["FAMILY", "FAMILYCHAT", "SPEEDDAIL", "CUSTOM"]);
    window.$wrtc.enums.ContactGroupStatuses = $.defEnum(["active", "inactive"]);
    window.$wrtc.enums.contactImageHandler = $.defEnum(["new", "has", "none"]);
    window.$wrtc.enums.messageLocation = $.defEnum(["inbox", "sent", "trash", "draft"]);
    window.$wrtc.enums.modelTypes = $.defEnum(["NA", "Contact", "ContactPhone", "ContactEmail", "ContactAddress", "ContactNote", "InstantMessengerAddress"]);
    window.$wrtc.enums.updateStatus = $.defEnum(["readyToUpdate", "Updating", "Updated", "NotUpdated"]);

    window.$wrtc.enums.NetworkErrors = {
        400: "Bad Request",
        401: "Unauthorized",
        402: "Payment Required",
        403: "Forbidden",
        404: "Not Found",
        405: "Method Not Allowed",
        406: "Not Acceptable",
        407: "Proxy Authentication Required",
        408: "Request Timeout",
        409: "Conflict",
        410: "The requested resource is no longer available at the server",
        411: "Length Required",
        412: "Precondition Failed",
        413: "Request Entity Too Large",
        414: "Request-URI Too Long",
        415: "Unsupported Media Type",
        416: "Requested Range Not Satisfiable",
        417: "Expectation Failed",
        500: "Internal Server Error",
        501: "Not Implemented",
        502: "Bad Gateway",
        503: "Service Unavailable",
        504: "Gateway Timeout"
    };
});
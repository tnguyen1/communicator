define('collectionUtils', ['jquery'], function ($) {

    var collectionUtils = function () {
        var _this;
        return {

            init: function () {
                _this = this;
                return _this;
            },

            filter: $.grep,
            forEach: function(collection, callback) {
                if (typeof(callback) === 'string') {
                    callback = function(item) {
                        return item[callback]();
                    };
                }

                if (Array.isArray(collection)) {
                    return $.each(collection, function(idx, val){
                        return callback(val, idx);
                    });
                } else {
                    return $.each(collection, callback);
                }

            },
            map: $.map,

            eq: function(a, b) {
                if (a && b && typeof a === typeof b) {
                    if (a.constructor === Number || a.constructor === Date) {
                        return (a - b === 0);
                    } else {
                        return a === b;
                    }
                } else {
                    return !a && !b;
                }
            },

            values: function(obj) {
                return _this.map(obj, function(val){
                    return val;
                });
            },

            idGetter: function (id) {
                return function(item) {
                    return ' ' + item.id === ' ' + id;
                };
            },

            findByAttrValue: function(collection, attrName, attrValue) {
                var getter = _this.getter(attrName)
                    , groupByAttrName = _this.groupBy(collection, getter);

                return groupByAttrName[attrValue];
            },

            findOne: function (collection, testFn) {
                var found = null;
                _this.forEach(collection, function (item) {
                    if (testFn(item)) {
                        found = item;
                        return false;
                    }
                });
                return found;
            },

            findOneValue: function (collection, testFn) {
                var found = null;
                _this.forEach(collection, function (item) {
                    if (testFn(item)) {
                        found = collection[item];
                        return false;
                    }
                });
                return found;
            },

            findAndRemove: function(collection, testFn, findOne){
                var found = _this[findOne?'findOne':'filter'](collection, testFn);
                if (found) {
                    _this.forEach(found, function (item) {
                       collection.splice(collection.indexOf(item), 1);
                    });
                }
                return found;
            },

            getter: function(attrName) {
                return function(item){
                    return item[attrName];
                };
            },

            flatten: function(array, deep) {
                var result = Array.prototype.concat.apply([], array),
                    _flatten = _this.flatten;
                if (deep && result.length !== array.length) {
                    return _flatten(result, deep);
                }
                return result;
            },

            unique: function(collection, attrGetter) {
                attrGetter = attrGetter || 'id';
                var grouper = _this.getter(attrGetter);
                return _this.map(_this.groupBy(collection, grouper), function(value) {
                    return value[0];
                });
            },

            groupBy: function(collection, groupGetter) {
                var forEach = _this.forEach,
                    group,
                    resultMap = {};

                function addItem(key, item){
                    if (!resultMap[key]) {
                        resultMap[key] = [];
                    }
                    resultMap[key].push(item);
                }

                forEach(collection, function(item){
                    group = groupGetter(item);
                    if (Array.isArray(group)) {
                        forEach(group, function(grp) { addItem(grp, item); });
                    } else {
                        addItem(group, item);
                    }
                });

                return resultMap;
            },

            makeSorter: function (key, order) {
                // http://stackoverflow.com/questions/1129216/sorting-objects-in-an-array-by-a-field-value-in-javascript
                var ASC = 1,
                    DESC = -1;
                order = order || ASC;

                if (typeof key === "string") {
                    if (key[0] === '-') {
                        key = key.slice(1);
                        order = DESC;
                    }
                    key = _this.getter(key);
                }

                return function (a, b) {
                    a = key ? key(a) : a;
                    b = key ? key(b) : b;
                    if (typeof a === 'string' && typeof b === 'string') {
                        a = a.toLowerCase();
                        b = b.toLowerCase();
                    }
                    return order * (a > b ? 1 : a === b ? 0 : -1);
                };
            },

            sorters: {},  // predefined and cached sorters
            getSorter: function(key) {
                if (typeof key === 'function') {
                    return _this.makeSorter(key);
                }

                if (!_this.sorters[key]) {
                    _this.sorters[key] = _this.makeSorter(key);
                }
                return _this.sorters[key];
            }

        };
    };
    return collectionUtils().init();
});

define( 'eventEmitter', ['jquery'], function($) {

    var eventEmitter = function() {
        this.listeners = {};
    };

    eventEmitter.prototype = {

        Event: {
            ROUTER_CHANGE: 'router:change',
            ROUTER_AFTER_CHANGE: 'router:afterChange',  // fired when page controller initialized
            VIEW_RESIZE: 'view:resize'
        },

        on: function(event, listener) {
            $(this).on(event, listener);
            return this;
        },

        one: function(event, listener) {
            $(this).one(event, listener);
            return this;
        },

        off: function(event, listener) {
            $(this).off(event, listener);
            return this;
        },

        trigger: function(event, parameters) {
            $(this).trigger(event, parameters);
        }
    };

    return new eventEmitter();
});

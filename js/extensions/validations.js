define('validations', [],
    function () {
        var _this;
        return {
            regexp : {
                name: "^.{0,20}$",
                email: "^([\\w\\-\\.]+)@((\\[([0-9]{1,3}\\.){3}[0-9]{1,3}\\])|(([\\w\\-]+\\.)+)([a-zA-Z]{2,5}))$",
                phoneUS: "^(1-?)?(([2-9]d{2})|[2-9]d{2})-?[2-9]d{2}-?d{4}$",
                phoneUSALong: "^\\(?([0-9]{3})\\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$",
                phoneUSAShort: "^[0-9]{7}$",
                phoneInternational: "^\\+[0-9]{7,15}$",
                zip: "(^\\d{5}$)|(^\\d{5}-\\d{4}$)"
            },
            init: function () {
                _this = this;

                $.validator.addMethod('phoneUS', function (phone_number, element) {
                    phone_number = phone_number.replace(/\s+/g, '');
                    return this.optional(element) || phone_number.length > 9 &&
                        phone_number.match(_this.regexp.phoneUS);
                }, 'Please enter a valid USA phone number');

                return _this;
            },

            isValidPhone: function(value) {
                return value.match(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/);
            },

            isValidEmail: function(email) {
                var regEx = /^$|^((([a-zA-Z0-9]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z0-9]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z0-9]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z0-9]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z0-9]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z0-9]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z0-9]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z0-9]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z0-9]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z0-9]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/;
                return (email.match(regEx) !== null);
            },

            invalidPhoneNumberMessage: function() {
                return 'Phone number must be in the valid format (XXX) XXX-XXXX or XXXXXXXXXX or XXX-XXX-XXXX';
            },

            invalidEmailAddressMessage: function() {
                return 'Email address must be in the valid format localpart@domain (e.g. johndoe@centurylink.net)';
            }
        };
    });

